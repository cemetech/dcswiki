#!/usr/bin/env python3
import dataclasses
import json
import re
import subprocess
import tempfile
import xml.etree.ElementTree as ET
import yaml

from collections import defaultdict
from concurrent.futures import Executor, ThreadPoolExecutor
from dataclasses import dataclass, field
from pathlib import Path, PurePosixPath
from wikitextprocessor import Wtp, WikiNode, NodeKind

IGNORED_NAMESPACES = [
    # Authors probably aren't interesting in the new world
    'User',
    'User talk',
    # Talk of all kinds is pretty much useless at this point
    'Talk',
    # Templates are expanded during text parsing
    'Template',
    'Template talk',
    'MediaWiki',
]

INDEX_FILENAME = '_index'

XML_FILE = "dcscemetechnet-20221129-history.xml"
XML_NAMESPACES = {
    'mw': 'http://www.mediawiki.org/xml/export-0.10/'
}

@dataclass(frozen=True)
class PageRevision:
    author: str
    timestamp: str
    comment: str

    def as_json(self):
        return {
            'author': self.author,
            'timestamp': self.timestamp,
            'comment': self.comment
        }


page_metadata: dict[str, tuple[PageRevision]] = {}
dump_xml = ET.parse(XML_FILE)
# Fill in page_metadata to load revision history for each page
for page in dump_xml.iterfind('./mw:page', XML_NAMESPACES):
    title = page.findtext('./mw:title', namespaces=XML_NAMESPACES)
    revisions = []
    for revision in page.iterfind('./mw:revision', XML_NAMESPACES):
        author = revision.findtext('./mw:contributor/mw:username', namespaces=XML_NAMESPACES)
        timestamp = revision.findtext('./mw:timestamp', namespaces=XML_NAMESPACES)
        comment = revision.findtext('./mw:comment', namespaces=XML_NAMESPACES)
        revisions.append(PageRevision(
            author=author,
            timestamp=timestamp,
            comment=comment,
        ))
    page_metadata[title] = tuple(revisions)

# Generate a mapping of target pages to the ones that redirect to it,
# which we'll use to add aliases for the target later.
page_redirects: dict[str, set[str]] = defaultdict(set)
for page in dump_xml.iterfind('./mw:page', XML_NAMESPACES):
    redirect = page.find('./mw:redirect', XML_NAMESPACES)
    if redirect is not None:
        this_title = page.findtext('./mw:title', namespaces=XML_NAMESPACES)
        assert this_title is not None
        page_redirects[redirect.attrib['title']].add(this_title)

# Also map old pages to their new aliases, which we use when fixing links
# to pages that got redirected.
redirected_pages: dict[str, str] = {}
for destination, sources in page_redirects.items():
    for source in sources:
        redirected_pages[source.replace(' ', '_').lower()] = destination.replace(' ', '_').lower()

@dataclass(frozen=True)
class Page:
    text: str
    title: str
    raw_title: str
    categories: frozenset[str]
    revisions: tuple[PageRevision]

    def __repr__(self):
        return self.raw_title

wikitext = Wtp()

def extract_categories(node: WikiNode) -> set[tuple[str]]:
    """Return the categories for a wiki page, represented by its root node."""
    categories = set()

    if node.kind == NodeKind.LINK:
        [target, *_] = node.args[0]
        if not isinstance(target, str):
            # The arg should usually be a bare string, but malformed
            # markup might be something else.
            return categories

        if target.startswith('Category:'):
            # Includes this page in the category, and trim weird whitespace as well
            # as convert spaces back to underscores like we do for page titles.
            bits = tuple(s.strip().replace(' ', '_') for s in target.split(':')[1:])
            categories.add(bits)

    for child in node.children:
        if isinstance(child, WikiNode):
            categories.update(extract_categories(child))

    return categories


def sanitize_file_name(name: str) -> str:
    # Convert slashes in the title to lookalikes so they don't become spurious
    # directory separators. " and ? are forbidden in Windows paths.
    return name.replace('/', '\u2215').replace('"', 'quote').replace('?', 'questionmark')

def page_handler(model, title, text):
    # Skip things that aren't text
    if model != "wikitext":
        return None
    # Skip ignored namespaces too
    for ns in IGNORED_NAMESPACES:
        if title.startswith(f"{ns}:") or title == ns:
            return None

    raw_title = title.replace(' ', '_')
    revisions = page_metadata[title]
    tree = wikitext.parse(text, pre_expand=True, expand_all=True)

    # Find links that point to Category:Foo, mark as category member.
    # Links pointing to :Category:Foo link to the category index.
    categories = extract_categories(tree)

    # Apply title replacements to maintain valid output filenames.
    title = sanitize_file_name(title)

    # Page names in XML parsing converted _ to ' ', but that breaks links and
    # many of our pages actually do want underscores so keep them (and we'll
    # fix those that should be spaces manually later).
    title = title.replace(' ', '_')
 
    if title.startswith("Category:"):
        # This is the index page for a category; it definitely belongs in that
        # category, and should be changed to be the index for that directory.
        category = tuple(title.split(":")[1:])
        categories.add(category)
        title = INDEX_FILENAME
 
    # Assume any colons left in the page title also indicate a category, and
    # strip the category from the page title.
    bits = title.split(':')
    if len(bits) > 1:
        title = bits.pop()
        categories.add(tuple(bits))

    if title == 'Main_Page':
        # The magic global index page is also an index
        title = INDEX_FILENAME

    # Fixup page text to fix various weirdnesses that translate badly.
    # The wikitext tree is not really mutable, so we hack text fixups here
    # (working with the unparsed markup) instead of earlier.
    text = wikitext.node_to_wikitext(tree)
    # Strip category inclusion links since we already got the categories.
    text = re.sub(r'\[\[Category:.+?\]\]', '', text)
    # Conversion of nodes back to text seems to add a bunch of newlines to
    # tables that break them. Remove those.
    def remove_blank_lines(m: re.Match) -> str:
        lines = [line.strip() for line in m.group(0).split('\n') if len(line.strip()) > 0]
        # Table headers also seem to get messed up with extra newlines; we need to
        # put the text back on the same line: |+\n Title => |+ Title
        # For convenience, we'll join any non-table-like line to its predecessor.
        i = 1
        while i < len(lines):
            if lines[i][:1] not in ('|', '!'):
                lines[i - 1] = lines[i - 1] + lines.pop(i)
            else:
                i += 1

        return '\n'.join(lines)
    text = re.sub(r'\{\|.+?\|\}', remove_blank_lines, text, flags=re.DOTALL)

    return Page(text=text, raw_title=raw_title, title=title,
                revisions=revisions,
                categories=frozenset(categories))
 

pages = set(wikitext.process(XML_FILE, page_handler))
print('Loaded', len(pages), 'page(s) from XML dump')


# Convert the list of all pages into hierarchical categories
@dataclass(frozen=True)
class Category:
    name: str
    parent: 'Category'
    pages: list[Page] = field(default_factory=list)
    children: dict[str, 'Category'] = field(default_factory=dict)

    def add_at(self, category: list[str], page: Page):
        if category is None or len(category) == 0:
            self.pages.append(page)
        else:
            next = category[0]
            if next not in self.children:
                self.children[next] = Category(name=next, parent=self)
            self.children[next].add_at(category[1:], page)

    def path(self):
        out = []
        while self.parent is not None:
            out.append(self.name)
            self = self.parent
        out.reverse()
        return out

    def __repr__(self):
        if self.parent is None:
            return '<root>'
        return ':'.join(self.path())


root = Category(name='<root>', parent=None)

for page in pages:
    # Add this page to its "canonical" category, which is the most
    # specific one if it has multiple.
    categories = sorted(page.categories, key=lambda c: len(c))
    if len(categories) > 0:
        category = categories.pop()
    else:
        category = None

    root.add_at(category, page)


# Map each page (by title) to its primary category
page_categories = {}
categories_to_visit = [root]
while categories_to_visit:
    category = categories_to_visit.pop()
    categories_to_visit.extend(category.children.values())

    for page in category.pages:
        if page in page_categories:
            print("Page", page, "already exists in category", page_categories[page].name)
            continue
        page_categories[page.raw_title.lower()] = category


# Find links in each page and adjust them to point to the correct relative
# location in the category tree. This is used to fix up internal links during
# page output.
def rewrite_page_links(page: Page) -> str:
    def rewrite_link(m: re.Match) -> str:
        raw_target, _, anchor = m.group(1).replace(' ', '_').partition('#')
        if not raw_target:
            return m.group(0)

        # Match slash replacement in file path so links still work for pages
        # containing forbidden characters in their names.
        target = sanitize_file_name(raw_target)

        target_is_index = target.startswith(':') or target == 'Main_Page'
        link_text = m.group(2) or f'|{target}'

        if target.lower() in redirected_pages:
            target = redirected_pages[target.lower()]
            # Bit of a hack: category_for_page needs raw_target to refer to the
            # redirect target. There's probably a better way to do it..
            raw_target = target
        elif raw_target.lower() not in page_categories:
            print(f"Link target {target} doesn't match any known page (linked from {page.title})")
            return m.group(0)

        def category_for_page(title):
            if title == 'Main_Page':
                # Main_Page is the index of the root
                return []
            elif title.startswith(':'):
                # Page title gives the category path for other indexes
                return title.split(':')[2:]
            else:
                # For all other pages, look up the category to get its path
                return page_categories[raw_target.lower()].path()

        target_category = category_for_page(target)

        path = PurePosixPath(*target_category)
        # % in the target URL needs to be URL-escaped
        target = target.replace('%', '%25')
        # _index.md needs to be referred to by its directory only, otherwise
        # point at the file.
        if target_is_index:
            path = f'{path}/'
        else:
            # For pages with colons in the names, we already got the category
            # which covers the nonterminal portions but (probably because we're
            # doing something dumb earlier) we still need to split off the
            # nonterminal bits for the destination filename.
            file_name = target.rpartition(':')[2]
            path = path / f'{file_name}.md'

        # Spaces here get mangled because they're not permitted in wikitext
        # links. We fix them up with the relref_underscore_rewrite filter
        # when running pandoc.
        out = f'[[{{{{< ref "{path}" >}}}}{link_text}]]'
        return out

    return re.sub(r'\[\[(.+?)(\|.*?)?\]\]', rewrite_link, page.text)


# Write the hierarchy to files in the OUTDIR
OUTDIR = Path('content/')


def dump_page(page: Page, basepath: Path):
    """Write a single page as wikitext to a file under the given path."""
    filepath = basepath / f'{page.title}.wiki'
    mdpath = filepath.with_suffix('.md')
    assert filepath.parent.exists(), f'{page.title}'

    filepath.write_text(rewrite_page_links(page))

    # Convert wikitext (with templates already expanded) into markdown
    pandoc_extensions = [
        # If smart is on, quotes in the wiki input end up escaped (\') in the
        # output which is annoying to handle.
        '-smart',
        # Wikitext has identifiers in much of it anyway: turning on
        # auto_identifiers avoids emitting the ones that match what the
        # automatic ID would be.
        '+auto_identifiers',
        # Site generators usually consume these, and in particular some
        # constructs in our output can be mistaken for metadata blocks:
        # adding one will prevent those issues.
        '+yaml_metadata_block',
        # Pipe tables are more widely supported than grid tables, and
        # word wrapping of markdown in grid tables can behave badly.
        '-simple_tables',
        '-multiline_tables',
        '-grid_tables',
        '+pipe_tables',
        # Hugo's markdown engine doesn't support superscript or subscript.
        '-superscript-subscript',
    ]

    with tempfile.NamedTemporaryFile('wt') as metafile:
        metadata = {
            'revisions': [r.as_json() for r in page.revisions]
        }
        if page.title == '_index':
            # Default-collapse all categories in navigation
            metadata['bookCollapseSection'] = True

        if page.title in page_redirects:
            metadata['aliases'] = list(page_redirects[page.title])
        json.dump(metadata, fp=metafile)
        metafile.flush()

        subprocess.run(['pandoc', filepath,
                        '--standalone',
                        f'--metadata-file={metafile.name}',
                        '--to', ''.join(['markdown'] + pandoc_extensions),
                        '--filter', 'relref_underscore_rewrite.py',
                        f'--output={mdpath}'], check=True)

    # Pandoc interprets string scalars in metadata as markdown,
    # unfortunately- and there doesn't seem to be a way to make
    # it not do that. So to prevent it from mangling underscores in
    # page titles (treating them as emphasis) we insert the title
    # after running pandoc.
    with open(mdpath, 'r+') as mdfile:
        # yaml.read_all and friends can pull out individual YAML documents
        # from the input, but aren't guaranteed to leave the input past
        # the document so it's easier to find the delimiters manually.
        yaml_lines = []
        assert next(mdfile) == '---\n', f'{mdpath} lacks front matter?!'
        for line in mdfile:
            if line == '---\n':
                break
            else:
                yaml_lines.append(line)

        front_matter = yaml.safe_load('\n'.join(yaml_lines))
        if page.title == '_index':
            if len(page.categories) == 0:
                # Main page is special
                title = 'Main Page'
            else:
                # Category indexes get the category name as title, where
                # the name is the last path component and the primary category
                # is the one with the deepest path.
                title = sorted(
                    page.categories,
                    key=len,
                    reverse=True
                )[0][-1]
        else:
            title = page.title
        front_matter['title'] = title

        page_content = mdfile.read()

        mdfile.seek(0)
        mdfile.write('---\n')
        yaml.safe_dump(front_matter, mdfile)
        mdfile.write('---\n')
        mdfile.write(page_content)

    return (page, mdpath)


def dump_hierarchy(executor: Executor, category: Category, basepath:
                   Path=OUTDIR) -> list['concurrent.futures.Future']:
    """
    Recursively dump an entire category hierarchy to the provided directory.

    This processes individual pages in parallel on the provided executor,
    returning a list of futures for each page representing the result of
    calling dump_page.
    """
    if not basepath.exists():
        basepath.mkdir()

    page_tasks = [executor.submit(dump_page, page, basepath)
                  for page in category.pages]

    # Also run subtrees. This does not spawn a task for each subtree because
    # they would need to wait for their children to complete (in order to
    # collect all the page futures) and could lock up the executor with subtree
    # tasks blocked while page tasks are waiting to run.
    for subcategory in category.children.values():
        assert subcategory.parent is category
        page_tasks.extend(
            dump_hierarchy(executor, subcategory,
                           basepath=basepath / subcategory.name)
        )

    return page_tasks


with ThreadPoolExecutor() as executor:
    tasks = dump_hierarchy(executor, root)
    outpath_per_page = dict(task.result() for task in tasks)

print('Finished dumping', len(tasks), 'page(s)')

REDIRECTS_FILE = 'static/index.php/legacy-redirect-map.json'
redirect_mapping: dict[str, PurePosixPath]  = {}
for (page, mdpath) in outpath_per_page.items():
    relpath = mdpath.relative_to(OUTDIR)
    if relpath.name == "_index.md":
        # Index pages should be accessed by directory
        relpath = relpath.parent
    else:
        # Regular pages need to be accessed without the file extension
        relpath = relpath.with_suffix('')
    assert page.raw_title not in redirect_mapping
    redirect_mapping[page.raw_title] = str(PurePosixPath(relpath))

with open(REDIRECTS_FILE, 'w') as redirect_file:
    json.dump(
        redirect_mapping,
        fp=redirect_file,
        indent=2,
    )
print('Wrote redirects to', REDIRECTS_FILE)
