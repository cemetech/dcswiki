---
revisions:
- author: KermMartian
  timestamp: '2010-07-19T13:40:20Z'
title: Error Codes
weight: 40
aliases:
- /Error_Codes/
---

The following list is an exhaustive tabulation of all the error codes
Doors CS may throw. The majority only occur when executing BASIC
programs and are analogous to the TI-OS ERR:...... errors.

\
\# Error 501: The program you are trying to run is not supported by
Doors CS \[Error: Invalid Type\] (TYP)\
\# Error 502: A library this program needs to function is unavailable
\[Error: Missing Library\] (LIB)\
\# Error 503: The specified file could not be found \[Error: File Not
Found\] (FNF)\
\# Error 504: Insufficient memory to archive or unarchive \[Error:
Memory\] (MEM)\
\# Error 505: No programs present (TI=83 only) \[Error: No progs\]
(NPG)\
\# Error 506: The file Doors CS has attempted to archive or unarchive
contains errors, or there is insufficient memory \[Error: Archive\]
(MEM)

\# Error 507: Overflow Error \[ERR: OVERFLOW\] (OVR)\
\# Error 508: Divide by Zero Error \[ERR: DIVBY0\] (DV0)\
\# Error 509: Singular Matrix Error (SNG)\
\# Error 510: Domain Error \[ERR: DOMAIN\] (DOM)\
\# Error 511: Increment Error (INC)\
\# Error 512: Break \[ERR: BREAK\] (BRK)\
\# Error 513: Nonreal Error \[ERR: NONREAL\] (NRL)\
\# Error 514: Syntax Error \[ERR: SYNTAX\] (SYN)\
\# Error 515: Datatype Error \[ERR: DATATYPE\] (TYP)\
\# Error 516: Argument Error \[ERR: ARGUMENT\] (ARG)\
\# Error 517: Dim Mismatch \[ERR: MISMATCH\] (MSM)\
\# Error 518: Dimension Error \[ERR: DIM\] (DIM)\
\# Error 519: Undefined Error \[ERR: UNDEFINED\] (UND)\
\# Error 520: Memory Error \[ERR: MEMORY\] (MEM)\
\# Error 521: Invalid Error \[ERR: INVALID\] (INV)\
\# Error 522: Illegal Nest \[ERR: ILLEGAL NEST\] (ILN)\
\# Error 523: Bound Error \[ERR: BOUNDS\] (BND)\
\# Error 524: Graph Range \[ERR: RANGE\] (RNG)\
\# Error 525: Invalid Zoom \[ERR: ZOOM\] (ZOM)\
\# Error 526: Specified label does not exist \[ERR: LABEL\] (LBL)\
\# Error 527: Statistics calculation error \[ERR: STATS\] (STS)\
\# Error 528: Solver has generated errors \[ERR: SOLVER\] (SLV)\
\# Error 529: Singularity reached \[ERR: SINGULARITY\] (SNG)\
\# Error 530: No sign change \[ERR: NO SIGN CHANGE\] (SCH)\
\# Error 531: Too many iterations \[ERR: ITERATIONS\] (ITR)\
\# Error 532: Guess not within low & high bounds \[ERR: BAD GUESS\]
(BDG)\
\# Error 533: Stats plot error \[ERR: STAT PLOT\] (STP)\
\# Error 534: Tol was too small \[ERR: TOLNOTMET\] (TNM)\
\# Error 535: Reserved variable \[ERR: RESERVED\] (RSR)\
\# Error 536: Device in wrong mode \[ERR: MODE\] (MDE)

\# Error 537: Link connection error \[ERR: LINK\] (LNK)\
\# Error 538: Insufficient memory to receive \[ERR: MEMORY\] (MEM)\
\# Error 539: Transmission error \[ERR: XMIT\] (XMT)\
\# Error 540: A duplicate variable exists \[ERR: DUPLICATE\] (DUP)\
\# Error 541: Insufficient memory to receive \[ERR: MEMORY\] (MEM)\
\# Error 542: Unknown variable type \[ERR: TYPE\] (TYP)

\# Error 543: Scale incorrect \[ERR: SCALE\]\
\# Error 544: ID not found \[ERR: ID NOT FOUND\]\
\# Error 545: No mode\
\# Error 546: App could not be validated \[ERR: VALIDATION\]\
\# Error 547: Length\
\# Error 548: Application error\
\# Error 549: Application error 2\
\# Error 550: Specified app has expired\
\# Error 551: Bad address\
\# Error 552: The referenced variable is archived and cannot be accessed
\[ERR: ARCHIVED\]\
\# Error 553: Variable intended for a different version \[ERR:
VERSION\]\
\# Error 554: Archive is full \[ERR: ARC FULL\]\
\# Error 555: Variable\
\# Error 556: A variable with the same name exists \[ERR: DUPLICATE\]
