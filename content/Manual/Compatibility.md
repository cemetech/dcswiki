---
revisions:
- author: KermMartian
  timestamp: '2016-02-02T23:13:42Z'
title: Program Compatibility
weight: 70
---

Doors CS, Doors CSE, and Doors CE are capable of running more than just
programs written expressly for the shell. Doors CS can run the following
types of programs for the TI-83, TI-83 Plus, and TI-84 Plus-family
calculators.

Alphabetical list of DCS Compatible formats.

| Program Type         | Doors CS 5 (TI-83) | Doors CS 7 (TI-83+/TI-84+) | Doors CSE 8 (TI-84+CSE) | Doors CE 9 (TI-84+CE) |
|----------------------|--------------------|----------------------------|-------------------------|-----------------------|
| AShell ASM           | X                  |                            |                         |                       |
| Axe source (text)    |                    | X                          |                         |                       |
| TI-OS ASM ("Nostub") | X                  | X                          | X                       | X                     |
| Doors CS 5 ASM       | X                  |                            |                         |                       |
| Doors CS 6/7 ASM     |                    | X                          |                         |                       |
| Doors CSE 8 ASM      |                    |                            | X                       |                       |
| Ion ASM              | X                  | X                          |                         |                       |
| MirageOS ASM         |                    | X                          |                         |                       |
| MirageOS TI-BASIC    | X                  | X                          | X                       | X                     |
| SOS ASM              | X                  |                            |                         |                       |
| TI-BASIC             | X                  | X                          | X                       | X                     |
