---
revisions:
- author: KermMartian
  comment: /\* Gold and Patch Releases \*/
  timestamp: '2022-09-24T17:08:41Z'
title: Version History
weight: 98
aliases:
- /Version_History/
---

Doors CS has been under development for nine year as of September 2010,
one of the longest-running projects under more-or-less continual
development in the TI community. The very first version was in pure
BASIC for the TI-83 calculator as a simple list of BASIC programs to
run. Doors CS 2 was also pure BASIC, and Doors CS 3 was a prototype of
an ASM-supplemented BASIC shell. Doors CS 4 released this goal after a
year of hard work on TI-83 assembly coding. TI-83+ support was
introduced with Doors CS 5, a pure-ASM shell. Doors CS 6 brought it to
the level of a Flash application, abandoning TI-83 support at version
5.0, and Doors CS 7 brought a new era of stability, compatibility,
support, networking, and ubiquity to the shell. Doors CSE 8 is a port of
Doors CS to the color-screen TI-84 Plus C Silver Edition calculator

## Doors CSE 8 {#doors_cse_8}

### Gold and Patch Releases {#gold_and_patch_releases}

**8.2.0 - May 19, 2015**\
[Cemetech news article](http://www.cemetech.net/news.php?id=742) posted.
Repairs hook chaining with RawKeyHooks and AppChangeHooks. Also improves
power-off options in the enhanced TI-BASIC editor and fixes minor bugs
in Hybrid BASIC libraries. (Build 1603)

**8.1.3 - May 5, 2015**\
Repairs context menus not launching after some TI-BASIC programs are
executed. Changes custom App icon header field. (Build 1565)

**8.1.2 - December 4, 2014**\
Repairs the ColorPixel ASM function. Also adds flag detecting whether
multi-argument real() functions have been used yet, and if not,
interprets all real() calls as pure BASIC calls. (Build 1547)

**8.1.1 - August 19, 2014**\
Fixes a single bug with Celtic 2 CSE det(2) function for inserting a new
last line into programs/Appvars. (Build 1546)

**8.1.0 - July 9, 2014**\
[Cemetech news article](http://www.cemetech.net/news.php?id=679) and
[ticalc.org news
article](http://www.ticalc.org/archives/news/articles/14/148/148638.html)
posted. Adds the ability to list and run Applications, includes a new
version of xLIBC, fixes a number of annoying and in some cases fatal
bugs.

**8.0.1 - November 8, 2013**\
Fixes single bug with Celtic 2 CSE det(0) function for archived
programs/AppVars. (Build 1339)

**8.0.0 - November 5, 2013**\
First gold version of Doors CSE released. [Cemetech news
article](http://www.cemetech.net/news.php?id=632) and [ticalc.org news
article](http://www.ticalc.org/archives/news/articles/14/148/148414.html)
posted. Includes ability to run nostub and DCSE8 ASM programs, nostub
and DCSE8 TI-BASIC programs, HomeRun, Celtic 2 CSE, xLIBC, extensive
program management features, enhanced TI-OS BASIC editor, and much more.
(Build 1337)

### Development Releases {#development_releases}

**8.1 Release Candidate 2 - July 3, 2014**\
News article [here](http://www.cemetech.net/news.php?id=678). Fixed
problems with the Stats Wizards and running ASM programs, upgrading
folders, hiding programs from Doors CSE, and scrolling the desktop.
(Build 1523)

**8.1 Release Candidate 1 - July 3, 2014**\
News article [here](http://www.cemetech.net/news.php?id=675).
Implemented all DCSE 8.1 features like listing and running Apps and a
new xLIBC, plus many bug fixes. (Build 1517)

**8.0 Release Candidate 2 - October 24, 2013**\
News article [here](http://www.cemetech.net/news.php?id=629). Fixed
myriad tricky bugs and introduces a bit of new functionality, including
a New Prog feature in the Properties menu, new Hybrid BASIC libraries,
and more. (Build 1282)

**8.0 Release Candidate 1 - October 14, 2013**\
News article [here](http://www.cemetech.net/news.php?id=627).
Semi-complete version, with almost all features in place, including all
editor modifications. (Build 1203)

**8.0 Beta 4 - October 6, 2013**\
Several versions released, as new patches and bug reports were
introduced.

**8.0 Beta 3 - September 30, 2013**\
First discussed
[here](http://www.cemetech.net/forum/viewtopic.php?p=209471#209471). New
features and fixes include improved ALPHA-scrolling in the TI-BASIC
editor, improved and expanded Celtic 2 CSE functions, half-resolution
ASM header flag, and proper 2:Goto behavior.

**8.0 Beta 2 - September 21, 2013**\
TODO

**8.0 Beta 1 - September 18, 2013**\
First private beta release.

## Doors CS 7 {#doors_cs_7}

### Gold and Patch Releases {#gold_and_patch_releases_1}

**7.4 - September 24, 2022**\
Repairs support for newest TI-84 Plus LCD drivers, fixes a TI-BASIC
library regression discovered during testing.

------------------------------------------------------------------------

**7.3 - Expected October 5, 2014**\
Adds the corrected Goto option for BASIC errors from Doors CSE 8.0.
Removes the [Easter egg]({{< ref "Easter_Egg.md" >}}) from Doors CS 6
through 7.2. Never properly/widely released.

------------------------------------------------------------------------

**7.2 - July 10, 2013**\
The major new features of Doors CS 7.2 are CALCnet related, including
BASIC CALCnet, direct USB globalCALCnet, a new gCnClient, and more.

------------------------------------------------------------------------

**7.1.1 - January 1, 2011**\
Bug in CALCnet<sup>2.2</sup> checksumming routine discovered and fixed:
missing `inc hl` causing the first byte of a frame to be added N types
instead of each of the N frame bytes 1 time.

**7.1 - December 14, 2010**\
The major new feature of Doors CS 7.1 is the CALCnet2.2 drivers, tested,
perfected, and demonstrated via NetPong, Flourish, and SpeedTest. In
addition, Doors CS 7.1 offers a huge variety of tweaks and bugfixes
related to HomeRun, OS 2.53MP, saving/opening Associated Program files,
the Hourglass bug, and a few additions to the DCSB Libs.

------------------------------------------------------------------------

**7.0.1 - August 31, 2010**\
Four minor bugfixes: (1) one-pixel hotspot misalignment in DCS Menu, (2)
Canceling copy/rename caused archived programs to move to RAM, (3) small
LineRead bug in CIII libs due to flag transposition, and (4) Incorrect
functionality of DCSB Libs sum(7,8) PushGUIStack function for
GUIRButtonImgs.

------------------------------------------------------------------------

**7.0 - August 24, 2010**\
Nine years since its humble beginnings as a crude BASIC shell, Doors CS
7.0 aims to bring a full set of features and rock-solid stability to
users and coders alike. It supports MirageOS, Ion, Doors CS, and nostub
BASIC and Assembly programs, and contains full support libraries for
XLib, Celtic III, PicArc, (partially) Omnicalc, and the new DCSB Libs
that let Doors CS coders use features like the DCS GUI. The HomeRun
feature lets you execute any type of program, BASIC or ASM, archived or
not, from the TI-OS homescreen. The Doors CS desktop lets you view your
programs and folders, organize them into nested folders, cut, copy,
rename, lock, archive, hide, and even edit programs, and change settings
and options from the DCS Menu. Doors CS 7.0 adds more robust protection
from data loss due to RAM Clears, including automatic restoration of the
user's folder structure. For ASM developers, Doors CS offers a full
suite of features, adding the DCS GUI system, and Associated Program
system that automatically opens files in their associated viewer/editor,
and much more on top of a full complement of MirageOS and Ion-compatible
libraries. BASIC programmers can take advantage of support for every
popular BASIC library built directly into the shell. Full information
about Doors CS can be found at http://dcs.cemetech.net, including a
(very) exhaustive feature list and screenshots galore for your viewing
pleasure.

### Development Releases {#development_releases_1}

**7.2 Beta 3 - June 13, 2013**\
Introduces BASIC CALCnet libraries, much-improved direct USB stability,
and many optimizations.

------------------------------------------------------------------------

**7.2 Beta 2 - June 13, 2011**\

------------------------------------------------------------------------

**7.2 Beta 1 - March 22, 2011**\
Features Direct USB gCn

------------------------------------------------------------------------

**7.1 Beta 2 - December 5, 2010**\

------------------------------------------------------------------------

**7.1 Beta 1 - November 8, 2010**\

------------------------------------------------------------------------

**7.0 RC 2 - August 24, 2010**\
Doors CS 7.0 Release Candidate 2 has a bunch of tweaks and fixes from
7.0 RC 1. The issue with the CHAIN7 appvar getting occasionally left
behind has been fixed, a small issue with 2.53MP and HomeRun was fixed,
the Manual and SDK have been updated, the LCD tuning has been tweaked
and tweaked beyond measure, and the APD hook has been modified to
\[ON\]\[XTOn\].

------------------------------------------------------------------------

**7.0 RC 1 - August 20, 2010**\
A full listing of fixes and improvements over Doors CS 6.9 beta:

-   Myriad bug fixes and tweaks based on invaluable feedback from
    Cemetech members and Doors CS beta testers.
-   Addition of \[ON\]\[MODE\] hook and multi-page scrolling feature.
-   Additional DCSB Lib features and bug-fixes.
-   Updated Doors CS 7 manual and SDK (both included in the zip)

------------------------------------------------------------------------

**6.9 Beta - August 13, 2010**\
Doors CS 6.9 beta is the final beta of Doors CS 7.0, containing many
feature additions, optimizations, and bug fixes. It incorporates several
bug fixes based on reports from the loyal beta testers. Doors CS 6.9
beta adds incremental improvements to text input functions, and
introduces a Help item in the DCS Menu. It improves a small bug with the
BASIC "Instant Goto" feature, and contains a near-complete rewrite of
how programs are counted to optimize for speed and correctness. A full
listing of fixes and improvements:

-   Improved character width calculation fallback for "unknown"
    characters.
-   Fixed FileSaveAs/FileOpen bug from FileOSExclude addition.
-   After extensive searching and re-writing and debugging, reconfigured
    how programs are counted, and fixed several corner cases that broke
    proper type detection. Proper hiding of programs for future versions
    of DCS also implemented.
-   Residual problem with SE caching and InfoPops resolved.
-   Repaired Goto bug that occasionally went one line too far.
-   Found and fixed bug where pressing 1-6 to enter a sub-sub-folder
    would cause the properties menu to appear.
-   Found and fixed bug that would occasionally cause DCS to miscount
    programs in a folder, thanks to dcsSquish.
-   Completed Doors CS 7 Manual.

------------------------------------------------------------------------

**6.8 Beta - July 25, 2010**\
Shortly after the release of Doors CS 6.7.6, this release of Doors CS
represents the penultimate beta version, containing implementations of
very nearly every bug fix and feature request posed to me thus far in
the two-and-a-half month resurrection of Doors CS development. Doors CS
7 is swiftly looming, and I expect this release to be very close to the
final version, nearly Release Candidate quality despite not being tagged
as such. It's extremely important that Doors CS 7 be stable, reliable,
and as bug-free as humanly possible, so I urgently request that everyone
try out this release and look for issues. Use the editor, use the DCSB
Libs, run your favorite ASM and BASIC programs, and try to make things
break.

Here's the most important features and bugfixes among all those new
since Doors CS 6.7 Beta:

-   Plenty of optimizations for both speed and size, respectively making
    the shell feel faster and giving me more leeway for additional fixes
    and features. Tons of bug fixes based on reports from the loyal beta
    testers.
-   Fixes and additions to the DCSB Libs, including sum(13)
-   Expanded SE functionality within Doors CS desktop.
-   Repaired bugs in GUI text input functions, moved them to the edit
    buffer for faster typing, sped them up on 15MHz calculators, and
    added \[CLEAR\] as a backspace key to supplement \[DEL\] as a delete
    key.
-   Adding scroll wrapping to DCS desktop. Scroll up at the top to go to
    the bottom, scroll down at the bottom to go to the top.
-   Reduced DCS Menu to three items: Display, About, Options.
-   Added more understandable BASIC Exec errors.
-   Added folder and screen saving so after using the Properties menu,
    running a program, or quitting, you start where you left off.
-   Inclusion of an ALCDFix-like LCD delay tuner for those with
    TI-84+-series calculators, eliminating the need for that program.
-   Added features to FileOpen and FileSaveAs AP routines for ASM
    programmers

------------------------------------------------------------------------

**6.7.6 Beta - July 21, 2010**\
In the interest of completing all of the To-Do items on my To-Do List
and solve all the bugs on the same list, I have decided to push back
Doors CS 6.8 beta from its original July 20th deadline (today) until
August 1st or earlier, depending on how long each of the items takes. At
this point, all of the major primary features for Doors CS 7 have been
implemented in Doors CS, and almost all of the secondary features as
well. All of the remaining To-Do items are minor fixes, and as I'm
dealing with a current distribution of 606 bytes free on Page 0, 1202
bytes on Page 1, and 840 bytes on Page 2, I don't anticipate any
sizeable additional features. Since Doors CS 6.7 was released, a bunch
of new features and bugs have been implemented:

-   Plenty of optimizations for both speed and size, respectively making
    the shell feel faster and giving me more leeway for additional fixes
    and features.
-   Completed new sum(13) DCSB Lib function, facilitating attractive
    Menu()s in BASIC programs with minimal GUI effort.
-   Added \[STO\>\] as method to break out of sum(12).
-   Expanded SE functionality within Doors CS desktop.
-   Repaired three bugs with GUIRTextMultiline scrolling calculations.
-   Adding scroll wrapping to DCS desktop. Scroll up at the top to go to
    the bottom, scroll down at the bottom to go to the top.
-   Moved GUIRTextLineIn, GUIRPassIn, and GUIRTextMultiline to an edit
    buffer for faster typing.
-   Reduced DCS Menu to three items: Display, About, Options.
-   Added more understandable BASIC Exec errors.
-   Added folder and screen saving so after using the Properties menu,
    running a program, or quitting, you start where you left off.
-   A plethora of bug fixes.

------------------------------------------------------------------------

**6.7 Beta - July 2, 2010**\
Doors CS 6.7 beta introduces many bug fixes and features to Doors CS,
most notably the addition of the DCS BASIC libraries, also known as the
DCSB Libs. These allow TI-BASIC programmers to take advantage of most of
the Doors CS GUI subsystem features that ASM programmers have been using
since Doors CS 6.0. They also offer functions that can be used for
facilitating games and other programs, when combined with the XLib,
Celtic, and PicArc compatibility libraries that Doors CS packs in. With
Doors CS, you can take advantage of the widest variety of libraries
available in any current shell or library package. As always, please try
it out, report any bugs or feature suggestions, and enjoy! Don't forget
to see how the DCSB Libs could be used to enhance your Cemetech Contest
#7 entry.

------------------------------------------------------------------------

**6.6 Beta - June 19, 2010**\
This beta of Doors CS 7 adds many bug fixes, performance improvements,
and compatibility workarounds over Doors CS 6.5 beta, although it
implements no major new features. It adds the ability to recognize and
automatically edit Axe source code files, more keyboard shortcuts,
including the ability to leave any GUIRSmallWin by pressing \[CLEAR\],
and fixes issues with renaming and copying programs. It repairs several
known XLib compatibility issues, and importantly, works around the TI-OS
parser hook bugs in OS 2.53MP and 2.54MP. It adds a fix for the
long-standing problems with Stop and shells, catching and handling this
token. It adds the RunProg vector to allow ASM programs to execute one
or more ASM or BASIC programs, or even chain execution along several
programs. Finally, it adds true support for up to 255 files, programs,
and folders per folder (including the desktop), for virtually unlimited
total files on a calculator visible from Doors CS. Among many other
smaller tweaks and bugfixes, all of which can be viewed on the Doors CS
wiki, the following list hits the highlights of this release:

-   Added custom icon for Axe source files, with ID flags identical to
    nostub BASIC program; added check to edit Axe source files when
    executed instead of trying to run.
-   Fixed Instant Goto failing when scrolling backwards through two-byte
    tokens ending in \$3F such as \$AA,\$3F.
-   Pressing \[Clear\] from any Small Window now clicks its \[X\], if it
    has one.
-   Added hourglass for sorting, archiving/unarchiving, folder backup,
    and copying back after ASM execution
-   Avoided ridiculous failure of \_PutC to recognize Split-\>Full in
    2.53MP for DCS error messages.
-   Solved problem of catching Stop token with nonexistent error 127 and
    special-case silent handling within DCS.
-   Debugged, fixed, and tested workaround for 2.53MP's improper
    post-ParserHook flag/Op1 handling.
-   Resolved one many-programs bug by tracing it to CALCnet2 in
    DCS6.1/6.2. Bug repair pending reactivation of CALCnet.
-   Added folder backup before program execution (from desktop only).
-   Added and tested Runprog vector. One display bug remains.
-   Added GUIFindThis and PushGUIStacks routines.
-   Fixed all sections calculating remaining/onscreen programs from
    ProgsToDo and ProgsDone to correctly handle \>127 programs.

------------------------------------------------------------------------

**6.5 Beta - June 5, 2010**\
Doors CS 6.5 Beta is the first DCS7 beta to contain one of the biggest
feature additions to Doors CS 7, alongside Homerun, Folder Backup, and
Runprog Chaining: built-in support for XLib / Celtic III / PicArc /
Omnicalc compatibility. Building off Iambian's generous donation of the
Celtic III code base, this version of Doors CS adds many improvements,
bug fixes, and optimizations to that code base. In addition, it provides
many core Doors CS bug fixes and optimizations, including repair of a
graphics glitch with FileOpen/FileSaveAs, fixes for malfunctions when
copying and renaming programs, repair of an issue where deleting the
DCS7 appvar without a RAM reset could cause duplicate folders to be
created, and many more.

------------------------------------------------------------------------

**6.4 Beta - May 30, 2010**\
This release contains both new features and a host of bug fixed based on
all of your helpful reports on Doors CS 6.3 beta. Among the most notable
new features is the completed "Homerun" parser hook, which allows BASIC
and ASM programs, whether nostub or written for a shell, in ROM or in
RAM, and even Doors CS AP files such as Document DE files, to be run
directly from the homescreen. This is made possible by a complete
overhaul of the program execution system (RunProg) within Doors CS,
which some of you may have been following over the last several weeks.
Another new feature is much more powerful FileOpen, FileSave, and
FileSaveAs routines, which will be a boon for programmers writing
programs for DCS7 that work with files. With some heroic assistance from
the indomitable Brandon Wilson, I traced some very difficult bugs, and
patch quite a number of more minor bugs. As always, I worked on several
small tweaks to improve the user experience. It is recommended that you
don't use Document DE 6 with this release, as FileOpen and FileSaveAs
are unstable under TI-OS 2.41 and 2.53MP.

------------------------------------------------------------------------

**6.3 Beta - May 12, 2010**\
Doors CS 6.3 beta is the first development release of the next
generation of the Doors CS shell for TI graphing calculators. It
implements a variety of optimizations and fixes, as well as new features
including Folder Backup across RAM clears, the ability to Hide programs
from the TI-OS and the prgm menu, integrated TabFuncs, a new Properties
menu, a MemoryPop feature to see at-a-glance the precise amount of free
RAM and ROM on a device, and many others. It also implements
much-requested fixes for compatibility with Nspire calculators. Note
that although this is a three-page app, the XLib / Celtic III
compatibility libraries have not yet been added onto the third page.

## Doors CS 6 {#doors_cs_6}

### Gold and Patch Releases {#gold_and_patch_releases_2}

**6.2 - May 27, 2008**\
A low-key upgrade for a few long-standing but minor bugs and annoyances.

**6.1 "Montuori" - June 20, 2007**\
\* A new in-shell editor makes programming BASIC easy and fast. Based on
the TI-OS editor but enhanced with hooks and tweaks, you can use the
entire 8 rows to type.

-   BASIC developers will also enjoy Instantaneous Goto and four new
    header formats with descriptions for flexibility and ease-of-use.
-   Those who asked for a popup containing size and description info can
    rejoice in the InfoPop: simply hover over a program to see its size
    in bytes and embedded description
-   A multitude of bugfixes solve every issue reported from 6.0,
    including a new, perfected AP GUI system.
-   The program management functions including copying and renaming are
    now more robust, handling all file types including archived and/or
    locked programs properly.

------------------------------------------------------------------------

**6.1 "Montuori" Beta 4 - June 17, 2007**\
Several bug fixes, mostly on the BASIC editor functionality.

------------------------------------------------------------------------

**6.1 "Montuori" Beta 3 - June 7, 2007**\
Inclusion of several bug fixes based on reports.

------------------------------------------------------------------------

**6.1 "Montuori" Beta 2 - June 5, 2007**\
\* Added special [GUIMouse]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/GUIMouse.md" >}})
mousemode for drawing and games. (6/6/07)

-   Completed debugging of the editor feature. (6/5/07)
-   With BrandonW's help, coded a chainloader for the \[ON\]\[PRGM\]
    rawkeyhook. (6/5/07)
-   Tri-lingual options screens adjusted for aesthetics; \[ON\]\[PRGM\]
    checkbox added to all. (6/5/07)
-   All APD instances repaired for proper functionality. (6/5/07)
-   \[2nd\] now also closes the About screen in addition to the
    already-extant \[ALPHA\]. (6/5/07)
-   Temporary [Associated
    Files]({{< ref "/SDK/Mono/Asm/DCS/APs/" >}}) are now hidden from
    the File Open and File Save As dialogs. (6/4/07)
-   Constructed proper re-AP routine for FOpen GUI routine. (6/4/07)
-   Set the VFAT to clear on each RenderDesktop iteration to resolve
    edit and properties issues. (6/4/07)
-   Restricted editing to BASIC programs. (6/4/07)
-   Fixed number of items popped at SMPower close 10\>\>11 to resolve
    crash. (6/4/07)

------------------------------------------------------------------------

**6.1 "Montuori" Beta 1 - June 3, 2007**\
\* Implement stopgap AP file opening from FOpen GUI for archived files.
(6/3/07)

-   Completed writeback for AP files in FOpen GUI routine. (6/3/07)
-   Enabled editing and handling for archived programs. (6/3/07)
-   All known editor crash/freeze problems fixed with BrandonW's help.
    (6/3/07)
-   Folder name displayed in taskbar for non-main folders. (6/3/07)
-   Added MouseMode context to restrict InfoPop to the desktop only.
    (6/3/07)
-   BASIC editor added but disabled until bugs can be worked out.
    (6/2/07)
-   Modified ArcUnarcDCSBASIC to skip past description-prefixed DCS
    headers. (6/2/07)
-   Modified VATFind to understand description-prefixed DCS headers.
    (6/2/07)
-   Repaired property display for unarchived TI-OS BASIC programs.
    (6/2/07)
-   Fixed copying for archived programs. (5/31/07)
-   Fixed renaming for archived programs. (5/31/07)
-   Added renaming capabilities for folders. (5/31/07)
-   Added description of "Folder" for folders in InfoPop. (5/31/07)
-   Fixed all incorrect characters in the font table. (5/31/07)
-   \[ON\]\[PRGM\] keyhook implemented to launch DCS. (5/31/07)
-   Folder renaming and auto-rearchiving implemented. (5/31/07)
-   "Hybrid" DCS-MOS header with 16x16 icon on second line implemented.
    (5/31/07)
-   MOS BASIC description format now accepted and displayed by InfoPop.
    (5/31/07)
-   Elfprince's "ResetAppPage" problem turns out to be custom cursor
    issue. Solved. (5/31/07)
-   EE audio timing fixed to control text speed. (5/30/07)
-   InfoPop system created and debugged. (5/30/07)
-   Repaired jumpy cursor editor by adding Cn2Getk instead of direct
    input. (5/29/07)
-   Repaired "Galaxian" issue by optimizing GUIRRadio and GUIRCheckbox
    rendering. (5/29/07)
-   Power hotspots added to Power menu. Upgrade complete. (5/29/07)
-   Added new icons in place of radio buttons in Power menu. (5/29/07)
-   Rounded the upper corners of GUIRSmallWindows. (5/29/07)
-   Added several hotkeys; Hotkeys page created. (5/28/07)
-   Made 'Ans' (\$72) a valid 'hide' token. (5/28/07)
-   Commented out the Zelda music in the EE, saves about 2,000 bytes.
    (5/28/07)
-   Eliminated whitespace behind DCS Menu in rolldown. (5/28/07)

------------------------------------------------------------------------

**6.0** - April 20, 2007**\
Final** Doors CS 6.0 release. Full press release here: [Press
Release](http://www.cemetech.net/doorscs/docs/pressrelease2.pdf).
Publicity information on the [Publicize]({{< ref "Publicize.md" >}})
page.

### Development Releases {#development_releases_2}

**6.0 RC 4 - April 8, 2007**\
Variety of bug fixes, most of them having to do with the file and folder
system structures. Upgraded to 4.1 on 4/9/07 and 4.2 on 4/10/07.

------------------------------------------------------------------------

**6.0 RC 3 - March 18, 2007**\
Debouncing has been added to file renaming and folder creation, a
scrollbar misalignment was fixed in FileOpen and FileSaveAs, and the
alpha indicator was fixed from a previous change. In addition, interrupt
issues with Ion-derivative routines were repaired, FileSaveAs no longer
accepts blank filenames, and most importantly, a subtle misalignment bug
was fixed that prevents variable-size AP files from crashing Doors CS.

------------------------------------------------------------------------

**6.0 RC 2.1 - March 14, 2007**\
Of particular interest are tweaks to the Associated Program (AP) system
that should allow all subsequently-created AP-based programs to work
properly and use calls such as FileOpen and FileSaveAs. If you're
interested in testing out Document DE 6, this would be the release to
use. Thanks to a fix, it is no longer necessary to delete old Appvars
before installing an upgrade to Doors CS.

------------------------------------------------------------------------

**6.0 RC 2 - March 7, 2007**\
\* Resolved CALCnet2's predilection to crash on real calculators\
\* Resolved the GUI error handler for insufficient RAM\
\* Finished resolving the homescreen retention issue\
\* Made Doors CS aware of the correct handling for MOS hidden programs\
\* Finished completely debugging Tari's alpha sorting routine\
\* Resolved the speed issue reported in RC1\
\* GUIfied the Archiving... junk.

------------------------------------------------------------------------

**6.0 RC 1 - February 16, 2007**\
\* Completed CALCnet2 support\
\* Fixed all text input debouncing problems\
\* Tweaked Cn2 wrapper routines for more stable transfer\
\* Fixed crash from DCS menu ("start" menu) when enabling Cn2\
\* Added support for a "hook"-type routine during GUI text execution\
\* Fixed off-by-one error in displaying archived icons\
\* Repaired all bugs and issues with GUIRMultiline\
\* Brought "Defragmenting..." issue to full resolution\
\* Fixed rendering of multiple sequential line breaks in text routines\

------------------------------------------------------------------------

**5.9 Beta 1 - December 27, 2006**\
\* Implemented Tari's sort routine, but disabled it because of a bug.
(12/27/06)\
\* Fixed manual and automatic powerdown/up issues with Cn2 and gCn.
(11/24/06)\
\* Fixed runindic presence after BASIC exec. (11/22/06)\
\* BASIC pause if text on homescreen at quit. (11/22/06)\
\* Link menu started with default disabled txt. (11/22/06)\
\* Binaricized in/out count icon for all langs. (11/22/06)\
\* Added counting of Cn2 rec/send bytes, reset on Cn2 reenable.
(11/22/06)\
\* Switched the safe driver checkbox to alpha sort, allocated AV space.
(11/22/06)\
\* Harq(?)'s \[on\] problem resolution. (11/20/06)\
\* reenable silent link for BASIC and ASM programs - TI im1. (11/17/06)\
\* Lowercase flag disable. (11/17/06)\
\* Residual problem with the \[A/a/1\] indicator resolved. (11/16/06)\

------------------------------------------------------------------------

**5.8 IBeta 5 - November 27, 2006**\
For all those Cemetech members who have repeatedly alerted me to the
absence of the Link section of the DCS Menu, you may now put your
worries to rest. I have implemented an informational window that
displays number of bytes in and out since Cn2 was last enabled, as well
as an indication of whether Cn2 is enabled at all, and whether gCn is
currently available. I further propagated the new GUI through the three
multilingual versions (English, French, and Spanish), and I'm working on
trying to smooth out a few bugs from the CALCnet2 routines.
Specifically, the receiving routine seems to be triggering very, very
slowly; that is, there is too much time between each reception check.

------------------------------------------------------------------------

**5.8 Beta 4 - November 13, 2006**\
A special focus has been placed on the Associated Program (AP subsystem)
tools and GUIs, including completed FileOpen, FileSave, and FileSaveAs
routines. A new icon has been added for associated programs with no
known reader, and a small bug with type detection has been repaired.
Several minor quality-of-use issues have been resolved, such as more
debouncing and alpha setting masking. GUI tools that manipulate memory
have been modified to return information on the memory they have
changed. The crash-on-Cn2 enable bug was fixed, but another version of
the bug seems to have popped up. Finally, multilingual versions have
been implemented; an English, French, and Spanish version are included
in this release.

------------------------------------------------------------------------

**5.8 Beta 3 - November 1, 2006**\
\* Minor bug in BAU feature fixed with foamy3's help.

-   Error messages for battery and memory warnings masked over filename
    text.\
-   Version number added in About display.\
-   About display now appears at AppVar creation.\
-   Routine created and debugged to save AppVar data in an archives
    appvar.\
-   Routine to detect backed-up appvar and restore from backup created
    and debugged.\
-   Routine to display message after crash completed.\
-   Include files organized to put language-specific data together.\

------------------------------------------------------------------------

**5.8 Beta 2 - October 23, 2006**\
\* Debounce issue repaired for main and GUI mice.\
\* Error messages for battery and memory warnings masked over filename
text.\
\* \[ALPHA\] added as a rightclick key in GUI mouse.\
\* Desktop redraw flicker removed.\
\* \[ON\] as poweroff shortcut add in main and GUI mice.\
\* BASIC AutoUpgrade (BAU) feature added for Doors CS BASIC programs.\
\* Contrast meter bug fixed, made easier to see.\
\* Dynamic user-settable acceleration settings completed and
implemented.\

------------------------------------------------------------------------

**5.8 Beta 1 - October 15, 2006**\
This version contains a plethora of bug fixes and upgrades, such as
solutions to the icon misalignment with folders and several GUI issues
that elfprince13, our now-retired administrator, helped me to find. The
shell expansions system has been completely rewritten and optimized for
speed; information on SEs can be found in the wiki. New features have
been added as well, from the bright and shiny new Intelligent Writeback
(IW) execution subsystem to the spiffy second-generation associated
program subsystem.

------------------------------------------------------------------------

**5.6.0 Beta - June 3, 2006**\
GUI rendering routines for 6.0 completed. Minor changes may need to be
done, but for the most part they are bugfree. Next step is interactive
GUI routines.

------------------------------------------------------------------------

**5.5.0 Beta 1 - March 14, 2006**\
This is the first public release of the Doors CS shell as a Flash
Application. Taking up a mere single page of archive/ROM space, this
program is a complete shell and GUI for TI-83+/TI-84+ graphing
calculators. It also contains a complete shell for the TI-83, Doors CS
v5.1. Embedded but not yet activated in this version are such amazing
features as a full GUI API. Doors CS can recognize and run nearly every
type of TI-83/+/SE/84+/SE file available, including Ion and Doors CS ASM
programs, TI-OS BASIC and ASM programs, and many others. The interface
can be navigated with a mouse-like cursor, and is fully expandable with
small modules called Shell Expansions. Manual included.

------------------------------------------------------------------------

**5.5.0 IBeta 1 - March 8, 2006**\
At last, Doors CS has begun the transition to a Flash Application (app).
Going from usuing 6.5K of RAM and having a full feature set to using
16.3K of ROM and have at least twice as many features has been seen as a
good move: it will decrease the frustration of redownloading DCS
following random RAM clears, allow the preservation of precious RAM for
games and programs, and allow many more features to be packed it. Among
the current featureset in development for Doors CS 6 are networking/web
access, full GUI and filesystem routines for programs, and support for
MirageOS and CrunchyOS files.

------------------------------------------------------------------------

**5.4.0 Beta 5 - March 3, 2006**\
To the best of my knowledge, this is bugfree. Please go through, run it,
put it through its paces, and try to make it crash. Run Ion programs,
give it MOS programs, mess with folders. I believe that most of the
problems were being caused by a faulty sort routine.

------------------------------------------------------------------------

**5.4.0 Beta 4.1 - January 23, 2006**\
Fixed Ion crash bug: was due to \$C000 intrusion from IonPutSpriteMask
routine. Cf. bug ID#7.

------------------------------------------------------------------------

**5.4.0 Beta 3 - January 5, 2006**\
Final beta before 5.4

------------------------------------------------------------------------

**5.4.0 Beta 2 - December 21, 2005**\
Improved stability and folder support. Imrpoved properties. Standard
Edition abandoned.

------------------------------------------------------------------------

**5.3.2 - November 20, 2005**\
Doors CS 5.3.2 completed: partial folder support

------------------------------------------------------------------------

**5.1.Beta 1.1 - June 29, 2005**\
Doors CS Ion exec bug fixed

------------------------------------------------------------------------

**5.1.0 Beta 1 - June 22, 2005**\
Stack recover inconsistency discovered

## Doors CS 5 {#doors_cs_5}

### Gold Release {#gold_release}

**5.0.0 - June 15, 2005**\
Doors CS 5.0 released at long last! All features working to my knowledge

       Final 5.0 release delayed for archive bugfix    
       Final list of 13 bugs compiled and begun    
           Hotkeys added: [-]/[+], [2nd]/[ALPHA], [CLEAR]      
       Most Standard Edition bugs fixed, some Professional bugs fixed.

### Development Releases {#development_releases_3}

**4.9.3 \[+\] - March 3, 2005**\
Program running bug discovered. The routine swapping ADCS to high RAM
was inapplicable on the TI-83+. Progress resumed.

------------------------------------------------------------------------

**4.9.2 - February 4, 2005**\
ASM/ Ion/ DCS ASM bug discovered. Debugging begun, but problem is very
resilient. Several solutions tried with no luck.

------------------------------------------------------------------------

**4.9.0 \[+\] - January 20, 2005**\
SDK updated and rebuilt for new versions.

------------------------------------------------------------------------

**4.9.0 \[+\] - January 9, 2005**\
Shell Expansion support debugged.

------------------------------------------------------------------------

**4.8.3 \[+\] - January 6, 2005**\
ROM reading support added. Archive/unarchive routine created; slightly
buggy under ROM 1.15.

------------------------------------------------------------------------

**4.8.0 \[+\] - January 3, 2005**\
Logos created; Professional and Standard Editions initiated. To-do list
for all versions created. ASM support fixed temporarily.

------------------------------------------------------------------------

**4.7.0 \[+\] - December 21, 2004**\
More progress after one-month interim. Further file support, some
83-only features removed, scrollbar bug fix from 83 version ported.

------------------------------------------------------------------------

**4.6.0 \[+\] - October 2, 2004**\
Some icons now displayed on Doors CS desktop. Some Ion support added.
Archived programs recognized but not yet correctly handled.

------------------------------------------------------------------------

**4.5.5 \[+\] - September 28, 2004**\
TI-83+ version porting begun. \[+\] indicates progress on this version
while TI-83 edition is on hold.

------------------------------------------------------------------------

**4.9.9 - September 14, 2004**\
This is the final 5.0 release just about ready, but I have held it until
I can finish the TI-83+ version.

------------------------------------------------------------------------

**4.9.0 - July 26, 2004**\
Mouse routines debounced, ALE support completely debugged. Program
editing tools still absent.

------------------------------------------------------------------------

**4.8.0 - May 19, 2004**\
Program tools temporarily removed to save space while they are being
debugged; most features have been debugged.

------------------------------------------------------------------------

**4.7.0 - May 12, 2004** Shell Expansion support added, ALEs now
working, program locking/unlocking fixed. Scrolling desktop bug still
present. Some Ion/DCS ASM program instability when DCS is in lower RAM.

------------------------------------------------------------------------

**4.6.5 - May 6, 2004**\
RAM Recovery feature now added, ADCS automatically moved to high mem.
BASIC parser error at quit not yet fixed.

------------------------------------------------------------------------

**4.6.0 - May 4, 2004**\
Most file support debugged. Current support for: BASIC, AShell, DCS ASM
progs, DCS BASIC progs, SOS libless progs, SOS libbed progs, Ion
programs. TI-OS ASM programs not yet supported. Battery meter works,
memory meter not yet functional.

------------------------------------------------------------------------

**4.4.0 - April 20, 2004**\
Some desktop features ported, contrast meter complete.

------------------------------------------------------------------------

**4.3.0 - April 1, 2004**\
Framework of full ASM version begun.

------------------------------------------------------------------------

**4.2.0 - March 20, 2004**\
Ion support partially built into the shell.

------------------------------------------------------------------------

**4.1.0 - February 1, 2004**\
TI-83+ version finished, now works completely.

------------------------------------------------------------------------

**4.0.1 - January 9, 2004**\
Copywrite issues fixed, readme updated.

## Doors CS 4 {#doors_cs_4}

### Gold Release {#gold_release_1}

**4.0.0 - January 8, 2004**\
The first bug-free stable ASM release to the public. This version
features everything mentioned in this readme file, optimized for the
smallest possible size. Shutdown has been fixed, and the readme has been
updated and completed.

### Development Releases {#development_releases_4}

**3.9.5 Beta December 4, 2003**\
\[BTE\] This version has almost everything fixed ASM running has been
corrected, erase routines are fully optimized, and icon routines have
been corrected.

------------------------------------------------------------------------

**3.9.0 Beta - November 1, 2003**\
Almost complete, this version is mostly stable, most bugs have been
eliminated, and all ASM features work fairly well.

------------------------------------------------------------------------

**3.8.0 Beta - September 1, 2003**\
This version contains the ASM features, some of which are unstable and
occassionally crash. Optimization is good and almost all features are
included.

------------------------------------------------------------------------

**3.7.8 Beta - August 1, 2003**\
First assembly release of Doors CS in assembly at ticalc.org.

------------------------------------------------------------------------

**3.7.0 Beta - July 1, 2003**\
This version contains most of the ASM features, many of which can
occassionally become unstable and crash or behave unexpectedly. The
BASIC portion of the program is integrated well with the ASM portion.

------------------------------------------------------------------------

**3.6.1 Beta - May 1, 2003**\
Several minor problems fixed, two redundant icon files deleted, memcheck
and freeRAM fixed.

------------------------------------------------------------------------

**3.6.0 Beta - April 1, 2003**\
This is the first version to include ASM coding. All features are beta
and often become unstable. Many bugs still need to be ironed out.
Integration is fair/poor.

------------------------------------------------------------------------

**3.5.5 Beta - March 1, 2003**\
Preliminary ASM source released. Contains many bugs and coding errors,
but includes all necessary routines.

------------------------------------------------------------------------

**3.5.0 Beta - January 20, 2003**\
This is the first published version, a prototype made completely in
BASIC. It demonstrates many of the features to be included in the ASM
versions and offers a framework for the ASM features to be built upon.

------------------------------------------------------------------------

**3.2.0 - December 1, 2002**\
First improved version the program has been completely redone from
scratch. new graphical windows, advanced password protection, iconned
desktop, faster installation, prototype battery and memory checking,
mouse, prototype hardware detection.

## Doors CS 3 {#doors_cs_3}

**3.0.0 - November 1, 2002**\
Second version released at ticalc.org. Now includes .83p and .8xp files.

## Doors CS 2 {#doors_cs_2}

**2.1.0 - June 1, 2002**\
Fixed routines to reset graphscreen after a program is run.

------------------------------------------------------------------------

**2.0.0 - May 1, 2002**\
First version released at TiCalc.org. Improved graphics routines, more
complex cursor, cleaner menus, less loading time.

## Doors CS 1 {#doors_cs_1}

### Gold Releases {#gold_releases}

**1.5.0 - March 1, 2002**\
Several bugs fixed. Cross-column cursor movement added.

------------------------------------------------------------------------

**1.0.0 - December 1, 2001**\
First official Doors XP version; completely in BASIC. Has graphical
desktop with up to 16 programs, Start menu with screensaver, password
editor, and shutdown. Program list is embedded.

### Development Releases {#development_releases_5}

**0.6.0 - November 1, 2001**\
Two-column form used and implemented; cursor routines optimized.

------------------------------------------------------------------------

**0.5.0 - October 27, 2001**\
Installation screens and processes added.

------------------------------------------------------------------------

**0.4.0 - October 20, 2001**\
Password editor added to program.

------------------------------------------------------------------------

**0.3.0 - October 15, 2001**\
Password protection added to Doors XP.

------------------------------------------------------------------------

**0.2.0 - September 30, 2001**\
Transferred to the graphscreen.

------------------------------------------------------------------------

**0.1.0 - September 1, 2001** Simple menued list of programs named Doors
XP; all BASIC.
