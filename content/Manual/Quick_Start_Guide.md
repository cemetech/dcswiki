---
revisions:
- author: KermMartian
  timestamp: '2012-01-14T04:56:49Z'
title: Quick Start Guide
weight: 1
---

Welcome to the Doors CS Quick Start Guide, intended to get you up and
running with Doors CS as quickly as possible. Once you have finished
reading through this guide, we recommend that you return at some point
to read the full user manual to help you better utilize the power of
Doors CS. Without further ado, let's begin installation of Doors CS.

## Pre-Installation {#pre_installation}

Before you can install Doors CS, you must first unzip the file you
downloaded. If your computer is running Microsoft Windows XP, you can
simply double click this file and select the "Extract All" tab on the
side of the screen. Once you have done this, proceed to the "Transfer"
Section.

If you are not running Microsoft Windows XP, Windows Vista, or Windows 7
(or newer), you may need to use software such as
[7-zip](http://www.7-zip.org/) to unzip the files. Follow the
instructions included with your program to extract the files, then
proceed to the transfer step.

## Transfer

Once you have unzipped Doors CS, you can begin the transfer.

First, open your transfer program. On Windows or Macintosh machines, you
will probably do best with TIConnect software, available from [Texas
Instruments](http://education.ti.com) or on the CD that came with your
calculator. For Linux based computers, a number of utilities exist and
can be found at [Ticalc.org](http://www.ticalc.org).

**TI-Connect Instructions**

1.  Run TIConnect from your start menu.
2.  Select the "Device Explorer" button.
3.  Select the proper device from the list (Calculator should be plugged
    in)
4.  Open the folder you extracted Doors CS to.
5.  Inside this folder, select the appropriate version of DCS, based on
    your calculator model.
6.  Drag and Drop the file dcs6.8xk from the folder to the TIConnect
    Device Explorer.
7.  Follow the TIConnect prompts to send the file to your calculator.

**or**

1.  Find the appropiate file inside the ZIP file.
2.  Copy it to your desktop
3.  Attach calculator and turn it on
4.  Right click on file and select Send to TI
5.  Select appropiate connection
6.  Click on Send to Device to begin transfer

## First Use {#first_use}

Sending the file to your calculator installs it; no further installation
is necessary. To run Doors CS at any time, press the blue Apps key(83+)
or purple Apps key(84+) on your calculator, and either use the arrows to
select DoorsCS7 and press \[ENTER\] or press the number corresponding to
the DoorsCS7 entry in the menu. The first time that you run Doors CS, it
will invisibly install an AppVar that contains default settings. You can
edit these settings via the submenus of the DCS Menu in the lower left
of the Doors CS desktop.

## Usage Overview {#usage_overview}

### Keys

To navigate Doors CS, use the mouse cursor, controlled by the four arrow
keys on your calculator. To normal or "left" click, press the \[2nd\],
\[ENTER\] or \[TRACE\] key. To secondary or "right" click, press the
\[ALPHA\] or \[GRAPH\] key. You can turn your calculator off from Doors
CS by pressing the \[ON\] key; press it again to turn the device back
on. \[CLEAR\] quits immediately back to the TI-OS homescreen from the
Doors CS desktop.

See [Hotkeys]({{< ref "Hotkeys.md" >}}) for a comprehensive list of keys that
can be used to control the shell.

### Areas of the Screen {#areas_of_the_screen}

In the main area of the desktop are up to six icons representing
programs or files on your calculator, accompanied by filenames of up to
eight characters. You can left-click on any compatible file to run it.
If Doors CS cannot run a file for some reason, whether due to
incompatibility or corruption, it will alert you to this fact. When you
finish using a program, it should return you to the Doors CS desktop.
You can also right-click on files to edit their options or create
folders. At the right side of the screen is the scrollbar. You can
expedite scrolling by using the \[-\] and \[+\] keys to go one page up
and one page down respectively. You can also click on the bar itself to
scroll down one page, or click the arrows at each end of the bar. The
lower right of the screen contains, from left to right, the free RAM
meter, the battery power meter, and the close button. The close button
is equivalent to pressing \[CLEAR\]. The bottom left of the screen
contains the icon seen in the top left of this page, the Doors CS Menu,
usually abbreviated as the DCS Menu. This provides access to options and
advanced features of Doors CS. Some provide redundant access to features
for those familiar with desktop computer interfaces. You can read more
about the DCS Menu on the [DCS Menu](DCS_Menu) page.

## Conclusion and Troubleshooting {#conclusion_and_troubleshooting}

You now have a working knowledge of utilizing Doors CS and its features.
You are welcome to comment, ask questions, suggest features, or just
make your voice heard on the [Cemetech
Forum](http://www.cemetech.net/forum), or send an email to
admin\[at)cemetech(dot\]net. Good luck with Doors CS!

If you are having problems, please check the
[Troubleshooting]({{< ref "Manual/Troubleshooting.md" >}}) page for
answers.
