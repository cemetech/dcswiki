---
revisions:
- author: KermMartian
  comment: /\* Bug Resolution \*/
  timestamp: '2014-05-15T06:34:50Z'
title: Credits
weight: 100
aliases:
- /Credits/
---

## Code Used {#code_used}

### Full/Partial Packages {#fullpartial_packages}

-   **Iambian** - "Iambian" Zenith, author of Celtic III, for the
    routines that form the majority of the codebase for Doors CS 7's
    Celtic / xLIB / PicArc / Omnicalc support.

### New Code {#new_code}

-   **Chipmaster** for the cursor editing routine.
-   **KrYpT** and **Jim e** for the [Sprite
    Enlarger](http://www.detachedsolutions.com/forum/m/16407/?srch=enlarge#msg_16407)
    routine.
-   **Tari** for helping design and build the alpha-sorting routine.

### Routines

-   **DWedit** for selected Mirage OS routines.
-   **Joe W** for the Ion routines and ideas for the original ASM
    swaploader.
-   **Timendus** for the line and rectangle routines that were modified
    for use with Doors CS.
-   **Ben Ryves** for the PlayTone routine from QuadPlay.
-   **ThePenguin77** for the LCD Delay tuning routine (similar to
    ALCDFix).
-   **Anakclusmos** for the PushGUIStacks and GUIFindThis routines.
-   **Calc84Maniac** and **Ben Ryves** for their ISR guidance for
    CALCnet2.2.
-   Patrick **tr1p1ea** Prendergast, author of xLIB, for his original
    xLIB routines that helped solidify the idea of Hybrid BASIC
-   Tim **geekboy** Keller for his extensive help with CALCnet and the
    BASIC CALCnet libraries.

## Beta Testing {#beta_testing}

-   Doors CS 4-6: TIFreak8x, elfprince13, AtomBlastKX, Marc Tambini, and
    all the others who have done extensive beta-testing for Doors CS.
-   Doors CS 7: qazz42, Silver Shadow, xXEpicxXXxFailXx, Anakclusmos,
    Eeems, Xeno_Cre8or, calcdude84se, Daniel "TIFreak8x" Thorneycroft,
    Brandon "BrandonW" Wilson, WhiteValkery, Jon "Jonimus" Sturm, Shaun
    "Merthsoft" McFall, Alex "comicMAN" Glanville, Thomas "elfprince13"
    Dickerson, Iambian, Kevin "DJ Omnimaga" Ouellet, Deep Thought,
    Braden Hynes, Tanner "\_player1537" Hobson and all the other people
    who have helped me to track down bugs and have suggested features
    for Doors CS 7.
-   Doors CS 7.1: ZTrumpet, Beta7, JosJuice, Albert "AlbertHRocks"
    Huang, and all the other Cemetech members and Doors CS 7 beta
    testers. Many thanks especially to Beta7 for his tireless efforts in
    helping to track down a TI-OS 2.53MP-related bug set.

## Optimization

-   pacHa

## Bug Resolution {#bug_resolution}

-   **Jim e** and **mmartin** for Doors CS 5.0 bug resolution at
    [Detached
    Solutions](http://www.detachedsolutions.com/forum/m/28519/?srch=doors#msg_28519).\
-   **Saibot84** and **FloppusMaximus** for help with hooks.
-   **Tr1p1ea** for his help with interrupts.
-   **Braden Hynes** for going above and beyond in helping me replicate
    many obstinate DCS7 bugs.
-   **Brandon W** for help with the writeback failure bug.
-   **Sonlen** for help with the rare and elusive XLib/Text bug.
