---
revisions:
- author: KermMartian
  timestamp: '2013-06-23T20:59:10Z'
title: Features
weight: 5
aliases:
- /Features/
- /AutoArchive/
- /About/Intelligent_Writeback/
---

{{< image "000378.gif" >}}
{{< image "000379.gif" >}}
{{< image "000380.gif" >}}
{{< image "000381.gif" >}}

## General Doors CS 7.0 Shell Features {#general_doors_cs_7.0_shell_features}

-   Shell and GUI for managing and running any available BASIC and ASM
    program on TI graphing calculators
-   Mouse-based interface is intuitive for modern computer users and
    calculator enthusiasts alike.
-   Rock-solid stability from months of community beta-testing.
-   Powerful built-in libraries for ASM and BASIC programmers.
-   Folder-based organization system lets you find programs quickly and
    easily.
-   Built-in **file management** tools make renaming, copying, deleting,
    locking, archiving, and hiding a breeze.
-   Built-in 8-line BASIC editor can even edit archived and locked BASIC
    programs
-   Extensive keyboard shortcuts for power users.
-   Rendundancy and backup systems restore your folder organization and
    preferences should your calculator crash.
-   Robust, powerful CALCnet2.2
    **networking protocol** built-in.
-   TI-OS integration adds features like running any program directly
    from the homescreen with
    **HomeRun**, keyboard
    shortcuts to APD or launch Doors CS, and more.
-   Wide compatibility with existing MirageOS, Ion, xLib, Celtic III,
    PicArc, Omnicalc, Doors CS 5/6, and TI-OS programs and features.
-   In-shell battery and memory meter as well as low RAM/battery
    warnings.
-   Hover over programs to see their size and description with the
    InfoPop feature.
-   **MemoryPop** lets you
    see your RAM and ROM usage in Doors CS by hovering over the memory
    meter.

{{< image "000243.gif" "File management" >}}
{{< image "homerun.gif" "HomeRun" >}}
{{< image "memorypop.gif" "MemoryPop" >}}

## Program-Related Features {#program_related_features}

-   Doors CS’ Associated Program feature lets you open files with their
    viewer, like clicking a document to open it with a word processing
    program, or a game level to open it in its game.
-   HomeRun feature can run any BASIC or ASM program, archived, locked,
    or otherwise, from the TI-OS homescreen simply by executing
    prgmNAME.
-   Instant Goto replaces the TI-OS’ slow Goto feature, and uses Doors
    CS’ 8-line BASIC editor.
-   Archived programs are kept in ROM during execution, so ASM program
    crashes will not delete the program.

### Intelligent Writeback

Intelligent Writeback routines eliminate many of the
garbage collect messages that plague other shells for the TI-83+/84+
calculators. Instead of moving programs to RAM when they are executed,
it creates a RAM copy that may then be used to modify the original if
necessary when execution is complete. This protects against programs
loss due to faulty code or RAM clears, and also saves wear and tear on
your Flash ROM and annoying Garbage Collects.

## Customization Features {#customization_features}

-   Users can modify their mouse cursor, acceleration speed, folder
    backup preferences, and many other options and settings.
-   Rename any program, including ASM programs, from within Doors CS
-   Supports custom backgrounds on the Doors CS desktop
-   Users can easily add an icon and description to any existing BASIC
    program
-   Shell Expansions provide additional functionality to Doors CS, such
    as password protection or a clock.

## Compatibility

-   Runs MirageOS ASM programs and displays their icons
-   Runs MirageOS BASIC programs; InfoPop recognizes MirageOS program
    descriptions
-   Runs nostub (TI-OS) ASM programs and Ion ASM programs
-   Has built-in BASIC libraries for BASIC programs using xLib, Celtic
    III, PicArc, Omnicalc, and DCSB Libs.

## Features for ASM Programmers {#features_for_asm_programmers}

-   Full set of Ion and MirageOS libraries to support new and existing
    programs.
-   Extensive GUI system allows stacked windows, mouse input, text
    input, forms, and more for DCS7 ASM programs in an extremely simple
    API.
-   Associated Program functionality seamlessly passes files to their
    respective viewer programs, and handles archiving/unarchiving and
    cleanup.
-   Built-in FileOpen, FileSave, and FileSaveAs GUIs make the creation
    of small, powerful AP programs possible.
-   Shared libraries called Appended Library Extensions can be used to
    provide networking, 3D graphics, and grayscale support.

## Features for BASIC Programmers {#features_for_basic_programmers}

-   Full compatibility with popular xLIB library without needing to keep
    the 16KB app on your calculator. Doors CS versions of xLIB routines
    are faster, safer, and more optimized
-   Full compatibility with popular Celtic III, PicArc, and Omnicalc
    libraries, also built-into Doors CS 7 and optimized for speed and
    stability.
-   Powerful DCSB Libs introduced in Doors CS 7 provide access to
    advanced features, the Doors CS GUI system, stack structures for the
    Ans variable, and more.
-   Simple addition of icons and descriptions to programs without
    breaking compatibility or functionality
-   Hide programs from Doors CS, or specify a set of subprograms that
    are automatically unarchived when a BASIC program is run.

### AutoArchive

If a certain section is included in the
[header]({{< ref "/SDK/" >}}) of a Doors CS BASIC
program, subprograms that the program needs to operate can be
automatically archived and unarchived by Doors CS each time that program
is run. If a program is archived, Doors CS will unarchive it to avoid
Err:Archived messages. When the BASIC program finishes executing, Doors
CS will automatically rearchive the subprograms if the parent resides in
archive. 
