---
revisions:
- author: KermMartian
  timestamp: '2010-08-24T18:14:28Z'
title: Screenshots
weight: 10
---

## Doors CS 7 {#doors_cs_7}

Doors CS 7 has been released; the following are a small selection of
screenshots from the development process.

<div style="display: grid; grid-template-columns: 1fr 1fr 1fr;">
{{< figure "newabout_real.gif" "New Doors CS 7 About screen" >}}
{{< figure "memorypop.gif" "MemoryPop to accompany InfoPop" >}}
{{< figure "garbage2.gif" "Garbage Collect override" >}}
{{< figure "000243.gif" "New properties menu" >}}
{{< figure "tabfunc7.gif" "TabFuncs2 sample usage" >}}
{{< figure "fldrestore.gif" "Folder Restore feature" >}}
{{< figure "homerun.gif" "Homerun feature" >}}
{{< figure "homerun_djtunaxe.gif" "Homerun feature" >}}
{{< figure "dcs65_axesrcident.gif" "Identifying Axe source" >}}
{{< figure "runprogvec2.gif" "Runprog Vector in action" >}}
{{< figure "scrollsave.gif" "Scroll saving across various functions" >}}
{{< figure "dcshelp2.gif" "Doors CS Help menu" >}}
</div>

## Doors CS 6 {#doors_cs_6}

Most of Doors CS 7 will retain the look and feel of Doors CS 6.

<div style="display: grid; grid-template-columns: 1fr 1fr 1fr">
{{< figure "dsc6textsingledone.gif" "GUI API" >}}
{{< figure "startmenugui2.gif" "Contrast Adjustment" >}}
{{< figure "ap_pt.gif" "AP System" >}}
{{< figure "tabfuncs.gif" "TabFuncs SE" >}}
</div>