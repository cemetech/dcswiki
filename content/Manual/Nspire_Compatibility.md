---
revisions:
- author: KermMartian
  timestamp: '2010-07-26T18:36:21Z'
title: TI-Nspire Compatibility
weight: 71
---

Although Doors CS is coded with Nspire compatibility in mind, including
a careful avoidance of the unsupported opcodes known to make TI's 84+
emulator crash, several users have reported issues with Doors CS 6.2 and
higher when used with the 84+ keypad for the Nspire. If you have an
Nspire and have tried Doors CS 6.3 beta or higher, please post your
results here

### Tested Working {#tested_working}

-   BrandonW: No Nspire-specific problems reported. Nspire OS 1.1,
    Emulator OS 2.42
-   TIFreak8x: No Nspire-specific problems reported. Nspire OS 1.3,
    Emulator OS 2.46
-   Qazz42: No Nspire-specific problems reported. Nspire OS 1.6,
    Emulator OS 2.46
-   Player1537: No Nspire-specific problems reported. Nspire OS 1.??,
    Emulator OS 2.44
-   Qazz42: \[7/26/2010\]: No Nspire-specific problems reported, Nspire
    OS 1.7, Emulator OS 2.52

### Problems Reported {#problems_reported}

-   Qazz42: Poweroff from DCS menu and desktop is permanent. Nspire OS
    1.7, Emulator OS 2.52 (?)
-   SilverShadow: Tried using the On button and it seemed to work. The
    calc turned on. However, when he pressed Clear to exit DCS after
    that, the calc shut down and didn't want to turn on again. Nspire OS
    2.0.1, Emulator OS 2.54
-   Qazz42: Tried using the On button and it seemed to work. The calc
    turned on. However, when he pressed Clear to exit DCS after that,
    the calc shut down and didn't want to turn on again. Nspire OS
    2.0.1, Emulator OS 2.54
