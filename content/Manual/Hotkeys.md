---
revisions:
- author: JosJuice
  timestamp: '2011-03-05T20:23:21Z'
title: Hotkeys
weight: 3
---

| Key                             | Function                                                                     |
|---------------------------------|------------------------------------------------------------------------------|
| *Arrows*                        | Move mouse cursor                                                            |
| \[2nd\], \[ENTER\] or \[TRACE\] | Left-click                                                                   |
| \[ALPHA\] or \[GRAPH\]          | Right-click                                                                  |
| \[CLEAR\]                       | Quit Doors CS (or close any small window)                                    |
| \[+\] and \[-\]                 | Scroll down and up on the Doors CS desktop                                   |
| \[\*\] and \[/\]                | Scroll down and up by 2, 3, or 4 pages on the Doors CS desktop               |
| \[ON\]                          | Turn calculator on or off                                                    |
| \[4\] \[5\] \[6\]               | Run the programs on the top row of the Doors CS desktop                      |
| \[1\] \[2\] \[3\]               | Run the programs on the bottom row of the Doors CS desktop                   |
| \[Y=\]                          | Open the Doors CS Menu                                                       |
| \[WINDOW\]                      | Go up to parent folder                                                       |
| \[STAT\]                        | Tab forward with TabFuncs                                                    |
| \[XTΘn\]                        | Tab backward with TabFuncs                                                   |
| Hold \[ON\], tap \[PRGM\]       | Open Doors CS (only from TI-OS, and only if “\[ON\] hooks” option enabled)   |
| Hold \[ON\], tap \[STAT\]       | Immediately APD (only from TI-OS, and only if “\[ON\] hooks” option enabled) |

