---
revisions:
- author: KermMartian
  comment: Made a few grammatical tweaks.
  timestamp: '2012-11-02T20:41:59Z'
title: Troubleshooting
weight: 90
aliases:
- /Troubleshooting/Distorted_Screen/
---

## Distorted screen

{{< image "sc_small.jpg" "A TI-84 Plus displaying a typical sort of distorted screen." >}}

Some calculators (especially newer 84 Pluses) have display drivers built into
them that behave contrary to historical expectations, which often results in
garbled display such as pictured here, or makes the contents of the screen
appear tilted or "wavy".

For some calculators
[ALCDFIX](http://www.ticalc.org/archives/files/fileinfo/366/36608.html) can be
used to reprogram the LCD driver to correct these issues. Other calculators
(typically even newer than the ones for which ALCDFIX works) have different
issues that can only be fixed by changing programs to avoid problematic
behavior: **Doors CS 7.4** includes this fix, so ensure you have the newest
version if encountering display issues like this.

## Hook Interference

Most problems with Doors CS are caused by Inequalz and similar
applications that interfere with Doors CS 7.2. It is recommended
that you avoid using Inequalz application whenever possible as a
primary troubleshooting mechanism. If you must use Inequalz, or
another application that conflicts with Doors CS, read below.

### How To Identify If Doors CS May Be Causing Your Problems {#how_to_identify_if_doors_cs_may_be_causing_your_problems}

If your calculator won't let you edit code. For example:

-   You break a program and select Goto and the menu freezes on your
    screen.
-   You go to the \[PRGM\] menu, select EDIT, then select your program
    and the screen freezes there.
    -   You can fix this temporarily by pressing \[CLEAR\] to clear your
        screen.

### How To Identify A Problem Application {#how_to_identify_a_problem_application}

You can identify *some* problem apps by running that application while
Doors CS is enabled and looking for a "CONFLICTING APPS" message. This
can be solved by quitting that particular application. However, lack of
such a message by no means proves that a particular app is not the
culprit.

This screen may also provide a list of which apps cause problems. When
using the Inequalz application it states that Omnicalc is what is
interfering. While Doors CS is not stated there, when Omnicalc is
interfered and still "half working" it interferes with Doors CS 7.2 and
causes both to work improperly.

### Solution 1: Restart {#solution_1_restart}

1.  Run Inequalz or another conflicting application.
2.  Run the application again and disable it.
3.  Turn calculator off then back on.
4.  Restart Doors CS 7.2.

### Solution 2: RAM Clear {#solution_2_ram_clear}

1.  Run Inequalz or another conflicting application.
2.  Run the application again and disable it.
3.  Archive all the programs you want to keep.
4.  Remove your calculator's batteries for 30 seconds.
5.  Reinsert batteries and turn the calculator back on.
6.  Restart Doors CS 7.2.

**NOTE:** A RAM clear will also reset everything, including plots,
variables, lists, matrixes, and the graph screen. Never attempt a RAM
clear if aren't ready to lose the data stored on your calculator.

## Further Troubleshooting Assistance {#further_troubleshooting_assistance}

If this does not fix your problem, please make a post on [the Cemetech
forum](http://cemetech.net/forum/viewtopic.php?t=7279) and explain the problem,
**exactly** what causes it if possible and someone will look for a solution.
