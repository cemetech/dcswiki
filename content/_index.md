---
bookCollapseSection: true
bookToC: false
revisions:
- author: KermMartian
  comment: Fix social, ominous news.
  timestamp: '2020-06-12T00:18:44Z'
title: Main Page
---

<div style="display: flex; flex-wrap: wrap; gap: 1em;">
<section style="flex: 1 15em;">
{{<image "000378.gif" >}}
{{<image "000381.gif" >}}

### Doors CS 7.2 for the TI-83 Plus and TI-84 Plus

Start here: **[Download Doors CS 7.2](http://www.ticalc.org/archives/files/fileinfo/430/43068.html)**,
or come [**ask questions** on the Cemetech forum](http://www.cemetech.net/forum/viewforum.php?f=9).
Need educational programs and games? Try
[**assembly programs**](http://www.cemetech.net/programs/index.php?mode=folder&amp;path=/83plus/asm/)
or 
[**TI-BASIC programs**](http://www.cemetech.net/programs/index.php?mode=folder&amp;path=/83plus/basic/)

Doors CS 7.2 is the ultimate shell and GUI
for your TI-83 Plus or TI-84 Plus graphing calculator:

* Run **any** BASIC or ASM program, including MirageOS, Ion,
  Doors CS, and nostub programs.
* Organize your programs into folders, lock/archive/hide/edit directly from Doors CS
* Use the HomeRun feature to run any program from the TI-OS homescreen, without Asm(, even if
archived
* Extremely user-friendly, with a mouse and tons of keyboard
shortcuts
* Libraries and tools for programmers, networking, and much
more.

Doors CS is the most feature-packed, stable, and complete shell for
graphing calculators. Eliminate all your apps and replace them with
Doors CS 7!
</section>
<section style="flex: 1 15em">
{{< image "dcse8_1.png" >}}

### Doors CSE 8.2 for the TI-84 Plus C Silver Edition

Start here: [**Download Doors CSE 8.2**](http://www.ticalc.org/archives/files/fileinfo/456/45670.html),
or come [**ask questions** on the Cemetech forum](http://www.cemetech.net/forum/viewforum.php?f=9).
Need educational programs and games? Try
[**assembly programs**](http://www.cemetech.net/programs/index.php?mode=folder&path=/84pcse/asm/)
and [**TI-BASIC programs**](http://www.cemetech.net/programs/index.php?mode=folder&path=/84pcse/basic/")

Doors CSE 8.2 is the ultimate shell and GUI
for your color-screen TI-84 Plus C Silver Edition graphing calculator:
 * Run **any** BASIC or ASM program, for RAM or Archive.
 * Organize your programs into folders, lock/archive/hide/edit directly
from Doors CSE
 * Use the HomeRun feature to run any program from the
TI-OS homescreen, without Asm(, even if archived
 * Extremely user-friendly, with many tools and features for programmers and
users 

Doors CSE is the most feature-packed, stable, and complete shell
for color-screen graphing calculators. Eliminate all your apps and
replace them with Doors CSE 8!
</section>
</div>

The goal of Doors CS/Doors CSE is to provide diverse support and
powerful features to this inexpensive yet ubiquitous technological tool.
Within this site, you will find all of the documentation and information
you need to use and develop for Doors CS and Doors CSE. You can research
all of the various versions of Doors CS, better understand how to use
your copy, and find all the data on the libraries and routines for
creating Doors CS-compatible BASIC and Assembly-language programs.

## Getting Started {#getting_started}

**New Doors CS user?** **[Download Doors
CS](http://www.ticalc.org/archives/files/fileinfo/430/43068.html)** or
**[download Doors
CSE](http://www.ticalc.org/archives/files/fileinfo/456/45670.html)**
right now. Start with the
**[Quick Start Guide]({{< ref "Manual/Quick_Start_Guide.md" >}})**
and read the rest of the manual at your leisure.

**Developing for Doors CS?** Doors CS comes prepackaged with the Doors
CS SDK - all you'll need to harness the full power of Doors CS.
Additionally, BASIC as well as ASM programmers should visit the online
**[SDK]({{< ref "/SDK/_index.md" >}})** and **[ask
questions](http://www.cemetech.net/forum/index.php)**.

**Want to learn more?** Check
**[Compatibility]({{< ref "/Manual/Compatibility.md" >}})**,
**[Features]({{< ref "/Manual/Features/index.md" >}})**, and
**[Credits]({{< ref "/Manual/Credits.md" >}})**.

**Curious how Doors CS looks?** Then you need some
**[Screenshots]({{< ref "/Manual/Screenshots/index.md" >}})**.

**Want to report a bug or ask a question?** Visit the [Doors CS
Forum](http://www.cemetech.net/forum/viewforum.php?f=9).

**Want to connect on social media?** Check out the Doors CS
[Facebook](https://www.facebook.com/DoorsCS) page.

<div style="display: flex">
<section>

## Recent News {#recent_news}

- Released Doors CS 7.4, fixing [issues with some newer
  calculators]({{< ref "/Manual/Troubleshooting/index.md#distorted-screen" >}}).
  *(11/17/2022)*
- Doors CE 9 [will probably never be released](https://www.cemetech.net/forum/viewtopic.php?p=282769#282769),
  especially after [removal of official support for assembly](https://www.cemetech.net/news/2020/5/950)
  on CE calculators. *(5/22/2020)*
- Announced the [Doors CE 9](https://www.cemetech.net/forum/viewtopic.php?t=12185) project. *(12/10/2015)*
- Released Doors CSE 8.2. *(5/19/2015)*
- Released Doors CSE 8.0. *(11/5/2013)*
- Released Doors CS 7.2 and the DCS 7.2 SDK. *(7/10/2013)*

\>\> See more **[Older News]({{< ref "/Internals/Older_News.md" >}})** items \>\>

## Recent Screenshots {#recent_screenshots}

{{< image "homerun.gif" "Homerun feature: program execution from the homescreen" >}}
{{< image "000241.gif" "MemoryPop" >}}
{{< image "000243.gif" "New Doors CS 7 Properties Menu" >}}

</section>
<section>

## Download

-   **TI-83+/TI-84+:**
    [Download](http://www.ticalc.org/archives/files/fileinfo/430/43068.html)
    Doors CS 7.4 *(November 17, 2022)*
-   **TI-84 Plus C Silver Edition:**
    [Download](http://www.ticalc.org/archives/files/fileinfo/456/45670.html)
    Doors CSE 8.2.0, Build 1603 *(May 19, 2015)*

</section>
</div>
