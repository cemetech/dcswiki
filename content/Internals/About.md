---
revisions:
- author: KermMartian
  timestamp: '2009-03-27T05:12:27Z'
title: About
---

Doors CS is a shell and GUI for TI-83, TI-83+, TI-83+ Silver Edition,
TI-84+, and TI-84+ Silver Edition graphing calculators. Doors CS is a
free software initiative, and no copy of Doors CS may be sold or
distributed for monetary or other compensation under the current
license. Source code is available by contacting the author, but no
alternate distributions made be made as per the the license distributed
with the source. Cemetech, Doors CS, and Kerm Martian are in no way
related or affiliated with Texas Instruments or TI. All TI trademarks
are used only as identifying marks. The author and Cemetech are not
responsible for any damage, physical, mental, emotional, or other
adverse effects upon the user's hardware or the user from this program.
