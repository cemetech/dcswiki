---
revisions:
- author: KermMartian
  timestamp: '2010-06-21T06:20:42Z'
title: "XLib/Celtic III Compatibility"
aliases:
- "/XLib/Celtic_III_Compatibility/"
---

## Known Issues {#known_issues}

-   identity(4...) \[.stringtile\] Faulty offsets?
    -   cf. more information
        [here](http://www.unitedti.org/forum/index.php?showtopic=8897)
-   identity(9...) \[.boxshift\] Not fully implemented
    -   cf. more information
        [here](http://www.unitedti.org/forum/index.php?showtopic=8897)
-   det(12...) \[.extgroup\] Causes crashes for an unknown reason
    -   cf. more information
        [here](http://www.unitedti.org/forum/index.php?showtopic=8897)
    -   Iambian believes that the problem occurs at/after GroupGetNext
        in c3_util.asm
    -   Could be because one or more filetype is not being properly
        handled - ie, when a group contains file types other than
        programs
-   det(25...) \[.errorhandle\] Does not reliably work. Don't know why.
    -   cf. more information
        [here](http://www.unitedti.org/forum/index.php?showtopic=8897)

## Resolved Issues {#resolved_issues}

-   DrawLine problems, ie with real(12), solved by removing existing
    line routine and substituting mos_fastline.
-   Issues with sprites stored in the last row of a picture
    -   cf. more information
        [here](http://www.omnimaga.org/index.php?topic=1661.msg35644#msg35644)
    -   {{< image "celtic3_real2_lastrow.gif" >}}
        -   Notice extra line from row 63 (the 64th row) for lefthand
            sprite
    -   Solved by adding extra logic to not try to read last row, as
        well as clear out unwritten buffer pieces.
    -   Final cost is about 60 bytes and a few hundred T-states when
        routine is triggered, a few T-states otherwise.
-   real(14) does not properly return the current amount of free RAM.
    -   Repaired - programmer oversight in clearing Op1 after setting it
        up.
-   Looks like real(11) also returns invalid values?
    -   Turned out to be an emulator problem
-   Problems displaying lines/shapes smaller than 8x8
    -   cf. more information
        [here](http://www.omnimaga.org/index.php?topic=1661.msg35644#msg35644)
    -   Shown to happen with 2-pixel-high rectangles of any width, but
        1-pixel-wide rectangles are fine
    -   Experimentation details about problem:
        -   Real(12,5) (empty with inverted edges) have some render
            problems when randomly rendered with size (3x3)
        -   Real(12,6) (black rectangles) are uniforming distributed
            when randomly rendered with size (3x3)
        -   Real(12,7) (white rectangles) are uniforming distributed
            when randomly rendered with size (3x3)
        -   Real(12,8) (inverted rectangles) appear to render only in
            certain columns at (3x3), cf. image below. Notice that one
            symptom is that at (3x3), the unaffected columns are grouped
            in adjacent pairs:
            -   {{< image "celtic3_real128_3x3.gif" >}}
        -   Real(12,8) (inverted rectangles) appear to render only in
            certain columns at (4x4), cf. image below. Notice that one
            symptom is that at (4x4), the unaffected columns are grouped
            in adjacent groups of three:
            -   {{< image "celtic3_real128_4x4.gif" >}}
    -   Solved by removing PicArc box-drawing routine, substituting
        combination of [fastrectangle]({{< ref "fastrectangle.md" >}})
        and [filledrectangle]({{< ref "filledrectangle.md" >}})
-   real(12...) \[.drawshape\] Both box and line not rendering
    correctly.
    -   cf. more information
        [here](http://www.unitedti.org/forum/index.php?showtopic=8897)
    -   See previous fixes
-   LOOKUPINDEX pair suffers from not stripping newline characters
    -   cf. more information
        [here](http://www.unitedti.org/forum/index.php?showtopic=8897)
    -   fix here: [UTI
        thread](http://www.unitedti.org/forum/index.php?showtopic=8897&view=findpost&p=135351)
    -   more fix info:
        [here](http://www.unitedti.org/forum/index.php?showtopic=8897&view=findpost&p=135389)
    -   Believed already fixed in 3.09s? TODO: Verify.
    -   Confirmed fixed in 3.09s by Iambian
-   DRAWTEXT command does not reset flags after exiting.
    -   cf. more information
        [here](http://www.unitedti.org/forum/index.php?showtopic=8897)
    -   Believed already fixed in 3.09s? TODO: Verify.
    -   Confirmed fixed in 3.09s by Iambian
-   Real(1) spites are sometimes offset by (-1,-1)
    -   cf. more information
        [here](http://www.omnimaga.org/index.php?topic=1661.msg35644#msg35644)
    -   {{< image "celtic3_spritemapfail.gif" >}}
    -   Finally fixed after over a day of painful debugging. Real
        problem was that XLib's tilemapper Real(2) routine does not
        write into the 64th row of the display. The 16x16 and 8x8 tile
        mapping routines were modified accordingly, including setting up
        buffer-address-based copy height reductions, buffer clearing,
        and copy mode-based buffer clearing.
-   Real(2) problems, as seen with Metroid by CDI
    -   Fix turned out to have a second part, handling Pic'0' as Pic'10'
        instead of Pic8.
-   Two byte tokens containing \$3F as second byte does not properly
    parse in any lineread command
    -   cf. more information [here at
        UTI](http://www.unitedti.org/forum/index.php?showtopic=8897&view=findpost&p=140144)
    -   Solved by inserting a \_IsA2ByteTok check to RAM and Archive
        routines
-   MATTOSTR Output is not valid
    -   Description of problem: [thread
        here](http://www.unitedti.org/forum/index.php?showtopic=8897&view=findpost&p=136556)
    -   Partial description of fix:
        [here](http://www.unitedti.org/forum/index.php?showtopic=8897&view=findpost&p=136592)
    -   Fixed with code chunk from Iambian:
        [solution](http://iambianet.pastebin.com/ju9BCuXg)

## Un-Reproducible {#un_reproducible}

-   Clipping of aligned sprites is known to have issues.
    -   Un-reproducible so far.
