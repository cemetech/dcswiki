---
revisions:
- author: KermMartian
  comment: /\* Conversion Progress \*/
  timestamp: '2010-05-21T14:36:04Z'
title: RunProg Chaining
---

## Planning Stages {#planning_stages}

### Significant Doors CS 6 AppVar Sections {#significant_doors_cs_6_appvar_sections}

-   Start+0 PrognameSpace: used to store the top element in the Archive
    stack, essentially the name of the program being run. \[9 bytes\]
-   Start+9 PrognameSpace2: used to store the second element in the
    Archive stack, the name of the first program run if it called a
    second program. \[9 bytes\]
-   Start+18 SafeArcFlags: Used to remember the page that the primary
    program is on \[2 bytes\]
-   Start+19 SafeArcFlags+1: Page of data file if an AP. \[n/a bytes\]
-   Start+20 SafeArcFlags3: Used to remember the page that the secondary
    program is on \[1 byte\]
-   Start+23 ProgNameSpace3: Used for Associated Programs \[9 bytes\]
-   Start+56 ProgNameSpace4 - Used for Associated Programs \[9 bytes\]
-   Start+65 ????

### Significant Code Sections {#significant_code_sections}

-   **runprog.asm**
    -   The bulk of the changes to make. All kinds of messes, to be
        detailed in the section below.
    -   RunProgImmediate:
        -   Clears out the first nineteen bytes, then copies 9 bytes
            from +0 to +24, then copies 9 bytes from +0 to +56 (FAIL?)
        -   Gets page from +18.
    -   RunBASICProg:
        -   Uses GetArcStatus to determine if program is archived
        -   If unarchived, skips to execution of the original, with name
            stored in +0
        -   If archived, stores page in +18, creates temporary program,
            and executes that.
    -   GetRAMName:
        -   Returns ProgNameSpace at +0 if GetArcStatus returns 0, or
            ProgNameSpace2 at +9 if it returns !=0.
    -   GetRAMNameAP:
        -   Returns ProgNameSpace3 at +23 if +19 is 0, or ProgNameSpace4
            at +56 if +19 is !=0.
    -   ArcUnarcProgB:
        -   (Un)archives program at +0 if +18 is nonzero
    -   ArcUnarcDCSBasic:
        -   Uses +18 and then either +0 or TmpProgName to find
            subprograms to unarchive (or rearchive)
    -   RunAssociatedProg:
        -   Uses ArcUnarcProgA (looking at VFAT) to store the program
            archive status and name at +18 and +0 respectively
        -   Copies file name from +0 to +23
        -   Calls initTmpASM if file is archived
        -   Moves the archive status of the file from +18 to +19
        -   Copies from +9 to +56 (9 bytes to copy), then zeroes out
            first +18 bytes
        -   Calls GetRAMNameAP
        -   Get type of current file, search for a viewer
        -   On success, run ArcUnarcProgA_Op1 to store viewer program
            arc/name in +18 and +0
    -   RunTIOSASM, RunDCSasmProg, RunDCSasmProgAP:
        -   Uses +18 and +0
        -   On execution completion, uses GetRAMName for writeback if
            necessary
    -   Hook1:
        -   Uses +19 for ???
    -   InitTmpASM
        -   This is the routine that stores a temporary name into +9, of
            the same length as the original program's name
-   **basicprg.inc**
    -   Nothing in this file specifically, but will need to deal with
        all prgms called from BASIC prgms
-   **~~editor.asm~~**
-   **apguis.asm?**
    -   In FOpen, have to deal with writing back an existing open AP
        file, then closing it (shouldn't be too bad)
    -   FOpen seems to clear out +19, +20, then +23 to +30, then +56 to
        +63.
    -   FileSave / FileSaveAs don't do anything auspicious
-   **ap.asm**
    -   APOpenGuiCopyToRAM: Uses offset +19, +23, and +56.
    -   TmpProgName and TmpProgName2?
    -   Seems to use ProgNameSpace 3 and 4.
    -   Therefore ProgNameSpace 3 and 4 would seem to involve files,
        making ProgNameSpace 1 and 2 for programs?

    :   

### Summary of Existing ProgNameSpaces {#summary_of_existing_prognamespaces}

-   For a normal ASM program, +0 contains original name, +9 maybe
    contain a temporary name, and +18 marks whether +0 or +9 is the name
    of the program in RAM
-   For an AP file, +23 and +56 have the original \[and temporary\] name
    of the file, while +0 and +9 have the viewer. +19 marks whether +23
    or +56 is the RAM name of the AP file. Not sure what SafeArcFlag3
    (+20) is for, though.
-   As far as I can tell, SafeArcFlags3 isn't actually used for anything
    anymore.

## New Chaining System {#new_chaining_system}

### AppVar "CHAIN7" {#appvar_chain7}

|       |     |
|-------|-----|
| *+0*  | S   |
| *+1*  | A1  |
| *+11* | A2  |
| *+21* | A1  |
| *+31* | A2  |

**S**: Size in number of entries in the AppVar. AppVar size is 1+20\*S.
*(1 byte)*

**A1**: Flag for program (0) or associated file (1). *(1 byte)*

**B1**: 05/06 (ProgObj/ProtProgObj). *(1 byte)*

**C1**: program name (8-byte space, zero-padded). This is the original
name if A2 is non-zero, or the original and RAM name if A2 is zero. *(8
bytes)*

**A2**: Flag for RAM or archive, indicates page of original. Zero for
RAM, nonzero for ROM. *(1 byte)*

**B2**: 05/06 (ProgObj/ProtProgObj). *(1 byte)*

**C2**: program name (8-byte space, zero-padded). This is the temporary
RAM name if A2 is non-zero, or not used if A2 is zero. *(8 bytes)*

The AppVar will contain repeating instances of these two rows as it
grows.

### Constituent Functions {#constituent_functions}

**PushProgChain**: Pushes a new blank 20-byte slot consisting of A1
through C2 above onto the end of the CHAIN7 AppVar, and expands it
accordingly with InsertMem. Increments S. If the AppVar did not already
exist, creates it with size 21 bytes (S and first 20-byte slot). Returns
with HL pointing to the A1 byte of the new entry; if the carry flag is
set, something bad happened, and HL is probably invalid.
`<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
TESTED\]`</span>`{=html}

**PopProgChain**: Removes the 20-byte slot in the highest memory in
CHAIN7. If this would remove the last slot, removes the entire CHAIN7
AppVar from memory. There is no usable return value from this function.
`<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
TESTED\]`</span>`{=html}

**GetProgChainSize**: Returns the value of S in A. If the CHAIN7 AppVar
does not exist, returns A=0.
`<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
TESTED\]`</span>`{=html}

**GetProgChainTop**: Returns HL as the lowest byte of the topmost entry.
In other words, returns HL = (begin)+1+20\*(S-1). Returns the carry flag
set if the CHAIN7 AppVar does not exist, or if (somehow) S=0. Pushes and
pops Op1 around the whole thing, so Op1 is retained, at the cost of
trashing lots of
registers.`<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
TESTED\]`</span>`{=html}

### Conversion Progress {#conversion_progress}

-   Completed four functions above in 143 bytes. Untested. *(5/16/2010)*
-   Upgraded BASIC sections, tested, debugged. *(5/16/2010)*
-   Upgraded ASM sections, writeback, AP sections, performed testing,
    found more items to upgrade. *(5/16/2010)*
-   Debugging ASM functionality, began work on AP GUIs. *(5/17/2010)*
-   Wrote and debugged routine to move gui7 appvar to highmem. Removed
    Power menu to save 280 bytes. *(5/18/2010)*
-   Worked on FileOpen/Save/SaveAs, introduced RelocatablePtr1 and 2.
    *(5/19/2010)*
-   Continued debugging FileOpen/Save/SaveAs, also solve an unrelated
    graphics bug. *(5/20/2010)*

### Significant Code Upgrades {#significant_code_upgrades}

-   **runprog.asm**
    -   RunProgImmediate:
        `<span style="color%3A+%23bb0000%3B+font-weight%3A+bold">`{=html}\[INCOMPLETE\]`</span>`{=html}
    -   RunBASICProg:
        `<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
        TESTED\]`</span>`{=html}
    -   GetRAMName:
        `<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
        TESTED\]`</span>`{=html}
    -   GetRAMNameAP:
        `<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
        TESTED\]`</span>`{=html}
    -   ArcUnarcProgB:
        `<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[DELETED,
        UNNECESSARY\]`</span>`{=html}
    -   ArcUnarcDCSBasic:
        `<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
        TESTED\]`</span>`{=html}
    -   RunAssociatedProg:
        `<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
        TESTED\]`</span>`{=html}
    -   RunTIOSASM:
        `<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
        TESTED\]`</span>`{=html}
    -   RunDCSasmProg:
        `<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
        TESTED\]`</span>`{=html}
    -   RunDCSasmProgAP:
        `<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
        TESTED\]`</span>`{=html}
    -   ASMContinueAll:
        `<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
        TESTED\]`</span>`{=html}
    -   Hook1:
        `<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
        TESTED\]`</span>`{=html}
    -   InitTmpASM:
        `<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
        TESTED\]`</span>`{=html}
-   **writeback.asm**
    -   asmcheckwriteback:
        `<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
        TESTED\]`</span>`{=html}
    -   asmcheckwriteback2:
        `<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[DELETED,
        UNNECESSARY\]`</span>`{=html}
-   **basicprg.inc**
-   **apguis.asm?**
    -   FOpen:
        `<span style="color%3A+%2378ff00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
        TESTED BUGGY\]`</span>`{=html}
    -   FileSave:
        `<span style="color%3A+%2378ff00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
        TESTED BUGGY\]`</span>`{=html}
    -   FileSaveAs:
        `<span style="color%3A+%2378ff00%3B+font-weight%3A+bold">`{=html}\[COMPLETE,
        TESTED BUGGY\]`</span>`{=html}
-   **ap.asm**
    -   APOpenGuiCopyToRAM:
        `<span style="color%3A+%2300aa00%3B+font-weight%3A+bold">`{=html}\[DELETED,
        UNNECESSARY\]`</span>`{=html}
