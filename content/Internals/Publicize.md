---
revisions:
- author: KermMartian
  comment: "Created page with \u2019== Forum Post / News Item == \xB6\n`<table border=\"\
    1\">`{=html} \xB6 `<tr>`{=html} \xB6 `<td>`{=html} \xB6 The\nculmination of over\
    \ fourteen months of hard work, Doors CS 6, a new\nshell and GUI for TI-83+/TI-84+\
    \ graphing calculato\u2026\u2019"
  timestamp: '2009-07-14T00:31:26Z'
title: Publicize
---

## Forum Post / News Item {#forum_post_news_item}

<pre>
The culmination of over fourteen months of hard work, Doors CS 6, a new
shell and GUI for TI-83+/TI-84+ graphing calculators, is now available.
Doors CS 6 brings ease-of-use, a mouse-based GUI, folders, networking,
and more to your calculator. Run almost every filetype, including
MirageOS, Ion, BASIC, ASM, and Doors CS programs. Extreme expandability
and functionality for users and developers, plus a large and devoted
support community. Check out the full feature list:

» With an integrated [b]GUI API[/b] and Associated Program system,
Doors CS programs can be [b]smaller, faster, and better-looking[/b]
than those for any other shell.
» Navigating Doors CS is easy if you’ve
ever used a computer. Forget complicated menus and obscure keyboard
shortcuts; instead use an [b]intuitive mouse cursor[/b] to move and
click on onscreen items.
» No need to juggle multiple shells such as
[b]MirageOS[/b], [b]Ion[/b], and even the [b]TI-OS[/b]
itself: Doors CS can run all their files and many more.
» Tired of slowly searching through your program list? Organize your programs and
files into [b]nestable folders[/b], then scroll quickly to the
program you want. » With [b]CALCnet2[/b], a built-in feature of
Doors CS, you can connect two or more calculators together for
[b]chatting[/b], multiplayer [b]gaming[/b], and more.
» If you’ve ever lost valuable projects to RAM Clears from bugs in other
programs, you’ll be happy to discover Doors CS’s rock-solid stability.
With [b]Intelligent Writeback[/b], your archived programs are only
updated in Archive when changes have been made to the program itself,
thus saving wear and tear on your Archive and [b]drastically reducing
Garbage Collection messages[/b].
» Doors CS 6, your way: [b]customize[/b] everything from your mouse speed and cursor to your
desktop background. You can also install small modules called SEs to
further [b]extend the features and capabilities[/b] of Doors CS.
» Never get frustrated again with a question or problem; just visit the
helpful Cemetech [b]support forum[/b] and any of over five hundred
members will be happy to assist.

[img]http://www.cemetech.net/img/icon/dl.gif[/img] [b]Download:[/b] [url=http://www.cemetech.net/scripts/countdown.php?/83plus/asm/shells/dcs6.zip&location=news]Doors CS 6.0[/url]
[img]http://www.cemetech.net/img/icon/dl.gif[/img] [b]More Info:[/b] [url=http://dcs.cemetech.net]Doors CS Wiki[/url]
[img]http://www.cemetech.net/img/icon/dl.gif[/img] [b]View:[/b] [url=http://dcs.cemetech.net/index.php?title=Screenshots]More Screenshots[/url]

[img]http://www.cemetech.net/img/dcs/dcs6/gui/sm_english.gif[/img]
[img]http://www.cemetech.net/img/dcs/dcs6/desktopfinal1.gif[/img]
[img]http://www.cemetech.net/img/dcs/dcs6/desktopfinal2.gif[/img]
[img](http://www.cemetech.net/img/dcs/dcs6/desktopfinal3.gif[/img]
[img](http://www.cemetech.net/img/dcs/dcs6/desktopfinal4.gif[/img]
[img](http://www.cemetech.net/img/dcs/dcs6/desktopfinal5.gif[/img]
</pre>

## Feature Lists {#feature_lists}

### Short Feature List (Non-technical) {#short_feature_list_non_technical}

-   With an integrated GUI API and Associated Program system, Doors CS
    programs can be smaller, faster, and better-looking than those for
    any other shell.
-   Navigating Doors CS is easy if you’ve ever used a computer. Forget
    complicated menus and obscure keyboard shortcuts; instead use an
    intuitive mouse cursor to move and click on onscreen items.
-   No need to juggle multiple shells such as MirageOS, Ion, and even
    the TI-OS itself: Doors CS can run all their files and many more.
-   Tired of slowly searching through your program list? Organize your
    programs and files into nestable folders, then scroll quickly to the
    program you want.
-   With CALCnet2, a built-in feature of Doors CS, you can connect two
    or more calculators together for chatting, multiplayer gaming, and
    more.
-   If you’ve ever lost valuable projects to RAM Clears from bugs in
    other programs, you’ll be happy to discover Doors CS’s rock-solid
    stability. With Intelligent Writeback, your archived programs are
    only updated in Archive when changes have been made to the program
    itself, thus saving wear and tear on your Archive and drastically
    reducing Garbage Collection messages.
-   Doors CS 6, your way: customize everything from your mouse speed and
    cursor to your desktop background. You can also install small
    modules called SEs to further extend the features and capabilities
    of Doors CS.
-   Never get frustrated again with a question or problem; just visit
    the helpful Cemetech support forum and any of over five hundred
    members will be happy to assist.

### Long Feature List (Exhaustive) {#long_feature_list_exhaustive}

## Descriptions

### Very Brief (ticalc.org) {#very_brief_ticalc.org}

Doors CS 6 brings ease-of-use, a mouse-based GUI, folders, networking,
and more to your calculator. Run almost every filetype, including
MirageOS, Ion, BASIC, ASM, and Doors CS programs. Extreme expandability
and functionality for users and developers, plus a large and devoted
support community.
