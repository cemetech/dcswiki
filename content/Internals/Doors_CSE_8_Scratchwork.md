---
revisions:
- author: KermMartian
  comment: /\* WONTFIX Items \*/
  timestamp: '2015-04-07T21:22:07Z'
title: Doors CSE 8 Scratchwork
---

This page contains a list of current past issues and things to be done
on Doors CSE 8. It's divided up into to-do items, which are generally
features but not broken, and bugs, which are things that break or don't
work right in Doors CSE.

## Pending Items {#pending_items}

### To-Do Items {#to_do_items}

### Bugs

## Completed Items {#completed_items}

### WONTFIX Bugs {#wontfix_bugs}

-   Colon-delimited lines that include an instruction to run a program
    do not work correctly when the HomeRun feature is enabled. Because
    the TI-OS checks if programs are Archived before triggering the
    ParserHook on prgmXXX commands, the current system will remain in
    place. Lines like 1-\>A:prgmXXX:2-\>B will only execute prgmXXX; A
    and B will remain unchanged. [See exploration
    details](http://www.cemetech.net/forum/viewtopic.php?p=233661#233661).
