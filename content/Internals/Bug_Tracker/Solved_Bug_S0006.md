---
revisions:
- author: KermMartian
  timestamp: '2007-02-11T03:02:48Z'
title: Solved_Bug_S0006
---

**Calc:** 83+\
**Version:** 5.7 Beta 1\
**Severity:** 9\
**Reported by:** The Tari\
**Description:** I broke execution of a BASIC program (by pressing ON),
then exited the application, resulting in an "Error: Break" with only
Quit as an option. Pressed ENTER several times, to no avail. Was forced
to pull a battery. Executing another BASIC program after breaking
execution in one prevents this from happening.\
**First reported:** 8/18/06\
**Status:** Unconfirmed

**Update:** \[08/18/06\] Checked by Kerm, unable to replicate. Will add
fix anyway.

**Update:** \[08/19/06\] Bug fixed by resetting the ON flag after all
BASIC execution.
