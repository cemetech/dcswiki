---
revisions:
- author: KermMartian
  comment: '\[\[Active Bug A0008\]\] moved to \[\[Solved Bug S0008\]\]:

    Bug solved.'
  timestamp: '2006-10-15T14:08:45Z'
title: Solved_Bug_S0008
---

I was playing joltima with it unarchived. after I quit I went to play
Firetrack2 (which was archived) and got a TI-OS memory error which then
quit to the homescreen where the DCS 506 error screen showed up in the
background. I pressed clear, and a bunch of garbage showed up on the
screen. I went to the memory menu to reset and I could see the menu, but
the homescreen cursor was still blinking in the background and I
couldn't do anything other than toggle alpha and 2nd. when I turned the
calculator off and on I got some garbage on the homescreen but after I
pressed clear it was fine again.

**Update:** I believe this may be due to stack and interrupt problems. I
also think it's related to the 506 error you got with Gemini.

**Solution:** Memory checking added for the intelligent
forward-writeback routines. Fixed.
