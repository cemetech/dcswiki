---
revisions:
- author: KermMartian
  comment: '\[\[Bug A0002\]\] moved to \[\[Inactive Bug I0002\]\]:

    Switched to inactive status'
  timestamp: '2006-08-14T12:40:24Z'
title: Inactive_Bug_I0002
---

**Calc:** TI-84+SE\
**Version:** 5.6 Beta 2\
**Severity:** 10\
**Reported by:** Netham45\
**Description:** Bringing up the menu(Pressing \[Alpha\]) then pressing
the X in the lower left corner causes the calculator to lock up and
having to pull out the batteries to resume operation.\
**First Reported:** 08/11/06\
**Status:** Inactive, assumed solved.\
**Resolved:** 08/13/06
