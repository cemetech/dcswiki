---
revisions:
- author: KermMartian
  timestamp: '2006-10-19T13:18:33Z'
title: Solved_Bug_S0009
---

1\. Calculator model: 83, 83+, or 84+. Regular or silver edition

84+SE

2\. Version: Which version of Doors CS you were using for the test 5.7b1
I believe.

3\. Severity: How severe you estimate the problem to be. A RAM clear in
a common function would be 10, whereas a misspelling in an error message
would be a 1.

8

4\. Description: What programs did you have on your calc that might have
caused the crash? What were you doing immediately before the crash? What
are the syptoms? What do you estimate the problem to be, if applicable?

Running an empty BASIC program causes reboot and RAM Clear. No other
programs on calc at time, no folders or custom settings. Tried multiple
times, worked everytime.

**Update by Kerm:** this has been repaired in 5.8 Beta 1.
