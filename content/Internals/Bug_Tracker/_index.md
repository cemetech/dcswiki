---
bookCollapseSection: true
revisions:
- author: Netham45
  timestamp: '2006-08-11T18:01:14Z'
title: Bug Tracker
---

All current pending bugs begin with A followed by a number. Please
sequentially number bugs. Solved bugs are moved to the Solved Bugs
category.
