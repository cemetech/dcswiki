---
revisions:
- author: KermMartian
  timestamp: '2006-08-18T11:22:06Z'
title: Solved_Bug_S0004
---

**Calc:** TI-84+SE\
**Version:** 5.7 Beta 1\
**Severity:** 3\
**Reported by:** Netham45\
**Description:** Inside the options menu, on an 84, there are messed up
boxes for enableCALCnet2 and gCn, also there is a messed up box for
Allow background. Problem is not on my 83+, only on my 84+SE.\
**First Reported:** 08/15/06\
**Status:** Theoretical solution, implementation in progress.\
**Update:** \[08/15/06\] I have determined that this bug is *directly*
linked to
[Active_Bug_A0003]({{< ref "solved_bug_s0003.md" >}}), both
stemming from improper appvar size settings. This bug has been
replicated.

**Update:** \[08/18/06\] Solved along with
[Solved_Bug_S0003]({{< ref "Solved_Bug_S0003.md" >}}).
