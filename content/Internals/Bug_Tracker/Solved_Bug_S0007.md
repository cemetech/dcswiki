---
revisions:
- author: KermMartian
  comment: \[\[Active Bug A0007\]\] moved to \[\[Solved Bug S0007\]\]
  timestamp: '2007-04-10T20:20:49Z'
title: Solved_Bug_S0007
---

Calc: TI-84+SE

Version: 5.7 Beta 2

Severity: 2

Reported by: Netham45

Description: Having a BASIC program that uses a standard DCS Header when
either varible D, C, or S are undefined causes program crash.

First Reported: 09/19/06

Status: Maybe if DCS wrote to Varibles D, C, and S before executing a
BASIC program, it would work.

**Update by Kerm:** A-F are also an issue. I'm considering just
replacing the header with 0's, which would be the easiest option at this
point.

**Update by Netham45:** Wouldn't making it ignore the :DCS at the
begining destroy compatibally with older programs? Why not just make
DoorsCS just write 1 to the varibles before execution of the BASIC
programs.

**Update by Kerm:** various solutions are under consideration on the
Cemetech forum.

**Update by Kerm:** This bug has been resolved.
