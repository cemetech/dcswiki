---
revisions:
- author: Alex10819
  timestamp: '2006-08-18T19:38:03Z'
title: Solved_Bug_S0003
---

**Calc:** TI-84+\
**Version:** 5.6 Internal Beta 5\
**Severity:** 6\
**Reported by:** Kerm Martian\
**Description:** Executing BASIC programs causes the custom cursor to
get reset - AppVar problem?\
**First Reported:** 08/14/06\
**Status:** Active, under investigation\
**Resolved:** n/a

**Update:** (08/15/06) Discovered that size of appvar is still being set
to \$0021 instead of the proper \$0040. Need to find where this
incorrect size is being set.

**Update:** \[08/18/06\] I feel dumb. Didn't delete the appvar from the
previous version.

**Update:** \[8/18/06\] Kerm Feels Dumb. World Ending.
