---
revisions:
- author: Sorunome
  comment: /\* Basic example \*/
  timestamp: '2014-09-25T20:05:44Z'
title: Direct USB to gcnclient connection
---

This is a documention on the protocol from direct USB to the gcnclient
on the computer as it is a bit different than the normal gCn client
(documented by Sorunome).

I have only tried b-messages, I guess f-messages would be pretty
similar, I have also only tried it over the DCSB basic libraries, and it
seems to me that basic libraries add 3 bytes for some reason.

Usually a gCn message has a header, and in the case of b or f messages 2
bytes \\xFF and \\x89 right after the header for arduino handling. Those
5 bytes are omitted when sending. In addition is the last byte, the
\\x2A or '\*' also been omitted. Now the message looks like this:

`<5 bytes recipient id><5 bytes sending id><message payload 2 byte little endian>``<message>`{=html}

For example we send a b-message from the id ffffffffff containing 'Hello
World'

`\x00\x00\x00\x00\x00\xFF\xFF\xFF\xFF\xFF\x0B\x00Hello World`

Now, if you'll try to use it with the BASIC libs (i think it is only the
basic libs) you'll find that it won't work, because of the weird
encoding i found: the message needs to be enhanced by three bytes: the
byte \\x04 and again it's length encoded as little endian. so now
sending Hello World would look like this:

`\x00\x00\x00\x00\x00\xFF\xFF\xFF\xFF\xFF\x0E\x00\x04\x0B\x00Hello World`

## Python Example {#python_example}

After setting up an active connection to the calculator using libusb
this function will create a string that needs to be sent to the
calculator (using a b-message). Note that this creates the string, it
doesn't send it.

    def getMessage(message):
            message = struct.pack("<H",len(message))+message #we add the DCSB payload
            message = "\x04"+message #we add the misterious DCSB byte
            message="\x00\x00\x00\x00\x00\xFF\xFF\xFF\xFF\xFF"+struct.pack("<H",len(message))+message #we set the recipiant id to 00 because it is a b-message and the sender id to ff because it isn't a real calc, then we add the new payload and the message
            print "Created Message "+":".join("{0:x}".format(ord(c)) for c in message) #debug output as hexadecimal
            return message

## Basic example {#basic_example}

This program sends 'PING' to the server and then wait for a reply,
storing it in Str9. It won't exit until \[clear\] is been hit, if there
is a reply it'll output at least once 1. After exiting the program with
\[clear\] Str9 is the content of the latest recieved message of the
server.

    :9999999999→ϴ
    :0→C
    :"→Str9
    :sum(19,1 //set up gCn
    :sum(17,"PING //send to server
    :Repeat C=45 //wait until clear is been pressed
    :getKey→C
    :sum(20,1 //check if we need to recieve something
    :Disp Ans //debug
    :If Ans //if we need to recieve something
    :sum(18 //we recieve, data is been stored in Str9
    :End
    :sum(19,0 //close gCn
