---
revisions:
- author: KermMartian
  comment: "Created page with \u201C==Doors CS 6== Doors CS 6.0 introduced an\nEaster\
    \ egg into the Doors CS shell. When triggered, it would scroll\nthe names of individuals\
    \ who provided programming help, testing, \u2026\u201D"
  timestamp: '2014-05-15T06:24:46Z'
title: Easter Egg
---

## Doors CS 6 {#doors_cs_6}

Doors CS 6.0 introduced an Easter egg into the Doors CS shell. When
triggered, it would scroll the names of individuals who provided
programming help, testing, and moral support for the development of the
shell. In homage to Windows 95 and its similar (and famous) credits
screen, it played an amelodic series of tones at the same time. The
random pixels displayed are a bit-to-pixel mapping of the machine code
making up a vital part of the shell. The steps to trigger the Easter egg
were as follow:

1.  Hold down the \[2\] key while launching Doors CS. This is made much
    easier if Doors CS is the first App after Finance in the App menu.
2.  On the following fake crash screen (see below), press \[CLEAR\] to
    "crash",
3.  Enjoy the credits. Press \[2nd\] to escape to the desktop.

{{< image "easteregg61start.gif" >}}
{{< image "easteregg61.gif" >}}

## Doors CS 7 {#doors_cs_7}

By the time Doors CS 7 was released, many had figured out the secret to
the Doors CS 6 Easter egg, so a new twist was added. The fake crash
screen would only appear if a program named prgmQUAD with any contents
existed on the calculator. Otherwise, holding \[2\] while starting Doors
CS would have no effect. The list of credited individuals was also
shrunken, and partially replaced with a link to the
[Credits]({{< ref "Credits.md" >}}) page on this wiki.

{{< image "easteregg70.gif" >}}

The easter egg was finally completely removed for Doors CS 7.3 to make
room for additional features and bug fixes.
