---
revisions:
- author: KermMartian
  timestamp: '2016-01-13T23:19:28Z'
title: Older News
---

The following items are progress, feature, and bug-fix items from Doors
CS development. The oldest items are nearest the bottom of the page.

## Doors CE 9.x Development {#doors_ce_9.x_development}

## Doors CSE 8.x Development {#doors_cse_8.x_development}

## Doors CS 7.x Development {#doors_cs_7.x_development}

- Updated version numbers to 7.2 and copyright to 2013. *(6/23/2013)*
- Fixed prgmseq( HomeRun hook bug. *(6/23/2013)*
- Released Doors CS 7.2 Beta 3. *(6/13/2013)*
- Compressed header with geekboy's help. *(6/12/2013)*
- Repaired Cn2.2 behavior with ON key interrupt. *(6/2/2013)*
- Improved dUSB Cn2.2 stability. *(4/26/2013)*
- Initial Cn2 BASIC routines added by geekboy. *(4/13/2013)*
- Added pre-Cn2 interrupt hook. *(12/10/2012)*
-   Added skeleton for Cn2 BASIC library routines. *(11/9/2012)*
-   700 bytes trimmed from c3_std by geekboy. *(11/9/2012)*
-   MediaWiki version upgraded and PublishToSAX extension created.
    *(5/16/2012)*
-   Changed header for Celtic III index files to appear as hidden, not
    MirageOS, programs to DCS. *(5/1/2012)*
-   Fixed 2.53MP/2.55MP problem with [C3
    lib]({{< ref "/SDK/Mono/BASIC/#celtic" >}})
    [IndexFile]({{< ref "/SDK/Mono/BASIC/Celtic/IndexFile.md" >}}). *(5/1/2012)*
-   Fixed Yoffset=0 problem discovered by JoeYoung in [C3
    lib]({{< ref "/SDK/Mono/BASIC/#celtic" >}})
    [StringTile]({{< ref "/SDK/Mono/BASIC/PicArc/StringTile.md" >}}). *(4/27/2012)*
-   Solved CALCnet 2.2 poweroff issue by delegating poweroff to
    programs. *(4/6/2012)*
-   Fixed text input keys to make more characters available.
    *(4/6/2012)*
-   Repaired hook chaining, found optimization; total 1 byte less on
    Page 0. *(4/6/2012)*
-   Made fBinRead for [BinRead]({{< ref "/SDK/Mono/BASIC/Celtic/BinRead.md" >}}) work
    on RAM and Archive programs/appvars. *(4/6/2012)*
-   Optimized one byte and 10 cycles (20% speedup) out of cphlde.
    *(9/17/2011)*
-   Released Doors CS 7.2 beta 2, with greatly-improved DUSB gCn and
    other fixes. *(6/13/2011)*
-   Repaired Error 504 + HomeRun problem. *(6/13/2011)*
-   Improved DUSB gCn timeout handling. *(6/12/2011)*
-   Released Doors CS 7.2 beta 1, with an early DUSB gCn version.
    *(3/22/2011)*
-   Released Doors CS 7.1.1, with Cn2.2 checksum fix. *(1/1/2011)*
-   Released Doors CS 7.1. *(12/14/2010)*
-   Instituted real fix for hourglass "bug". Performed an hour of
    optimization. Free=135/14/262. *(12/14/2010)*
-   Fixed re-opening archived AP files moving files from folders back to
    main. Free=132/20/262. *(12/13/2010)*
-   Solved mis-counting in APGUIs
    [FileOpen]({{< ref "/SDK/Mono/Asm/DCS/APs/FileOpen.md" >}}) and
    [FileSave]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSave.md" >}}) for number of files
    to display. *(12/13/2010)*
-   Tracked down *:Input* crashing bug to three Mathprint-specific
    2.53MP flags, testing workaround. *(12/13/2010)*
-   Implemented sprite dither on cut at a cost of 49 bytes on Page 0.
    *(12/11/2010)*
-   Optimized GUI stack manipulations for the AP GUIs on Page 1 to save
    a few dozen bytes. *(12/10/2010)*
-   Added \[+\]/\[-\] as scrolling shortcuts in
    [FileOpen]({{< ref "/SDK/Mono/Asm/DCS/APs/FileOpen.md" >}}) and
    [FileSaveAs]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSaveAs.md" >}}) GUIs. Cost=45
    bytes on Page 1. *(12/9/2010)*
-   Fixed CALCnet2.2 speed issue on 15MHz calcs. Lost 4 bytes on Page 0.
    *(12/7/2010)*
-   Page 1 at 12 free bytes; optimizations performed. Page
    0/1/2=191/56/262 bytes free. *(12/6/2010)*
-   Band-aid placed on the create-folder, open-properties-menu issue.
    *(12/6/2010)*
-   Adjusted hotspots on Properties menu by three pixels. *(12/6/2010)*
-   Disallowed hiding of programs with names starting in non-uppercase
    letters. *(12/5/2010)*
-   Merged emulator/non-emulator DCS 7.1 versions. *(12/5/2010)*
-   Resolved problems with 3-argument non-DCSB Lib sum() calls.
    *(12/4/2010)*
-   Doors CS 7.1 Beta 1 released. *(11/8/2010)*
-   Made rudimentary optimizations to offset new features.
    \[241,27,296\] *(11/8/2010)*
-   Added \[ENTER\] as alternative left-click, in addition to \[2nd\]
    and \[TRACE\]. *(11/8/2010)*
-   Completed eliminating Cn2.2 hang bugs thanks for Calc84maniac and
    Ben Ryves. *(11/8/2010)*
-   Resolved xLIB/Celtic bug where some TI-OS drawing routines would
    incorrectly clear the LCD. *(11/8/2010)*
-   Worked on eliminating CALCnet2.2 issues on real hardware.
    *(11/7/2010)*
-   Added and documented
    [RecallPic]({{< ref "/SDK/Mono/BASIC/XLib/RecallPic.md" >}}) full-size (64-row)
    feature. *(11/6/2010)*
-   Ameliorated problem with canceling copy/rename causing archived
    programs to be left unarchived. *(8/30/2010)*
-   One-pixel offset in DCS Menu click hotspots repaired. *(8/30/2010)*
-   Minor bug in CIII function LineRead for archived files found and
    fixed. *(8/30/2010)*
-   Small bug in DCSB Libs subroutine for GUIRButtonImg found and fixed.
    *(8/29/2010)*

## Doors CS 7 Development {#doors_cs_7_development}

-   Doors CS 7.0 [featured on
    ticalc.org](http://www.ticalc.org/archives/news/articles/14/146/146642.html).
    *(8/27/2010)*
-   Doors CS 7.0 released. *(8/24/2010)*
-   ON-break from BASIC Menu() crash found and repaired. *(8/24/2010)*
-   Folder paste glitch found and repaired. *(8/24/2010)*
-   Edit-locked folders can now be renamed properly. Edit-locked folders
    may no longer be archived. *(8/22/2010)*
-   Properly initialized Current Screen byte of Appvar. *(8/20/2010)*
-   Updated Easter Egg with more complicated steps to find it, fixed a
    typo. *(8/20/2010)*
-   In-shell help file and manual updated to note multi-page scrolling.
    *(8/20/2010)*
-   Multi-page scrolling added. *(8/20/2010)*
-   DCSB Libs scrollbar crash issue discovered and fixed. *(8/18/2010)*
-   TabFunc + clicking on blank space no longer leaves artifact.
    *(8/18/2010)*
-   Myriad bug fixes and tweaks. *(8/17/2010)*
-   Myriad bug fixes and tweaks. *(8/16/2010)*
-   Clicking a blank space after deleting a program no longer causes an
    infinite hourglass. *(8/15/2010)*
-   Fixed rare issue where Launch DCS on Startup checkbox would
    malfunction and check other boxes. *(8/15/2010)*
-   Fixed two culprits for the TI-OS interrupt being left enabled at the
    DCS desktop. *(8/15/2010)*
-   Completed Doors CS 7 Manual. *(8/13/2010)*
-   Found and fixed bug that would occasionally cause DCS to miscount
    programs in a folder, thanks to dcsSquish. Free=\[719,499,407\].
    *(8/13/2010)*
-   Found and fixed bug where pressing 1-6 to enter a sub-sub-folder
    would cause the properties menu to appear. *(8/11/2010)*
-   Repaired Goto bug that occasionally went one line too far.
    *(8/10/2010)*
-   Residual problem with SE caching and InfoPops resolved. *(8/9/2010)*
-   After extensive searching and re-writing and debugging, reconfigured
    how programs are counted, and fixed several corner cases that broke
    proper type detection. Proper hiding of programs for future versions
    of DCS also implemented. *(8/9/2010)*
-   Fixed
    [FileSaveAs]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSaveAs.md" >}})/[FileOpen]({{< ref "/SDK/Mono/Asm/DCS/APs/FileOpen.md" >}})
    bug from FileOSExclude addition. *(8/6/2010)*
-   Improved character width calculation fallback for "unknown"
    characters. *(8/6/2010)*
-   Optimized 48 bytes from GUIRScroll\* routines by pulling out common
    section. *(8/1/2010)*
-   Expanded [DCSB_Libs]({{< ref "/SDK/Mono/BASIC/DCS/" >}}) AnsStack functions
    to allow up to 10 individual stacks. *(7/31/2010)*
-   Modified cursor editor to use \[2nd\]\[alpha\]\[del\] to set pixels,
    saving 50 bytes on page 1. *(7/31/2010)*
-   "Help" DCS Menu updated with neater display, as per critiques on
    Cemetech. *(7/31/2010)*
-   "Help" DCS Menu item completed. To-Do list now empty pending any new
    requests. *(7/31/2010)*
-   AnsStack now cleaned up after BASIC execution completes.
    *(7/31/2010)*
-   Solved crash on pushing/popping zero-length strings with AnsStack.
    *(7/30/2010)*
-   Debugged and repaired several non-fatal BASIC execution convenience
    issues. *(7/30/2010)*
-   Repaired corner case with BASIC InstantGoto and expr().
    *(7/30/2010)*
-   Restricted keys in BASIC error Quit/Goto menu. *(7/30/2010)*
-   Repaired crash on BASIC programs with half of DCS header, traced to
    BASIC AutoUpgrade feature with no size check. *(7/28/2010)*
-   Repaired residual hourglass in limited circumstances on desktop due
    to FldBackup feature. *(7/27/2010)*
-   Removed used *PlayTone* routine routine, saved 166 bytes on Page 0.
    ''(7/27/2010)'

*\* Repaired [GUIRWrappedText]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRWrappedText.md" >}}) to
allow \$D6,\$XX sequence to represent newline.*(7/26/2010)''

-   Poweroff (APD and manual) from DCS desktop now works on Nspire
    devices with OS \>= 1.7. *(7/26/2010)*
-   Better [iFastCopy]({{< ref "/SDK/Mono/Asm/Ion/iFastCopy.md" >}}) routine for
    Nspire and PresentationLink devices completed. *(7/25/2010)*
-   15MHz-capable calculators now render the GUI and TextInr refreshes
    in 15MHz mode. *(7/25/2010)*
-   Added ability to backspace in TextInr input fields. *(7/24/2010)*
-   Replaced with [ThePenguin77]({{< ref "Credits.md" >}})'s [LCD tuning
    routine](http://pastebin.com/hZGN4DSp). Free=\[593,1126,840\].
    *(7/24/2010)*
-   Implemented "Retune" button in Display dialog, providing
    ALCDFix-like functionality. Free=\[601,1039,840\]. *(7/23/2010)*
-   Verified partial fix of GUIMouseHookLoc destruction on clicks,
    repaired ScrollVert/Horiz hotspots. *(7/23/2010)*
-   Repaired returns from ArcUnarcDCSBASIC so that programs smaller than
    18 bytes work properly. *(7/23/2010)*
-   Repaired a rendering bug for
    [GUIRTextMultiline]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRTextMultiline.md" >}}) where
    three CRs were displayed as two. *(7/22/2010)*
-   Added folder and scroll location saving across program execution and
    TI-OS usage. Free=\[606, 1202, 840\]. *(7/20/2010)*
-   Trimmed in-shell Credits, moved some credits to the
    [Credits]({{< ref "Credits.md" >}}) page here. *(7/20/2010)*
-   Added three-char codes to BASIC exec errors. Cost of 166 bytes on
    Page 0. *(7/19/2010)*
-   Removed last vestiges of Link and Power items in DCS Menu, 106 bytes
    saved. Free=\[730, 1202, 840\]. *(7/18/2010)*
-   Resolved 503-\>DCS desktop with DCS BASIC headers and missing
    programs run via Homerun. *(7/17/2010)*
-   Updated [FileOpen]({{< ref "/SDK/Mono/Asm/DCS/APs/FileOpen.md" >}}) and
    [FileSaveAs]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSaveAs.md" >}}) documentation
    about return pointers when dialog canceled. *(7/17/2010)*
-   [GUIRTextMultiline]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRTextMultiline.md" >}}),
    [GUIRTextLineIn]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRTextLineIn.md" >}}), and
    [GUIRPassIn]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRPassIn.md" >}}) now use TI-OS edit
    buffers, at cost of 15 bytes on Page 1. *(7/17/2010)*
-   Scrolling wrap, from the bottom to top and vice versa, at cost of 42
    bytes on Page 0. *(7/16/2010)*
-   Found and fixed problem with right-clicking program 1 or 4 in
    folders. GUI push problem with GUIDesktop_HS_Quit. *(7/16/2010)*
-   Repaired [GUIRTextMultiline]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRTextMultiline.md" >}})
    functionality for scrolling up/left corner cases, optimized.
    Free=\[702,1145,840\]. *(7/15/2010)*
-   Added \[STO\>\] as "panic button" from
    [sum(12)]({{< ref "/SDK/Mono/BASIC/DCS/GUIMenu.md" >}}) (GUIMouse)
    [DCSB_Libs]({{< ref "/SDK/Mono/BASIC/DCS/" >}}) function. *(7/13/2010)*
-   Completed, tested, and debugged
    [sum(13)]({{< ref "/SDK/Mono/BASIC/DCS/GUIMenu.md" >}}) function of the
    [DCSB_Libs]({{< ref "/SDK/Mono/BASIC/DCS/" >}}). Free=\[702,1163,854\].
    *(7/9/2010)*
-   Make SEMouse section of
    [Shell_Expansions]({{< ref "Shell_Expansions.md" >}}) work within
    the DCS Menu and submenus. *(7/9/2010)*
-   Uploaded new version of
    [dcs7.inc]({{< ref "/SDK/" >}}).. *(7/9/2010)*
-   Repaired cursor movement triggered from SEs, problem was in
    GUIMouse_ClrMse checks. *(7/8/2010)*
-   Continued work on [DCSB_Libs]({{< ref "/SDK/Mono/BASIC/DCS/" >}}) function
    sum(13). *(7/8/2010)*
-   Repaired [GUIRWinButtons]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRWinButtons.md" >}}) click
    spacing, DCSBL clicking vagaries thereof. Free=\[705,1189,1396\].
    *(7/7/2010)*
-   Consolidated GUIMSWinbuttons, saving 98 bytes on Page 1.
    *(7/7/2010)*
-   Modified GUIMouse callbacks for scrollbars, DCSBL P1-\>P2 jump to
    properly prevent stack corruption. *(7/2/2010)*
-   Augmented functionality of real(1) and real(9) for 64-row Pic
    variables. *(7/1/2010)*
-   Repaired all [Shell Expansion]({{< ref "Shell_Expansions.md" >}})
    functionality, added one-SE speed optimizations. *(6/30/2010)*
-   Completed all components of sum(12). Free=\[725,1203,1437\].
    *(6/30/2010)*
-   Optimized string-length calculation and GUI pushing in dbfPGS
    functions. Free=\[725,1245,2227\]. *(6/28/2010)*
-   Completed sum(2), sum(7,0) through sum(7,9). Free=\[725,1245,2192\].
    *(6/28/2010)*
-   Completed sum(0), sum(1), sum(3), sum(4), sum(5), sum(6), sum(8),
    sum(9), sum(10), and sum(11). Free=\[725,1251,2801\]. *(6/25/2010)*
-   Chose sum() for DCSB Lib token. *(6/24/2010)*
-   Added option to disable acceleration. Free=\[733,1288,3446\].
    *(6/22/2010)*
-   Resolved issues with running/InfoPop of non-existent programs in
    blank desktop spaces introduced by fixes for \>127 programs.
    *(6/21/2010)*
-   Repaired potential problem with Celtic III routine findLineArc and
    very large files, with Iambian's help. *(6/21/2010)*
-   Solved issue with
    [FileSaveAs]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSaveAs.md" >}}) and the final
    state of the GUI stack, identified issue with
    [FileOpen]({{< ref "/SDK/Mono/Asm/DCS/APs/FileOpen.md" >}}) and canceling, also
    solved. *(6/21/2010)*
-   Saved 24 bytes by converting GarbageCollecting override window to
    [GUIPushStacks](GUIPushStacks)-type system. *(6/20/2010)*
-   Saved 64 bytes by creating *SMOptions_NextAVFromCheck* routine to
    match *SMOptions_NextCheckFromAV*. Free=\[709,1369,3448\]
    *(6/20/2010)*
-   Cleaned up hourglass display after editor from Homerun + BASIC exec
    error + archived program + Goto *(6/19/2010)*
-   Fixed all sections calculating remaining/onscreen programs from
    ProgsToDo and ProgsDone to correctly handle \>127 programs.
    ''(6/19/2010)'

*\* For desktop speed optimizations, rearranged
[PushGUIStacks]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/PushGUIStacks.md" >}}) to push entire chunk
in one shot.*(6/18/2010)''

-   For desktop speed optimizations, retooled
    [OpenGUIStack]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/OpenGUIStack.md" >}}) to optionally
    moved the VAT entry for the gui7
    [Appvar]({{< ref "Appvar_Format.md" >}}) to the top of the
    VAT. *(6/18/2010)*
-   Added [GUIFindThis]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/GUIFindThis.md" >}}), used to
    remove 78 bytes from start menu. Free=\[722,1438,3448\].
    *(6/17/2010)*
-   Used [PushGUIStacks]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/PushGUIStacks.md" >}}) to remove
    more space. Free=\[775,1380,3444\]. *(6/17/2010)*
-   Trimmed more space, particularly on Page 1. Free=\[554,1239,3444\].
    *(6/17/2010)*
-   Removed 603 bytes from Page 1 and 835 bytes from Page 2 by removing
    vectors2 and vectors3. Free=\[546,1053,3444\]. *(6/16/2010)*
-   Trimmed 129 bytes from Celtic III libraries. *(6/16/2010)*
-   Added and tested Runprog vector. One display bug remains.
    *(6/15/2010)*
-   Added folder backup before program execution (from desktop only).
    *(6/15/2010)*
-   Resolved many-programs bug by tracing it to CALCnet2 in DCS6.1/6.2.
    Bug repair pending reactivation of CALCnet. *(6/15/2010)*
-   Added flag check to avoid clearing graphscreen before executing
    BASIC programs run from Homerun. *(6/15/2010)*
-   Repaired \[X\] icon left on graphscreen after error in Homerun-run
    program by setting regraph flag after leaving the call to
    BASICErrorParse in runprog.asm. *(6/14/2010)*
-   Debugged, fixed, and tested workaround for 2.53MP's improper
    post-ParserHook flag/Op1 handling. *(6/10/2010)*
-   Found and prevent crash with XLib compatibility where
    Real(12,\[3-10\]) when x2\<=x1 or y2\<=y1. *(6/9/2010)*
-   Solved problem of catching Stop token with nonexistent error 127 and
    special-case silent handling within DCS. *(6/9/2010)*
-   Resolved sorting bug introduced with changes to sort hidden programs
    properly. *(6/8/2010)*
-   Avoided ridiculous failure of \_PutC to recognize Split-\>Full in
    2.53MP for DCS error messages. *(6/8/2010)*
-   Mouse cursor now properly erased when left- or right-clicking with
    TabFuncs 2. *(6/8/2010)*
-   Solved issue with \[Clear\] shortcut for Small Windows and 2.53MP.
    *(6/8/2010)*
-   Added hourglass for sorting, archiving/unarchiving, and copying back
    after ASM execution. *(6/8/2010)*
-   Fixed crash in Rename function by removing extraneous \_PopOp1
    routine. *(6/7/2010)*
-   Pressing \[Clear\] from any [Small
    Window]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRSmallWin.md" >}}) now clicks its **\[X\]**,
    if it has one. *(6/7/2010)*
-   Pressing \[Clear\] from the Properties menu quits back to the DCS
    desktop, at a cost of 23 bytes. *(6/7/2010)*
-   Fixed Instant Goto failing when scrolling backwards through two-byte
    tokens ending in \$3F such as \$AA,\$3F. *(6/7/2010)*
-   Implemented check to switch to Real(3) under special inputs to
    Real(20). Small speed improvement in some games. *(6/7/2010)*
-   Added check to edit Axe source files when executed instead of trying
    to run. *(6/6/2010)*
-   Added custom icon for Axe source files, with ID flags identical to
    nostub BASIC programs. *(6/6/2010)*
-   Removed Link dialog to save about 300 bytes, DCS menu entry still
    needs to be removed/changed. *(6/6/2010)*
-   Modified GUISCheckClick to properly handle checkboxes with values
    other than 0 or 1. Free=\[765,248,2451\]. *(6/5/2010)*
-   det(5) and related Celtic III routines using LineRead properly
    handle 2-byte tokens. Free=\[765,248,2451\]. *(6/5/2010)*
-   Repaired inconsistencies in XLib DrawShape real(12,\[0,1,2\])
    behavior emulation. *(6/4/2010)*
-   Deleting the DCS7 appvar without a RAM reset no longer creates
    duplicate folders. *(6/4/2010)*
-   Editing locked programs no longer triggers an OS-spawned Syntax
    error. *(6/4/2010)*
-   Repaired malfunctions in PropCopy and PropRename functions.
    *(6/4/2010)*
-   Spent all day finding "offset" problems in Real(1), turned out to be
    a need to replicate an XLib glitch in Real(2). *(6/3/2010)*
-   Repaired rectangle routine, improving speed and reducing size by
    using MOS libs instead. Free=\[781,253,2559\]. *(6/2/2010)*
-   XLib experience improvements, especially line routine replacement
    and last-line bug fix in real(1). Free=\[781,253,2154\].
    *(6/1/2010)*
-   Preliminary integration of CelticIII code into Doors CS. Freespace
    on Pp\[0,1,2\]=\[804,253,1989\]. *(5/31/2010)*
-   Moved save of RawKey, AppChange, and Parser Hooks to appvar during
    BASIC exec. *(5/31/2010)*
-   Expanded BASIC RawKeyHook with an AppChangeHook to disallow
    \[2nd\]\[OFF\]. *(5/30/2010)*
-   Repaired abuse of GUIMouseHookLoc, visible when using FileOpen and
    FileSaveAs. *(5/30/2010)*
-   Repaired situations with detritus left on the homescreen after BASIC
    via ParserHook. *(5/30/2010)*
-   Fixed display of mouse after InfoPop/MemoryPop. *(5/30/2010)*
-   BASIC programs pause on a non-empty homescreen only when run from
    the DCS desktop. *(5/30/2010)*
-   \[2nd\]\[quit\] now blocked from Input and Prompt in BASIC programs,
    thanks to BrandonW's help. *(5/30/2010)*
-   Repaired buggy running of Nostub ASM programs. *(5/29/2010)*
-   Repaired AP open/saveas routines to properly hide temporary RAM
    copies of AP documents. *(5/29/2010)*
-   Temp progs now have unique stack-location-based names. Fixed
    residual arc_unarc bug. *(5/29/2010)*
-   Added MouseHook to use \[F1\] to close DCS Menu or \[Clear\] to quit
    Doors CS from DCS Menu. *(5/28/2010)*
-   Disabled DCS Menu scrolldown when Cancel button in Options window is
    clicked. *(5/28/2010)*
-   Completed Garbage Collect override and notification window.
    *(5/28/2010)*
-   After a day of single-stepping through OS code, found fix for
    remaining problems with homescreen execution. *(5/27/2010)*
-   Repaired failures of DisableParserHook; some problems still remain
    with homescreen program execution. *(5/26/2010)*
-   Initial successes running ASM programs and AP files from the
    homescreen. *(5/24/2010)*
-   Repaired cleanup of RunProg chain when no handler is found for an AP
    file. *(5/23/2010)*
-   Fixed responsiveness issue with shortcut keys (\[1\] through \[6\]).
    *(5/23/2010)*
-   Fixed a minor bug where an unnecessary temporary file would be
    created and left on the DCS desktop under certain FileOpen
    conditions. *(5/22/2010)*
-   Repaired bug in canceling out of a FileOpen prompt, properly removes
    GUI items added for opening. *(5/22/2010)*
-   Fixed the Launch DCS on Poweron bug - one instance of "DoorsCS6" had
    not been changed to "DoorsCS7". *(5/22/2010)*
-   Fixed three bugs related to custom mouse cursors, including editing
    the cursor and resetting the default. *(5/21/2010)*
-   Renamed and copied files now end up in the same folder as their
    original. *(5/21/2010)*
-   Repaired renaming folders. *(5/21/2010)*
-   Repaired two bugs with folders: both hiding folders and pasting a
    folder into itself are now detected and prevented. *(5/21/2010)*
-   Found final (?) bug in FileOpen/SaveAs, Save remains untested.
    *(5/21/2010)*
-   Continued debugging FileOpen/Save/SaveAs, also solved an unrelated
    graphics bug. *(5/20/2010)*
-   Worked on FileOpen/Save/SaveAs, introduced RelocatablePtr1 and 2.
    *(5/19/2010)*
-   Wrote and debugged routine to move gui7 appvar to highmem. Removed
    Power menu to save 280 bytes. *(5/18/2010)*
-   Debugging ASM [RunProg_Chaining]({{< ref "RunProg_Chaining.md" >}})
    functionality, began work on AP GUIs. *(5/17/2010)*
-   Upgraded ASM sections, writeback, and AP sections of
    **[RunProg_Chaining]({{< ref "RunProg_Chaining.md" >}})**, performed
    testing and debugging, found and more items to upgrade.
    *(5/16/2010)*
-   Upgraded BASIC sections of RunProg, tested, and debugged as per
    **[RunProg_Chaining]({{< ref "RunProg_Chaining.md" >}})**.
    *(5/16/2010)*
-   Finished auditing existing ProgNameSpace usage, designed core
    functionality of new
    **[RunProg_Chaining]({{< ref "RunProg_Chaining.md" >}})** system,
    and implemented four base functions to manage the chain.
    *(5/16/2010)*
-   With heroic assistance from BrandonW, fixed an issue where the
    Instant Goto would fail to find what program needed to be edited on
    error. *(05/15/2010)*
-   Found and repaired problem with Instant Goto'ing to first line of a
    program. *(5/12/2010)*
-   Added automatic creation of FLDSV7 appvar even if it's not dirty, if
    it didn't exist. *(5/12/2010)*
-   Temporarily disabled ParserHook to avoid BASIC crashes.
    *(5/12/2010)*
-   Repaired some folder-save checkpoints to use DAVLCheckOffset instead
    of DAVLCheck. *(5/10/2010)*
-   Added InfoPop disappearance, repaired APD from desktop and GUIMouse.
    *(5/9/2010)*
-   Repaired poweroff issue on Nspires, thanks to BrandonW and TIFreak8x
    for testing. *(5/6/2010)*
-   Updated all three iFastCopy routines to do Nspire-specific speedups,
    redirected mos_fastcopyb to imFastCopy. *(5/6/2010)*
-   Fixed vector displacement by adding dummy CalcNET2 vectors.
    *(5/6/2010)*
-   Used 13 bytes on page 1 to make \[ALPHA\] a right-click alias for
    TabFuncs. *(5/6/2010)*
-   Completed updated properties menu, GUImouse, and TabFuncs with new
    Hide function. *(5/6/2010)*
-   Updated Alpha Sort function to handle hidden programs properly.
    *(5/5/2010)*
-   Released **PwdSE v2.0** for Doors CS 6.1+. *(4/29/2010)*
-   Debugged **\[RunProg\]**-related crash from running Doors CS
    programs. *(4/29/2010)*
-   Added SE functions in mouse. *(4/29/2010)*
-   Completed all hotkeys in
    **[GUIMouse]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/GUIMouse.md" >}})** port of desktop.
    *(4/27/2010)*
-   Converted Properties Menu to use
    **[GUIMouse]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/GUIMouse.md" >}})** except for the
    hotkeys and SE functions. *(4/25/2010)*
-   Changed "Doors CS 6" to "Doors CS 7" in the DCS Menu for all
    languages. *(4/24/2010)*
-   Converted Desktop to use
    **[GUIMouse]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/GUIMouse.md" >}})** except for the
    hotkeys and SE functions. *(4/23/2010)*
-   Began upgrading Properties menu to new format, discarded as memory
    hog. *(4/22/2010)*
-   Created new [InfoPop](InfoPop)-like **MemoryPop** hover tip to show
    free memory and archive. *(4/16/2010)*
-   Updated with new
    [SmallWindow]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRSmallWin.md" >}})-based About screen,
    saving about 200 bytes *(4/16/2010)*
-   Freezes after various RunProg scenarios fixed by forcing RealStart
    instead of RenderDesktop *(4/16/2010)*
-   Repaired issue where folder names are not displayed properly
    *(4/15/2010)*
-   Disabled CALCnet2.2 pending further protocol progress. *(8/30/09)*
-   Separated program execution from main.asm. *(8/30/09)*
-   Tested preliminary parserhook. *(8/28/09)*
-   Finished separating DetectType from VATProcessEntry from VATFind.
    *(8/24/09)*
-   Found and fixed program-counting crash in separated
    DetectType/VATFind. *(7/23/09)*

## Doors CS 6.2 Development {#doors_cs_6.2_development}

-   Fixed Nspire problems *(6/15/08)*

## Doors CS 6.1 Development {#doors_cs_6.1_development}

-   Fixed 8th-pixel right-side screen clipping in MainMouse and
    GUIMouse. *(6/17/07)*
-   Repaired InfoPop to handle tokenized descriptions properly.
    *(6/17/07)*
-   Released Doors CS 6.1 'Montuori' Beta 4. *(6/17/07)*
-   Fixed editor permissions to remove ability to edit Ion ASM programs.
    *(6/17/07)*
-   Multilingual error string length fixed for screen width. *(6/17/07)*
-   Manual updated and re-PDFized to account for new features, menus,
    and layout. *(6/17/07)*
-   Finally able to make BasIG (Instantaneous Goto) function properly -
    editTail updating fixed. *(6/17/07)*
-   Doors CS 6.1 'Montuori' Beta 3 released. *(6/13/07)*
-   DCS Menu crash resolved by splitting safeRAM used. *(6/13/07)*
-   'Fast' goto option added, but still takes roughly as long as the
    TI-OS. *(6/10/07)*
-   Repaired functionality of error messages with Cn2 enabled (switch to
    Cn2GetK). *(6/9/07)*
-   Add typemask to disallow editing of non-BASIC programs. *(6/9/07)*
-   Editor now handles archived and/or locked programs properly.
    *(6/8/07)*
-   Repaired DGetCharWidth for proper functionality. *(6/6/07)*
-   Added special [GUIMouse]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/GUIMouse.md" >}})
    mousemode for drawing and games. *(6/6/07)*
-   Completed debugging of the editor feature. *(6/5/07)*
-   With BrandonW's help, coded a chainloader for the \[ON\]\[PRGM\]
    rawkeyhook. *(6/5/07)*
-   Tri-lingual options screens adjusted for aesthetics; \[ON\]\[PRGM\]
    checkbox added to all. *(6/5/07)*
-   All APD instances repaired for proper functionality. *(6/5/07)*
-   \[2nd\] now also closes the About screen in addition to the
    already-extant \[ALPHA\]. *(6/5/07)*
-   Temporary [ Associated Files ](Associated_Programs) are now hidden
    from the File Open and File Save As dialogs. *(6/4/07)*
-   Constructed proper re-AP routine for FOpen GUI routine. *(6/4/07)*
-   Set the VFAT to clear on each RenderDesktop iteration to resolve
    edit and properties issues. *(6/4/07)*
-   Restricted editing to BASIC programs. *(6/4/07)*
-   Fixed number of items popped at SMPower close 10\>\>11 to resolve
    crash. *(6/4/07)*
-   Implemented stopgap AP file opening from FOpen GUI for archived
    files. *(6/3/07)*
-   Completed writeback for AP files in FOpen GUI routine. *(6/3/07)*
-   Enabled editing and handling for archived programs. *(6/3/07)*
-   All editor crash/freeze problems fixed with BrandonW's help.
    *(6/3/07)*
-   Folder name displayed in taskbar for non-main folders. *(6/3/07)*
-   Added MouseMode context to restrict InfoPop to the desktop only.
    *(6/3/07)*
-   BASIC editor added but disabled until bugs can be worked out.
    *(6/2/07)*
-   Modified ArcUnarcDCSBASIC to skip past description-prefixed DCS
    headers. *(6/2/07)*
-   Modified VATFind to understand description-prefixed DCS headers.
    *(6/2/07)*
-   Repaired property display for unarchived TI-OS BASIC programs.
    *(6/2/07)*
-   Fixed copying for archived programs. *(5/31/07)*
-   Fixed renaming for archived programs. *(5/31/07)*
-   Added renaming capabilities for folders. *(5/31/07)*
-   Added description of "Folder" for folders in InfoPop. *(5/31/07)*
-   Fixed all incorrect characters in the font table. *(5/31/07)*
-   \[ON\]\[PRGM\] keyhook implemented to launch DCS. *(5/31/07)*
-   Folder renaming and auto-rearchiving implemented. *(5/31/07)*
-   "Hybrid" DCS-MOS header with 16x16 icon on second line implemented.
    *(5/31/07)*
-   MOS BASIC description format now accepted and displayed by InfoPop.
    *(5/31/07)*
-   Elfprince's "ResetAppPage" problem turns out to be custom cursor
    issue. Solved. *(5/31/07)*
-   EE audio timing fixed to control text speed. *(5/30/07)*
-   InfoPop system created and debugged. *(5/30/07)*
-   Repaired jumpy cursor editor by adding Cn2Getk instead of direct
    input. *(5/29/07)*
-   Repaired "Galaxian" issue by optimizing
    [GUIRRadio]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRRadio.md" >}}) and
    [GUIRCheckbox]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRCheckbox.md" >}}) rendering.
    *(5/29/07)*
-   Power hotspots added to Power menu. Upgrade complete. *(5/29/07)*
-   Added new icons in place of radio buttons in Power menu. *(5/29/07)*
-   Rounded the upper corners of GUIRSmallWindows. *(5/29/07)*
-   Added several hotkeys; [Hotkeys]({{< ref "Manual/Hotkeys.md" >}})
    page created. *(5/28/07)*
-   Made 'Ans' (\$72) a valid 'hide' token. *(5/28/07)*
-   Commented out the Zelda music in the EE, saves about 2,000 bytes.
    *(5/28/07)*
-   Eliminated whitespace behind DCS Menu in rolldown. *(5/28/07)*
-   Only registered, logged-in users may edit pages due to an abundance
    of spam. *(5/19/07)*

## Doors CS 6.0 Development {#doors_cs_6.0_development}

-   Made up-folder icon not appear if in main folder. *(4/19/07)*
-   Fixed homescreen residue problem. *(4/18/07)*
-   Added vector table entry for new Cn2GetK. *(4/18/07)*
-   Resolved longstanding delete problem. *(4/18/07)*
-   Resolved text input+CALCnet2 problems. *(4/17/07)*
-   Made Ren&Copy ignore archived programs. *(4/16/07)*
-   Added debounce to desktop scrolling routine. *(4/16/07)*
-   GUIfied the rename/copy/new folder section. *(4/16/07)*
-   Cleared out AppVar RAM on initialization. *(4/15/07)*
-   Released Doors CS 6.0 RC4.4. *(4/13/07)*
-   Removed autoarchiving from folders - was causing more problems than
    it solved due to TI-OS suckage. *(4/13/07)*
-   Recoded PropMenu to use a larger, more self-explanatory sprite and
    hotspotting. *(4/13/07)*
-   Turned off run indicator for BASIC prgm errors. *(4/11/07)*
-   Released Doors CS 6.0 RC4.3. *(4/11/07)*
-   Found and fixed a problem with FldSearch not autofolderizing
    3-letter-named files. *(4/11/07)*
-   Upgraded PropDelete to deal gracefully with folders. Also handles
    unarced folders properly (safeguard). *(4/11/07)*
-   Fixed and reimplemented blocking of arc/unarc of folders.
    *(4/11/07)*
-   DCS Menu forced to quit to desktop after Cn2 enable to prevent Link
    menu freeze/crash. *(4/10/07)*
-   Folders set to automatically archive once they are created.
    *(4/10/07)*
-   Folder name display now works perfectly well with archived and/or
    locked folders. *(4/10/07)*
-   Doors CS 6.0 RC4.1 upgraded to Doors CS 6.0 RC4.2 to publish
    incorrect back fix. *(4/10/07)*
-   Incorrect back button function repaired (fixed jr nc OBOE in
    FolderSearch). *(4/10/07)*
-   Doors CS 6.0 RC4 upgraded to Doors CS 6.0 RC4.1 to publish
    disappearing programs fix. *(4/9/07)*
-   Disappearing programs with and without folders fixed by switching
    back to 2-byte offset table. *(4/9/07)*
-   Released Doors CS 6.0 RC4. *(4/8/07)*
-   Repaired the rearchive bug for writeback with the help of BrandonW.
    *(4/8/07)*
-   Significantly abridged
    **[To_Do]({{< ref "doors_cs_7_scratchwork.md" >}})** list by
    removing completed items. *(4/8/07)*
-   Fixed **[AutoArchive]({{< ref "/Manual/Features.md" >}})** of BASIC
    subprograms under all header types. *(4/8/07)*
-   Fixed writeback issue with archived APs changing size. *(3/19/07)*
-   Fixed [FileSaveAs]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSaveAs.md" >}}) to return
    cancel state on blank filename. *(3/18/07)*
-   Corruption error on writeback resolved. *(3/18/07)*
-   Fixed small issue with MouseHooks in
    [GUIMouse]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/GUIMouse.md" >}}). *(3/17/07)*
-   Fully fixed interrupt issues in Ion and derivative routines.
    *(3/17/07)*
-   Alpha save distortion - resolved by moving 5-byte save to Op6.
    *(3/15/07)*
-   Scroll misalign on FOpen and FSaveAs (uninited ProgsDone).
    *(3/15/07)*
-   Proper debounce added to file/folder name entry from Properties
    menu. *(3/14/07)*
-   Appvar AutoUpgrade feature fully repaired - no longer freezes on
    upgrade. *(3/14/07)*
-   GUIMouse \[off\] retriggering problem. *(3/14/07)*
-   CEdit debounce problems resolved. *(3/14/07)*
-   Demon's archive problem (too many files) *(3/14/07)*
-   "Fixed" elfprince13's double-rendering "bug" - PEBKAC. *(3/14/07)*
-   Made PGS_NMR work properly for executing programs. *(3/14/07)*
-   Fixed pointer-correction routines in the AP GUIs. *(3/12/07)*
-   Repaired black boxes on homescreen in BASIC execution. *(3/8/07)*
-   Finished 'The Easter Egg' *(3/8/07)*
-   Cn2 enabling disables turning off via \[On\] repaired by adding
    handler into interrupt *(3/8/07)*
-   RC2 released *(3/8/07)*
-   Cn2 crash on real calcs *(3/8/07)*
-   GUI Insufficient RAM invokes err handler, not internal handler --\>
    bcall(enoughRAM) seems a bit unstable. *(3/7/07)*
-   Final remaining sort bug found - OP1 was not getting updated when a
    new top prog found. *(3/7/07)*
-   Implemented smoother DCSMenu close - no more pixel noise at bottom.
    *(3/7/07)*
-   MOS has the same "homescreen retention dealio;" deemed too
    intricately bound up in the vagarities of the TI-OS to deal with
    atm. *(3/7/07)*
-   Hidden files created by MirageOS now properly handled by DCS.
    *(3/7/07)*
-   It appears that all AlphaSort bugs have been worked out, at last.
    *(3/6/07)*
-   Dealt with the speed issue by explicitly setting speed. *(3/6/07)*
-   Archiving... screen GUIfied. *(3/6/07)*
-   CALCnet2 support library debugging completed. *(2/16/07)*
-   Text input exit debounce problem *(2/16/07)*
-   cn2 ready2send flag *(2/15/07)*
-   Foamy's cursor/crash error - Cn2?? - assumed fixed from Cn2
    crash/DCS init problem. *(2/14/07)*
-   New Cn2 crash from DCS Menu finally solved through careful checking
    of stuff. *(2/15/07)*
-   Implemented functionality of \`Allow Background\` checkbox in
    options. *(2/14/07)*
-   Icon OBOE fixed for archived icons. Did ld a,(hl) instead of
    GetArcByte. *(2/13/07)*
-   After another six hours of debugging, all text movement routines
    work perfectly. *(2/12/07)*
-   After about four months, **[FineLineEnd](FineLineEnd)** finally
    works properly! *(2/11/07)*
-   Fixed width association for character \$7E (tilde). *(2/7/07)*
-   Defragmenting... bug declared fixed via community concensus.
    *(2/7/07)*
-   Updated data files for all languages to reflect new versions and
    year. *(2/7/07)*
-   **[GUIMouse]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/GUIMouse.md" >}})** routine and wiki
    page edited to allow a custom user mouse hook. *(2/7/07)*
-   Noticed and fixed a mis-equate for 'FPopsHowMuch'. *(2/7/07)*\
-   Fixed multiline-text rendering problem with multiple CRs and
    optimized. *(2/7/07)*\
-   Released **DCS 5.9 beta 1**. *(12/27/06)*\
-   Implemented Tari's **sort routine**, but disabled it because of
    bugs. *(12/27/06)*\
-   Fixed manual and automatic powerdown/up issues with **Cn2** and
    **gCn**. *(11/24/06)*\
-   Fixed runindic presence after BASIC exec. *(11/22/06)*\
-   BASIC pause if text on homescreen at quit. *(11/22/06)*\
-   **Link menu** started with default disabled txt. *(11/22/06)*\
-   Binaricized in/out count icon for all langs. *(11/22/06)*\
-   Added counting of Cn2 rec/send bytes, reset on Cn2 reenable.
    *(11/22/06)*\
-   Switched the safe driver checkbox to alpha sort, allocated AV space.
    *(11/22/06)*\
-   Harq(?)'s \[on\] problem resolution. *(11/20/06)*\
-   reenable silent link for BASIC and ASM programs - TI im1.
    *(11/17/06)*\
-   Lowercase flag disable. *(11/17/06)*\
-   Residual problem with the \[A/a/1\] indicator resolved.
    *(11/16/06)*\
-   Quit to DCS from unsupported MOS calls repaired. *(11/12/06)*\
-   Extra +2 offset in FileSaveAs fixed. *(11/9/06)*\
-   [GUI_Stack]({{< ref "/SDK/Mono/Asm/DCS/GUI/Memory_Areas.md" >}}) pops for
    **[FileOpen]({{< ref "/SDK/Mono/Asm/DCS/APs/FileOpen.md" >}})** and
    **[FileSaveAs]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSaveAs.md" >}})** added.
    *(11/9/06)*\
-   Return values for
    **[FileSaveAs]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSaveAs.md" >}})** implemented.
    *(11/9/06)*\
-   Removed delay on close entirely, added message during appvar backup.
    *(11/8/06)*\
-   CALCnet2 debug pixels disabled (moved to #cn2debug flag).
    *(11/8/06)*\
-   Crash on close the DCSMenu after enabling Cn2/gCn found and
    repaired. *(11/8/06)*\
-   Extra information outputs added to
    **[PopGUIStack]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/PopGUIStack.md" >}})** and
    **[CloseGUIStack]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/CloseGUIStack.md" >}})**.
    *(11/8/06)*\
-   Masked alpha indicator added for textin routines. *(11/7/06)*\
-   Alpha/2nd not debounced on close repaired. *(11/6/06)*\
-   Archiving compatibility with
    **[FileOpen]({{< ref "/SDK/Mono/Asm/DCS/APs/FileOpen.md" >}})** added.
    *(11/6/06)*\
-   unknown type problem with APs fixed - Op1 needed to be cleared.
    *(11/5/06)*\
-   FileSaveAs scrollbar problem fixed. *(11/5/06)*\
-   AP open & save as routines & GUls completed. *(11/5/06)*\
-   **[FileSaveAs]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSaveAs.md" >}})** completed.
    *(11/5/06)*\
-   Rename hotspot misalignment in properties menu fixed. *(11/5/06)*\
-   **[FileSave]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSave.md" >}})** routine written,
    tested, and debugged. Routine caused weird unknown type on run bug
    to surface. *(11/3/06)*\
-   All of **[FileOpen]({{< ref "/SDK/Mono/Asm/DCS/APs/FileOpen.md" >}})** except
    for archived AP handling written, debugged, and tested. *(11/3/06)*\
-   Crash icon implemented for unknown-type AP files. *(11/3/06)*\
-   Doors CS 5.8 Beta 3 released. *(11/1/06)*\
-   Include files organized to put language-specific data together.
    *(10/27/06)*\
-   Routine to display message after crash completed. *(10/26/06)*\
-   Routine to detect backed-up appvar and restore from backup created
    and debugged. *(10/26/06)*\
-   Routine created and debugged to save AppVar data in an archives
    appvar. *(10/26/06)*\
-   Version number added to About display. *(10/26/06)*\
-   About display now appears at AppVar creation. *(10/26/06)*\
-   Version number added in About display. *(10/26/06)*\
-   Minor bug in BAU feature fixed with foamy3's help. *(10/23/06)*\
-   Scrollbar OBOE fixed - now all values 1-65536 should work properly.
    *(10/23/06)*\
-   Doors CS 5.8 Beta 2 released. *(10/23/06)*\
-   Debounce issue repaired. *(10/23/06)*\
-   Masked battery and memory meter. *(10/23/06)*\
-   Alpha added as GUIMouse rightclick key alias. *(10/23/06)*\
-   Desktop flicker on redraw removed. *(10/23/06)*\
-   \[ON\] poweroff feature updated and working in GUI mouse routine.
    *(10/22/06)*\
-   \[ON\] poweroff feature updated and working in main mouse routine.
    *(10/22/06)*\
-   BASIC AutoUpgrade feature added to add a quote (") before DCS BASIC
    icons. *(10/20/06)*\
-   Contrast increment changed 1-\>2, minimum changed 0-\>1.
    *(10/20/06)*\
-   Desktop upgraded with new scrollbar from the DCS GUI API (DGA).
    *(10/20/06)*\
-   Dynamic user-settable acceleration in
    [GUIMouse]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/GUIMouse.md" >}}) complete.
    *(10/19/06)*\
-   Dynamic user-settable acceleration in regular mouse complete.
    *(10/19/06)*\
-   Dynamic user-settable acceleration in [Options Menu](Options_Menu)
    complete. *(10/19/06)*\
-   [Active_Bug_A0009]({{< ref "Bug_Tracker/solved_bug_s0009.md" >}})
    reported by Alex10819; solved and moved. *(10/19/06)*\
-   Contrast bug introduced by GUI scrollbar onclick change found and
    repaired. *(10/18/06)*\
-   Doors CS 5.8 Beta 1 released. *(10/15/06)*\
-   Intelligent reset of recopy pointer after ASM execution added.
    *(10/15/06)*\
-   Force-load on poweron feature completed. *(10/15/06)*\
-   First Cn2.2 packet sent successfully. *(10/13/06)*\
-   Cn2.2 bit level protocol progressing. *(10/12/06)*\
-   Checks for enough memory in **[Writeback](Writeback)** creation
    routines. *(10/10/06)*\
-   Folder and lock-status protection added to
    **[Writeback](Writeback)** routines. *(10/09/06)*\
-   **[Shell_Expansions]({{< ref "Shell_Expansions.md" >}})** system
    completely upgraded. TabFunc SE made. *(10/03/06)*\
-   Powerdown repaired in
    **[GUIMouse]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/GUIMouse.md" >}})** routine.
    *(09/28/06)*\
-   Debouncing added to **[Text Input](Text_Input)** routines. \[TRACE\]
    and \[GRAPH\] both exit typing mode now. *(09/28/06)*\
-   **[Active_Bug_A0008]({{< ref "Bug_Tracker/solved_bug_s0008.md" >}})**
    added to the Bug Tracker. *(09/20/06)*\
-   **[Lies](http://www.youtube.com/watch?v=I_R-cutaUKE)**, the first
    DCS6 promo video has been released *(09/20/06)*\
-   **[Active_Bug_A0007]({{< ref "Bug_Tracker/solved_bug_s0007.md" >}})**
    added to the Bug Tracker. *(09/19/06)*\
-   **[Associated Program](Associated_Program)** icon recognition added
    and completed. *(09/16/06)*\
-   Doors CS 6 internal **[Custom Font](Custom_Font)** completed with
    almost all TI-OS 6x6 chars. *(09/13/06)*\
-   Password **[Text Input](Text_Input)** routine complete and debugged.
    *(09/12/06)*\
-   **Desktop alignment** of [Icons]({{< ref "SDK/Icons.md" >}}) and
    [Program Names](Program_Names) repaired from [Folder](Folder)
    rendering bug. *(09/07/06)*\
-   Start menu **[Power Dialog](Power_Dialog)** made functional for
    Shutdown, Restart, and Quit to TI-OS. *(09/07/06)*\
-   **[Mouse Acceleration](Mouse_Acceleration)** feature added to both
    the main mouse and the GUIMouse routines. *(09/06/06)*\
-   **[Writeback](Writeback)** problems solved, leading to release of
    **[Doors CS 5.7 Beta 2](http://www.cemetech.net/news.php?252)**.
    *(09/06/06)*\
-   Routines for limited Garbage Collection using intelligent writeback
    created, now being debugged. Details in the
    **[ASM_Execution_Flowchart]({{< ref "ASM_Execution_Flowchart.md" >}})**.
-   **About_Screen** completed, using
    intro fade routine. Intro removed.\
-   **[Active_Bug_A0003]({{< ref "Bug_Tracker/solved_bug_s0003.md" >}})**
    and
    **[Active_Bug_A0004]({{< ref "Bug_Tracker/solved_bug_s0004.md" >}})**
    repaired. *(08/17/06)*\
-   **Doors CS 5.7 Beta 1** released at
    [Cemetech](http://www.cemetech.net) ([Download
    now](http://www.cemetech.net/news.php?248)) *(08/14/06)*\
-   **[Default Cursor](Default_Cursor)** button added to the options GUI
    *(08/14/06)*\
-   A misalignment in **Window Buttons**, the \[-\] \[\] \[X\] buttons
    at the top of windows, has been repaired following detection of a
    math error. *(08/14/06)*\
-   **[Bug_A0001]({{< ref "Bug_Tracker/solved_bug_s0001.md" >}})** solved after a
    few days of careful thought. It is suspected that
    **[Bug_A0002]({{< ref "Bug_Tracker/inactive_bug_i0002.md" >}})** may
    have stemmed from the same problem, and it has been switched to
    inactive status based on this assumption. *(08/13/06)*\
-   **[Cursor Editing](Cursor_Editing)** is now operational, thanks to a
    routine written by **Chipmaster**. *(08/13/06)*
-   Single-line **[Text Input](Text_Input)** routine complete and
    debugged. *(09/11/06)*\
-   **Appvar checks** for size and RAM/ROM location status added to
    prevent crashes and freezes. *(09/07/06)*\
