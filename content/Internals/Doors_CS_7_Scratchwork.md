---
revisions:
- author: KermMartian
  timestamp: '2016-02-02T07:06:15Z'
title: Doors CS 7 Scratchwork
---

```{=html}
<div style="padding%3A+0.5em%3B+border%3A+1px+solid+%23000%3B+border-left%3A+0.7em+solid+%23000%3B">
```
This page contains archived development notes on Doors CS 7 for the
monochrome TI-84 Plus/TI-84 Plus calculators. That project is no longer
in active development.

```{=html}
</div>
```
This page contains a list of current past issues and things to be done
on the road to Doors CS 7. It's divided up into to-do items, which are
generally features but not broken, and bugs, which are things that break
or don't work right in Doors CS. There's related information on the
**[Nspire_Compatibility]({{< ref "Nspire_Compatibility.md" >}})** and
**[XLib/Celtic III Compatibility]({{< ref "XLib-Celtic_III_Compatibility/index.md" >}})**
pages.

## Pending Items {#pending_items}

### To-Do Items {#to_do_items}

-   Extend scrollbar hotspots

### Bugs

-   Incorrect entry followed by Quit/Goto in Input/Prompt causes many
    key combos to stop functioning. See [this
    post](http://www.cemetech.net/forum/viewtopic.php?p=223906#223906)
-   In some cases, running a CALCnet2.2 program, then trying to quit
    HomeRun, quit Doors CS, or run another program causes the run
    indicator to run and the calculator to require a battery pull.

### Inactive Bugs {#inactive_bugs}

-   Low memory program corruption bug. Steps are
    [here](http://www.cemetech.net/forum/viewtopic.php?p=138879#138879).
-   ZTrumpet and Qazz42 report problems with HomeRun leaving a temporary
    RAM copy around, later causing a crash. Very difficult to replicate.
-   506 error from homescreen Axe editing (caused by low batt) causes
    return to DCS, then crash, instead of quit to homescreen.
-   prgmseq( bug reported
    [here](http://www.cemetech.net/forum/viewtopic.php?p=201540#201540).
-   Error handling of WFRNG TI-OS system error jumping
-   Incorrect value loaded to Op from WFRNG's TI-OS based input function
-   DJ_O bumped his Reuben title screen issue.

## Completed Items {#completed_items}

### To-Do Items {#to_do_items_1}

-   Add a way to type "=" (also added "\_" and not-equals)
-   Made [BinRead]({{< ref "/SDK/Mono/BASIC/Celtic/BinRead.md" >}}) work with
    archived programs.
-   Poweroff and APD from Tabfuncs?
-   Folder Saving
    -   Separate archived appvar called FLDSV7
    -   Byte-flag in DCS7 appvar to indicate whether folder backup is
        up-to-date or not
    -   Cache folder changes until a program runs or DCS quits, two
        situations where DCS will lose control and it's more likely that
        a RAM clear will occur
    -   Try to create FLDVS7 even if not dirty, if it doesn't already
        exist.
-   Need to add \[ALPHA\] as right-click key for TabFuncs
-   HAVE TO fix the FileOpen, FileSave, and FileSaveAs routines to take
    into account the shifting size of the CHAIN7 appvar.
-   Can probably combine a lot of stuff in writeback.asm - two
    nearly-duplicate sections.
-   As per BrandonW: force acceptance of Garbage Collect, cf. [code
    fragment by BrandonW](http://pastie.org/964700)
-   Pseudoprogrammer suggested that just like F1 opens the DCS Menu, F1
    close it again. I agree, and have added this to the To Do list.
    Also, \[clear\] quits DCS from the DCS Menu.
-   Need to make programs and files in the ProgChain have unique names,
    something with prefixing and the current size.
-   Deleting the DCS7 AppVar without a RAM reset causes duplicate
    folders to be created.
-   Via Kevin: would it be possible for DoorCS to interpret
    real(20,0,0,0,96,62,0,0 as real(3,0,3,0 ? The first command pretty
    much just does what the second does anyway, but much slower.
    -   Completed, with small improvements. Cost of 59 bytes.
-   Make \[clear\] a close option for the right-click menu ~~(or make it
    quit Doors CS?)~~
-   Make \[clear\] be a default close on
    [SmallWindows]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRSmallWin.md" >}}).
    -   GUIMseSave is now used on all clicking. Must double-check that
        there are no negative effects.
-   Keep thinking about the hourglass cursor idea
    -   Added for Sort and Arc_Unarc, will see what people say.
-   Why is PrevVATArray only 123 bytes? Is that 61 programs of 61
    screens = 366 programs? Can I make it negative offsets instead of
    absolute values to make it only a single byte (=123 spaces or
    programs)?
    -   Player1537 has 139 programs, and the desktop glitches
        -   This would imply a max of (139/6)\*2 = 44 bytes necessary,
            or 129 + 44 bytes = 173 bytes total of var RAM in use
        -   For some reason, header.asm marked with 172-byte limit
    -   What's special about \$9872 + 172 = \$991e?
        -   Possible that CALCnet2 was responsible? Checking on Doors CS
            6 version
    -   Confirmed that CALCnet2 was to blame. First random value seen at
        \$991f, with scattered other words, then several longer chunks,
        then 256-byte table beginning at \$9a00.
    -   If CALCnet2 is resurrected (CALCnet3?!), then will have to
        return to resolve this
    -   Bug marked inactive / un-reproducible for now.
-   Still need to add folder saving right before program run, including
    updating the VFAT if necessary for things getting moved around
    -   Using RelocatablePtr1 and 2 to store (VFATn+0,1) and (VFATn+2,3)
-   GUIfied backing up folders... window?
    -   Decided to forego it in the interest of the hourglass
-   Add the actual RunProg vector, including remembering to deal with
    the whole hook issue with chained BASIC programs (ie, don't re-save
    our own hooks), and test it by nesting execution a few times.
    -   Talk to Elfprince about getting A-Drive underway again.
-   GUIfied archiving... window?
    -   Solved with hourglass instead
-   As per BrandonW: recalculate RAM checksum on poweroff, cf. [code
    fragment by BrandonW](http://pastie.org/964783)
    -   calcAndStore_RAMChecksum in dcslibsp.inc
-   Add Xenon's routines [from
    here](http://www.cemetech.net/forum/viewtopic.php?p=99355#99355)
    -   Added [PushGUIStacks]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/PushGUIStacks.md" >}}) and
        [GUIFindThis]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/GUIFindThis.md" >}}), both in
        modified form.
-   Make scrolling up from the top go to the bottom
-   Make scrolling down from the bottom go to the top
-   Use the edit buffer for multiline text input in the GUI
-   Remove Linking and Power from the DCS Menu.
    -   Link window removed, item remains in DCS menu
    -   Removed item, power item, power menu
    -   Updated side text to say DCS7 instead of Doors CS 7
    -   Saved 106 bytes on Page 1
-   Error strings for 501-560 (or thereabouts)
-   TI84p requests saving (curfldr, progsdone) upon entering a program
    and restoring it afterwards.
    -   curfldr is now saved, progsdone still pending
    -   Both now complete, tested, debugged
-   Write custom chkfindsym / delmem / insmem routines for use with the
    GUI?
    -   Decided not to do this for now.
-   Must check that GUIMouseHookLoc issue is indeed repaired, test a
    window with a scrollbar and radio buttons and a hook.
    -   Clicking on scrollbar destroys hook pointerto \$0000 now,
        apparently
    -   Repaired and tested with prgmHOOKTEST
-   Update credits screen, including [Credits]({{< ref "Credits.md" >}})
    page in Wiki, think about new Easter Egg. (shhh!)
-   Add an LCD delay slider bar to the Display window, and a button that
    attempts to tune it to the display, a la ALCDFIX. Don't forget SE/84
    only.
    -   For now just a "Retune" button in Display window. Pending
        [opinions](http://www.cemetech.net/forum/viewtopic.php?p=103338#103338)
        from Cemetech members on this.
    -   Switched to Penguin77's routine. According to the topic on
        Cemetech, people seem to think that just a retune button is
        fine.
-   Make \[CLEAR\] a backspace alias in textinr.
-   Filetype checking - the routines should allow built in filtering
    (with an on/off choice) (from Anakclusmos)
    -   Implemented for [FileOpen]({{< ref "/SDK/Mono/Asm/DCS/APs/FileOpen.md" >}})
        and [FileSaveAs]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSaveAs.md" >}}) as input
        *a*; tested.
-   Make GUI rendering and textinr scroll calculations take advantage of
    15MHz mode.
-   As per BrandonW: continually force a set of flag 0,(iy+16h) during
    the mouse keyloop and right after (before?) halt'ing in the Off
    routine.
    -   Nevermind, that only makes sense when the OS im1 interrupt is
        active (flag is to avoid ram clears on batt pulls)
-   As per BrandonW, bit 3,(iy+41h) (cf. \$000b on page \$00) for delays
    for PresentationLink.
    -   Implementation: combined Nspire and PresentationLink fastcopies.
-   Ans stack: sum(14) and sum(15)
    -   Also added: sum(16) to clear the stack
-   DCS Menu item: Help
-   Extra keys for cursor editor
    -   \[2nd\]=black, \[alpha\]=white, \[del\]=transparent,
        \[enter\]=finish
-   SwapAnsStack (on second thought, multiple stacks would be infinitely
    easier)
    -   Use of multiple stacks implemented for sum(14), sum(15),
        sum(16).
-   Replace Cn2 checkbox with folder backups, add Backup Now button.

### Bugs {#bugs_1}

-   Two programs of the same name, one hidden, can be loaded; unhiding
    or hiding one causes instability
-   Fix CALCnet \[on\] code?
-   Fix two bugs in BASIC code on page 18 of SDK
-   Laziness in hook chaining: [more
    info](http://www.cemetech.net/forum/viewtopic.php?p=167562#167562)
-   Acceleration problems as reported by TIFreak8x (esp in GUIMouse,
    non-desktop)
-   APD when infopop is active?
-   TIFreak confirms total lack of APD, as well as slower InfoPop launch
    and timed disappearance
-   Poweroff from Power menu or APD seems permanent
-   Poweroff from desktop does nothing
-   Folder names are not displayed properly *(4/15/2010)*
-   ASM program execution from ROM causes crash only on actual writeback
    *(4/16/2010)*
    -   Nope - happens for writeback on unarchived programs too
    -   Not easy to reproduce for unarchived programs though
-   Running an unarchived BASIC program causes a crash *(4/16/2010)*
    -   Seems to be crashing ~~\_before\_~~ after execution
-   File Open GUI (and File Save As GUI) crash in DocDE6 under current
    build of DCS7 (was due to vector misalignment from taking out
    calcNet2)
-   Executing BASIC programs seems to cause a crash
    -   Goto error crashes?
    -   Executing TIFreak's Star Trek 83 seemed fine, and Gotos seemed
        to be working with no problems...
-   Can't edit anything?
    -   Looks like I can edit things fine to me...
-   TIFreak reported two errors, one of which I've been able to
    replicate so far. When choosing Goto on an error in a BASIC program,
    when the error occurs on a line of the program that takes more than
    seven lines of the display, the editor freezes. However, I had
    confirmed that simply using the built-in editor (without Goto) does
    not freeze on such programs.
    -   Turned out it was actually a problem with Goto'ing to the first
        line of the program
-   "prgmREAPEX": Err: Undefined after Error 518 (Invalid Dim) -\> 2.
    Goto, happens for both arc'ed and unarc'ed. Simple edit is fine.
    Also happens when another part of the same line is edited, but does
    not occur if the second line of the program is edited. Seems to be a
    problem with the contents of "parseVar".
    -   Fixed with epic help from the inimitable Brandon Wilson.
-   Qazz42 reports that running DCS with \~45 bytes free causes GUI low
    RAM message to appear, followed by a freeze
    -   With 418 bytes free (148 in DCS), no problems seen
    -   With 231 bytes free in the TI-OS, flashes DCS desktop, then
        freezes on error message
    -   Turned out that although AV+21 and AV+22 was supposed to be the
        word for saving the SP, I was using AV+21, AV+22, and AV+23 to
        store the current GUI window type, window X, and window Y. I
        moved the WinType, WinX, and WinY to +23, +24, and +25, but
        until I implement the new program chaining, this will be a
        problem, as it overlaps ProgNameSpace3 (although the first byte
        of ProgNameSpace3 was already getting trashed)
-   FileOpen has extensive problems, including showing the temp RAM copy
    of the current program, and freezing on its second use.
-   Schoolhacker points out that if you have folders in layers and cut
    the folder in the root and paste it into itself, you have yourself
    some funny troubling stuff. When you press back, it will go in a
    loop between those folders, and if you quit DCS to go back to the
    root, the folder you cut and pasted will disappear and be
    unaccessible. (along with the apps that were originally in it). The
    only way to revert this is to delete the folder appvar and reset
    then the calc.
-   Folders go crazy when you try to hide them, turning into basic
    programs editable by DCS, but not by TIOS. Hitting hide enough times
    (5-6) turns them back.
-   Fixed folder renaming
-   Schoolhacker reports this that when you rename a file that's with in
    a folder it will pop back out to the root.
-   Schoolhacker mentions that the cursor default button doesn't work
    anymore
-   "Default" cursor button in Options does nothing
-   DCS freezes on completion of cursor editing
-   Qazz42 correctly reports that "Launch DCS at Startup" is broken
-   Opening a file in RAM or DCS with
    [FileOpen]({{< ref "/SDK/Mono/Asm/DCS/APs/FileOpen.md" >}}) (after starting
    DocDE with no document), then resaving it, causes a temp file to be
    left on the desktop, even if it never should have existed.
-   \[1\] and \[2\] shortcut keys malfunctioning?
-   Archived BASIC programs cause myriad homescreen problems, keep
    working on idea of reloading App context
-   Qazz42 reports that Doors CS freezes at the "insufficient RAM"
    message when running programs
-   If the GUI encounters insufficient RAM, it can get caught in an
    infinite loop
-   Temporary RAM copies of open AP files sometimes show in
    FileOpen/FileSave screens.
-   2nd-quit'ing from BASIC Input prompts works again for some reason
-   Random pixels appear in FileOpen FileSaveAs under 2.53MP, and
    actually 2.41 as well.
-   Cannot power off the calculator after running a BASIC program
-   Rename or duplicate any file to a name with lowercase letters. DCS
    6.4 beta will crash.
    -   Schoolhacker reports rename error/freeze
    -   In fact, renaming files seem to instead duplicate them, with no
        folder.
-   If you delete the unarchived DCS appvar, the application will create
    duplicates of the folders you originally have and its contents
-   Editing locked programs causes an os-triggered Syntax Error
    -   Solved by making the program a ProgObj before starting the
        editor context, then restoring its previous type byte.
-   Editor fails when scrolling backwards through two-byte tokens ending
    in \$3F such as \$AA,\$3F.
-   As per SchoolHacker: "\[...\] duplication still has problems, though
    you no longer loose the designated file. When you use copy on a
    program, and after you type in a name (this name could be anything,
    full caps included) and once you click OK, the app freezes, then
    after a few seconds the calc crashes.
    -   Replicated under 2.53MP / TI-84+SE
    -   Replicated under 2.41 / TI-84+SE
    -   Replicated under 1.18 / TI-83+
    -   Was due to extra \_PopOp1
-   After pressing STAT and then ALPHA, the right-click menu appears. If
    after that I move the mouse with the arrow keys, a 2nd mouse cursor
    appears.
-   Strange sorting order for, eg, SMSD0 vs. SMSD.
    -   Solved, had to do with adding \$40 to low-numbered characters
        for hidden programs
-   Kevin O's XLib game *SMSD* crashes on quit when run via Homerun, but
    not if you press \[ON\] to quit. Does NOT fail this way when run
    from DCS.
    -   The same behavior occurs with HEXPIC, as reported by Silver
        Shadow.
    -   Seems to occur when a Stop token is encountered. Further tracing
        will be necessary to determine a solution - perhaps catch Stops
        with the ParserHook and deal with execution stack cleanup
        myself?
    -   Solved by triggering nonexistent error %01111111, then handling
        it as a special silent case.
-   The new DOCDE7 crashed when a new file was created, and saved it in
    to a folder; after clicking OK the text field turned blank, then I
    clicked close application which resulted in a system crash.
    -   Un-reproducible thus far
-   In DocDE7, creating a new file, then clicking Open, then canceling
    both the Save As... and Open... dialogs causes a crash.
    -   Possibly the correct reproduction of the above bug?
-   Real(12,\[3-10\],...) with x2 \<= x1 or y2\<=y2 crashes.
    -   Repaired by aborting if x2 \<= x1 or y2\<=y2
    -   Double-checked that Real(12,\[0-2\],...) works properly for
        x1\>=\<x2, y1\>=\<y2.
-   Kevin reports that SMSD does not run properly via Homerun. cf. [more
    information at
    Omnimaga](http://www.omnimaga.org/index.php?topic=1661.msg39224#msg39224)
    -   Probably has to do with that 2.53MP bug. Searching #cemetech
        logs for BrandonW's fix.
    -   Currently discussing a fix with BrandonW.
-   Clear the gbuf and mark it as dirty when an error message is
    triggered by a BASIC program from within the Homerun system (or
    otherwise?)
    -   Done by setting graphDraw,(iy+GraphFlags) after leaving the call
        to BASICErrorParse in runprog.asm
-   Try to avoid clearing the graphscreen for programs run via Homerun.
    -   Checks the dirty graph bit, also sets it in RunprogFromDesktop
-   Running programs in first folder via Homerun brings up Properties
    menu.
-   More of a To-Do than a bug, but scrolling is much much slower in
    DCS6.6b than 6.2. Search down GUI subsystem slowdowns causing this.
    Perhaps lots of chkfindsyms or something?
    -   Modified [PushGUIStacks]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/PushGUIStacks.md" >}}) and
        [PopGUIStacks]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/PopGUIStacks.md" >}}) to use
        faster PushGUIStackFast and PopGUIStackFast routines that depend
        on a cached pointer to the gui7 appvar.
    -   Pending attempt to move gui7 VAT entry to beginning of VAT.
-   Qazz discovered that the hourglass and error bubble are getting left
    on the graphscreen after execution still. More details are
    [here](https://www.cemetech.net/forum/viewtopic.php?p=100326#100326).
    -   Repaired
-   Schoolhacker believes that Garbage Collect may be unstable under
    2.53 MP?
    -   He duplicated it
    -   Repaired, both fixing the GetCSChook to be more polite when at
        the wrong menu, and realizing that 2.53MP uses \$40 instead of
        \$3F for mGarbageCollect.
-   DJ_Omnimaga reports that the DCS version of Axe Tunnel is not
    behaving initially.
    -   Replicated. Axe is generating an invalid header, with a relative
        jump off by \$18. Pending Quigibo's fix.
    -   Quigibo reports this issue fixed in Axe.
-   When Quigibo hovers the mouse over some black space (he had less
    than 6 programs) then DCS gives a bunch of errors presumably because
    it is trying to look up the tooltip for a non-existent program.
    -   Repaired.
-   Quigibo also reports that he is able to click on a white program
    space and DCS throws 3 errors in a row trying the execute it.
    -   Repaired.
-   TI84p reports that acceleration does not work from the DCS menu
    -   False - turns out omission of long mousehook just makes it run
        quite fast.
-   TI84p reports that the checkbox to disable acceleration does nothing
    -   Working on re-introducing this feature.
    -   Implemented by setting the acceleration pause value to \$ff to
        indicate acceleration disabled
-   Clicking arrows on scrollbars from DCSBL sum(12) causes a freeze.
-   Folder/Back bug reported by xxxEpicFailxxx [in this
    post](http://www.cemetech.net/forum/viewtopic.php?p=102610#102610).
    -   Related issue: double-right-clicking on first item in a folder
        causes a crash.
    -   Resolved by changing ld hl,GUIDesktop_HS_Quit to ld
        hl,GUIDesktop_HS_Quit+3 in dcspropp.asm
-   Err 503 triggered by header of DCS program run from Homerun causes
    return to DCS desktop, not homescreen (thanks qazz)
    -   Preliminary fix in place on Prhh pending assembly and testing
    -   Repair verified.
-   Returning 0 - the AP routines aren't returning HL as 0 if you
    cancel. (wouldn't it be easier to return z with xor a and nz with
    or 1) (from Anakclusmos)
    -   APGUI routines use GetRAMNameAP to figure out cancelling; valid
        for Open, valid for SaveAs?
    -   Fix: Updated documentation to note that they no longer return 0,
        now they only return 0 if no AP file is open. If nothing was
        opened (or saved), the outputs might point to an existing file.
-   Three [GUIRTextMultiline]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRTextMultiline.md" >}})
    bugs from CalcDude84p.
    -   1 and 2 solved, 3 still pending; CalcDude84p will report whether
        the bug is extant or has been solved.
    -   Calcdude reports that 3 is fixed as well.
-   Eeems reports loss of modified data with FileSaveAs (as per [this
    thread](http://www.cemetech.net/forum/viewtopic.php?p=103221#103221))
    -   Fixed, except now it's possible to get a double FileSaveAs in
        DocDE7. :P
    -   Completely repaired in DocDE7
-   Nspire powerdown problem
    -   Reported solved under Nspire OS 1.7 by Qazz42 using DCS 6.8.1,
        with Port 03 fix.
    -   Also reported solved under OS 2.0. Bug marked as fixed.
-   Eeems reports improper wrapping with \$D6 and GUIRWrappedText
    -   Repaired
        [GUIRWrappedText]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRWrappedText.md" >}}); wiki
        page updated accordingly.
-   Qazz's sum(13) GetCalc bug
    -   Turned out not to be an actual bug, just a confusion of use
-   Scrolling down and back up inside a full first screen of FileOpen
    causes a crash.
    -   Can't seem to replicate (6.7.5, 7/18/2010)
    -   Finally replicated and repaired, thanks to \_player1537
        (8/6/2010)
-   Player1537's InfoPop bug
    -   Finally resolved after EPIC debugging, uncovered a host of other
        related issues
-   Player1537's "expr()"-like bug with "v"
    -   Fixed by checking VAT type byte for 05 or 06 before jumping into
        editor
-   BASIC goto offset problems with XLib/Celtic?
    -   Repaired by moving backwards instead of forwards if starting on
        a \$3F
-   Qazz reports occasional 503 error when running a program
-   Braden reports some subfolders have a very long scrollbar for a
    single screen of programs.
-   Running "iGo" leaves flashing cursor on DCS desktop, even though it
    functions normally.
    -   Solved by removing a bcall(\_RunIndicOff) and adding conditional
        ei's to mos_fastline.
-   Complex set of steps replicates a bug where unchecking Launch on
    Startup causes the next three checkboxes to be checked.
    -   Bug is fixed. This bug was due to not taking into account that
        if the gui7 AppVar is above the OFFSCRPT variable (which it must
        be) and then OFFSCRPT gets deleted, gui7 is now in a different
        place. Judicious use of RelocatablePtr1 and RelocatablePtr2
        fixed this; thanks to BrandonW for making me aware of those two
        pointers.
-   Clicking a blank spot after deleting a program causes an infinite
    hourglass on the desktop
    -   Fixed. Will need beta-testers to verify I didn't break anything
        else with my fix.
-   Launch on Startup appvar does not persist across upgrades or RAM
    Clears
    -   Actually, the cause may be more convoluted than this. Then
        again, it may not.
    -   Simple and repaired. Also nullifies need for previous Launch on
        Startup checkbox fix
-   Shmibs reports that Bubble Breaker and Serpent break DCS on quit.
    -   Fixed using \_FillAppBasePage. Fix confirmed.
-   Running an extant program after deleting a program causes a series
    of 503 errors or a crash
    -   Believed fixed, verify before removing from this list.
    -   Users verified fix
-   Some BASIC progs like DCSCALC crashing?
    -   Issue appears fixed, but need positive verification.
    -   No replications reported
-   Check JoeyBelgier's \[ON\] from ON-Launch bug.
    -   Irreplicable on real hardware. However, \[ON\] seems bouncy on
        84 calcs.
    -   Bounciness fixed
-   Tabbing to a blank space and clicking leaves the highlight onscreen.
    -   Bug repaired.
-   Doc DE 7 bug: Type, save, cancel causes document to clear
    -   Fixed; the culprit was in Document DE 7 itself.
    -   Confirmed fixed
-   LCD glitchy on some calcs even with tuning?
    -   Repaired
-   Deal with now-revealed CHAIN7 problem
    -   Repaired by moving PushProgStack after jp to PropMenu
-   Make InfoPop and MemoryPop appear slightly faster
    -   Delay before InfoPop/MemoryPop halved
-   Problem with hiding archived programs?
    -   Replicated, fix in progress.
    -   Fix believed complete.
-   Certain Ion programs aren't runnable?
    -   Not a bug - the given Ion program has a broken header
        (\$BB,\$06)
-   Modify APD hook to \[ON\]\[XTOn\] to avoid Emu8x conflict
    -   Completed (future reference: getKey code for \[XTOn\] is \$B4)
-   Homescreen blank and untypeable after HomeRunning the RunProg1 test
    program?
    -   textWrite,(iy+SgrFlags) wasn't getting reset
-   Calcdude reports "Small, rather insignificant bug I found. On the
    DCS menu (the one you get by pressing "Y="), clicking one pixel
    above the separator between items selects the item below it, rather
    than the one on the top side of the separator."
    -   Fixed the relevant hotspot coordinates
-   Tanner reports significant issue with sum(7,8: the hex appears to
    start 2 bytes / 4 nibbles too early.
    -   Fixed: single '4' that should have been '6'
-   Cancelling a copy or rename causes an archived program to become
    unarchived
    -   Fixed
-   Problems with LineRead from archived program in Celtic III libs?
    -   Traced and fixed; a "jr z" was a "jr nz"
-   Matt Pierce reports that the TI-OS Text( command, when run after
    recalling a pic using the xLib real(13) function, causes the screen
    to clear (but only the first time). Graph dirty issue or something?
-   CALCnet ISR is eventually disabled by iFastCopy despite
    Calc84maniac's fix
    -   Damn you TI-OS; problem was in conditional ei in
        \_vputs/\_vputmap
-   Nspire problems with CALCnet
    -   See above
-   Fixed 15MHz calcs going to full speed with CALCnet
-   Alberthro reports 2.53MP ASM+Input crash
    -   Tracked to three Mathprint-specific flags that don't get changed
        when DCS is launched via ON-PRGM.
-   1 folder and 5 programs makes
    [FileOpen]({{< ref "/SDK/Mono/Asm/DCS/APs/FileOpen.md" >}}) GUI show a second
    (blank, crashy) page
    -   Folder/file display was correctly hiding temp RAM copies of
        files, but the folder/file counting routine was not.
-   Bottom row name corruption. Steps for replication are
    [here](http://www.cemetech.net/forum/viewtopic.php?p=132330#132330).
    -   Solved
        [here](http://www.cemetech.net/forum/viewtopic.php?p=141904#141904)
-   504 error (mem) from homescreen causes CHAIN7 appvar to remain

### Un-Reproducible Bugs {#un_reproducible_bugs}

-   New folders have no name?
-   Freeze pressing up-folder button in FileSave window.
-   Occasionally pressing On, Goto, edit, quit from Homerun returns you
    to the Doors CS desktop.
    -   Unduplicable...
-   Seems that CHAIN7 is not getting reduced to zero-size and eliminated
    in some BASIC program situations?
    -   Hasn't recurred
-   "RunProg1" seems to be stuck at some kind of pause when it exits?
    -   Unable to replicate (6.7.7.2, 7/24/2010)
-   Eeems reports "key-mashing bug" with ASM programs, [details
    here](http://www.cemetech.net/forum/viewtopic.php?p=103248#103248)
    -   Completely non-replicable, marked idle pending confirmation from
        Eeems or other reports.
-   Issue with some text not appearing in XLib programs? Cf. contrast
    text for Reuben: Ev by Kevin O.
    -   Seems to only occur in Omnicalc-based programs; ignored for now.
-   Is CHAIN7 getting left un-cleaned in some BASIC execution cases? Try
    Braden's backup
    -   Cannot replicate, and Braden's CHAIN7 is filled with zeros.
-   Alberthro reports 2.53MP editor corruption
    -   Users indicate this happens without Doors CS
-   JosJuice reports Horiz/Full+Input problem
    -   Could not reproduce

    :   
