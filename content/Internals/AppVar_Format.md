---
revisions:
- author: KermMartian
  timestamp: '2010-08-16T01:43:39Z'
title: AppVar Format
---

Two AppVars are currently used during Doors CS execution, AppVar "DCS6"
and Appvar "gui6". In Doors CS 6.3 beta and higher, including Doors CS
7.0, these AppVars will respectively be called "DCS7" and "gui7".

## DCS6\[b\] & DCS7\[b\] {#dcs6b_dcs7b}

This AppVar (the b version is the archived copy) contains all of the
permanent and semipermanent data that must be stored in safer memory
than SafeRAM. Ordinarily, it is data that must be preserved in between
run sessions, such as archive data, the cursor icon, and more. It also
contains data that must be stored while ASM and BASIC programs are
running, both of which have the potential to overwrite safeRAM areas.

**Start-2** Size \[2 bytes\]

~~**Start+0** PrognameSpace: used to store the top element in the
Archive stack, essentially the name of the program being run. \[9
bytes\]~~

~~**Start+9** PrognameSpace2: used to store the second element in the
Archive stack, the name of the first program run if it called a second
program. \[9 bytes\]~~

~~**Start+18** SafeArcFlags: Used to remember the page that the primary
program is on \[2 bytes\]~~

~~**Start+19** SafeArcFlags+1: Page of data file if an AP. \[n/a
bytes\]~~

~~**Start+20** SafeArcFlags3: Used to remember the page that the
secondary program is on \[1 byte\]~~

**Start+0** HomeSaveA/HomeSaveHL (1+2 bytes)

**Start+3** HomeSaveA2/HomeSaveHL2 (1+2 bytes)

**Start+6** HomeSaveA3/HomeSaveHL3 (1+2 bytes)

**Start+9** CelticIII "AppVar" (8 bytes)

**Start+21** SP Save \[2 bytes\]

~~**Start+23** ProgNameSpace3: Used for Associated Programs \[9
bytes\]~~

**Start+23** GUI Win Type \[1 byte\]

**Start+24** GUI Win X \[1 byte\]

**Start+25** GUI Win Y \[1 byte\]

**Start+26** Current folder \[1 byte\]

**Start+27** Current screen in folder \[1 byte\]

**Start+28 through Start+31** *Unused* \[4 bytes\]

**Start+32** Current alpha settings \[1 byte\]

**Start+33** Lowercase Enabled \[1 byte\]

**Start+34** Enable XLib/CelticIII compatibility \[1 byte\]

**Start+35** ~~Enable CALCnet2~~ Back up folders \[1 byte\]

**Start+36** Launch DCS at startup \[1 byte\]

**Start+37** Mouse acceleration/speed \[1 byte\]

**Start+40** Mouse Sprite Mask \[8 bytes\]

**Start+48** Mouse Sprite \[8 bytes\]

~~**Start+56** ProgNameSpace4 - Used for Associated Programs \[9
bytes\]~~

**Start+56 through Start+64** *Unused* \[9 bytes\]

**Start+65** ????

**Start+66** Mouse acceleration decrement. \[1 byte\]

**Start+67** Crash context. 1 = crash due to DCS, 0 = other cause

**Start+68** Alpha sort enabled. 0=disabled, 1=enabled \[1 byte\]

**Start+69** Executing ASM program?? \[1 byte\]

**Start+70** Allow \[ON\]\[PRGM\] hook? \[1 byte\]

**Start+71** Previous rawkeyhook backup \[4 bytes\]

**Start+75** Allow homescreen execution? \[1 byte\]

**Start+76** Previous parserhook backup \[4 bytes\]

**Start+80** Folder backup dirty? (0=up-to-date, 1=out-of-date) \[1
byte\]

## gui6

\[Information coming soon\]
