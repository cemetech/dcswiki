---
revisions:
- author: KermMartian
  timestamp: '2006-09-07T19:05:22Z'
title: ASM Execution Flowchart
---

1\. Regular ASM: *done*\
o If archived, copy to ZTmp and set ProgNameSpace to original progname.

    o Also set (SafeArcFlags) to the ROM page of the original
    o If unarchived, set ProgNameSpace, set (SafeArcFlags) to 0, and do not create ZTmp
    o Jump to Step 3.

2\. Doors CS and Ion ASM: *done*\
o If archived, copy to ZTmp and set ProgNameSpace to original progname.

    o Also set (SafeArcFlags) to the ROM page of the original
    o If unarchived, set ProgNameSpace, set (SafeArcFlags) to 0, and do not create ZTmp
    o Set up all routine vectors as per DCS and program tables

3\. Continue for all: *done*\
o Jump to hook1 swaploader

    o Swap and execute
    o Swap back
    o Set up progname into Op1 based on SafeArcFlags, TmpProgName, and ProgName
    o If (SafeArcFlags) is zero, don't bother doing the comparison, just quit.
    o Set up CurROMPage, then call the VAT pointer setup routines
    o Do the comparison for the length of the program
    o If the same, delete ZTmp and quit.
    o If different, delete the original
    o Next, swap ZTmp into a new program with the original name
    o Set up the size after the swap is complete
    o Delete the now empty ZTmp
    o Quit.

**Current Status:** Complete and debugged.\
**Date of Completion:** September 07, 2006
