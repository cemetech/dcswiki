---
revisions:
- author: Olav nordmann
  timestamp: '2007-06-26T20:57:15Z'
title: Icons
weight: 50
---

Doors CS can handle 8x8 and 16x16 icons for a variety of uses, most
often specified in a BASIC or assembly program's header
to be shown in the shell's program list.

## Icon playground

The tool included here can be used to experiment with writing icon
data in hex. Edit the Icon Text field and the image will update to
match its contents, provided they can represent an icon.

<iframe height=220 style="width: 100%;" src="hexaicon.html" frameborder=0 ></iframe>

## Explanation

Icons for Doors CS programs are either 8 bytes (for an 8x8 icon) or 32
bytes (for 16x16) in size. For users familiar with assembly programming,
icons are specified in the same way as sprite data. When writing assembly
programs, it is often easiest to write the data with binary literals in
the [program header]({{< ref "/SDK/Mono/Asm/Header.md" >}}).

In BASIC programs, the icon is also part of the [program
header]({{< ref "/SDK/Mono/BASIC/Header.md" >}}) but must be written
as a hexadecimal string in the same format as understood by the
[playground tool](#icon-playground) above. For readers unfamiliar
with how to specify this data, continue reading.

### Data representation

Hexadecimal is a system that lets humans represent binary or graphical
data in a form digital devices can understand. The icons used in Doors
CS are made up of 16 or 64 hexadecimal digits:

```
+----+----+
+----+----+
+----+----+
+----+----+
+----+----+
+----+----+
+----+----+
+----+----+
```

```
+----+----+----+----+
+----+----+----+----+
+----+----+----+----+
+----+----+----+----+
+----+----+----+----+
+----+----+----+----+
+----+----+----+----+
+----+----+----+----+
+----+----+----+----+
+----+----+----+----+
+----+----+----+----+
+----+----+----+----+
+----+----+----+----+
+----+----+----+----+
+----+----+----+----+
+----+----+----+----+
```

Here, each cell represents a 4x1 piece of the icon, each one hexadecimal
digit. The digits are placed in order from left to right, top to bottom.
Because there are 16 possible combinations of 4 pixels (2⁴=16), there
are sixteen hexadecimal digits:

0. □□□□
1. □□□■
2. □□■□
3. □□■■
4. □■□□
5. □■□■
6. □■■□
7. □■■■
8. ■□□□
9. ■□□■
10. ■□■□ (A) 
11. ■□■■ (B)
12. ■■□□ (C)
13. ■■□■ (D)
14. ■■■□ (E)
15. ■■■■  (F)

## Samples

These sample icons can be pasted into the [playground](#icon-playground)
above to view them and check your understanding of the format.

```
/DCS 4 SDK SAMPLE ICONS (ONE ON EACH LINE) 8*8
        
44AA2244004400EE
0042E74E1C387060
70888870810E1111
07072727275288F8
74325181818A4C2E
F1F2F4F4F2F2E448
003E46FA8A8A8CF8
437A6E00FF00C3C3
302866C166283000
C3992404080089C3
40E7400025428500
FF8989FF8181423C
00480503C7F0FCFF
0080809001010100
424221294A428484
3C42A581A599423C
EFFDBFF7FF081408
78480E4202DE90F0
0C14E4A4A4E4140C
FFE78393C9E3EFFF
E0989492A2A1C1FF
A870D870AA070D07
08080800183C66C3
00383838FE7C3810
A870D870AA070D07
10307EFE7E301000
080C7E7F7E0C0800
827F8200827F8200
003C42BD817E4200
FC66667C666666FC
3844C6C0C0C64438
FC666666666666FC
FE666878686066FE
FE666878686060E0
E766667E666666E7
E3666C78786C66E3
FC66667C606060E0
11FF44FF11FF44FF
FF81BD81D5ABD5FF
3844829282443800
3844AA92AA443800
8244281028448200
3C4281C3BD81423C
00001024428181FE
FC868781818181FF
02070E142850A0C0
3F43FD85858586FC
3666C3C7C7EF763C
5454AB28003844EE
082840604A444A80
103844C644381000
C0C000D9F9E0E0E0
3F213F212163E742
3C4299A5A599423C
E098845A5A281907
0A1522312A2460F0
017EA42424242444
F888888FF911111F
384482928A443800
382838103854106C
18245AA55A241800
09182AC9C92A1809
AA00550000205088
3C7ECFDFFBF37E3C
0205091F2141E300
1C6281F18F814638
243C464A52523C24
387CBA92BA7C3800
009F759F759F9000
40475860FF605847
929292545438FF10
18244242427E7E42
3C72B899998D4E3C
FF8181FF81BDAD7F
0C126599A6484830
3C67BC8080780408
007F415D45F505FF
030F3FFF0040A000
FC66667C786C66E7
00001F2345FA8CF8
E766663C8181813C
00000FA0AF404080
0018FCFC30FCFC60
081414141414367F
36493600E060A080
C7C3E3F3DBCFC7E6
C3E7E7DBDBC3C3E7
F0606060606066FE
F0606060606060F0
FE6C6C6C6C6C6CFE
3C7EC3DBDB818181
02392595A9A29C40
88C8E8F8E8C88800
0066666666666600
1824428181422418
1028448244281000
FF818181818181FF
18244242818181FF
10284292BABA82FE
60908888483F10E0
113377FF77331100
88CCEEFFEECC8800
00003C3C3C3C0000
E0F0810C1E3663C1
1818242C524A9189
```