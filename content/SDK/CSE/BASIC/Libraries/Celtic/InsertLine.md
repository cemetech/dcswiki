---
revisions:
- author: KermMartian
  timestamp: '2013-09-30T16:45:14Z'
title: InsertLine
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Description

Inserts a line into a program or AppVar. Ans=1 is the first line of the
program, Ans=2 is the second, and so on.

## Technical Details {#technical_details}

### Arguments

det(2)\
**Str0**: Name of program to write to\
**Ans**: Line number to write to(first line is "1")\
**Str9**: Material to insert into a program. The line that was occupied
is shifted down one line and this string is inserted into the resulting
location.

### Outputs

**Str9**: Intact if no error occurred; otherwise, contains an error
code.
