---
revisions:
- author: KermMartian
  comment: "Created page with \u201C==Description== Executes the string of\nASCII-encoded\
    \ hexadecimal in Ans. Although a C9 (ret) at the end of\nyour hex string is highly\
    \ encouraged, Doors CE 9+ automatically\nadds\u2026\u201D"
  timestamp: '2016-02-12T22:10:14Z'
title: ExecHex
---

## Description

Executes the string of ASCII-encoded hexadecimal in Ans. Although a C9
(ret) at the end of your hex string is highly encouraged, Doors CE 9+
automatically adds a C9 of its own for safety, and you can rely on this
always being the case.

## Technical Details {#technical_details}

The code provided is executed at plotsScreen on the TI-84 Plus CE, if
you need to use absolute references or jumps. Code can be up to 256
bytes (xLIBCE's UserVars start at plotsScreen + 256).

### Arguments

det(14)

### Outputs

None
