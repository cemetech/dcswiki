---
revisions:
- author: KermMartian
  comment: "Created page with \u201CThis routine is only available on Doors\nCSE 8.0\
    \ and later for the color-screen TI-84 Plus C Silver Edition.\n==Description==\
    \ Deletes a line from a program or AppVar. Ans=1 is \u2026\u201D"
  timestamp: '2013-10-01T15:24:28Z'
title: DeleteLine
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Description

Deletes a line from a program or AppVar. Ans=1 is the first line of the
program, Ans=2 is the second, and so on.

## Technical Details {#technical_details}

### Arguments

det(7)\
**Str0**: Name of program to delete from\
**Ans**: Line number to delete (first line is "1"). If Ans=0, this
routine instead computes the number of lines in the program and returns
them in Theta.

### Outputs

**Str9**: Contains an error code if an error occurs.
