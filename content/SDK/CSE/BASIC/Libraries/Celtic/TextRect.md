---
revisions:
- author: KermMartian
  comment: /\* Technical Details \*/
  timestamp: '2016-09-07T18:50:51Z'
title: TextRect
---

## Description

Draw a rectangle on the homescreen. You must use
[DispColor]({{< ref "DispColor.md" >}}) to enable colors
before you use this. Thanks to [TextLib by
DrDnar](https://www.cemetech.net/programs/index.php?mode=file&id=1340)
for inspiring this function.

## Technical Details {#technical_details}

### Arguments

det(15,FG_LO,FG_HI,ROW,COL,HEIGHT,WIDTH\
**FG_LO**: Low byte of color\
**FG_HI**: High byte of color\
**ROW**: Starting row of rectangle\
**COL**: Starting column of rectangle\
**HEIGHT**: Rectangle height, in characters\
**WIDTH**: Rectangle height, in characters\
Because of TI-OS argument-parsing limitations, foreground and background
colors must be provided as a sequence of two numbers in the range 0-255.

Alternative format: det(15,FG_OS,ROW,COL,HEIGHT,WIDTH\
**FG_OS**: Foreground color from TI-OS Colors menu, like RED or BLUE or
NAVY.\
**BG_OS**: Background color from TI-OS Colors menu, like RED or BLUE or
NAVY.\
**ROW**: Starting row of rectangle\
**COL**: Starting column of rectangle\
**HEIGHT**: Rectangle height, in characters\
**WIDTH**: Rectangle height, in characters\
'''See [DispColor]({{< ref "DispColor.md" >}}) page for
color codes.

'''

### Outputs

See description.
