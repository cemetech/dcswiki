---
revisions:
- author: KermMartian
  timestamp: '2013-09-30T16:38:00Z'
title: ReadLine
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Description

Reads a line from a program or AppVar. If Ans (line number) equals 0,
then Theta will be overwritten with the number of lines in the program
being read. Useful for editors, curiosities, and verification.
Otherwise, Ans=1 is the first line of the program, Ans=2 is the second,
and so on.

## Technical Details {#technical_details}

### Arguments

det(0)\
**Str0**: Name of program to read from\
**Ans**: Line number to read from (first line is "1")

### Outputs

**Str9**: Contents of line read. Error "..NULLINE" if the line is empty.
May also contain other error codes if conditions are wrong.
