---
revisions:
- author: KermMartian
  comment: /\* Arguments \*/
  timestamp: '2013-10-06T15:33:26Z'
title: BufSpriteSelect
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Description

Draws indexed (palette-based) sprite onto the LCD and into the graph
buffer. Good for drawing tilemaps, backgrounds, and other sprites that
you won't want to individually erase. If you want to be able to erase
the sprite drawn and restore the background, you should consider
[BufSprite]({{< ref "BufSprite.md" >}}) instead. This
routine takes an offset into Str9 and a sprite length as arguments, so
that you can pack multiple sprites of different lengths into Str9.

## Technical Details {#technical_details}

### Arguments

det(10,width,X,Y,start,length)\
**Str9** = Sprite data of one or more sprites as ASCII hex, one pixel
per character. The digits 1-F are valid colors (1=blue, 2=red, 3=black,
etc), while G will cause the routine to skip to the next line. 0 is
normal transparency, and lets the background show through. H is a
special kind of transparency that erases back to transparency instead of
leaving the background color intact.\
**X,Y** = Coordinates of top-left corner of sprite\
**width** = Sprite width (height gets computed)\
**start** = Offset into Str9 of the start of pixel data. *0 is the first
character, not 1*\
**length** = Length of the sprite data in characters

### Outputs

Sprite drawn to LCD and stored to graph buffer.
