---
revisions:
- author: KermMartian
  comment: "Created page with \u201CThis routine is only available on Doors\nCSE 8.0\
    \ and later for the color-screen TI-84 Plus C Silver Edition.\n==Description==\
    \ Archive/unarchive a program variable given a name\u2026.\u201D"
  timestamp: '2013-10-06T15:13:36Z'
title: ArcUnarcVar
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Description

Archive/unarchive a program variable given a name.

## Technical Details {#technical_details}

### Arguments

det(5)\
**Str0**: Name of program or AppVar to move between Archive and RAM.

### Outputs

Moves a program or AppVar into RAM if it was in Archive, or into Archive
if it was in RAM.
