---
revisions:
- author: KermMartian
  comment: "Created page with \u201CThis routine is only available on Doors\nCSE 8.0\
    \ and later for the color-screen TI-84 Plus C Silver Edition.\n==Description==\
    \ Generates a two-character string containing the ST\u2026\u201D"
  timestamp: '2013-09-30T16:47:42Z'
title: SpecialChars
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Description

Generates a two-character string containing the STO character and
double-quote character, in that order.

## Technical Details {#technical_details}

### Arguments

det(3)\
*(No other arguments)*

### Outputs

**Str9**: Sto and doublequote characters, in that order. Use substrings
to extract them. If using the standard version of Celtic, the string
will be 9 characters long, the other 7 being junk. This should not
affect the integrity of string just as long as you extract only the
first two characters.
