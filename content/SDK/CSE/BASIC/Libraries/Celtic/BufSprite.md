---
revisions:
- author: KermMartian
  comment: /\* Technical Details \*/
  timestamp: '2013-10-06T15:34:14Z'
title: BufSprite
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Description

Draws indexed (palette-based) sprite onto the LCD and into the graph
buffer. Copies the contents of the graph buffer under the sprite back
into Str9, so that you can "erase" the sprite back to the original
background. Good for moving player characters, cursors, and the like.
Interacts politely with Pic variables and OS drawing commands like
Line(, Circle(, Text(, and so on. If you want to draw a lot of different
sprites to the screen and won't need to erase them back to the
background, then use
[BufSpriteSelect]({{< ref "BufSpriteSelect.md" >}})
instead.

## Technical Details {#technical_details}

### Arguments

det(9,width,X,Y)\
**Str9** = Sprite data as ASCII hex, one nibble per byte. The digits 1-F
are valid colors (1=blue, 2=red, 3=black, etc), while G will cause the
routine to skip to the next line. 0 is normal transparency, and lets the
background show through. H is a special kind of transparency that erases
back to transparency instead of leaving the background color intact.\
**X,Y** = Coordinates of top-left corner of sprite\
**width** = Sprite width (height gets computed)

### Outputs

**Str9**: Same length as input, contains the previous contents of the
graph buffer where the sprite was drawn. You can call det(9...) again
without changing Str9 to effectively undo the first sprite draw.
