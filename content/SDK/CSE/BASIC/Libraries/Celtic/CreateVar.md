---
revisions:
- author: KermMartian
  comment: "Created page with \u201CThis routine is only available on Doors\nCSE 8.0\
    \ and later for the color-screen TI-84 Plus C Silver Edition.\n==Description==\
    \ Create a program variable or an AppVar given a nam\u2026\u201D"
  timestamp: '2013-10-06T15:11:56Z'
title: CreateVar
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Description

Create a program variable or an AppVar given a name.

## Technical Details {#technical_details}

### Arguments

det(4)\
**Str0**: Name of program or AppVar to create.

### Outputs

**Str9**: Intact if nothing went wrong. Otherwise, an error code
results. If successful, you will be able to read the first line of the
newly created program as a null line.\
**Str0**: Intact with program's name to be created
