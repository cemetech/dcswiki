---
revisions:
- author: KermMartian
  comment: "Created page with \u201CThis routine is only available on Doors\nCSE 8.0\
    \ and later for the color-screen TI-84 Plus C Silver Edition.\n==Description==\
    \ Delete a program variable or an AppVar given a nam\u2026\u201D"
  timestamp: '2013-10-06T15:15:47Z'
title: DeleteVar
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Description

Delete a program variable or an AppVar given a name.

## Technical Details {#technical_details}

### Arguments

det(6)\
**Str0**: Name of program or AppVar to delete.

### Outputs

The indicated program or AppVar is deleted.
