---
revisions:
- author: KermMartian
  timestamp: '2013-09-30T16:45:50Z'
title: ReplaceLine
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Description

Replaces (overwrites) a line in a program or AppVar. Ans=1 is the first
line of the program, Ans=2 is the second, and so on.

## Technical Details {#technical_details}

### Arguments

det(1)\
**Str0**: Name of program to read from\
**Ans**: Line number to replace (first line is "1")\
**Str9**: Contents to replace the line with

### Outputs

**Str9**: Intact if no error occurred; otherwise, contains an error
code.
