---
revisions:
- author: KermMartian
  comment: "Created page with \u201CThis routine is only available on Doors\nCSE 8.0\
    \ and later for the color-screen TI-84 Plus C Silver Edition.\n==Description==\
    \ Output status string describing a program or AppVa\u2026\u201D"
  timestamp: '2013-10-06T15:19:48Z'
title: VarStatus
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Description

Output status string describing a program or AppVar's current state,
including size, visibility, and more.

## Technical Details {#technical_details}

### Arguments

det(8)\
**Str0**: Name of program to examine

### Outputs

**Str9**: Contains 9 byte output code.\
1st character: "A"=Archived "R"=RAM "\
2nd character: "V"=Visible "H"=Hidden\
3rd character: "L"=Locked "W"=Writable "O"=AppVar\
4th character: --RESERVED-- (filled with space char)

Five character string afterward is the size of data portion of variable.

Example: "AVL 00314" = Archived, visible, locked, and 314 bytes.
