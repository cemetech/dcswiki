---
revisions:
- author: KermMartian
  comment: "Created page with \u201C==Description== Converts a numeric or\nstring\
    \ variable in Ans into its string representation, returned in\nAns. ==Technical\
    \ Details== ===Arguments=== det(13) Input: any\nstrin\u2026\u201D"
  timestamp: '2016-02-12T21:59:00Z'
title: ToString
---

## Description

Converts a numeric or string variable in Ans into its string
representation, returned in Ans.

## Technical Details {#technical_details}

### Arguments

det(13)

Input: any string-able type in Ans, including numbers, lists, matrices,
and even strings.

### Outputs

The string representation of the input, in Ans.
