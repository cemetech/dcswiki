---
revisions:
- author: KermMartian
  timestamp: '2013-11-04T05:28:16Z'
title: DispColor
---

## Description

Changes the foreground and background color for Output(, Disp, and Pause
to arbitrary 16-bit colors, or disables this feature. Due to technical
limitations, the foreground and background for Text() cannot be changed
to arbitrary colors.

## Technical Details {#technical_details}

### Arguments

det(12,FG_LO,FG_HI,BG_LO,BG_HI\
**FG_LO**: Low byte of foreground color\
**FG_HI**: High byte of foreground color\
**BG_LO**: Low byte of background color\
**BG_HI**: High byte of background color\
Because of TI-OS argument-parsing limitations, foreground and background
colors must be provided as a sequence of two numbers in the range 0-255.
Sample low and high bytes are below.

Alternative format: det(12,FG_OS,BG_OS\
**FG_OS**: Foreground color from TI-OS Colors menu, like RED or BLUE or
NAVY.\
**BG_OS**: Background color from TI-OS Colors menu, like RED or BLUE or
NAVY.\
To disable this mode, you should call det(12,300) before exiting your
program.

|                      |          |     |     |
|----------------------|----------|-----|-----|
| ALICEBLUE            | \_\_\_\_ | 223 | 247 |
| ANTIQUEWHITE         | \_\_\_\_ | 90  | 255 |
| AQUA                 | \_\_\_\_ | 255 | 7   |
| AQUAMARINE           | \_\_\_\_ | 250 | 127 |
| AZURE                | \_\_\_\_ | 255 | 247 |
| BEIGE                | \_\_\_\_ | 187 | 247 |
| BISQUE               | \_\_\_\_ | 56  | 255 |
| BLACK                | \_\_\_\_ | 0   | 0   |
| BLANCHEDALMOND       | \_\_\_\_ | 89  | 255 |
| BLUE                 | \_\_\_\_ | 31  | 0   |
| BLUEVIOLET           | \_\_\_\_ | 92  | 137 |
| BROWN                | \_\_\_\_ | 69  | 161 |
| BURLYWOOD            | \_\_\_\_ | 208 | 221 |
| CADETBLUE            | \_\_\_\_ | 244 | 92  |
| CHARTREUSE           | \_\_\_\_ | 224 | 127 |
| CHOCOLATE            | \_\_\_\_ | 67  | 211 |
| CORAL                | \_\_\_\_ | 234 | 251 |
| CORNFLOWERBLUE       | \_\_\_\_ | 189 | 100 |
| CORNSILK             | \_\_\_\_ | 219 | 255 |
| CRIMSON              | \_\_\_\_ | 167 | 216 |
| CYAN                 | \_\_\_\_ | 255 | 7   |
| DARKBLUE             | \_\_\_\_ | 17  | 0   |
| DARKCYAN             | \_\_\_\_ | 81  | 4   |
| DARKGOLDENROD        | \_\_\_\_ | 33  | 188 |
| DARKGRAY             | \_\_\_\_ | 85  | 173 |
| DARKGREEN            | \_\_\_\_ | 32  | 3   |
| DARKKHAKI            | \_\_\_\_ | 173 | 189 |
| DARKMAGENTA          | \_\_\_\_ | 17  | 136 |
| DARKOLIVEGREEN       | \_\_\_\_ | 69  | 83  |
| DARKORANGE           | \_\_\_\_ | 96  | 252 |
| DARKORCHID           | \_\_\_\_ | 153 | 153 |
| DARKRED              | \_\_\_\_ | 0   | 136 |
| DARKSALMON           | \_\_\_\_ | 175 | 236 |
| DARKSEAGREEN         | \_\_\_\_ | 241 | 141 |
| DARKSLATEBLUE        | \_\_\_\_ | 241 | 73  |
| DARKSLATEGRAY        | \_\_\_\_ | 105 | 42  |
| DARKTURQUOISE        | \_\_\_\_ | 122 | 6   |
| DARKVIOLET           | \_\_\_\_ | 26  | 144 |
| DEEPPINK             | \_\_\_\_ | 178 | 248 |
| DEEPSKYBLUE          | \_\_\_\_ | 255 | 5   |
| DIMGRAY              | \_\_\_\_ | 77  | 107 |
| DODGERBLUE           | \_\_\_\_ | 159 | 28  |
| FIREBRICK            | \_\_\_\_ | 4   | 177 |
| FLORALWHITE          | \_\_\_\_ | 222 | 255 |
| FORESTGREEN          | \_\_\_\_ | 68  | 36  |
| FUCHSIA              | \_\_\_\_ | 31  | 248 |
| GAINSBORO            | \_\_\_\_ | 251 | 222 |
| GHOSTWHITE           | \_\_\_\_ | 223 | 255 |
| GOLD                 | \_\_\_\_ | 160 | 254 |
| GOLDENROD            | \_\_\_\_ | 36  | 221 |
| GRAY                 | \_\_\_\_ | 16  | 132 |
| GREEN                | \_\_\_\_ | 0   | 4   |
| GREENYELLOW          | \_\_\_\_ | 229 | 175 |
| HONEYDEW             | \_\_\_\_ | 254 | 247 |
| HOTPINK              | \_\_\_\_ | 86  | 251 |
| INDIANRED            | \_\_\_\_ | 235 | 202 |
| INDIGO               | \_\_\_\_ | 16  | 72  |
| IVORY                | \_\_\_\_ | 254 | 255 |
| KHAKI                | \_\_\_\_ | 49  | 247 |
| LAVENDER             | \_\_\_\_ | 63  | 231 |
| LAVENDERBLUSH        | \_\_\_\_ | 158 | 255 |
| LAWNGREEN            | \_\_\_\_ | 224 | 127 |
| LEMONCHIFFON         | \_\_\_\_ | 217 | 255 |
| LIGHTBLUE            | \_\_\_\_ | 220 | 174 |
| LIGHTCORAL           | \_\_\_\_ | 16  | 244 |
| LIGHTCYAN            | \_\_\_\_ | 255 | 231 |
| LIGHTGOLDENRODYELLOW | \_\_\_\_ | 218 | 255 |
| LIGHTGRAY            | \_\_\_\_ | 154 | 214 |
| LIGHTGREEN           | \_\_\_\_ | 114 | 151 |
| LIGHTPINK            | \_\_\_\_ | 184 | 253 |
| LIGHTSALMON          | \_\_\_\_ | 15  | 253 |
| LIGHTSEAGREEN        | \_\_\_\_ | 149 | 37  |
| LIGHTSKYBLUE         | \_\_\_\_ | 127 | 134 |
| LIGHTSLATEGRAY       | \_\_\_\_ | 83  | 116 |
| LIGHTSTEELBLUE       | \_\_\_\_ | 59  | 182 |
| LIGHTYELLOW          | \_\_\_\_ | 252 | 255 |
| LIME                 | \_\_\_\_ | 224 | 7   |
| LIMEGREEN            | \_\_\_\_ | 102 | 54  |
| LINEN                | \_\_\_\_ | 156 | 255 |
| MAGENTA              | \_\_\_\_ | 31  | 248 |
| MAROON               | \_\_\_\_ | 0   | 128 |
| MEDIUMAQUAMARINE     | \_\_\_\_ | 117 | 102 |
| MEDIUMBLUE           | \_\_\_\_ | 25  | 0   |
| MEDIUMORCHID         | \_\_\_\_ | 186 | 186 |
| MEDIUMPURPLE         | \_\_\_\_ | 155 | 147 |
| MEDIUMSEAGREEN       | \_\_\_\_ | 142 | 61  |
| MEDIUMSLATEBLUE      | \_\_\_\_ | 93  | 123 |
| MEDIUMSPRINGGREEN    | \_\_\_\_ | 211 | 7   |
| MEDIUMTURQUOISE      | \_\_\_\_ | 153 | 78  |
| MEDIUMVIOLETRED      | \_\_\_\_ | 176 | 192 |
| MIDNIGHTBLUE         | \_\_\_\_ | 206 | 24  |
| MINTCREAM            | \_\_\_\_ | 255 | 247 |
| MISTYROSE            | \_\_\_\_ | 60  | 255 |
| MOCCASIN             | \_\_\_\_ | 54  | 255 |
| NAVAJOWHITE          | \_\_\_\_ | 245 | 254 |
| NAVY                 | \_\_\_\_ | 16  | 0   |
| OLDLACE              | \_\_\_\_ | 188 | 255 |
| OLIVE                | \_\_\_\_ | 0   | 132 |
| OLIVEDRAB            | \_\_\_\_ | 100 | 108 |
| ORANGE               | \_\_\_\_ | 32  | 253 |
| ORANGERED            | \_\_\_\_ | 32  | 250 |
| ORCHID               | \_\_\_\_ | 154 | 219 |
| PALEGOLDENROD        | \_\_\_\_ | 85  | 239 |
| PALEGREEN            | \_\_\_\_ | 211 | 159 |
| PALETURQUOISE        | \_\_\_\_ | 125 | 175 |
| PALEVIOLETRED        | \_\_\_\_ | 146 | 219 |
| PAPAYAWHIP           | \_\_\_\_ | 122 | 255 |
| PEACHPUFF            | \_\_\_\_ | 215 | 254 |
| PERU                 | \_\_\_\_ | 39  | 204 |
| PINK                 | \_\_\_\_ | 25  | 254 |
| PLUM                 | \_\_\_\_ | 27  | 221 |
| POWDERBLUE           | \_\_\_\_ | 28  | 183 |
| PURPLE               | \_\_\_\_ | 16  | 128 |
| RED                  | \_\_\_\_ | 0   | 248 |
| ROSYBROWN            | \_\_\_\_ | 113 | 188 |
| ROYALBLUE            | \_\_\_\_ | 92  | 67  |
| SADDLEBROWN          | \_\_\_\_ | 34  | 138 |
| SALMON               | \_\_\_\_ | 14  | 252 |
| SANDYBROWN           | \_\_\_\_ | 44  | 245 |
| SEAGREEN             | \_\_\_\_ | 74  | 44  |
| SEASHELL             | \_\_\_\_ | 189 | 255 |
| SIENNA               | \_\_\_\_ | 133 | 162 |
| SILVER               | \_\_\_\_ | 24  | 198 |
| SKYBLUE              | \_\_\_\_ | 125 | 134 |
| SLATEBLUE            | \_\_\_\_ | 217 | 106 |
| SLATEGRAY            | \_\_\_\_ | 18  | 116 |
| SNOW                 | \_\_\_\_ | 223 | 255 |
| SPRINGGREEN          | \_\_\_\_ | 239 | 7   |
| STEELBLUE            | \_\_\_\_ | 22  | 68  |
| TAN                  | \_\_\_\_ | 177 | 213 |
| TEAL                 | \_\_\_\_ | 16  | 4   |
| THISTLE              | \_\_\_\_ | 251 | 221 |
| TOMATO               | \_\_\_\_ | 8   | 251 |
| TURQUOISE            | \_\_\_\_ | 26  | 71  |
| VIOLET               | \_\_\_\_ | 29  | 236 |
| WHEAT                | \_\_\_\_ | 246 | 246 |
| WHITE                | \_\_\_\_ | 255 | 255 |
| WHITESMOKE           | \_\_\_\_ | 190 | 247 |
| YELLOW               | \_\_\_\_ | 224 | 255 |
| YELLOWGREEN          | \_\_\_\_ | 102 | 158 |

*Thanks to Shaun "Merthsoft" McFall for the original color.h data used
for this table.*

### Outputs

See description.
