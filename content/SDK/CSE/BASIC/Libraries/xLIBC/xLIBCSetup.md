---
revisions:
- author: Tr1p1ea
  timestamp: '2014-11-26T21:17:44Z'
title: xLIBCSetup
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Description

Utility functions used when setting up an xLIBC program. real(0,0) gets
the current xLIBC version, real(0,1,\<bool\>) turns half-resolution mode
on or off, and real(0,2,\<bool\>) turns fast (15MHz) mode on or off.

## Technical Details {#technical_details}

### GetXLIBCVersion

**real(0,0)**: Returns version in Ans

### SetupGraphics

**real(0,1,1)**: Enable half-resolution (160x240-pixel) mode\
**real(0,1,0,REDRAW_STATUS)**: Disable half-resolution (160x240-pixel)
mode\
REDRAW_STATUS = 1 to redraw the status area at the top of the screen, 0
otherwise.

This function will set the calculator to half h-resolution/interlaced
mode. Please note that ALL xLIB drawing functions expect the calculator
to be in this mode.

THIS MUST BE TURNED OFF BEFORE EXITING YOUR PROGRAM OR THE TIOS WILL BE
STUCK IN THIS MODE!

Also remember to redraw the status bar up the top of the screen or the
TIOS will look funny.

To disable 160 mode and redraw the status bar

`real(0,1,0,1`

(If you forget to set this mode you will see how xLIB buffers each side
of GRAM. This might be useful for debugging.

### SetSpeed

**real(0,2,0)**: Disable fast (15MHz) mode, switching to 6MHz mode\
**real(0,2,1)**: Enable fast (15MHz) mode\
Note that the default CPU speed is 15MHz

### SetupColorMode

**real(0,3,VALUE)**:\
VALUE defines action to take:\
0 = FULL colour\
1 = 8COLOUR\
2 = COLOURINVERT\
3 = COLOURINVERTOFF (restore to normal)\
4 = FILLSCREEN\
5 = SETCOLOUROFFSET ***(DCSE8.1)***

To invert the colours on the screen:

`real(0,3,2`

To restore the colours back to normal:

`real(0,3,2`

This is because inverting something twice will restore it back to
normal.

To restore the colours back to normal when you don't know the previous
state of the screen:

`real(0,3,3`

To FILL the screen (active GRAM side only) with a colour from the
standard xLIB 256 colour palette:

`real(0,3,4,COLOUR,UPDATELCD`

To set the COLOUR_OFFSET value which is used by sprite and shape
routines (note that VALUE is between 0-255) This will change the colour
values per pixel of sprites as they are drawn to the LCD. It can be used
for special effects (magic animations for example):

    real(0,3,5,VALUE
