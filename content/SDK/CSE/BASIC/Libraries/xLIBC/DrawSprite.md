---
revisions:
- author: Tr1p1ea
  timestamp: '2014-06-10T05:00:17Z'
title: DrawSprite
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Description

**NOTE:** You must load tiledata into temp memory before drawing
tilemaps. See
[DCSE:BasicLibs:ManagePic]({{< ref "ManagePic.md" >}}).\

## Technical Details {#technical_details}

### DrawSpriteA (TI-OS Values) {#drawspritea_ti_os_values}

**real(4,0,X,Y,WIDTH,HEIGHT,XOFFSET,YOFFSET,TRANSINDEX,UPDATELCD,PICINDEXSTART,PICINDEX0,PICINDEX1...etc)**:\
X = x value\
Y = y value\
WIDTH = width of sprite in 8x8 chunks (an 8x8 sprite is 1, 16x16 is 2,
12x12 is also 2 etc)\
HEIGHT = height of sprite in 8x8 chunks (an 8x8 sprite is 1, 16x16 is 2,
12x12 is also 2 etc)\
XOFFSET = offset for x value\
YOFFSET = offset for y value\
TRANSINDEX = transparent colour index, any colour in the sprite that
matches this will be drawn transparent (0-255)\
UPDATELCD = 0/1 to update LCD after drawing\
PICINDEXSTART = pic index to start drawing from (in following list)\
PICINDEX0 = pic index in sprite data sheet

Note that sprites are drawn from 8x8 chunks (or tiles) and are stored in
tile/sprite data appvars that must be loaded into memory before use (see
MANAGEPIC). For sprites larger than 8x8 the function takes a list of
arguments for each index of the sprite (in 8x8 chunks). The sprites are
drawn \*column\* first:

16x16 sprite list layout:

    ---------
    | 1 | 3 |
    ---------
    | 2 | 4 |
    ---------

24x24 sprite list layout:

    -------------
    | 1 | 4 | 7 |
    -------------
    | 2 | 5 | 8 |
    -------------
    | 3 | 6 | 9 |
    -------------

Example: To draw an 8x8 sprite at 32,32 with a pic index of 155 that has
a transparent colour index of 248:

`real(4,0,32,32,1,1,248,1,0,155`

Example: To draw a 16x16 sprite at 32,32 with a pic index list of
12,13,14,15 that has a transparent colour index 248:

`real(4,0,32,32,2,2,248,1,0,12,13,14,15`

You can use the PICINDEXSTART argument to specify which sprite to draw
out of a list of sprites. This can be useful for drawing a differen
sprite depending on a direction variable. For example drawing an 8x8
sprite where pic index 10=up, 11=down, 12=left & 13=right, and you have
a direction variable "A" which holds 0=up, 1=down, 2=left & 3=right you
can do:

`real(4,0,32,32,1,1,248,1,A,10,11,12,13`

This will select index 10 if A=0, 11 if A=1, 12 if A=2 & 13 if A=3 and
so on.

### DrawSpriteB (Uservar Values) {#drawspriteb_uservar_values}

**real(4,1,USERVAR_X,USERVAR_Y,WIDTH,HEIGHT,XOFFSET,YOFFSET,TRANSINDEX,UPDATELCD,PICINDEXSTART,PICINDEX0,PICINDEX1...etc)**:\
X = x value\
Y = y value\
WIDTH = width of sprite in 8x8 chunks (an 8x8 sprite is 1, 16x16 is 2,
12x12 is also 2 etc)\
HEIGHT = height of sprite in 8x8 chunks (an 8x8 sprite is 1, 16x16 is 2,
12x12 is also 2 etc)\
XOFFSET = offset for x value\
YOFFSET = offset for y value\
TRANSINDEX = transparent colour index, any colour in the sprite that
matches this will be drawn transparent (0-255)\
UPDATELCD = 0/1 to update LCD after drawing\
PICINDEXSTART = pic index to start drawing from (in following list)\
PICINDEX0 = pic index in sprite data sheet

See above, the only difference is that it will reference user variables
for x/y.

### DrawSpriteList8x8A (TIOS Values) {#drawspritelist8x8a_tios_values}

**real(4,2,LISTCOUNT,XOFFSET,YOFFSET,TRANSINDEX,UPDATELCD,X0,Y0,PICINDEX0,X1,Y1,PICINDEX1...etc**:\
LISTCOUNT = number of sprites in list\
XOFFSET = offset for x value\
YOFFSET = offset for y value\
TRANSINDEX = transparent colour index, any colour in the sprite that
matches this will be drawn transparent (0-255)\
UPDATELCD = 0/1 to update LCD after drawing\
X0 = x value for 1st sprite in list\
Y0 = y value for 1st sprite in list\
PICINDEX0 = pic index for 1st sprite in list

This function draws a list of 8x8 sprites (max 32 sprites per call).

EXAMPLE: To draw 3 sprites at (10,10),(20,20),(30,30) with pic indicies
12,14,16 with a transparent index of 248:

`real(4,2,3,0,0,248,1,10,10,12,20,20,14,30,30,16`

### DrawSpriteList8x8B (Uservar Values) {#drawspritelist8x8b_uservar_values}

**real(4,3,LISTCOUNT,XOFFSET,YOFFSET,TRANSINDEX,UPDATELCD,X0,Y0,PICINDEX0,X1,Y1,PICINDEX1...etc**:\
LISTCOUNT = number of sprites in list\
XOFFSET = offset for x value\
YOFFSET = offset for y value\
TRANSINDEX = transparent colour index, any colour in the sprite that
matches this will be drawn transparent (0-255)\
UPDATELCD = 0/1 to update LCD after drawing\
X0 = x value for 1st sprite in list\
Y0 = y value for 1st sprite in list\
PICINDEX0 = pic index for 1st sprite in list

This function is the same as above but it takes USERVAR values for
X0,Y0,X1,Y1 etc

### DrawSpriteTileBGA (TIOS Values) {#drawspritetilebga_tios_values}

**real(4,4,X,Y,WIDTH,HEIGHT,XOFFSET,YOFFSET,TRANSINDEX,UPDATELCD,MAPWIDTH,MAPSTRING**:\
X = x value\
Y = y value\
WIDTH = width of sprite in 8x8 chunks (an 8x8 sprite is 1, 16x16 is 2,
12x12 is also 2 etc)\
HEIGHT = height of sprite in 8x8 chunks (an 8x8 sprite is 1, 16x16 is 2,
12x12 is also 2 etc)\
XOFFSET = offset for x value\
YOFFSET = offset for y value\
TRANSINDEX = transparent colour index, any colour in the sprite that
matches this will be drawn transparent (0-255)\
UPDATELCD = 0/1 to update LCD after drawing\
MAPWIDTH = width of tilemap\
MAPSTRING = string variable holding tilemap data (0-10)

This function will draw the tiles for width\*height at a specific sprite
coordinate. The resultant tiles will be aligned to the map (it will only
draw at intervals of 8-pixels). This is useful for restoring a tilemap
background that has been overwritten by a sprite

### DrawSpriteTileBGB (Uservar Values) {#drawspritetilebgb_uservar_values}

**real(4,5,X,Y,WIDTH,HEIGHT,XOFFSET,YOFFSET,TRANSINDEX,UPDATELCD,MAPWIDTH,MAPSTRING**:\
X = x value\
Y = y value\
WIDTH = width of sprite in 8x8 chunks (an 8x8 sprite is 1, 16x16 is 2,
12x12 is also 2 etc)\
HEIGHT = height of sprite in 8x8 chunks (an 8x8 sprite is 1, 16x16 is 2,
12x12 is also 2 etc)\
XOFFSET = offset for x value\
YOFFSET = offset for y value\
TRANSINDEX = transparent colour index, any colour in the sprite that
matches this will be drawn transparent (0-255)\
UPDATELCD = 0/1 to update LCD after drawing\
MAPWIDTH = width of tilemap\
MAPSTRING = string variable holding tilemap data (0-10)

This function is the same as above but references user variables instead
of tios values

### DrawSpriteCheckCollisionA (TIOS VALUES, *DCSE8.1*) {#drawspritecheckcollisiona_tios_values_dcse8.1}

**real(4,6,COUNT,X,Y,W,H,CX0,CY0,CW0,CH0....CXn,CYn,CWn,CHn\
** COUNT = number of coordinates to check against\
X = master X to test list of coordinates against\
Y = master Y to test list of coordinates against\
W = master W to test list of coordinates against\
H = master H to test list of coordinates against\
CX0 = first X to test against master X\
CY0 = first Y to test against master Y\
CW0 = first W to test against master W\
CH0 = first H to test against master H\
CXn = nth X to test against master X\
CYn = nth Y to test against master Y\
CWn = nth W to test against master W\
CHn = nth H to test against master H\
nth should be equal to COUNT

This function will test the rectangular coordinates specified by the
'master X,Y,W,H' against each iteration of rectangular coordinates from
CX0,CY0,CW0,CH0 to CXn,CYn,CWn,CHn and will return

0 or 1 in Ans where 0 = no collision between the 'master set' and the
list and 1 = a collision with at least 1 set is found A list of collided
coordinate indexes in the user-defined 'real list' "XL" in the format:

{TOTAL_COORDS_COLLIDED,INDEX0...INDEXn} Where:\
TOTAL_COORDS_COLLIDED = total number of rectangular indexes in the call
that collide with the 'master set'\
INDEX0 = first index where 0 = the rectangular coordinates
\[CX0,CY0,CW0,CH0\] and n would equal the \[CX0,CY0,CW0,CH0\]\
Note that user-define list "XL" is overwritten if it already exists

For example

Rectangles at:

X = 10\
Y = 10\
W = 8\
H = 8\
Tested against others where:

X = 15\
Y = 15\
W = 8\
H = 8\
X = 48\
Y = 32\
W = 16\
H = 16\
X = 8\
Y = 0\
W = 64\
H = 16\
The call would be: real(4,6,3,10,10,8,8,15,15,8,8,48,32,16,16,8,0,64,16\
And the result would be:\
Ans = 1\
XL = {1,2,0,2\
Ans=1 = collision found\
{1 = collision found (would be 0 if no collisions occured)\
2 = number of collisions found\
0 = collision with index 0\
2 = collision with index 2\
Index 1 is the square for which there was no collision.

You can check if there is a collision by using:\
If Ans\
If LXL(1)\
Or by a similar method.

### DrawSpriteCheckCollisionB (USERVAR VALUES, *DCSE8.1*) {#drawspritecheckcollisionb_uservar_values_dcse8.1}

**real(4,7,COUNT,X,Y,W,H,CX0,CY0,CW0,CH0....CXn,CYn,CWn,CHn**\
COUNT = number of coordinates to check against\
X = master X to test list of coordinates against (uservar)\
Y = master Y to test list of coordinates against (uservar)\
W = master W to test list of coordinates against (uservar)\
H = master H to test list of coordinates against (uservar)\
CX0 = first X to test against master X (uservar)\
CY0 = first Y to test against master Y (uservar)\
CW0 = first W to test against master W (uservar)\
CH0 = first H to test against master H (uservar)\
CXn = nth X to test against master X (uservar)\
CYn = nth Y to test against master Y (uservar)\
CWn = nth W to test against master W (uservar)\
CHn = nth H to test against master H (uservar)\
This function is the same as above however it takes references to
USERVAR values as opposed to values directly.

### DrawSpriteSequentialListA (TIOS VALUES, *DCSE8.1*) {#drawspritesequentiallista_tios_values_dcse8.1}

**real(4,8,X,Y,WIDTH,HEIGHT,XOFFSET,YOFFSET,TRANSINDEX,UPDATELCD,PICINDEXSTART,PICINDEX0\
** X = x value\
Y = y value\
WIDTH = width of sprite in 8x8 chunks (an 8x8 sprite is 1, 16x16 is 2,
12x12 is also 2 etc)\
HEIGHT = height of sprite in 8x8 chunks (an 8x8 sprite is 1, 16x16 is 2,
12x12 is also 2 etc)\
XOFFSET = offset for x value\
YOFFSET = offset for y value\
TRANSINDEX = transparent colour index, and colour in the sprite that
matches this will be drawn transparent (0-255)\
UPDATELCD = 0/1 to update LCD after drawing\
PICINDEXSTART = pic index to start drawing from (in following list)\
PICINDEX0 = pic index in sprite data sheet\
This function will draw a sprite that of any size as per the same
fashion as DRAWSPRITEA with the only difference being that you DONT need
to specify each PICINDEX for a largesprite, rather you only need to
specify the FIRST PICINDEX. This means that your 8x8 sprite chunks will
need to follow each other in your TILEPIC in SEQUENTIAL ORDER. The
makeup of a large sprite is the same:

16x16 sprite list layout:\

    ---------
    | 1 | 3 |
    ---------
    | 2 | 4 |
    ---------

24x24 sprite list layout:\

    -------------
    | 1 | 4 | 7 |
    -------------
    | 2 | 5 | 8 |
    -------------
    | 3 | 6 | 9 |
    -------------

For both of the above you only need to specifiy the PICINDEX '1' (along
with appropriate WIDTH/HEIGHT ETC) to draw.

The advantage is that you save space in BASIC code, speed of execution
and you can maximise the space in your TILEPICS. The drawback is that
the layout of sprites requires more work when creating your TILEPICS.

You can use the PICINDEXSTART argument to have largesprite 'frames' as
each PICINDEX you supply will be the STARTING INDEX for each frame so
having:\
real(4,8,10,10,2,2,0,0,248,1,0,10,20

Will draw a 16x16 sprite with the 4 '8x8 chunks' starting at INDEX 10
(10,11,12,13). If you change the PICINDEXSTART argument to 1 then the
16x16 sprite will be made up of the 4 '8x8 chunks' starting at INDEX 20
(20,21,22,23).

### DrawSpriteSequentialListB (USERVAR VALUES, *DCSE8.1*) {#drawspritesequentiallistb_uservar_values_dcse8.1}

**real(4,9,USERVAR_X,USERVAR_Y,WIDTH,HEIGHT,XOFFSET,YOFFSET,TRANSINDEX,UPDATELCD,PICINDEXSTART,PICINDEX0\
** X = x value (uservar 0-255)\
Y = y value (uservar 0-255)\
WIDTH = width of sprite in 8x8 chunks (an 8x8 sprite is 1, 16x16 is 2,
12x12 is also 2 etc)\
HEIGHT = height of sprite in 8x8 chunks (an 8x8 sprite is 1, 16x16 is 2,
12x12 is also 2 etc)\
XOFFSET = offset for x value\
YOFFSET = offset for y value\
TRANSINDEX = transparent colour index, and colour in the sprite that
matches this will be drawn transparent (0-255)\
UPDATELCD = 0/1 to update LCD after drawing\
PICINDEXSTART = pic index to start drawing from (in following list)\
PICINDEX0 = pic index in sprite data sheet\
This function is the same as above however it takes references to
internal USERVARS as opposed to values directly\

### DrawSpriteTileBGListA (TIOS VALUES, *DCSE8.1*) {#drawspritetilebglista_tios_values_dcse8.1}

**real(4,10,LISTCOUNT,LISTWIDTH,HEIGHT,XOFFSET,YOFFSET,TRANSINDEX,UPDATELCD,MAPWIDTH,MAPSTRING,X0,Y0...Xn,Yn\
** LISTCOUNT = number of tiles in list\
WIDTH = width of sprite in 8x8 chunks (an 8x8 sprite is 1, 16x16 is 2,
12x12 is also 2 etc)\
HEIGHT = height of sprite in 8x8 chunks (an 8x8 sprite is 1, 16x16 is 2,
12x12 is also 2 etc)\
XOFFSET = offset for x value\
YOFFSET = offset for y value\
TRANSINDEX = transparent colour index, and colour in the sprite that
matches this will be drawn transparent (0-255)\
UPDATELCD = 0/1 to update LCD after drawing\
MAPWIDTH = width of tilemap\
MAPSTRING = string variable holding tilemap data (0-10)\
X0 = first X value to draw tile at\
Y0 = first Y value to draw tile at\
Xn = last X value to draw tile at\
Yn = last Y value to draw tile at\
This function will draw the tiles for width\*height at the sprite
coordinate listed from X0,Y0 to Xn,Yn. The resultant tiles will be
aligned to the map (it will only draw at intevals of 8-pixels). This is
useful for restoring a tilemap background that has been overwritten by a
list of sprites. As mentioned this function is the same as
DRAWSPRITETILEBG just with a list.

### DrawSpriteTileBGListB (USERVARS VALUES, *DCSE8.1*) {#drawspritetilebglistb_uservars_values_dcse8.1}

**real(4,11,LISTCOUNT,LISTWIDTH,HEIGHT,XOFFSET,YOFFSET,TRANSINDEX,UPDATELCD,MAPWIDTH,MAPSTRING,X0,Y0...Xn,Yn\
** LISTCOUNT = number of tiles in list\
WIDTH = width of sprite in 8x8 chunks (an 8x8 sprite is 1, 16x16 is 2,
12x12 is also 2 etc)\
HEIGHT = height of sprite in 8x8 chunks (an 8x8 sprite is 1, 16x16 is 2,
12x12 is also 2 etc)\
XOFFSET = offset for x value\
YOFFSET = offset for y value\
TRANSINDEX = transparent colour index, and colour in the sprite that
matches this will be drawn transparent (0-255)\
UPDATELCD = 0/1 to update LCD after drawing\
MAPWIDTH = width of tilemap (uservar 0-255)\
MAPSTRING = string variable holding tilemap data (0-10)\
X0 = first X value to draw tile at (uservar 0-255)\
Y0 = first Y value to draw tile at (uservar 0-255)\
Xn = last X value to draw tile at (uservar 0-255)\
Yn = last Y value to draw tile at (uservar 0-255)\
This function is the same as above how it takes references to internal
USERVARS as opposed to direct values in the call
