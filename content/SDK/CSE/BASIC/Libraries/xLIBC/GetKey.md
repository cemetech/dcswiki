---
revisions:
- author: Tr1p1ea
  timestamp: '2014-07-08T04:16:57Z'
title: GetKey
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Description

These routines have been significantly overhauled from the monochrome
xLIB getKey function.

## Technical Details {#technical_details}

### GetKey

**real(2,0,0)**: Key code stored to Ans (as per \_getCSC codes)

### GetKeyCheckList

**real(2,0,GETKEY_CHECKNUM,GETKEY_KEYVAL,GETKEY_USERVAR,GETKEY_VALUE...etc)**:\
GETKEY_CHECKNUM = number of checks to perform (length of list to check
against)\
GETKEY_KEYVAL = key value to check for\
GETKEY_USERVAR = Uservar to update if key value is pressed\
GETKEY_VALUE = value to update Uservar by if key is pressed

Checks for a list of keypresses and updates the appropriate user
variables

### GetKeyArrows

**real(2,1,USERVAR_X,USERVAR_Y,VALUE_X,VALUE_Y)**:\
USERVAR_X = user variable index holding x coordinate to update (0-255)\
USERVAR_Y = user variable index holding y coordinate to update (0-255)\
VALUE_X = amount to update uservar by if left/right is pressed\
VALUE_Y = amount to update uservar by if up/down is pressed

Checks for up,down,left,right arrows and updates the specified user
variables:

-   If up is pressed then USERVAR_Y = USERVAR_Y - VALUE_Y
-   If down is pressed then USERVAR_Y = USERVAR_Y + VALUE_Y
-   If left is pressed then USERVAR_X = USERVAR_X - VALUE_X
-   If right is pressed then USERVAR_X = USERVAR_X + VALUE_X

### GetKeyArrowsDiagonals

**real(2,2,USERVAR_X,USERVAR_Y,VALUE_X,VALUE_Y)**:

Checks for up,down,left,right and diagonal arrows and updates the
specified user variables:

-   If up is pressed then USERVAR_Y = USERVAR_Y - VALUE_Y
-   If down is pressed then USERVAR_Y = USERVAR_Y + VALUE_Y
-   If left is pressed then USERVAR_X = USERVAR_X - VALUE_X
-   If right is pressed then USERVAR_X = USERVAR_X + VALUE_X
-   If up+left is pressed then USERVAR_Y = USERVAR_Y - VALUE_Y,
    USERVAR_X = USERVAR_X - VALUE_X
-   If up+right is pressed then USERVAR_Y = USERVAR_Y - VALUE_Y,
    USERVAR_X = USERVAR_X + VALUE_X
-   If down+left is pressed then USERVAR_Y = USERVAR_Y + VALUE_Y,
    USERVAR_X = USERVAR_X - VALUE_X
-   If down+right is pressed then USERVAR_Y = USERVAR_Y + VALUE_Y,
    USERVAR_X = USERVAR_X + VALUE_X

### GetKeyArrowsCheckTile

**real(2,3,USERVAR_X,USERVAR_Y,VALUE_X,VALUE_Y,USERVAR_MAPWIDTH,COLLISIONTILE,MAPSTRING,X0,Y0,X1,Y1)**:\
USERVAR_MAPWIDTH = width of tilemap in tiles (uservar 0-255)\
COLLISIONTILE = upper limit of walkable tiles (any tile less than this
will be walkable)\
MAPSTRING = string variable holding tilemap data (0-10)\
X0 = left x coordinate of collision box\
Y0 = top y coordinate of collision box\
X1 = right x coordinate of collision box\
Y1 = bottom y coordinate of collision box\
This performs the same as the above GetKeyArrows function but will only
update the user variables if the move is to a walkable tile in the
supplied tilemap. The X0,Y0,X1,Y1 is used to specify a 'collision box'
around the x/y coordinate. Anything inside this coordinate must be
walkable for the function to update the user variables.

### GetKeyArrowsDiagonalsCheckTile

**real(2,4,USERVAR_X,USERVAR_Y,VALUE_X,VALUE_Y,USERVAR_MAPWIDTH,COLLISIONTILE,MAPSTRING)**:

Refer to the GetKeyArrowsCheckTile function; this adds diagonal
keypresses as well.

### GetKeyArrowsCheckTileList *(DCSE8.1)* {#getkeyarrowschecktilelist_dcse8.1}

**real(2,5,USERVAR_X,USERVAR_Y,VALUE_X,VALUE_Y,USERVAR_MAPWIDTH,COLLISIONTILE,MAPSTRING,X0,Y0,X1,Y1**\
USERVAR_MAPWIDTH = width of tilemap in tiles (uservar 0-255)\
COLLISIONTILE = upper limit of walkable tiles (any tile less than this
will be walkable)\
MAPSTRING = string variable holding tilemap data (0-10)\
X0 = left x coordinate of collision box\
Y0 = top y coordinate of collision box\
X1 = right x coordinate of collision box\
Y1 = bottom y coordinate of collision box\
This function is the same as the above however it will return
information regarding any keypresses and any collided tiles in a 'real
list' contained in a user-defined list named "XL" with the format:

{KEY_PRESS, NUM_COLLIDED_TILES, COLLIDED_TILES_LIST} where:

KEY_PRESS = -1,0,1,2,3 = NOARROW,UP,DOWN,LEFT,RIGHT\
NUM_COLLIDED_TILES = number of tiles collided against given the
arguments in the call\
COLLIDED_TILES_LIST = list of tiles collided against given the arguments
in the call\
The KEY_PRESS value is also stored to Ans for easy reference\

### GetKeyArrowsDiagonalsCheckTileList *(DCSE8.1)* {#getkeyarrowsdiagonalschecktilelist_dcse8.1}

**real(2,6,USERVAR_X,USERVAR_Y,VALUE_X,VALUE_Y,USERVAR_MAPWIDTH,COLLISIONTILE,MAPSTRING,X0,Y0,X1,Y1**\
refer to the above function, adds diagonal keypresses as well
