---
revisions:
- author: Merthsoft
  comment: /\* GetRand (TI-OS Values) \*/
  timestamp: '2013-11-19T01:36:47Z'
title: xLIBCUtility
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Technical Details {#technical_details}

### GetLCDBuffer (TI-OS Values): {#getlcdbuffer_ti_os_values}

**real(8,0):** This function returns 0/1 = which side of the GRAM buffer
to mark as active for drawing in Ans

### SetLCDBuffer (TI-OS Values): {#setlcdbuffer_ti_os_values}

**real(8,1,VALUE):** VALUE = 0/1 which side if the GRAM buffer to mark
as active for drawing

### SetGRAMOffset (TI-OS Values) {#setgramoffset_ti_os_values}

*real(8,2,VALUE):*'\
VALUE = value to offset the LCD by

This function will offset the LCD by VALUE. This is useful for screen
shaking effects etc.

### GetRand (TI-OS Values) {#getrand_ti_os_values}

**real(8,3,VALUE):**\
VALUE = (0-255) upper bound of random number

This function returns a random integer between 0-VALUE exclusive in Ans
