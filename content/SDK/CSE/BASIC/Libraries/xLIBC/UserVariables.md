---
revisions:
- author: Tr1p1ea
  timestamp: '2013-11-04T00:24:27Z'
title: UserVariables
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Description

The TI-84+CSE version of xLIB has some key differences from its previous
counterparts. One of the most obvious ones is the facility to utilize
'internal user variables' for data storage and calculations as opposed
to using TI-OS variables. Note that most functions have versions
(usually version 'A' (like 'DRAWMAPA' for example) still take TI-OS
variables as arguments. These user vars help xLIBC update data in the
functions themselves and can save calculation time in your program.

IMPORTANT - User variables are only temporary and are stored at
PlotsScreen (RAM: \$987C). Please ensure that you are not using this
memory area while executing your program as it may produce unexpected
results.

## Technical Details {#technical_details}

### GetUservar

**real(1,0,Uservar_Num)**: Get the value of the given Uservar

This function returns the value stored in Uservar_Num to Ans

### SetUservar

**real(1,1,Uservar_Num,Value)**: Set the value of the given Uservar to
Value

This function writes Value to Uservar_Num and returns the Uservar_Num
that was modified to Ans

### AddToUservar

**real(1,2,Uservar_Num,Value)**: Add Value to the value of the given
Uservar

This function adds Value to Uservar_Num and returns the Uservar_Num that
was modified to Ans

### SubFromUservar

**real(1,3,Uservar_Num,Value)**: Subtract Value from the value of the
given Uservar

This function subtracts Value to Uservar_Num and returns the Uservar_Num
that was modified to Ans
