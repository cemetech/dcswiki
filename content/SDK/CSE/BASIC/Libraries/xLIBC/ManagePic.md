---
revisions:
- author: jacobly
  comment: Fixed typo.
  timestamp: '2017-03-06T19:27:36Z'
title: ManagePic
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Technical Details {#technical_details}

### LoadTilePic (TI-OS Values) {#loadtilepic_ti_os_values}

**real(5,0,PICSLOT)**:\
Ans = AppVar name (as string, no rowSwap( prefix)\
PICSLOT = 0/1 pic slot (temp RAM) to load tilepic into (slot0=tiles
0-127, slot1=tiles128-255)

This function loads custom tilepic image appvars (name stored as a
string in ANS) into temp RAM (slot0/1). These images are 128x64 pixels
using the custom xLIB palette. They each hold 128 \* 8x8 tiles.

picslot0 = tiles loaded into 0-127\
picslot1 = tiles loaded into 128-255

You must load a tilepic into temp RAM before using DRAWMAP or DRAWSPRITE

EXAMPLE: To load a tilepic named "TESTTILE" into slot0:

    :"TESTTILE
    :real(5,0,0

### LoadBGPic (TI-OS Values) {#loadbgpic_ti_os_values}

**real(5,1,BGSLOT)**:\
Ans = AppVar name (as string, no rowSwap( prefix)\
BGSLOT = 0/1 bg slot (temp RAM) to load bgpic into

This function loads custom bgpic image appvars (name stored as a string
in ANS) into temp RAM (slot0/1). These images are 80x60 pixels using the
custom xLIB palette. These images are low resolution and can be useful
for low detail backgrounds.

### DisplayBGPic (TI-OS Values) {#displaybgpic_ti_os_values}

**real(5,2,UPDATELCD)**:\
Ans = AppVar name (as string, no rowSwap( prefix)\
UPDATELCD = 0/1 to update LCD after drawing

This function will display a custom bgpic image appvar (name stored as a
string in ANS).

To display a bgpic named "BGTEST":

    :"BGTEST
    :real(5,2,1

### DrawPicSectionA (TI-OS Values) {#drawpicsectiona_ti_os_values}

**real(5,3,X,Y,WIDTH,HEIGHT,BGSLOT,UPDATELCD)**:\
X = source X in bgpic\
Y = source Y in bgpic\
WIDTH = width of section to draw\
HEIGHT = height of section to draw\
BGSLOT = source BGPIC slot 0/1\
UPDATELCD = 0/1 to update LCD after drawing\
This function will draw a section of a BGPIC to a specified location on
the screen.

You must have loaded a BGPIC into temp RAM before using this function.

### DrawPicSectionB (Uservar Values) {#drawpicsectionb_uservar_values}

**real(5,4,X,Y,WIDTH,HEIGHT,BGSLOT,UPDATELCD)**:\
X = source X in bgpic\
Y = source Y in bgpic\
WIDTH = width of section to draw\
HEIGHT = height of section to draw\
BGSLOT = source BGPIC slot 0/1\
UPDATELCD = 0/1 to update LCD after drawing\
This function is the same as above but takes user variables as arguments
instead.

### LoadSingleTile(TI-OS Values) {#loadsingletileti_os_values}

**real(5,5,TILEID)**:\
Ans = Tile data\
TILEID = id of tile to write data to (0-255)\
This function will write data from a string stored in ANS to the
specified TILEID in TEMPRAM.

Tile data in ANS is stored as a 'HEX encoded string' (2 bytes per pixel)
where each HEX pair represents a colour index in the standard xLIB
palette.

Tile pixel data is arraged in columns and from left to right like so:

    00 08 16 24 32 40 48 56
    01 09 17 25 33 41 49 57
    02 10 18 26 34 42 50 58
    03 11 19 27 35 43 51 59
    04 12 20 28 36 44 52 60
    05 13 21 29 37 45 53 61
    06 14 22 30 38 46 54 62
    07 15 23 31 39 47 55 63

So the string in ANS would look like:

"00010203040506070809101112131415161718192021222324252627282930313233343536373839404142434445464748495051525354555657585960616263

This way you can manually write tile/sprite data to TEMPRAM without the
need for TILEPIC's.

Note that tiles are 8x8 pixels in size.

### LoadBGPic32 (TI-OS Values, *DCSE8.1*) {#loadbgpic32_ti_os_values_dcse8.1}

**:"APPVARNAME**\
**real(5,6)**:\
Ans = AppVar name (as string, no rowSwap( prefix)\
This function loads custom bgpic32 image appvars (name stored as a
string in ANS) into temp RAM. These images are 160x120 pixels using 32
colours out of the custom xLIB palette. These images can be useful for
low detail backgrounds.

Note that this function will only be useful with future releases

### DisplayBGPic32 (TIOS VALUES, *DCSE8.1*) {#displaybgpic32_tios_values_dcse8.1}

**:"APPVARNAME**\
**real(5,7,UPDATELCD**\
UPDATELCD = 0/1 to update LCD after drawing\
This function will display a custom 32 colour 160x120 image (scaled to
fullscreen) appvar (name stored as a string in ANS).

To display a bgpic named "BG32TEST":\
:"BG32TEST\
:real(5,7,1\
