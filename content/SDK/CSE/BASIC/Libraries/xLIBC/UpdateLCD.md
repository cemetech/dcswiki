---
revisions:
- author: Tr1p1ea
  timestamp: '2013-11-17T02:59:51Z'
title: UpdateLCD
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Technical Details {#technical_details}

**real(9)**\
This function updates the LCD. Effectively it switches positions in LCD
GRAM.
