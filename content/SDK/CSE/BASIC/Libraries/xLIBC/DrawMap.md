---
revisions:
- author: Tr1p1ea
  timestamp: '2014-06-11T01:37:52Z'
title: DrawMap
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Description

Draw a tilemap. Tile data is stored in strings as hex encoded ASCII. For
example, a 4x4 tilemap array (base 10 numbers):

    [01,01,01,01]
    [01,95,95,01]
    [01,44,44,01]
    [01,01,01,01]

would translate to:

`"01010101015F5F01012C2C0101010101"->Str0`

**NOTE:** You must load tiledata into temp memory before drawing
tilemaps. See
[DCSE:BasicLibs:ManagePic]({{< ref "ManagePic.md" >}}).

## Technical Details {#technical_details}

### DrawMapA (TI-OS Values) {#drawmapa_ti_os_values}

**real(3,0,X,Y,MAPWIDTH,MAPSTRING,XSTART,YSTART,XEND,YEND,UPDATELCD)**:\
X = map x in tiles\
Y = map y in tiles\
MAPWIDTH = width of tilemap\
MAPSTRING = string variable holding tilemap data (0-10)\
XSTART = start x tile position on LCD to draw from (0-19)\
YXSTART = start y tile position on LCD to draw from (0-14)\
XEND = end x tile position on LCD to draw from (0-19)\
YEND = end y tile position on LCD to draw from (0-14)\
UPDATELCD = 0/1 to update LCD after drawing

Example: To draw a tilemap starting at 0,0 in a 32x32 tilemap and drawn
with a 1 tile border around the outside (18x12 tiles) with tiledata
stored in TIOS string 9:

`real(3,0,0,0,32,9,1,1,18,13,1`

### DrawMapB (Uservar Values) {#drawmapb_uservar_values}

**real(3,1,USERVAR_X,USERVAR_Y,USERVAR_MAPWIDTH,MAPSTRING,XSTART,YSTART,XEND,YEND,UPDATELCD)**:\
USERVAR_X = map x in tiles (uservar 0-255)\
USERVAR_Y = map y in tiles (uservar 0-255)\
USERVAR_MAPWIDTH = width of tilemap in tiles (uservar 0-255)\
MAPSTRING = string variable holding tilemap data (0-10)\
XSTART = start x tile position on LCD to draw from (0-19)\
YXSTART = start y tile position on LCD to draw from (0-14)\
XEND = end x tile position on LCD to draw from (0-19)\
YEND = end y tile position on LCD to draw from (0-14)\
UPDATELCD = 0/1 to update LCD after drawing

### DrawMap_GetTileA (TI-OS Values) {#drawmap_gettilea_ti_os_values}

**real(3,2,X,Y,MAPWIDTH,MAPSTRING,XOFFSET,YOFFSET)**:\
X = map x in PIXELS\
Y = map y in PIXELS\
MAPWIDTH = width of tilemap\
MAPSTRING = string variable holding tilemap data (0-10)\
XOFFSET = x offset for X value\
YOFFSET = y offset for Y value

Returns tile at map(X,Y) in Ans

### DrawMap_GetTileB (Uservar Values) {#drawmap_gettileb_uservar_values}

**real(3,3,USERVAR_X,USERVAR_Y,USERVAR_MAPWIDTH,MAPSTRING,XOFFSET,YOFFSET)**:\
X = map x in PIXELS (uservar 0-255)\
Y = map y in PIXELS (uservar 0-255)\
MAPWIDTH = width of tilemap (uservar 0-255)\
MAPSTRING = string variable holding tilemap data (0-10)\
XOFFSET = x offset for X value\
YOFFSET = y offset for Y value

Returns tile at map(X,Y) in Ans

### DrawMap_SetTile (TI-OS Values) {#drawmap_settile_ti_os_values}

**real(3,4,X,Y,MAPWIDTH,MAPSTRING,TILEID,XOFFSET,YOFFSET)**:\
X = map x in tiles\
Y = map y in tiles\
MAPWIDTH = width of tilemap\
MAPSTRING = string variable holding tilemap data (0-10)\
TILEID = index of tile to write into map\
XOFFSET = x offset for X value\
YOFFSET = y offset for Y value

Sets tile at map(x,y) in tilemap string(0-10)

### DrawMap_ReplaceTile (TI-OS Values) {#drawmap_replacetile_ti_os_values}

**real(3,5,MAPSTRING,CHECKNUM,TILEID0,REPLACETILID0,TILEID1,REPLACETILID1...etc)**:\
MAPSTRING = string variable holding tilemap data (0-10)\
CHECKNUM = number of checks to perform (length of list to replace)\
TILEID0 = 1st tile id to search for\
REPLACETILEID0 = tile to replace 1st tile id if found

Replaces all occurrences of tileidX with replacetileidX for CHECKNUM

### DrawMap_GetSectionA (TI-OS Values) {#drawmap_getsectiona_ti_os_values}

**real(3,6,VALUE_XY,SECTIONSIZE)**:\
VALUE_XY = x/y value in \*\*\* pixels \*\*\*\
SECTIONSIZE = section width/height in tiles

This function will give you the section in multiples of SECTIONSIZE
depending on VALUE_XY stored to Ans. It effectively performs
int(VALUE_XY / (SECTIONSIZE \* 8)) \* SECTIONSIZE. This is useful for
drawing different sections of a tilemap depending on x/y values

### DrawMap_GetSectionB (USERVAR Values) {#drawmap_getsectionb_uservar_values}

**real(3,7,VALUE_XY,SECTIONSIZE)**:\
VALUE_XY = x/y value in \*\*\* pixels \*\*\* (uservar 0-255)\
SECTIONSIZE = section width/height in tiles (tios value)

This function is the same as above, however it references an internal
uservar for the VALUE_XY argument

### DrawMap_TestSectionA (TIOS VALUES, *DCSE8.1*) {#drawmap_testsectiona_tios_values_dcse8.1}

**real(3,6,VALUE_XY,SECTIONSIZE,TESTVALUE**\
VALUE_XY = x/y value in \*\*\* pixels \*\*\*\
SECTIONSIZE = section width/height in tiles\
TESTVALUE = value to conditionally test against, usually section
width/height in tiles\
This function will give you the result of a comparison between:\
the section in multiples of SECTIONSIZE depending on VALUE_XY as per
int(VALUE_XY / (SECTIONSIZE \* 8)) \* SECTIONSIZE\
and TESTVALUE\
The result is stored in Ans where:

0 = (int(VALUE_XY / (SECTIONSIZE \* 8)) \* SECTIONSIZE) - TESTVALUE = 0\
1 = (int(VALUE_XY / (SECTIONSIZE \* 8)) \* SECTIONSIZE) - TESTVALUE \<\>
0\
This is useful for deciding when to draw a different section of a
tilemap depending on x/y values. You would use it to see if a set of X/Y
coordinates is outside of the current 'map drawing window'

### DrawMap_TestSectionB (USERVAR VALUES, *DCSE8.1*) {#drawmap_testsectionb_uservar_values_dcse8.1}

**real(3,6,VALUE_XY,SECTIONSIZE,TESTVALUE**\
VALUE_XY = x/y value in \*\*\* pixels \*\*\* (uservar 0-255)\
SECTIONSIZE = section width/height in tiles\
TESTVALUE = value to conditionally test against, usually section
width/height in tiles (uservar 0-255)\
This function is the same as above, however it references a internal
uservars for the VALUE_XY and TESTVALUE arguments
