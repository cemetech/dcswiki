---
revisions:
- author: Tr1p1ea
  timestamp: '2013-11-06T04:52:11Z'
title: DrawString
---

This routine is only available on Doors CSE 8.0 and later for the
color-screen TI-84 Plus C Silver Edition.

## Description

This function draws a coloured string with a transparent background
using the custom xLIBC palette, in the custom xLIBC 8x8 font. The
NEWLINECHAR code can be used kind of like a carriage return (X is reset
and Y=Y+8). The string data is stored in Ans.

## Technical Details {#technical_details}

### DrawString (TI-OS Values) {#drawstring_ti_os_values}

**real(6,0,X,Y,COLOUR,NEWLINECHAR,UPDATELCD)**:\
Ans = Input string\
X = x value\
Y = y value\
COLOUR = colour index (standard xLIB palette)\
NEWLINECHAR = ascii character code that indicates a newline. String
moved down 1 line and x is reset\
UPDATELCD = 0/1 to update LCD after drawing

Note that the NEWLINE char is the ASCII character code for the character
that you want to use. For example the '!' character is ASCII code 33.

This function draws a coloured string with a transparent background
using the custom xLIB palette, in the custom xLIB 8x8 font. The
NEWLINECHAR code can be used kind of like a carriage return (X is reset
and Y=Y+8). The string data is stored in ANS.

### DrawStringValueA (TI-OS Values) {#drawstringvaluea_ti_os_values}

**real(6,1,X,Y,COLOUR,VALUE,UPDATELCD)**:\
X = x value\
Y = y value\
COLOUR = colour index (standard xLIB palette)\
VALUE = value to display\
UPDATELCD = 0/1 to update LCD after drawing\
This function draws a coloured string of VALUE in the same fashion as
DRAWSTRING. VALUE is also returned as a string in Ans.

IMPORTANT - VALUE must range from -9999 to 9999 otherwise the TIOS will
throw a DIM error.

### DrawStringValueB (Uservar Values) {#drawstringvalueb_uservar_values}

**real(6,2,X,Y,COLOUR,VALUE,UPDATELCD)**:\
X = x value\
Y = y value\
COLOUR = colour index (standard xLIB palette)\
VALUE = uservar holding value to display\
UPDATELCD = 0/1 to update LCD after drawing\
This function is the same as above but takes a Uservar reference for
VALUE instead.
