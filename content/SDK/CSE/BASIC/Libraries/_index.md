---
revisions:
- author: KermMartian
  comment: /\* Celtic Libraries \*/
  timestamp: '2016-09-07T16:50:03Z'
title: Libraries
bookCollapseSection: true
aliases:
- /Third-Party_BASIC_Libraries_(Color)/
---

Doors CSE and Doors CE offer compatibility for all the major TI-84 Plus
C Silver Edition and TI-84 Plus CE BASIC libraries, including Celtic 2
CSE/CE and xLIBC/xLIBCE. If you need to detect whether the Doors
CSE/CE-provided Hybrid BASIC libraries are present (which you should do
in any program or game that uses them), we recommend this format:

    ::DCS
    :"Icon data...
    :If 80>det([[20
    :Then
    :Disp "Get Doors CSE/CE to run:","http://dcs.cemetech.net
    :Return
    :End

det(\[\[20\]\]) will equal 80 for Doors CSE 8.0, 81 for Doors CSE 8.1,
82 for Doors CSE 8.2, 90 for Doors CE 9.0, 91 for Doors CE 9.1, and so
on. You can use the value to make sure the user has a minimum required
Doors CSE or Doors CE version on their calculator.

### Celtic Libraries {#celtic_libraries}

The Celtic libraries are converted from Celtic 2 CSE/CE, itself a port
of Celtic 2 for the TI-83 Plus/TI-84 Plus by Iambian Zenith. Iambian
wrote det(0) through det(8), while Kerm Martian wrote det(9) and det(10)
and debugged the remaining functions. det(0) through det(12) are
available from Doors CSE 8.0 to 8.2. All functions are available from
Doors CE 9.0 onwards.

Most of the Celtic 2 CSE/CE functions take arguments in Str9, Str0, and
Ans, and output to Str9 and/or Str0. To use an AppVar instead of a
program as input, prepend the string with the "rowSwap(" token.

**[ReadLine]({{< ref "./Celtic/ReadLine.md" >}})** - det(0),
Str0=program name, Ans=line number\
**[ReplaceLine]({{< ref "./Celtic/ReplaceLine.md" >}})** - det(1),
Str0=program name, Ans=line number, Str9=replacement\
**[InsertLine]({{< ref "./Celtic/InsertLine.md" >}})** - det(2),
Str0=program name, Ans=line number, Str9=contents\
**[SpecialChars]({{< ref "./Celtic/SpecialChars.md" >}})** -
det(3)\
**[CreateVar]({{< ref "./Celtic/CreateVar.md" >}})** - det(4),
Str0=program/AppVar name\
**[ArcUnarcVar]({{< ref "./Celtic/ArcUnarcVar.md" >}})** - det(5),
Str0=program/AppVar name\
**[DeleteVar]({{< ref "./Celtic/DeleteVar.md" >}})** - det(6),
Str0=program/AppVar name\
**[DeleteLine]({{< ref "./Celtic/DeleteLine.md" >}})** - det(7),
Str0=program name, Ans=line number\
**[VarStatus]({{< ref "./Celtic/VarStatus.md" >}})** - det(8),
Str0=program name\
**[BufSprite]({{< ref "./Celtic/BufSprite.md" >}})** -
det(9,width,X,Y), Str9=sprite data\
**[BufSpriteSelect]({{< ref "./Celtic/BufSpriteSelect.md" >}})** -
det(10,width,X,Y,start,length), Str9=sprite data\
**[ExecArcPrgm]({{< ref "./Celtic/ExecArcPrgm.md" >}})** -
det(11,FN,NUMBER)\
**[DispColor]({{< ref "./Celtic/DispColor.md" >}})** -
det(12,FG_LO,FG_HI,BG_LO,BG_HI) or det(12,FG_OS,BG_OS)\
**[ToString]({{< ref "./Celtic/ToString.md" >}})** - det(13)
(Doors CE 9.0+ only)\
**[ExecHex]({{< ref "./Celtic/ExecHex.md" >}})** - det(14) (Doors
CE 9.0+ only)\
**[TextRect]({{< ref "./Celtic/TextRect.md" >}})** - det(15)
(Doors CE 9.0+ only)\

### xLIBC/xLIBCE Libraries {#xlibcxlibce_libraries}

xLIBC/xLIBCE is written by Patrick Prendergast, aka tr1p1ea. It is based
on the concepts used to create xLIB for the TI-83 Plus/TI-84 Plus,
included in Doors CS 7.2 for the monochrome calculators. xLIBC uses the
TI-84 Plus C Silver Edition and TI-84 Plus CE in half-resolution
(160x120-pixel) mode, allowing one half of the LCD buffer to be modified
off-screen while the other half is displayed. tr1p1ea has written an
[xLIBC_Tutorial]({{< ref "xLIBC_Tutorial.md" >}}) with an example
program. PT\_ and Unicorn are working on
[XLIBC_For_Beginners]({{< ref "XLIBC_For_Beginners.md" >}}) to help
TI-BASIC programmers learn to use xLIBC.

**[xLIBCSetup]({{< ref "./xLIBC/xLIBCSetup.md" >}})** -
real(0,FN,Value)\
**[UserVariables]({{< ref "./xLIBC/UserVariables.md" >}})** -
real(1,FN,Uservar_Num\[,Value\])\
**[GetKey]({{< ref "./xLIBC/GetKey.md" >}})** -
real(2,FN\[,Args\])\
**[DrawMap]({{< ref "./xLIBC/DrawMap.md" >}})** -
real(3,FN\[,Args\])\
**[DrawSprite]({{< ref "./xLIBC/DrawSprite.md" >}})** -
real(4,FN\[,Args\])\
**[ManagePic]({{< ref "./xLIBC/ManagePic.md" >}})** -
real(5,FN\[,Args\])\
**[DrawString]({{< ref "./xLIBC/DrawString.md" >}})** -
real(6,FN\[,Args\])\
**[DrawShape]({{< ref "./xLIBC/DrawShape.md" >}})** -
real(7,FN\[,Args\])\
**[xLIBCUtility]({{< ref "./xLIBC/xLIBCUtility.md" >}})** -
real(8,FN,Val)\
**[UpdateLCD]({{< ref "./xLIBC/UpdateLCD.md" >}})** - real(9)\
