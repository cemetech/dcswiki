---
revisions:
- author: KermMartian
  timestamp: '2013-11-04T20:16:56Z'
title: Program Header
weight: 10
aliases:
- /BASIC_Header_(Color)/
---

Doors CSE 8 and above automatically recognizes all BASIC programs as
BASIC programs. However, if you want your programs to be a bit more
spiffy, Doors CSE offers the ability to add icons, and you can specify
that certain BASIC programs, like support subprograms, be hidden from
the Doors CSE desktop. The header formats below explain each of these
features.

## Icons

You can get more info about how icons are made
[here]({{< ref "/SDK/Icons/index.md" >}}). If you want an automated program to
let you draw 16x16 icons that will give you the hex code, I recommend
[TIFreak8x's Doors CSE 8 Icon
Creator](http://www.cemetech.net/programs/index.php?mode=file&path=/84pcse/basic/programs/dcs8ic.zip).

### 16x16 Icon {#x16_icon}

The hex digits in the following icon match the OS color palette, where
1=blue, 2=red, 3=black, and so on.

    ::DCS
    :"256-char_hex_icon
    :Program code

## Hybrid TI-BASIC {#hybrid_ti_basic}

If you wish to use xLIBC and Celtic 2 CSE functions (detailed
[here]({{< ref "Libraries.md" >}})), you
should use the following header:

    ::DCS
    :"256-char_hex_icon
    :If 80>det([[20
    :Then
    :Disp "Get Doors CSE to run this:","http://dcs.cemetech.net
    :Return
    :End

det(\[\[20\]\]) will equal 80 for Doors CSE 8.0, 81 for Doors CSE 8.1,
82 for Doors CSE 8.2, 90 for Doors CSE 9.0, and so on. You can use the
value to make sure the user has a minimum required Doors CSE version on
his or her calculator.

## Ignore Program {#ignore_program}

    :rand
    :Program code

or

    :Ans
    :Program code
