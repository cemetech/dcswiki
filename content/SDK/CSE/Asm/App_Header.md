---
revisions:
- author: KermMartian
  timestamp: '2015-05-05T19:19:10Z'
title: App Header
weight: 11
aliases:
- /App_Header_(Color)/
---

Doors CSE 8.1 and above can list and execute standard TI-OS Applications
(Apps). While Doors CSE 8 can display unmodified Apps, you can also give
your Apps special header fields that Doors CSE will recognize, and which
will not interfere with the TI-OS running your App.

## Header Fields {#header_fields}

Doors CSE 8.1 recognizes a single header field, containing a 32x32-pixel
icon. It is placed within the standard App header, and has field type
03F with size 2 bytes. The 2-byte data is a little-endian pointer to an
icon formatted according to the
[ASM Header]({{< ref "Header.md" >}}) icon
guidelines. The field type used was originally 290, since prefix 29 was
unused, but it was discovered that the TI-OS doesn't handle unknown
prefixes well. The example below shows the 03F header field in action.

## Header Format {#header_format}

The general header format is as follows, and is designed for the Brass
assembler:

    ; App Name: 
    ; Author: 
    ; Version: 
    ; Date: 
    ; Written for Doors CSE 8.1 and higher (http://dcs.cemetech.net)
    .binarymode intel                 ; TI-83+ Application
    .nolist
    #include "ti84pcse.inc"
    .list
    NUM_PAGES = 1
    .defpage 0, 16*1024, $4000          ; Page 0 definition
    ; Add additional page definitions as needed
    .page 0                             ; Start page 0
    .echoln "-Page 0----------------------"
       ; Master Field
       .db 80h, 0Fh, 0, 0, 0, 0
       ; Signing Key ID
       .db 80h, 12h, 1, 15 ; 15 for the TI-84+CSE
       ;revision
       .db 80h,21h,8   ; 8 
       .db 80h,31h,1   ; Pre-release
       ; Name
       .db 80h, 44h, "AoE2"
       ; Disable TI splash screen.
       .db 80h, 90h
       ; Pages
       .db 80h, 81h, NUM_PAGES
       ; Date stamp.  Apparently, the calculator doesn't mind if you put
       ; nothing in this.
       .db 03h, 22h, 09h, 00h
       ; Date stamp signature.  Since nothing ever checks this, there's no
       ; reason ever to update it.  Or even have data in it.
       .db 02h, 00
       ; Doors CSE 8 Icon
       .db 03h, F2h
       
       .dw AppIcon
       ; Final field
       .db 80h, 70h
    ASMStart:
       ; Your App's code goes here
       bjump(_jforcecmdnochar)
       
    AppIcon:
    Icon_Start:
       .db 2                                   ;Icon type: 32x32, 2-bit color
       .db $ff,$ff,$d6,$99,$8c,$51,$73,$6d
       .db $ad,$54,$31,$85,$52,$89,$20,$e3
       .db $62,$a7,$72,$44,$93,$27,$51,$83
       .db $c4,$09,$f5,$8e,$9a,$04,$9a,$c9     ;32-byte (16-color) palette
       .db 32,32                               ;Image dimensions, for the sprite routine
       .db $00,$00,$01,$23,$34,$10,$00,$00
       .db $00,$01,$55,$33,$63,$56,$10,$00
       .db $00,$17,$24,$28,$97,$33,$71,$00
       .db $00,$7a,$89,$66,$95,$b6,$27,$00
       .db $06,$8c,$aa,$a9,$9b,$77,$32,$60
       .db $07,$da,$99,$aa,$8b,$77,$64,$70
       .db $47,$ba,$9a,$a9,$9b,$77,$87,$74
       .db $4b,$ec,$39,$9a,$cb,$57,$a7,$52
       .db $47,$ba,$2a,$cc,$ea,$95,$a8,$72
       .db $17,$7e,$3a,$dd,$ec,$b5,$85,$71
       .db $05,$be,$26,$ca,$9e,$55,$85,$70
       .db $04,$7b,$23,$be,$eb,$65,$55,$20
       .db $00,$55,$33,$3a,$f3,$55,$75,$00
       .db $00,$05,$63,$3f,$f6,$35,$50,$00
       .db $01,$26,$86,$8f,$f8,$58,$62,$10
       .db $00,$14,$23,$33,$33,$32,$41,$00
    Icon_End:
       .fill 256, 0                           ;Your App must be >256 bytes, or the TI-OS will reject it
                                              ;Remove this for Apps that are already >256 bytes
    .end
