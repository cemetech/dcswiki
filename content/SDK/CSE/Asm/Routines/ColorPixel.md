---
revisions:
- author: KermMartian
  comment: "Created page with \u201C==Description== Colors a pixel on the LCD\nat\
    \ the given coordinates using the specified color. ==Technical\nDetails== (None)\
    \ ===Inputs=== \u2019\u2018\u2019hl\u2019\u2019\u2019 = x`<br>`{=html} \u2019\u2018\
    \u2019de\u2019\u2019\u2019 =\ny`<br>`{=html} \u2019\u2019\u2019\u2026\u201D"
  timestamp: '2013-10-07T01:43:58Z'
title: ColorPixel
---

## Description

Colors a pixel on the LCD at the given coordinates using the specified
color.

## Technical Details {#technical_details}

(None)

### Inputs

**hl** = x\
**de** = y\
**iy** = 16-bit color

### Outputs

Pixel drawn to LCD.

### Destroyed

All.
