---
revisions:
- author: KermMartian
  comment: "Created page with \u201C==Description== Generates a random integer\nbetween\
    \ given bounds. Modifies (errSP). ==Technical Details==\nModified from the original\
    \ ionRandom routine in Ion 1.0 by Joe\nWingbe\u2026\u201D"
  timestamp: '2013-10-07T02:01:15Z'
title: RandInt
---

## Description

Generates a random integer between given bounds. Modifies (errSP).

## Technical Details {#technical_details}

Modified from the original ionRandom routine in Ion 1.0 by Joe
Wingbermuehle.

### Inputs

**b** = Upper bound (exclusive)

### Outputs

**a** = Resulting number, 0 \<= a \< b

### Destroyed

af, bc
