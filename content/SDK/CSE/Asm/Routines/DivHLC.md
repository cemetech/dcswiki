---
revisions:
- author: KermMartian
  comment: "Created page with \u201C==Description== Divides a 16-bit dividend\nby\
    \ an 8-bit divisor; returns a 16-bit quotient and an 8-bit\nremainder. ==Technical\
    \ Details== Special thanks to\n\\[http://baze.au.com/m\u2026\u201D"
  timestamp: '2013-10-07T01:57:27Z'
title: DivHLC
---

## Description

Divides a 16-bit dividend by an 8-bit divisor; returns a 16-bit quotient
and an 8-bit remainder.

## Technical Details {#technical_details}

Special thanks to [z80 Bits](http://baze.au.com/misc/z80bits.html) for
the inspiration for this routine.

### Inputs

**hl** = dividend\
**c** = divisor

### Outputs

**hl** = quotient **a** = remainder

### Destroyed

All
