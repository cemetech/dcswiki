---
revisions:
- author: KermMartian
  timestamp: '2013-10-14T22:15:32Z'
title: DrawSprite_4Bit
---

## Description

Draws a 4-bit (16-color) sprite to the LCD.

## Technical Details {#technical_details}

This routine draws an arbitrary-sized sprite to the LCD. In Doors CSE
8.0, it is implemented to allow any 16-bit position and any 8-bit width
and height for sprites. It will sanely handle sprites that are partially
off-screen by not drawing them at all. In future Doors CSE versions, it
will perform proper clipping to display the onscreen portions of
partially off-screen sprites.

### Inputs

**de** = top-left x\
**hl** = top-left y\
**ix** = pointer to sprite data with the following format:\
.dw pointer_to_palette

    .db widthpx, heightpx
    .db bitpacked_padded_rows...

The routine accepts any 16-bit x-coordinate and any 16-bit y-coordinate.
Widths must be at most 255 pixels; heights must be at most 240 pixels.
**Note:** After Doors CSE 8.0, this routine will properly handle
transparency at the edges of sprites with widths not divisible by 8, and
clipping of partially off-screen sprites.

### Outputs

Sprite drawn to the LCD.

### Destroyed

All.
