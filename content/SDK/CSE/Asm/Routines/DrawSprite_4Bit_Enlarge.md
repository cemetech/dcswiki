---
revisions:
- author: KermMartian
  comment: "Created page with \u201C==Description== Draws a 4-bit (16-color)\nsprite\
    \ to the LCD, and displays each pixel as a 2x2 pixel block.\n==Technical Details==\
    \ This routine draws an arbitrary-sized sprite\nt\u2026\u201D"
  timestamp: '2013-10-14T22:16:21Z'
title: DrawSprite_4Bit_Enlarge
---

## Description

Draws a 4-bit (16-color) sprite to the LCD, and displays each pixel as a
2x2 pixel block.

## Technical Details {#technical_details}

This routine draws an arbitrary-sized sprite to the LCD, enlarged 2x. In
Doors CSE 8.0, it is implemented to allow any 16-bit position and any
8-bit width and height for sprites. It will sanely handle sprites that
are partially off-screen by not drawing them at all. In future Doors CSE
versions, it will perform proper clipping to display the onscreen
portions of partially off-screen sprites.

### Inputs

**de** = top-left x\
**hl** = top-left y\
**ix** = pointer to sprite data with the following format:\
.dw pointer_to_palette

    .db widthpx, heightpx ;Note: width and height _after_ enlarging
    .db bitpacked_padded_rows...

The routine accepts any 16-bit x-coordinate and any 16-bit y-coordinate.
Widths must be at most 255 pixels; heights must be at most 240 pixels.
**Note:** After Doors CSE 8.0, this routine will properly handle
transparency at the edges of sprites with widths not divisible by 8, and
clipping of partially off-screen sprites.

### Outputs

Sprite drawn to the LCD.

### Destroyed

All.
