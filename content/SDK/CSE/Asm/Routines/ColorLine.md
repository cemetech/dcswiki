---
revisions:
- author: KermMartian
  comment: "Created page with \u201C==Description== Draws a color line\nroutine to\
    \ the LCD using\n\\[http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm\n\
    Bresenham\u2019s line algorithm\\]. ==Technical Details== (Non\u2026\u201D"
  timestamp: '2013-10-07T01:42:46Z'
title: ColorLine
---

## Description

Draws a color line routine to the LCD using [Bresenham's line
algorithm](http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm).

## Technical Details {#technical_details}

(None)

### Inputs

**de** = x0\
**bc** = y0\
**hl** = x1\
**ix** = y1\
**iy** = 16-bit color

### Outputs

Line drawn to LCD.

### Destroyed

All.
