---
revisions:
- author: KermMartian
  comment: "Created page with \u201C==Description== Clears the entire LCD to\nwhite.\
    \ Takes about 2.957M cycles. ==Technical Details== (None)\n===Inputs=== None ===Outputs===\
    \ None. Entire LCD is cleared to\nwhite\u2026\u201D"
  timestamp: '2013-10-07T01:33:58Z'
title: ClearLCDFull
---

## Description

Clears the entire LCD to white. Takes about 2.957M cycles.

## Technical Details {#technical_details}

(None)

### Inputs

None

### Outputs

None. Entire LCD is cleared to white.

### Destroyed

All
