---
revisions:
- author: KermMartian
  comment: "Created page with \u201C==Description== Multiplies two 8-bit\nnumbers\
    \ and returns a 16-bit product. ==Technical Details== Special\nthanks to \\[http://baze.au.com/misc/z80bits.html\
    \ z80 Bits\\] for the\nins\u2026\u201D"
  timestamp: '2013-10-07T01:55:12Z'
title: MultHE
---

## Description

Multiplies two 8-bit numbers and returns a 16-bit product.

## Technical Details {#technical_details}

Special thanks to [z80 Bits](http://baze.au.com/misc/z80bits.html) for
the inspiration for this routine.

### Inputs

**h** = multiplier\
**e** = multiplicand

### Outputs

**hl** = product

### Destroyed

**b** = 0\
c is left intact\
a, hl, and de are destroyed
