---
revisions:
- author: KermMartian
  comment: "Created page with \u201C==Description== Runs a program via Doors\nCSE\u2019\
    s RunProg subsystem. This call can handle any program that Doors\nCSE itself can\
    \ handle, including nostub BASIC and Hybrid BASIC, \u2026\u201D"
  timestamp: '2013-10-07T02:02:26Z'
title: RunProg
---

## Description

Runs a program via Doors CSE's RunProg subsystem. This call can handle
any program that Doors CSE itself can handle, including nostub BASIC and
Hybrid BASIC, nostub and DCS ASM programs, plus any other filetype that
gets added to Doors CSE after this document was written. Be very careful
to take into account all the things that the running program could do to
this program's SafeRAM, modes, and variables (see Outputs below).

## Technical Details {#technical_details}

### Inputs

**hl =** Pointer to high (first) byte of VAT entry of given program or
file.\

### Outputs

Runs the program. Be aware this may trash a variety of variables and
settings, create new variables and settings, and switch many calculator
modes, depending on the type of program run. Also note that Hybrid BASIC
programs and an ASM program may completely destroy any SafeRAM in use,
so be sure to either back up necessary variables

### Destroyed

All
