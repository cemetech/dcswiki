---
revisions:
- author: KermMartian
  comment: "Created page with \u201C==Description== Multiplies two 16-bit\nnumbers\
    \ and returns a 32-bit product. ==Technical Details== Special\nthanks to \\[http://baze.au.com/misc/z80bits.html\
    \ z80 Bits\\] for the\nin\u2026\u201D"
  timestamp: '2013-10-07T01:56:12Z'
title: MultDEBC
---

## Description

Multiplies two 16-bit numbers and returns a 32-bit product.

## Technical Details {#technical_details}

Special thanks to [z80 Bits](http://baze.au.com/misc/z80bits.html) for
the inspiration for this routine.

### Inputs

**de** = multiplier\
**bc** = multiplicand

### Outputs

**dehl** = product

### Destroyed

**a** = 0\
bc, de, hl destroyed
