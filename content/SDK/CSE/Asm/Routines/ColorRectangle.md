---
revisions:
- author: KermMartian
  comment: "Created page with \u201C==Description== Colors or inverts a\nrectangular\
    \ area of the LCD. ==Technical Details== This routine\ngenerally colors in a rectangular\
    \ area of the LCD. However, color\nbc=\\$1337\u2026\u201D"
  timestamp: '2013-10-07T01:49:57Z'
title: ColorRectangle
---

## Description

Colors or inverts a rectangular area of the LCD.

## Technical Details {#technical_details}

This routine generally colors in a rectangular area of the LCD. However,
color bc=\$1337 is a special case; this color makes the routine invert
all the colors on the screen by XORing each 16-bit pixel with \$FFFF.
Also, this routine can only display rectangles at even coordinates (h=10
means top-left X=20, for example), and only even widths are possible
(d=15 is 30 pixels wide). This limitation allows X coordinates and
widths to fit in 8-bit registers.

### Inputs

**h** = top-left x (note: X-coordinate divided by 2!)\
**l** = top-left y\
**d** = width\
**e** = height\
**bc** = 16-bit color. Color \$1337 is a special case.

### Outputs

Rectangular area colored or inverted on LCD.

### Destroyed

All.
