---
revisions:
- author: KermMartian
  comment: /\* Header Field Types \*/
  timestamp: '2016-09-08T21:30:05Z'
title: Program Header
weight: 10
aliases:
- /ASM_Header_(Color)/
---

Doors CSE for the TI-84 Plus C Silver Edition color-screen calculator
uses a more modular header than the earlier black-and-white version for
the TI-83 Plus/Silver Edition and TI-84 Plus/Silver Edition.

## Header Format {#header_format}

The general header format is as follows, and is designed for the Brass
assembler:

    ; Program Name: 
    ; Author: 
    ; Version: 
    ; Date: 
    ; Written for Doors CSE 8.0 and higher (http://dcs.cemetech.net)
    .nolist
    #include "ti84pcse.inc"
    #include "dcse8.inc"
       
       .org UserMem
    BinaryStart:
       .db $DE,$2A,"N",$BB,$B4,$BB,$B4,$BB,$B3,$BB,$C3,")D"   ;Disp "Needs D
       .db $BB,$BF,$BB,$BF,$BB,$C2,$BB,$C3,")CSE",$2A,$3F     ;oors CSE"
       .db $EF,$11                                            ;OpenLib(
       .db "D",$BB,$BF,$BB,$BF,$BB,$C2,$BB,$C3,"CSE",$11,$3F  ;(tokenized "DoorsCSE")
       .db $EF,$12,$3F                                        ;ExecLib
       .db $D5,$3F                                            ;Return
       .db tExtTok,tAsm84CPrgm,$3F                            ;Asm84CPrgm
    HeaderStart:
       .dw ASMStart-HeaderStart        ;offset to code
       ; Header sections start here
       .dw 10
       .db ASMHEADER_FIELD_TYPE_LIB    ;== 3
       .db "DoorsCSE",8,0              ;Lib name, min major version, min minor version
       .dw SECTIONSIZE ;(excludes type byte)
       .db SECTIONTYPE
       .db ...data...
       .dw SECTIONSIZE ;(excludes type byte)
       .db SECTIONTYPE
       .db ...data...  ;0 or more header fields in total
       .dw 0     ;End of header field: 0 bytes of data
       .db $ff   ;End of header field: type 255
    .list
    ASMStart:
       .relocate UserMem
    ProgramStart:
       ...code...
    .endrelocate
    .end

## Header Field Types {#header_field_types}

Note: Most of these fields are optional. The Type 3 header is *not*
optional for Doors CSE 8.0+ programs.

-   **0:** Description. Data is variable-length: zero-terminated string.
    Short descriptions are recommended.
-   **1:** Icon. Data is variable length.
    -   Offset 0: Icon type (1 byte).
        -   Type 0: 32\*32-pixel icon, 2-bit color (4 colors), 8-byte
            (4-color) palette
        -   Type 1: 32\*32-pixel icon, 1-bit color (2 colors), 4-byte
            (2-color) palette
        -   Type 2: 16\*16-pixel icon, 4-bit color (16 colors), 32-byte
            (16-color) palette
        -   Type 3: 16\*16-pixel icon, 8-bit color (256 colors), xLIBC
            palette (no palette data) (Doors CE only)
    -   Offset 1: Palette (bytes as indicated above)
    -   Offset 1+(palette size): Icon width, in *displayed* pixels. 32
        for icon types 0-2.
    -   Offset 2+(palette size): Icon height, in *displayed* pixels. 32
        for icon types 0-2.
    -   Offset 3+(palette size): Icon data
-   **2:** Author name. Data is a zero-terminated, variable-length
    string. Short names are recommended.
-   **3:** Required library. Data is fixed-length at 10 bytes.
    -   Offset 0: Library name (8 bytes, zero-padded)
    -   Offset 8: Minimum required major library version (1 byte)
    -   Offset 9: Minimum required minor library version (1 byte)
-   **4:** If present, shell will set half-resolution mode before
    executing program. Zero-length data.
