---
revisions:
- author: KermMartian
  timestamp: '2006-09-02T00:24:43Z'
title: Doors CS 5
weight: 90
aliases:
- /ASM_Header_(Doors_CS_5)/
---

Doors CS 5 was the last version of Doors CS to support the TI-83 (non-Plus),
providing compatibility for AShell and Ion programs.

## Program header

### Minimal

    .nolist
    #include "DoorsCS.inc"
    .list
       .org progstart ;$9327 (83) or $9D93 (83+/84+)
    #IFDEF TI83P
       .db $BB,$6D ;AsmProg token
    #ENDIF
       xor d ;defines as DCS 5 program
    #IFDEF TI83P
       .db $C9 ;=ret
    #ENDIF
    jr Start
       .dw 0
       .db $05,$00 ;Doors CS version - 5.0
       .dw 0
       .dw 0
    Start:
       ;program contents

### Description

    .nolist
    #include "DoorsCS.inc"
    .list
       .org progstart ;$9327 (83) or $9D93 (83+/84+)
    #IFDEF TI83P
       .db $BB,$6D ;AsmProg token
    #ENDIF
       xor d ;defines as DCS 5 program
    #IFDEF TI83P
       .db $C9 ;=ret
    #ENDIF
    jr Start
       .dw Description ;or .dw $0000 if no description
       .db $05,$00 ;Doors CS version - 5.0
       .dw 0
       .dw 0
    Start:
       ;program contents
    Description:
       .db "Zero-terminated description",0

### Description and Icon

    .nolist
    #include "DoorsCS.inc"
    .list
       .org progstart ;$9327 (83) or $9D93 (83+/84+)
    #IFDEF TI83P
       .db $BB,$6D ;AsmProg token
    #ENDIF
       xor d ;defines as DCS 5 program
    #IFDEF TI83P
       .db $C9 ;=ret
    #ENDIF
    jr Start
       .dw Description ;or .dw $0000 if no description
       .db $05,$00 ;Doors CS version - 5.0
       .dw Icon ;or .dw $0000 if no icon
       .dw 0
    Start:
       ;program contents
    Description:
       .db "Zero-terminated description",0
    Icon:      ;the 32-byte icon
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF

### ALEs, Description, and Icon

    .nolist
    #include "DoorsCS.inc"
    .list
       .org progstart ;$9327 (83) or $9D93 (83+/84+)
    #IFDEF TI83P
       .db $BB,$6D ;AsmProg token
    #ENDIF
       xor d ;defines as DCS 5 program
    #IFDEF TI83P
       .db $C9 ;=ret
    #ENDIF
    jr Start
       .dw Description ;or .dw $0000 if no description
       .db $05,$00 ;Doors CS version - 5.0
       .dw Icon ;or .dw $0000 if no icon
       .dw ALE ;or .dw $0000 if no ALE(s)
    Start:
       ;program contents
    Description:
       .db "Zero-terminated description",0
    Icon:      ;the 32-byte icon
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
       .db $FF,$FF
    ALEs:
       .db "ZALE1",0,0,0   ;always padded with zeros to 8 chars
       .db "ALEPRG2",0
       .db "LASTALE3"
       .db $FF
