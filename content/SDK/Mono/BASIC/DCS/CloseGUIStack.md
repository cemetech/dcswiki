---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== This routine closes the\nGUI stack,\
    \ removing all elements in it and freeing the memory used\nby the GUI stack Appvar.\
    \ If you call this function without first\nhavin\u2026\u2019"
  timestamp: '2010-07-04T13:16:23Z'
title: CloseGUIStack
aliases:
- /BasicLibs/CloseGUIStack/
---

## Description

This routine closes the GUI stack, removing all elements in it and
freeing the memory used by the GUI stack Appvar. If you call this
function without first having called
[OpenGUIStack]({{< ref "OpenGUIStack.md" >}}) or
[PushGUIStack]({{< ref "PushGUIStack.md" >}}), it does
nothing. Please see the [GUI_API]({{< ref "/SDK/Mono/Asm/DCS/GUI/" >}}) guide for
assembly programmers for full information on how the GUI stack works.
You can also check out the ASM equivalents of the DCSB Lib GUI stack
routines at the [GUI_Tools]({{< ref "/SDK/Mono/Asm/DCS/GUI/" >}}) page. The
ASM equivalent of this function is
[ASMLibs:CloseGUIStack]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/CloseGUIStack.md" >}}).

## Technical Details {#technical_details}

### Arguments

'''sum(10

'''

### Outputs

None.

### Destroyed

*None*
