---
revisions:
- author: Geekboy1011
  timestamp: '2013-06-13T17:45:03Z'
title: Cn2Status
aliases:
- /BasicLibs/Cn2Status/
---

## Description

Returns the current state of the send or get FlagBytes.

## Technical Details {#technical_details}

### Arguments

**sum(20,Flag)**\
**0**: Send byte.\
**1**: Receive byte.

### Outputs

Returns the Current state of the send or get FlagBytes in *Ans*.\
This routine can not fail.

### Destroyed

Ans
