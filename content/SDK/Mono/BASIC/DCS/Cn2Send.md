---
revisions:
- author: Geekboy1011
  timestamp: '2013-06-13T17:45:01Z'
title: Cn2Send
aliases:
- /BasicLibs/Cn2Send/
---

## Description

Takes a string or list and puts them into the CALCnet buffer to be sent
to the recipient in θ.\

## Technical Details {#technical_details}

### Arguments

**sum(17,message**

The recipient is stored in theta. If theta has a value of 0 a broadcast
frame is sent instead.

A message can be a string (252 bytes or less) or a list (28 entries or
less).\

### Outputs

Packet is prepared to be sent out over the CALCnet network.

#### Returns

An integer is returned in Ans:\
**1**: Packet was sent successfully.\
**0**: The previous frame has not been processed yet.

### Destroyed

Ans
