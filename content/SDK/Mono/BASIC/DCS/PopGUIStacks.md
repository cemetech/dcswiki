---
revisions:
- author: KermMartian
  comment: /\* Description \*/
  timestamp: '2010-07-04T13:15:19Z'
title: PopGUIStacks
aliases:
- /BasicLibs/PopGUIStacks/
---

## Description

This routine pops one or more elements off of the GUI stack, as pushed
by sum(7,...). Be very careful not to pop more elements than you have
pushed, as this may cause unpredictable behavior up to freezes and RAM
clears! You may pop off two elements and then push one element, for
example, but make sure the total number of elements does not fall below
0 at any time. If you want to remove all elements on the GUI stack, you
may instead use
[CloseGUIStack]({{< ref "CloseGUIStack.md" >}}), or sum(10).
Please see the [GUI API]({{< ref "/SDK/Mono/Asm/DCS/GUI/" >}}) guide for assembly
programmers for full information on how the GUI stack works.
The ASM equivalent of this function is
[ASMLibs:PopGUIStacks]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/PopGUIStacks.md" >}}).

## Technical Details {#technical_details}

### Arguments

**sum(8,ELEMENTS\
**ELEMENTS''': The number of elements to pop off of the GUI stack.

'''

### Outputs

None.

### Destroyed

*None*
