---
revisions:
- author: KermMartian
  timestamp: '2010-08-18T16:54:50Z'
title: GUIMenu
---

## Description

This routine offers a very simple way to invoke a fully-GUIfied
alternative to the TI-BASIC Menu( command. The user can specify an icon,
window location, title, and list of items and labels. Just like Menu(,
the sum(13) command will Goto a specific label corresponding to the item
selected, or return an error if no such label can be found. An alternate
mode has been added in which if the LABELLIST is omitted, then the index
of the chosen menu item will instead be returned in Ans.

## Technical Details {#technical_details}

### Arguments

**sum(13,X,Y,TITLE,ICON,ITEMLIST,LABELLIST\
**X**: X-coordinate of top-left corner of small window\
**Y**: Y-coordinate of top-left corner of small window\
**ICON**: 10-character hex string defining a 5x5 icon\
**TITLE**: A string to be used as the window title\
**ITEMLIST**: A list of items to be in the menu. Each menu item should
conclude with the Stats Plots "+" symbol (in
\[2nd\]\[Y=\]\[\>\]\[\>\]\[2\]), so the ITEMLIST string should contain
as many Stats Plots "+" symbols as items.\
**LABELLIST''': A string of 2\*(number of items) characters.
Two-character labels, like AA or Q2 or PA, are represented verbatim.
One-character labels such as A or U are represented as a two character
string, where the first character is a space, such as " A" or " U". Take
a look at label E in the sample below for another example. If this
argument is omitted completely, then no Goto'ing will be attempted;
instead, Doors CS will simply return the index of the chosen menu item.

### Outputs

If the LABELLIST is specified, this routine will not modify Ans, but
will Goto to the label corresponding to the chosen menu item. If the
LABELLIST is instead omitted, Ans will indicate which item was chosen.

### Destroyed

**None**

## Sample Usage {#sample_usage}

{{< image "sum13test3.gif" >}}

    :sum(13,8,8,"TEST","F8888888F8","Item 1┼ITEM TWO┼tan(*tan(4┼log(²X+][┼²‾.
     ./┼Six itemth one┼HOW ABOUT NOW?prgm┼ln(End┼Scrolling!┼More and more┼EVEN 
     MORE AsmPrgm┼","AAABACAD E EABACAD EAB
    :Lbl AA:ClrHome
    :Pause "1
    :Return
    :Lbl AB:ClrHome
    :Pause "2
    :Return
    :Lbl AC:ClrHome
    :Pause "3
    :Return
    :Lbl AD:ClrHome
    :Pause "4
    :Return
    :Lbl E:ClrHome
    :Pause "5
    :Return 
