---
revisions:
- author: KermMartian
  timestamp: '2010-07-04T12:45:00Z'
title: StringWidth
aliases:
- /BasicLibs/StringWidth/
---

## Description

This routine will find the width in pixels of any TI-BASIC screen, if it
is written on the graphscreen in small variable-width font. For example,
it would return 1 for a single space, 14 for the "cos(" token, and 18
for "Hello". Since it does not actually draw the string, it accepts
arbitrarily-long strings, including those wider than 96 pixels.

## Technical Details {#technical_details}

### Arguments

sum(0,"STRING"

### Outputs

The width in pixels of "STRING" is returned in Ans from this routine,
and can be used as Ans or stored in a variable.

### Destroyed

Ans

## Sample Usage {#sample_usage}

    PROGRAM:SUM0TEST
    :Input "STRING:",Str1
    :Disp "WIDTH:",sum(0,Str1
