---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Simply copy the homescreen\nto\
    \ the graphscreen. Because of the limited metadata that the TI-OS\nstores about\
    \ homescreen characters, this can only apply formatting\n\u2026\u2019"
  timestamp: '2010-07-04T12:36:43Z'
title: HomescreenToGraphscreen
aliases:
- /BasicLibs/HomescreenToGraphscreen/
---

## Description

Simply copy the homescreen to the graphscreen. Because of the limited
metadata that the TI-OS stores about homescreen characters, this can
only apply formatting like text inversion to the entire screen or none
of it. This routine can optionally update the LCD with the contents of
the graph screen.

## Technical Details {#technical_details}

### Arguments

**sum(2,ULCD**\
**ULCD**: 1 to update the LCD with the new graph screen contents, 0 to
leave it as-is

### Outputs

Graph buffer updated.

### Destroyed

*None*
