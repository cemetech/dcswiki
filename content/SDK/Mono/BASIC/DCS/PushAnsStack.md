---
revisions:
- author: KermMartian
  timestamp: '2010-08-01T04:24:42Z'
title: PushAnsStack
aliases:
- /BasicLibs/PushAnsStack/
---

## Description

This routine pushes the current value of Ans into a special Ans stack.
The [PopAnsStack]({{< ref "PopAnsStack.md" >}}) command is the
reverse function, popping a value back into Ans. It is recommended that
you call [ClearAnsStack]({{< ref "ClearAnsStack.md" >}})
before touching the AnsStack in case another programmer failed to clean
it up properly.

If no second argument is provided, then the value in Ans is pushed to
the default stack, called Ans0. However, a second argument between 0 and
9 inclusive will instead specify that the value should be pushed to one
of Ans0 through Ans9.
[PopAnsStack]({{< ref "PopAnsStack.md" >}}) allows a similar
second argument.

## Technical Details {#technical_details}

### Arguments

**sum(14\[,stackNumber\]**

### Outputs

The value of Ans is pushed to the AnsStack if and only if there is
enough memory, and the value in Ans is one of the following types:

-   Real Number
-   Complex Number
-   Real List
-   Complex List
-   String
-   Real Matrix

### Destroyed

*None*
