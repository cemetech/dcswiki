---
revisions:
- author: KermMartian
  comment: /\* Outputs \*/
  timestamp: '2012-11-09T19:55:26Z'
title: DCSLibVersion
aliases:
- /BasicLibs/DCSLibVersion/
---

## Description

This routine simple returns an integer indicating which version of the
Doors CS libraries are present. **Note:** this routine cannot be used to
verify whether Doors CS libraries or present or not. For that,
det(\[\[42 must be used; see [DCSB Libs](_index) for details.

## Technical Details {#technical_details}

### Arguments

**sum(5**

### Outputs

An integer is returned in Ans indicating the version of the DCSB Libs
present. Known versions:\
**1**: Supplies sum(0) through sum(12), as present in Doors CS 6.7
beta.\
**2**: Supplies sum(0) through sum(13), as present in Doors CS 6.8
beta.\
**3**: Supplies sum(0) through sum(16), as present in Doors CS 6.9
beta.\
**4**: Now including Flash-capable
[BinRead]({{< ref "BinRead.md" >}}), as present in Doors CS
7.2 Beta 3\
**5**: Supplies sum(0) through sum(19), as present in Doors CS 7.2 Beta
3

### Destroyed

Ans
