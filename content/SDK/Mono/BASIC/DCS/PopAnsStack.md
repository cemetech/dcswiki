---
revisions:
- author: KermMartian
  timestamp: '2010-08-01T04:40:43Z'
title: PopAnsStack
aliases:
- /BasicLibs/PopAnsStack/
---

## Description

This routine pops the current value of Ans from a special Ans stack. The
[PushAnsStack]({{< ref "PushAnsStack.md" >}}) command is the
reverse function, pushing a value into Ans. It is recommended that you
call [ClearAnsStack]({{< ref "ClearAnsStack.md" >}}) before
touching the AnsStack in case another programmer failed to clean it up
properly.

If no second argument is provided, then the value in Ans is popped from
the default stack, called Ans0. However, a second argument between 0 and
9 inclusive will instead specify that the value should be popped from
one of Ans0 through Ans9.
[PushAnsStack]({{< ref "PopAnsStack.md" >}}) allows a similar
second argument.

## Technical Details {#technical_details}

### Arguments

**sum(15,\[stackNumber\]**

### Outputs

The value of Ans is popped from the AnsStack if and only if there is
enough memory, and the value in Ans is one of the following types:

-   Real Number
-   Complex Number
-   Real List
-   Complex List
-   String
-   Real Matrix

If the AnsStack is empty, Ans is left untouched.

### Destroyed

*None*
