---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== This routine opens the GUI\nstack\
    \ for use by your BASIC program. Please see the \\[\\[GUI API\\]\\]\nguide for\
    \ assembly programmers for full information on how the GUI\nsta\u2026\u2019"
  timestamp: '2010-07-04T13:13:57Z'
title: OpenGUIStack
aliases:
- /BasicLibs/OpenGUIStack/
---

## Description

This routine opens the GUI stack for use by your BASIC program. Please
see the [GUI_API]({{< ref "/SDK/Mono/Asm/DCS/GUI/" >}}) guide for assembly
programmers for full information on how the GUI stack works. You can
also check out the ASM equivalents of the DCSB Lib GUI stack routines at
the [GUI_Tools]({{< ref "/SDK/Mono/Asm/DCS/GUI/" >}}) page. The ASM equivalent
of this function is
[ASMLibs:OpenGUIStack]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/OpenGUIStack.md" >}}). Please note
that if you
[BasicLibs:PushGUIStack]({{< ref "PushGUIStack.md" >}})
without calling OpenGUIStack first, Doors CS will automatically open the
GUI stack.

**Note:** It is **vital** that your program be polite and close the GUI
stack via sum(10),
[CloseGUIStack]({{< ref "CloseGUIStack.md" >}}), before
Return'ing or Stop'ing!

## Technical Details {#technical_details}

### Arguments

**sum(9\
**

### Outputs

None.

### Destroyed

*None*
