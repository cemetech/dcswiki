---
revisions:
- author: Techboy6601
  comment: Cleared up some confusion for 8x5 hex sprites
  timestamp: '2012-11-03T01:31:59Z'
title: PushGUIStack
aliases:
- /BasicLibs/PushGUIStack/
---

## Description

This routine pushes one element onto the GUI stack; it is among the most
complex of the [DCSB_Libs]({{< ref "../" >}}). Read carefully
the documentation for each function, as invalid or improper arguments
may cause unpredictable behavior up to freezes and RAM clears. Please
see the [GUI_API]({{< ref "/SDK/Mono/Asm/DCS/GUI/" >}}) guide for assembly
programmers for full information on how the GUI stack works. You can
also check out the ASM equivalents of the DCSB Lib GUI stack routines at
the [GUI_Tools]({{< ref "/SDK/Mono/Asm/DCS/GUI/" >}}) page. The ASM equivalent
of this function is
[ASMLibs:PushGUIStack]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/PushGUIStack.md" >}}).

## Technical Details {#technical_details}

### Arguments

**sum(7,TYPE,\[ARGS...\]\
**TYPE**: The type of element to push onto the GUI stack. There are 24
total valid types, as detailed below. Each is linked to the ASM SDK page
on that type.\
**\* **[GUIRnull]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRnull.md" >}})**: sum(7,0,N). This is
one of the three types that *must* be the first element pushed. The
value of N should be 255 if the graphscreen should be completely erased
before rendering the elements on top of this, or 254 if the contents of
the graphscreen should be preserved.

-   **[GUIRLargeWin]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRLargeWin.md" >}})**:
    sum(7,1,ICON,TITLE). This is one of the three types that *must* be
    the first element pushed. ICON must be a 10-character hex string
    (8x5), but with only a 5x5 usable area - the last 3 rows should be
    left empty. The TITLE string may contain any typeable characters and
    tokens.
-   **[GUIRSmallWin]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRSmallWin.md" >}})**:
    sum(7,2,X,Y,ICON,TITLE). This is one of the three types that *must*
    be the first element pushed. X and Y are the coordinates of the
    top-left corner of the small window, and must be between 0 and 15,
    inclusive. An X or Y value greater than 15 may cause memory
    corruption and crashes. ICON must be a 10-character hex string
    defining five bytes containing a 5x5 icon. The TITLE string may
    contain any typeable characters and tokens.
-   **[GUIRFullScreenImg]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRFullScreenImg.md" >}})**:
    sum(7,3,PIC#). This will insert a fullscreen image into the GUI
    stack, and is usually used directly after sum(7,0,255), creation of
    a new GUI stack group headed by a
    [GUIRnull]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRnull.md" >}}). The value of Pic#
    specifies the Pic variable from which the image should be copied.
    The Pic must be in RAM, and as with all Pic functions, 10=Pic 0.
-   **[GUIRText]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRText.md" >}})**:
    sum(7,4,X,Y,0,"STRING"). The specified string will be displayed in
    the GUI stack, where X,Y is the top-left corner of the text. The 0
    specifies what font to use; as of Doors CS 7.0, only one font is
    available, but future versions of Doors CS may add additional fonts.
    The STRING should not overflow the right edge of the container or
    the screen.
-   **[GUIRWinButtons]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRWinButtons.md" >}})**:
    sum(7,5,N). Display one or more of the possible window buttons at
    the top of this container. This should only be used if a
    [GUIRSmallWin]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRSmallWin.md" >}}) or
    [GUIRLargeWin]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRLargeWin.md" >}}) is the head
    element of this GUI stack group. N is a sum of the possible values
    32 (for the close or X button), 64 (for the maximize or square
    button), and 128 (for the minimize or underbar button). For example,
    to render the close and minimize buttons, N should equal 32+128 =
    160.
-   **[GUIRWrappedText]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRWrappedText.md" >}})**:
    sum(7,6,X,Y,WIDTH,0,STRING). Draws a block of text starting at
    (X,Y), of width WIDTH, automatically wrapped as necessary. The WIDTH
    should not be wider than the container, nor should it cause any part
    of the text to be rendered below the bottom of the screen. The 0
    indicates what font to use; as of Doors CS 7.0, only font 0 is
    available. Do not confuse this with the
    [GUIRTextMultiline]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRTextMultiline.md" >}}) element.
-   **[GUIRButtonText]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRButtonText.md" >}})**:
    sum(7,7,X,Y,"STRING"). Renders a clickable button at (X,Y)
    containing string STRING. If the button is clicked, a GUI Mouse call
    will be terminated.
-   **[GUIRButtonImg]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRButtonImg.md" >}})**:
    sum(7,8,X,Y,IMWIDTH,BUTTONIMG,ICON). Renders a clickable button at
    (X,Y) containing the image specified by the hex code in ICON. The
    IMWIDTH may be 1 for 8 pixels, 2, for 16 pixels, etc. The BUTTONIMG
    specifies the width of the image in pixels, for example 5, 6, or 8
    if IMWIDTH=1. The ICON is a string of hex nibbles 10\*IMWIDTH
    characters long.
-   **[GUIRTextLineIn]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRTextLineIn.md" >}})**:
    sum(7,9,X,Y,WIDTH,MAXCHARS,STRING). Renders an interactive
    single-line text box WIDTH pixels wide. Up to MAXCHARS characters
    may be typed inside. The initial contents of the text box will be
    STRING, or blank if STRING is "".
-   **[GUIRRadio]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRRadio.md" >}})**:
    sum(7,10,x,y,STRING,GROUPID,STATE). Renders a radio button at (X,Y),
    including a descriptive STRING to the right of the button. The STATE
    argument can be 0 or 1, indicating whether the button should be
    initially unselected or selected, respectively. The GROUPID
    indicates which radio buttons are in the same group; when one radio
    button with a given GROUPID is clicked, all other radio buttons with
    the same GROUPID are unselected. The GROUPID is a number.
-   **[GUIRCheckbox]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRCheckbox.md" >}})**:
    sum(7,11,X,Y,STATE,GROUPID,STRING). Renders a checkbox at (X,Y),
    including a descriptive STRING to the right of the box. The STATE
    argument can be 0 or 1, indicating whether the box should be
    initially unchecked or checked, respectively. The GROUPID indicates
    which check boxes are in the same group, but has no effect
    currently. It is included mainly for symmetry with
    [GUIRRadio]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRRadio.md" >}}) elements.
-   **[GUIRByteInt]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRByteInt.md" >}})**:
    sum(7,12,X,Y,INITIAL,MIN,MAX). A numerical input box, accepting a
    number between 0 and 255, inclusive. The MIN and MAX arguments
    specify some subset of that range as valid for this box. The INITIAL
    argument specifies what number it should start at when rendered.
-   **[GUIRWordInt]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRWordInt.md" >}})**:
    sum(7,13,X,Y,INITIAL,MIN,MAX). A numerical input box, similar to
    [GUIRByteInt]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRByteInt.md" >}}) but accepting a
    number between 0 and 65535, inclusive. The MIN and MAX arguments
    specify some subset of that range as valid for this box. The INITIAL
    argument specifies what number it should start at when rendered.
-   **[GUIRHotspot]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRHotspot.md" >}})**:
    sum(7,14,X,Y,WIDTH,HEIGHT). An invisible clickable hotspot with
    upper-left corner at (X,Y) and a given WIDTH and HEIGHT. It masks
    any elements underneath, so a
    [GUIRHotspot]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRHotspot.md" >}}) can be used to
    provide custom clickability on top of other elements.
-   **[GUIRTextMultiline]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRTextMultiline.md" >}})**:
    sum(7,15,X,Y,ROWS,WIDTH,STRING). An interactive multiline textbox
    with ROWS rows (ie, 6\*ROWS pixels tall) and WIDTH pixels wide. The
    initial contents of the textbox is STRING; line returns can be
    represented with the Stat Plots symbol "dot". An empty initial
    STRING "" is also acceptable.
-   **[GUIRSprite]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRSprite.md" >}})**:
    sum(7,16,X,Y,HEIGHT,SPRITE). An 8-pixel wide, HEIGHT-pixel-high
    sprite. SPRITE is a 2\*HEIGHT-character string of hex nibbles. (X,Y)
    is the upper-left corner of the sprite.
-   **[GUIRLargeSprite]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRLargeSprite.md" >}})**:
    sum(7,17,X,Y,BYTEWIDTH,PXHEIGHT,SPRITE). An 8\*BYTEWIDTH-pixel-wide,
    PXHEIGHT-pixel-high sprite. X should be less than 97-8\*BYTEWIDTH,
    and Y should be less than 65-PXHEIGHT. SPRITE should be a string of
    hex nibbles, 2\*PXHEIGHT\*BYTEWIDTH characters long.
-   **[GUIRPassIn]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRPassIn.md" >}})**:
    sum(7,18,X,Y,WIDTH,MAXCHARS,STRING). Renders an interactive
    single-line password input box WIDTH pixels wide. Up to MAXCHARS
    characters may be typed inside. The initial contents of the text box
    will be STRING, or blank if STRING is "". The functionality of this
    element is identical to that of
    [GUIRTextLineIn]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRTextLineIn.md" >}}) except that
    the characters are obfuscated.
-   **[GUIRScrollVert]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRScrollVert.md" >}})**:
    sum(7,19,X,Y,HEIGHT,ID,INC/DEC,MIN,MAX,CUR). Render a vertical
    scrollbar starting at upper-left at (X,Y), with scroll area height
    HEIGHT (total vertical height 12+HEIGHT). The MIN, MAX, and CUR
    values define the size of the scroll slider and its current
    position. The ID is arbitrary, but should be unique for future use.
    INC/DEC is also irrelevant in the context of the DCSBLibs, and
    should be left as 1.
-   **[GUIRScrollHoriz]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRScrollHoriz.md" >}})**:
    sum(7,20,X,Y,WIDTH,ID,INC/DEC,MIN,MAX,CUR). Render a horizontal
    scrollbar starting at upper-left at (X,Y), with scroll area width
    WIDTH (total horizontal width 12+WIDTH). The MIN, MAX, and CUR
    values define the size of the scroll slider and its current
    position. The ID is arbitrary, but should be unique for future use.
    INC/DEC is also irrelevant in the context of the DCSBLibs, and
    should be left as 1.
-   **[GUIRBorder]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRBorder.md" >}})**:
    sum(7,21,X,Y,WIDTH,HEIGHT,COLOR). Render an outlined, unfilled
    rectangle with upper-left corner (X,Y), width WIDTH, and height
    HEIGHT. COLOR is 0=white, 1=black.
-   **[GUIRRect]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRRect.md" >}})**:
    sum(7,22,X,Y,WIDTH,HEIGHT,COLOR). Render a filled rectangle with
    upper-left corner (X,Y), width WIDTH, and height HEIGHT. COLOR is
    255=white, 254=black, 253=invert, 170=checkered, 0=striped.
-   **[GUIRMouseCursor]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRMouseCursor.md" >}})**:
    sum(7,24,X1\*256+Y1,X2\*256+Y2,MASK,CURSOR). (X1,Y1) is the top-left
    corner of the area; (X2,Y2) is the bottom-right corner. Note that
    the coordinates are LCD-based, not based on the coordinates of the
    current window. The MASK is a 16-character string of hex nibbles
    specifying an 8x8 white-on-black mask, where black is transparent
    and white is opaque. The CURSOR is a similar 16-character string of
    hex nibbles specifying an 8x8 sprite. Inside the specified area, the
    mouse cursor will be changed from the default to the specified
    cursor.

### Outputs

The GUI stack is modified to include the new element, and enlarged to
hold it. Note that memory contraints apply; it is recommended that the
programmer be cautious and verify there is enough available memory
before large pushes to the stack.

### Destroyed

*None*
