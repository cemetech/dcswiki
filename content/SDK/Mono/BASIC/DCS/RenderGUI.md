---
revisions:
- author: KermMartian
  timestamp: '2010-08-19T03:48:21Z'
title: RenderGUI
aliases:
- /BasicLibs/RenderGUI/
---

## Description

This routine renders the contents of the GUI stack, but then returns
control to the calling BASIC program rather than invoking the
[GUIMouse]({{< ref "GUIMouse.md" >}}). This routine is useful
if you wish to render an interface via the DCS GUI stack system, but
either don't wish to use the mouse, or wish to use the
[SimpleGUIMouse]({{< ref "SimpleGUIMouse.md" >}}) instead of
the full GUI mouse. Please see the [GUI_API]({{< ref "/SDK/Mono/Asm/DCS/GUI/" >}})
guide for assembly programmers for full information on how the GUI stack
works. You can also check out the ASM equivalents of the DCSB Lib GUI
stack routines at the [GUI_Tools]({{< ref "/SDK/Mono/Asm/DCS/GUI/" >}}) page.
The ASM equivalent of this function is
[ASMLibs:RenderGUI]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/RenderGUI.md" >}}).

## Technical Details {#technical_details}

### Arguments

**sum(11\[,ULCD\]**

If the function is called as sum(11), the GUI is rendered to the graph
buffer and copied to the LCD. Sum(11,1) will also achieve the same
thing. However, sum(11,0) will render the GUI to the graph buffer but
not copy it to the LCD.

### Outputs

None.

### Destroyed

*None*
