---
revisions:
- author: KermMartian
  comment: /\* Arguments \*/
  timestamp: '2010-07-04T12:49:19Z'
title: ArcUnarcPic
aliases:
- /BasicLibs/ArcUnarcPic/
---

## Description

This routine will archive or unarchive a specified Pic variable. If the
picture is already in the desired state, nothing happens. If the picture
does not exist, nothing happens. This works with Pic 1 through Pic 255.

## Technical Details {#technical_details}

### Arguments

**sum(4,PIC,DESIRED**\
**PIC**: The number of the Pic variable to use. 3=Pic 3, 60=Pic 60,
10=Pic 0, for example\
**DESIRED**: 0 if it should be put into (or left in) RAM, 1 if it should
be put into (or left in) the Archive.

### Outputs

The Picture is archived, left in RAM, or left undisturbed, depending on
where it started and what the desired final state was. Ans is not
modified.

### Destroyed

*None*
