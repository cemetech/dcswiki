---
revisions:
- author: Geekboy1011
  timestamp: '2013-06-13T17:45:00Z'
title: Cn2Get
aliases:
- /BasicLibs/Cn2Get/
---

## Description

Takes the data in the CALCnet receive buffer and outputs it into its
appropriate variable.

## Technical Details {#technical_details}

### Arguments

**sum(18)**

### Outputs

Retrieves the pending packet, if any, from the CALCnet buffer, and
clears it.\
If the packet contains a list, it is loaded into LCN2 (a list
automatically created by Cn2Get when it finds a pending packet ready for
processing.)\
If the packet contains a string, it is loaded into Str9.

#### Results

An integer is returned in Ans:\
**1**: There was no frame to retrieve.\
**0**: A frame was successfully retrieved.

### Destroyed

Ans
