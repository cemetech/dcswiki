---
bookCollapseSection: true
revisions:
- author: Geekboy1011
  comment: /\* Doors CS BASIC Libraries \*/
  timestamp: '2013-02-18T05:25:19Z'
title: DCSB
weight: 14
aliases:
- "/DCSB_Libs/"
---

Doors CS contains a powerful set of libraries for use in BASIC programs
via the `sum()` token, centering around GUI features but also making
other tasks impossible in pure BASIC or with other libraries possible.
Doors CS also offers
[compatibility]({{< ref "/SDK/Mono/BASIC/" >}}) for
all the major BASIC libraries, including Celtic III, XLib, PicArc, and
(partially) Omnicalc.

To detect whether or not the DCSB Libs are available inside a basic
program, use the sequence det(\[\[42.

-   If it returns 42, no libraries are available.
-   If it returns 0, only Celtic III/Xlib/PicArc/Omnicalc is available.
-   If it returns 1337, all libraries are available, including the DCSB
    Libs.

### Doors CS BASIC Libraries {#doors_cs_basic_libraries}

**[StringWidth]({{< ref "StringWidth.md" >}})** -
sum(0,"String of token(s)")\
**[ScanForPx]({{< ref "ScanForPx.md" >}})** -
sum(1,X,Y,direction) dir: 1=up, 2=down, 3=left, 4=right\
**[HomescreenToGraphscreen]({{< ref "HomescreenToGraphscreen.md" >}})** -
sum(2,ULCD) ULCD: 0=no LCD update, 1=copy to LCD\
**[EnumeratePicVars]({{< ref "EnumeratePicVars.md" >}})** -
sum(3)\
**[ArcUnarcPic]({{< ref "ArcUnarcPic.md" >}})** -
sum(4,Pic#,desired) desired: 0=in RAM, 1=in Archive\
**[DCSLibVersion]({{< ref "DCSLibVersion.md" >}})** - sum(5)\
**[SimpleGUIMouse]({{< ref "SimpleGUIMouse.md" >}})** -
sum(6,startX,startY)\
**[PushGUIStack]({{< ref "PushGUIStack.md" >}})** -
sum(7,type,{args...})\
**[PopGUIStacks]({{< ref "PopGUIStacks.md" >}})** -
sum(8,numItems)\
**[OpenGUIStack]({{< ref "OpenGUIStack.md" >}})** - sum(9)\
**[CloseGUIStack]({{< ref "CloseGUIStack.md" >}})** - sum(10)\
**[RenderGUI]({{< ref "RenderGUI.md" >}})** -
sum(11\[,ULCD\])\
**[GUIMouse]({{< ref "GUIMouse.md" >}})** -
sum(12,startX,startY,dataStrN)\
**[GUIMenu]({{< ref "GUIMenu.md" >}})** -
sum(13,X,Y,"title","icon","itemlist","labellist")\
**[PushAnsStack]({{< ref "PushAnsStack.md" >}})** -
sum(14\[,stackNumber\])\
**[PopAnsStack]({{< ref "PopAnsStack.md" >}})** -
sum(15\[,stackNumber\])\
**[ClearAnsStack]({{< ref "ClearAnsStack.md" >}})** -
sum(16\[,stackNumber\])\
**[Cn2Send]({{< ref "Cn2Send.md" >}})** - sum(17,message)\
**[Cn2Get]({{< ref "Cn2Get.md" >}})** - sum(18)\
**[Cn2Ctrl]({{< ref "Cn2Ctrl.md" >}})** - sum(19,Boolean)\
**[Cn2Status]({{< ref "Cn2Status.md" >}})** - sum(20,Flag)\
