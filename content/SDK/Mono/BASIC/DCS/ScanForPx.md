---
revisions:
- author: KermMartian
  comment: /\* Arguments \*/
  timestamp: '2010-07-04T12:34:00Z'
title: ScanForPx
aliases:
- /BasicLibs/ScanForPx/
---

## Description

This routine scans the graphscreen in a given direction from a given
starting point, and returns the coordinate of the last pixel it
encounters that is the same color as the starting pixel. For example, if
the starting pixel is black, and 2 (down) is specified as the direction,
the routine will move down one pixel at a time from the starting pixel
until it finds a white pixel, then return the y-coordinate of the pixel
**above** that pixel. If the pixel directly below the starting pixel was
the first white pixel, it would return the coordinates of the starting
pixel. If the edge of the screen is reached, the coordinate of the edge
pixel will be returned (0 for up or left, 95 for right, 63 for down).
This routine is useful for things like flood filling and collision
detection.

## Technical Details {#technical_details}

### Arguments

**sum(1,X,Y,DIR**\
**X**: x-coordinate of starting pixel\
**Y**: y-coordinate of starting pixel\
**DIR**: Direction to scan. 1=up, 2=down, 3=left, 4=right, all others
invalid

### Outputs

The coordinate of the last pixel of the same color as the starting pixel
in the given direction is returned in Ans. If the chosen direction is up
or down, the coordinate is a y-coordinate (the x will be the same as the
initial x). If the direction is left or right, Ans will contain an
x-coordinate.

### Destroyed

Ans
