---
revisions:
- author: KermMartian
  comment: /\* Description \*/
  timestamp: '2010-07-14T06:06:55Z'
title: GUIMouse
aliases:
- /BasicLibs/GUIMouse/
---

## Description

This routine both renders the current GUI stack, and invokes the GUI
mouse; it is not necessary to call sum(11),
[RenderGUI]({{< ref "RenderGUI.md" >}}), before this function.
Please see the [GUI_API]({{< ref "/SDK/Mono/Asm/DCS/GUI/" >}}) guide for assembly
programmers for full information on how the GUI stack works. You can
also check out the ASM equivalents of the DCSB Lib GUI stack routines at
the [GUI_Tools]({{< ref "/SDK/Mono/Asm/DCS/GUI/" >}}) page. The ASM equivalent
of this function is
[ASMLibs:GUIMouse]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/GUIMouse.md" >}}).

To avoid possible situations where a BASIC programmer calls sum(12)
without any way of escaping out of the call, like a GUIRButton or
GUIRWinButtons, \[STO\>\] acts as a panic buttons, and will return with
the special outputs Ans(3)=0 and Ans(4)={number of interactive hotspots
in stack}. If you determine what was clicked by checking Ans(4), you can
either handle this as a special case or ignore it and re-loop, giving
you a short time to press \[ON\] and trigger an ERR:BREAK. Savvy
programmers may even find a way to use this as a feature.

**Notes:** Do not call this function without at least two elements on
the GUI stack, namely a GUI master (specifically, a
[GUIRnull]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRnull.md" >}}), a
[GUIRSmallWin]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRSmallWin.md" >}}), or a
[GUIRLargeWin]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRLargeWin.md" >}})), and a hotspot,
button, or [GUIRWinButtons]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRWinButtons.md" >}}) that
will allow control to return to the BASIC program. If you call this
function without anything clickable in the GUI stack, then there will be
no way to break out of the mouse other than a battery pull. The \[ON\]
key turns off the calculator when the GUI mouse is active. The
calculator will also APD if the GUI mouse is active and idle for two to
three minutes.

## Technical Details {#technical_details}

### Arguments

**sum(12,X,Y,N\
**X**: Initial x-coordinate of mouse.\
**Y**: Initial y-coordinate of mouse.\
**N''': String in which to store interactive element contents. 4=Str 4,
8=Str 8, 10=Str 0, for example.

'''

### Outputs

This routine has two output variables:

-   **Ans**: A four-element list is returned in Ans containing
    {X,Y,CLICK,TRIGGER}. The X and Y values are the final x- and
    y-coordinates of the mouse when a click occurred. The CLICK element
    is 1 if a left click was registered, or 2 for a right click. The
    value of TRIGGER indicates which hotspot in the GUI stack was
    clicked; its meaning is very dependent on the contents you have
    pushed into the GUI stack. If you pushed a
    [GUIRWinButtons]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRWinButtons.md" >}}) containing
    only an X and a
    [GUIRButtonText]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRButtonText.md" >}}), for instance,
    clicking the X will return TRIGGER=0, and clicking the text button
    will return TRIGGER=1. Scrollbars have two hotspots, so clicking the
    left or up arrow will return a distinct trigger from clicking the
    right or down arrow. You may also wish to correlate the values of
    {X,Y} and TRIGGER for more fine-grained detection. Generally,
    trial-and-error is the best (and most intuitive) way to figure out
    what TRIGGER values correspond to what GUI element for a particular
    GUI stack configuration.
-   **Str N**: The contents of most interactive GUI elements are
    returned concatenated into a single string, Str N, where the value
    of N is specified as one of the arguments to this function. Elements
    are separated with the Stat Plots "+" symbol (different from the
    addition symbol). Line returns in string elements are represented by
    the Stat Plots "dot" symbol. This string will always contain the
    same number of Stat Plots "plus" symbols as it does elements,
    preceeding each element by its "plus" symbol. The inString() and
    sub() functions, combined when necessary with the expr() command,
    can be used to extract all the individual string and numbers from
    this string.

### Destroyed

Ans, Str *N* (see above)
