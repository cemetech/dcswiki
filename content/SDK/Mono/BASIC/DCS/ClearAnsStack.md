---
revisions:
- author: KermMartian
  timestamp: '2010-08-01T04:44:20Z'
title: ClearAnsStack
aliases:
- /BasicLibs/ClearAnsStack/
---

## Description

This routine clears the special Doors CS **AnsStack**. The
[PushAnsStack]({{< ref "PushAnsStack.md" >}}) and
[PopAnsStack]({{< ref "PopAnsStack.md" >}}) functions are used
to add and remove items from the AnsStack. If no second argument is
specified, then **all** AnsStacks, Ans0 through Ans9, are cleared and
deleted. Specify sum(16,0) to clear the default AnsStack, Ans0. Specify
another value to clear and delete that AnsStack. Valid values are 0-9.

## Technical Details {#technical_details}

### Arguments

**sum(16,\[stackNumber\]**

### Outputs

AnsStack cleared and deleted, if it existed.

### Destroyed

*None*
