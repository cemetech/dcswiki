---
revisions:
- author: Geekboy1011
  timestamp: '2013-06-13T17:44:58Z'
title: Cn2Ctrl
aliases:
- /BasicLibs/Cn2Ctrl/
---

## WARNING

If you utilize any of the BASIC Calcnet 2.2 interface commands (Cn2Ctrl,
Cn2Get, Cn2Send, Cn2Status) make sure to destroy the interface at the
end of your program.\
`<b>`{=html}FAILURE TO DO SO WILL RESULT IN YOUR CALCULATOR'S LINKING
CAPABILITIES BREAKING.`</b>`{=html}\
To fix this, you will need to perform a RAM clear.

## Description

Enables or disables the CALCnet interface based on the value passed.

## Technical Details {#technical_details}

### Arguments

**sum(19,\[Boolean\])**\
**0**: Destroys the interface.\
**1**: Sets up the interface.

### Outputs

Sets up or destroys the CALCnet interface.\
These routines can not fail.

### Destroyed

*None*
