---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Enumerate all picture\nvariables\
    \ present on the calculator, between Pic 1 and Pic 255 (Pic\n0 is indicated as\
    \ #10). This function returns a list containing\nnumbers\u2026\u2019"
  timestamp: '2010-07-04T12:44:29Z'
title: EnumeratePicVars
aliases:
- /BasicLibs/EnumeratePicVars/
---

## Description

Enumerate all picture variables present on the calculator, between Pic 1
and Pic 255 (Pic 0 is indicated as #10). This function returns a list
containing numbers, each pertaining to a picture variable present. A
negative number indicates that the particular picture is archived. If
this function returns a one-element list containing the number 0, then
no pictures are present.

## Technical Details {#technical_details}

### Arguments

**sum(3**

### Outputs

A list is returned in Ans containing as many elements as there are Pic
variables present. The numerical value of each element indicates the
presence of one Pic variable; a negative element indicates an archived
Pic. Note that the list returned is NOT sorted. The case of no Pics
present causes the list {0} to be returned.

### Destroyed

Ans
