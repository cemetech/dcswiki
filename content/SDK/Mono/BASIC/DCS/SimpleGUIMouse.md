---
revisions:
- author: KermMartian
  comment: /\* Sample Usage \*/
  timestamp: '2010-07-04T12:58:52Z'
title: SimpleGUIMouse
aliases:
- /BasicLibs/SimpleGUIMouse/
---

## Description

This routine will invoke a simplified version of the Doors CS GUI Mouse.
It runs until a click is registered, and will return regardless of where
the click is. It does not take into account the contents of the screen
or GUI stack, if any. It will properly preserve the graphscreen, and
return with it in the same state as when it started.

## Technical Details {#technical_details}

### Arguments

**sum(6,X,Y**\
**X**: The x-coordinate at which to start the mouse. Valid values are 0
through 95, inclusive.\
**Y**: The y-coordinate at which to start the mouse. Valid values are 0
through 63, inclusive.

### Outputs

This routine returns a 3-element list in the form {X,Y,CLICK}. X and Y
are the final x-coordinate and y-coordinate of the mouse when a click
was registered. The CLICK element of the list is 1 if the click was a
left-click (2nd, TRACE, or ENTER) or a right-click (ALPHA or GRAPH).

### Destroyed

Ans

## Sample Usage {#sample_usage}

    PROGRAM:SUM6TEST
    :{45,32,1
    :While 2≠Ans(3
    :sum(6,Ans(1),Ans(2
    :Text(1,1,"YOU CLICKED AT (",Ans(1),",",Ans(2),") [",Ans(3),"]
    :End
