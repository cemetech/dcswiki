---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== This command takes Pic X\n(Pic0\
    \ = 10) and performs an action designated by a function number.\nIf arg_1 = \u2026\
    \u2019\u2018\u20190:\u2019\u2019\u2019 Copies Pic file to the graph buffer using\
    \ lo\u2026\u2019"
  timestamp: '2010-08-03T23:14:55Z'
title: TogglePic
---

## Description

This command takes Pic X (Pic0 = 10) and performs an action designated
by a function number. If arg_1 = ...

**0:** Copies Pic file to the graph buffer using logic method. arg_1
specifies the Pic file arg_2 specifies the logic code. The supported
codes are:

:   0: REPLACE, no screen update.\
:   1: AND, no screen update.\
:   2: OR, no screen update.\
:   3: XOR, no screen update.\
:   4: REPLACE, the screen is updated.\
:   5: AND, the screen is updated.\
:   6: OR, the screen is updated.\
:   7: XOR, the screen is updated.\

**1:** Creates a new pic file using the contents of the graph buffer.
Any preexisting picture will be replaced.\
**2:** Swaps a pic file between archive and RAM. If it is now in RAM,
the output is 0, else it is some other number.\
**3:** Deletes a picture. This is not an undoable action, so be cautious
while using this function.

## Technical Details {#technical_details}

### Arguments

identity(1,pic_number,function,\[arg_1,arg_2...\])

### Outputs

Modifies the LCD contents. See above.
