---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description==\u2019\u2018\u2019Note: this command\
    \ is\nincompletely implemented\u2019\u2019\u2019 This command shifts or rotates\
    \ within a\nbox-shaped area on the screen (and may be the whole screen if you\n\
    wa\u2026\u2019"
  timestamp: '2010-08-03T23:51:20Z'
title: BoxShift
---

## Description

**Note: this command is incompletely implemented**

This command shifts or rotates within a box-shaped area on the screen
(and may be the whole screen if you want). If you don't update the LCD,
this command is very useful for dynamically editing tilemaps by loading
them to the buffer, using this command to edit them, then storing them
back to the pic file, all without the buffer ever being updated. The
procedure is fast enough to be dismissed as a slight slowdown due to the
BASIC parser.

## Technical Details {#technical_details}

This routine does not perform clipping. All coordinates and dimensions
must remain onscreen.

### Arguments

identity(9,x,y,w,h,Dir,Type,Repeat,LCD_update)

**x:** top-left corner x, 0 thru 11. (each 8 pixels is a byte)\
**y:** top-left corner y, 0 thru 63.\
**w:** width of box object, 1 thru 12. Specifies byte widths.\
**h:** height of box object, 1 thru 64\
**Dir:** Each will have a basic direction followed by what you must add
to it in order to perform a different type of shift.

:   left = 1\
:   right = 2\
:   up = 4\
:   up-left = 5\
:   up-right = 6\
:   down = 8\
:   down-left = 9\
:   down-right = 10\

**Type:** Determines what type of shifting is to take place:

:   0 = Keep bit being shifted in\
:   1 = Shift in a black bit (for black backgrounds)\
:   2 = Shift in a white bit (for white backgrounds)\
:   3 = Let bit that shifts out reappear on other side (for scrolling
    backgrounds)\

**Repeat:** A number that repeats the shift operation as many times as
specified. Cannot be zero.\
**LCD_update:** If it is a number other than zero, the screen is updated
with the results.\

### Outputs

Modifies the graph buffer and optionally the LCD.
