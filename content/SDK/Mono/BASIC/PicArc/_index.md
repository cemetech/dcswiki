---
bookCollapseSection: true
title: PicArc
weight: 13
---

**[DBQuery1]({{< ref "DBQuery1.md" >}})** -
identity(0,"DB",f#,args) 0=fm;1=ad;2=dl;3=di;4=ac;5=cb;6=cp\
**[TogglePic]({{< ref "TogglePic.md" >}})** -
identity(1,pic#,f#) 0-7=picToBuf;8=newPic;9=TARC;10=del\
**[GetPicGroup]({{< ref "GetPicGroup.md" >}})** -
identity(2,"GFN")\
**[ExtPicGroup]({{< ref "ExtPicGroup.md" >}})** -
identity(3,"GFN",pic#)\
**[StringTile]({{< ref "StringTile.md" >}})** -
identity(4,"BSTR",xS,xE,yS,yE,W,xO,yO,pic#S,displogic)\
**[PutSprite]({{< ref "PutSprite.md" >}})** -
identity(5,"HEXSTRING",x,y,w,h,logic,flip,update_lcd)\
**[ShiftScreen]({{< ref "ShiftScreen.md" >}})** -
identity(6,sft#,sftDir,scrnUpdate)\
**[DrawBox]({{< ref "DrawBox.md" >}})** -
identity(7,x1,y1,x2,y2,dispmeth)\
**[FillMap]({{< ref "FillMap.md" >}})** -
identity(8,"HEXSTRING",right,down,LOGIC,updateLCD)\
**[BoxShift]({{< ref "BoxShift.md" >}})** -
identity(9,x,y,w,h,shiftType,shiftRepeat,updateLCD)\
**[DrawText]({{< ref "DrawText.md" >}})** -
identity(10,flag,\[x,y\],"TEXT")\
**[CopyBox]({{< ref "CopyBox.md" >}})** -
identity(xx,x1s,y1s,x2s,y2s,x1d,y1d,x2d,y2d,dispmeth)
