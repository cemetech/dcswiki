---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Draws text to the graph\nscreen\
    \ with the coordinates x,y using the small font. The screen is\nNOT updated. If\
    \ you wanted to update it, you should\u2019ve used the\nText()\u2026\u2019"
  timestamp: '2010-08-03T23:55:11Z'
title: DrawText
---

## Description

Draws text to the graph screen with the coordinates x,y using the small
font. The screen is NOT updated. If you wanted to update it, you
should've used the Text() command instead. If you omit the x,y coords,
the text is drawn to the last coordinate the previous DRAWTEXT command
ended up. Does not line-wrap. It's your duty to do that yourself. If you
don't want the text to be modified in any way, set **flag** to zero. If
you want to modify, you should do addition to the value using these
numbers:

**+1** = Draw using large font.\
**+2** = Erase 1 row of pixels below the small font base\
**+4** = Invert text\
**+8** = Hex string is used in place of text to use font map. If you
know the character's hex value, you can use it. Advantage: Get chars you
can't access any other way.\
**+16** = Update screen anyway. Just a throwback.\
Example: I want to use the large font AND invert text at x=4 and y=8
using "THIS IS TEXT" as text.

`identity(10,0+1+4,4,8,"THIS IS TEXT")`

\- or -

`identity(10,5,4,8,"THIS IS TEXT")`

*note that the flags have been condensed*

## Technical Details {#technical_details}

### Arguments

identity(10,flag,\[x,y\],"TEXT")

### Outputs

Put text into the graph buffer.
