---
revisions:
- author: KermMartian
  timestamp: '2010-08-03T21:58:09Z'
title: DBQuery1
---

## Description

A nifty little function that allows many kinds of operations to take
place with the PicArc database. The db_argument will vary depending on
what you are going to do, but not by that much. Just read below.

If function=\
**0:** Format this database. Just like anything that says "FORMAT", you
lose all preexisting data if that file happens to already exist. This is
not an undoable action, so take care. If the file does not already
exist, it is created and then formatted. After you use this command, you
will be able to work around with the file as a PicArc database. No
arguments needed\
**1:** Append a picture file. db_arg1 will be the pic number you want to
refer to. For Pic1, argument will be 1, for Pic9, it'll be 9. For Pic0,
it'll be 10. Keep in mind that this command only appends Pic files. No
insertion is available, so be careful on the order of Pic files you
append.\
**2:** Delete an entry. Starting at db_arg1 = 0, this command removes
the specified entry off of the database. This command is NOT undoable,
so exercise extreme caution.\
**3:** Retrieves the number of entries in the database.\
**4:** Works just like function 1, except that this command will attempt
to compress the pic. It automatically chooses the method of best
compression so you'll always get the smallest entry that the application
is capable of making, but using this command may be a little slower than
its counterpart. All the other commands transparently decompresses
compressed entries without much of a speed loss.\
**5:** Copies the entry denoted by db_arg1 to the graph buffer with the
logic indicated by db_arg2. 0: REPLACE, no screen update. 1: AND, no
screen update. 2: OR, no screen update. 3: XOR, no screen update. 4:
REPLACE, the screen is updated. 5: AND, the screen is updated. 6: OR,
the screen is updated. 7: XOR, the screen is updated.\
**6:** Copies an entry from the database as denoted by db_arg1 (starting
at zero) to a Pic file as denoted by db_arg2. For example. to copy entry
6 of the database "FOO" to Pic1, do the following:
identity(1,"FOO",6,6,1)

## Technical Details {#technical_details}

### Arguments

identity(0,"DATABASENAME",function,db_arg1,db_arg2)

### Outputs

Query database
