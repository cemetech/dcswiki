---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description==\u2019\u2018\u2019Not yet implemented\u2019\
    \u2019\u2019\nNot implemented yet. Hope to implement it soon, as it will be one\
    \ of\nthe more useful functions in the Celtic III PicArc extension code.\u2026\
    \u2019"
  timestamp: '2010-08-03T23:56:28Z'
title: CopyBox
---

## Description

**Not yet implemented**

Not implemented yet. Hope to implement it soon, as it will be one of the
more useful functions in the Celtic III PicArc extension code.

## Technical Details {#technical_details}

### Arguments

identity(11,...)

### Outputs

Copy a section of the screen, to be completed in a future version.
