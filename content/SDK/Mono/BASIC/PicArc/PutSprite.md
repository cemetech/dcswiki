---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== This command works just\nlike\
    \ the xLIB command real(1,\u2026)\n(\\[\\[BasicLibs:DrawSprite\\|DrawSprite\\\
    ]\\]) except that the Pic and\nthe coordinates on that Pic file are not d\u2026\
    \u2019"
  timestamp: '2010-08-03T23:25:15Z'
title: PutSprite
---

## Description

This command works just like the xLIB command real(1,...)
([DrawSprite]({{< ref "../XLib/DrawSprite.md" >}})) except that the
Pic and the coordinates on that Pic file are not defined. Instead, a
string consisting of hex digits is used to define the sprite as inline
data. For example, if you wanted to draw a black 8\*8 block at the
top-left corner of the screen with XOR logic and drawn immediately:

`identity(5,"FFFFFFFFFFFFFFFF",0,0,1,8,3,0,1)`

This is useful for those that want to display sprites without the use of
bulky image files. For large sprites, each byte goes LEFT first, then
DOWN, so specifying "80FF0180000180000180FF01" would be rendered as a
perfect box 3 bytes wide and 4 pixels tall.

Note that any missing arguments will default to the value of zero (0)
instead of 32 as in xLIB.

## Technical Details {#technical_details}

### Arguments

identity(5,"HEXSTRING",x,y,w,h,logic,flip,update_lcd)

### Outputs

Draws the sprite to the LCD buffer, optionally also updating the LCD.
