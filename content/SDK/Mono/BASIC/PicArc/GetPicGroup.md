---
revisions:
- author: KermMartian
  timestamp: '2010-08-03T23:17:07Z'
title: GetPicGroup
---

## Description

Outputs a list containing all the names of the pic files in the
specified TI-OS group file. Each name is denoted by a number, so 1
denotes Pic1, 0 denotes Pic10, and 68 denotes Pic68.

## Technical Details {#technical_details}

### Arguments

identity(2,"GROUPFILENAME")

### Outputs

List of the Pics in a group file.
