---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== A command that fills the\nscreen\
    \ with the same 8 x 8 tiles as defined by 16 hex digits. Along\nwith the right\
    \ and down shift arguments, this command makes\nscrolling\u2026\u2019"
  timestamp: '2010-08-03T23:45:55Z'
title: FillMap
---

## Description

A command that fills the screen with the same 8 x 8 tiles as defined by
16 hex digits. Along with the right and down shift arguments, this
command makes scrolling backgrounds far easier. The hexstring, as said
before, is 16 digits long, 8 pairs which makes up an 8 by 8 pixel tile.
The right and down arguments determine how many pixels the filled map
should be shifted. You can either choose not to shift or shift in either
direction up to 8 pixels. Since the routine does mod 8, you can actually
have these numbers increment and decrement bounded by -65565 to 65535 so
you should not actually have to test for bounds save for those already
mentioned.

LOGIC applies a method used to write the tiles to the buffer.
0=overwrite ; 1=AND ; 2=OR ; 3=XOR. If UpdateLCD is anything other than
zero, the screen is updated with whatever was just written to the
buffer.

## Technical Details {#technical_details}

The bounds are actually -99999 and 99999 and it'll still function
properly due to how the custom float to integer routine works. This
"bug" is not likely to be fixed. It's not hurting anything. It's safe.
No one is going to wait at a menu for as long as it takes to cycle
through the routine \*that\* many times.

### Arguments

identity(8,"HEXSTRING",Right,Down,LOGIC,UpdateLCD)

### Outputs

Fills the screen with the given tiles.
