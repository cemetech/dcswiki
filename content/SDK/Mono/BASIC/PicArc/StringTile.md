---
revisions:
- author: KermMartian
  timestamp: '2010-08-03T23:22:54Z'
title: StringTile
---

## Description

See xLIB command [DrawTileMap]({{< ref "../XLib/DrawTileMap.md" >}})
for information regarding the inputs. The only difference is that
"Matrix_name" is replaced with "BINARYSTRING". For this, you supply a
hex string converted to binary with the
[HexToBin]({{< ref "HexToBin.md" >}}) command. The Height and
Width property of the command is used to provide a two-dimensional
matrix feel to an inherently one-dimensional structure that is a string.

If 2ByteMode is set to something other than 0, then the input string is
considered words instead of bytes. This allows the user to use 4 hex
digits (two bytes) per tile so up to 65536 different tiles can be
accessed using this command. All other arguments function as they do
with the xLIB command.

As a developer, you should develop your tilemaps in hex and then use the
[HexToBin]({{< ref "HexToBin.md" >}}) command to convert it to
the format required by this command. To edit this tilemap, you should
use the [Edit1Byte]({{< ref "Edit1Byte.md" >}}) command while
this tilemap is in its binary format. Note that any missing arguments
will default to the value of zero (0) instead of 32 as in xLIB.

## Technical Details {#technical_details}

### Arguments

identity(4,"BINSTR",xPos,yPos,Width,Height,StartX,EndX,SStartY,SEndY,Pic#,Logic,TileSize,Update_LCD,2ByteMode)

### Outputs

Draws the specified tilemap to the graph buffer.
