---
revisions:
- author: Merthsoft
  comment: /\* Description \*/
  timestamp: '2010-08-03T23:32:16Z'
title: DrawBox
---

## Description

Draws a box which corners are (x_left,Y_top) and (x_right,y_bottom)
using a specified display method. Since this display method code is not
complete, let's say that it is xLIB-compatible with the drawshape
command, minus three. Add 128 to this number and the screen will also be
updated. In addition, the code to support this command in Doors CS is a
complete replacement of Celtic's code for the same, and should fix
almost all the bugs with this command present in Celtic.

## Technical Details {#technical_details}

### Arguments

identity(7,x_left,y_top,x_right,y_bottom,display_method)

### Outputs

Draw a rectangle to the graph buffer, and optionally also the LCD.
