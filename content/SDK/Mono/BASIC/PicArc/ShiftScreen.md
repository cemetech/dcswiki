---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Shifts the image on the\ngraphbuffer\
    \ a number of times in a direction indicated\nbelow:`<br>`{=html} : left : 1`<br>`{=html}\
    \ : right : 2`<br>`{=html}\n: up : 4`<br>`{=html} : up-left : 5`<br>`{=html} :\
    \ up-right\u2026\u2019"
  timestamp: '2010-08-03T23:28:06Z'
title: ShiftScreen
---

## Description

Shifts the image on the graphbuffer a number of times in a direction
indicated below:\
: left : 1\
: right : 2\
: up : 4\
: up-left : 5\
: up-right : 6\
: down : 8\
: down-left : 9\
: down-right : 10

Note that any area that does not have new contents shifted onto it, for
example the last row of the LCD if the image is shifted up, will be left
with its existing contents. If screen_update is zero, the screen will
not update. Otherwise, it will update.

## Technical Details {#technical_details}

### Arguments

identity(6,shift_number_of_times,direction,screen_update)

### Outputs

Shifts the screen in one of eight possible directions.
