---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Copies a pic by name from\nthe\
    \ group and puts it into a pic file of its corresponding number.\nAny preexisting\
    \ pic files are overwritten. ==Technical Details==\n==\u2026\u2019"
  timestamp: '2010-08-03T23:18:56Z'
title: ExtPicGroup
---

## Description

Copies a pic by name from the group and puts it into a pic file of its
corresponding number. Any preexisting pic files are overwritten.

## Technical Details {#technical_details}

### Arguments

identity(3,"GROUPFILENAME",pic_name_in_group)

### Outputs

Copy a Pic from a group into RAM. It is not possible to copy to a Pic
name other than the one specified for that Pic inside the group.
