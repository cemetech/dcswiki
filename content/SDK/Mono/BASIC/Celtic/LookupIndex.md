---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Uses the index file\nspecified\
    \ for quicker and much more consistent lookup of lines in a\nfile. Very useful\
    \ for large data files that would take a while to\nscan thr\u2026\u2019"
  timestamp: '2010-08-03T21:04:39Z'
title: LookupIndex
---

## Description

Uses the index file specified for quicker and much more consistent
lookup of lines in a file. Very useful for large data files that would
take a while to scan through using the normal lookup method.

The major drawback is that once a file is indexed, the file cannot be
changed unless re-indexed by command 23. A good feature is that for
purely read-only purposes, BOTH the program and index files can be
archived.

## Technical Details {#technical_details}

### Arguments

det(24,"FILENAME","INDEXNAME",this_line,\[by_this_many\])

### Outputs

(See Description)
