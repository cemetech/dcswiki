---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Extracts the nth item from\na\
    \ group in the order it was found from the previous command. Outputs\na real \u201C\
    1\u201D if this command was successful. ==Technical Details== \u2026\u2019"
  timestamp: '2010-08-03T02:28:36Z'
title: ExtGroup
---

## Description

Extracts the nth item from a group in the order it was found from the
previous command. Outputs a real "1" if this command was successful.

## Technical Details {#technical_details}

### Arguments

det(12,"GROUPFILENAME",extract_this_item)

### Outputs

Ans = 1 if the command succeeded, otherwise it was unsuccessful (for
example, specified an item larger than the number of items in the group)
Extracts one specified file out of a group.
