---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Instead of extracting the\nnth\
    \ item from a group, this determines how much data is in the file.\nAdd some reasonable\
    \ number, like 13, and you have the amount of\nmem\u2026\u2019"
  timestamp: '2010-08-03T02:29:54Z'
title: GroupMem
---

## Description

Instead of extracting the nth item from a group, this determines how
much data is in the file. Add some reasonable number, like 13, and you
have the amount of memory this file would take up in RAM if you were to
extract this. Great for group extraction managers.

## Technical Details {#technical_details}

### Arguments

det(13,"GROUPFILENAME",get_size_of_this_item)

### Outputs

Ans = the amount of memory consumed by the data portion of the specified
file in the specified group.
