---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Creates an index of a\nprogram\
    \ file and stores it as another program file as indicated by\nINDEXNAME. This\
    \ is for use with the following command,\n\\[\\[BasicLibs:Lookup\u2026\u2019"
  timestamp: '2010-08-03T03:00:07Z'
title: IndexFile
---

## Description

Creates an index of a program file and stores it as another program file
as indicated by INDEXNAME. This is for use with the following command,
[LookupIndex]({{< ref "LookupIndex.md" >}})

Any pre-existing files will be overwritten automatically, as if to
update the index, so please take care that an important file is not
specifed. If the new index cannot be fit in memory after the old one is
deleted, a new one is not created. Unfortunately, you will have lost the
old index. Keep a tight rein on memory.

## Technical Details {#technical_details}

### Arguments

det(23,"FILENAME","INDEXNAME")

### Outputs

The index is created from FILENAME and stored in INDEXNAME, if enough
memory exists.
