---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Converts the hex string\ninto\
    \ bytes and then inserts that data into a file starting at the\nspecified byte.\
    \ The zeroth byte is the start of the file.\n==Technical \u2026\u2019"
  timestamp: '2010-08-03T02:32:18Z'
title: BinWrite
---

## Description

Converts the hex string into bytes and then inserts that data into a
file starting at the specified byte. The zeroth byte is the start of the
file.

## Technical Details {#technical_details}

### Arguments

det(15,"FILENAME","HEXSTRING",start_at_this_byte)

### Outputs

Modifies the specified file.
