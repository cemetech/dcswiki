---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Performs an action with a\nfile\
    \ given the function_number provided. If function_number is\u2026 \\* 0\n= Archive/unarchive\
    \ a file. The output will be zero if it has been\u2026\u2019"
  timestamp: '2010-07-30T18:19:31Z'
title: ToggleSet
---

## Description

Performs an action with a file given the function_number provided. If
function_number is...

-   0 = Archive/unarchive a file. The output will be zero if it has been
    unarchived. Some other number if archived.
-   1 = Lock/unlock a file. The output is 0 if unlocked, 1 if it is
    locked, 2 if you attempted this on an appvar.
-   2 = Program to appvar and vice versa. A "1" will be output if the
    operation was a success.
-   3 = Hide/unhide a file. Outputs 0 if the file is visible, 1 if it is
    now hidden.
-   4 = Delete. Just. Deletes a file. Not undoable. Be careful on how
    you use this.
-   5 = Create. Creates a zero-sized file with the name you choose.
    There so you can insert your own stuff in it.
-   6 = Prog to string. Outputs the entire program file into a string so
    you can paste it into some other file.
-   7 = Output stats. Somewhat complicated. It outputs three letters
    followed by five digits indicating its size. Those three letters
    will tell you a lot about the file:
    -   1st letter: A= archived, R= in RAM
    -   2nd letter: H= hidden, V= visible
    -   3rd letter: W= writable, L= locked (uneditable)
    -   Naturally, archived files are uneditable. To get just the size
        of the file, do this: expr(sub(det(0,"FILENAME",7),4,5))

    :   

If you omit the function number, the command defaults to the
archive/unarchive function. Useful for saving bytes.

## Technical Details {#technical_details}

### Arguments

det(0,"FILENAME",\[function_number\])

### Outputs

**(See Description)**
