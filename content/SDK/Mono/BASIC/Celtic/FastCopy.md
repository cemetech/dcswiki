---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== It calls the\n\\[\\[IFastCopy\\\
    ]\\] routine to updates the screen with the plotsscreen\nbuffer. ==Technical Details==\
    \ ===Arguments=== det(19) ===Outputs===\nCopies the gr\u2026\u2019"
  timestamp: '2010-08-03T02:47:14Z'
title: FastCopy
---

## Description

It calls the [IFastCopy]({{< ref "/SDK/Mono/Asm/Ion/IFastCopy.md" >}}) routine
to updates the screen with the plotsscreen buffer.

## Technical Details {#technical_details}

### Arguments

det(19)

### Outputs

Copies the graph buffer to the LCD. Ans is unmodified.
