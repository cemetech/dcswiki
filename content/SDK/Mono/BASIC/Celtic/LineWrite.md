---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Writes the contents of a\nstring\
    \ to a file starting at the specified line. A newline character\nis appended to\
    \ the end of the string you want to write, so you do\nno\u2026\u2019"
  timestamp: '2010-07-30T20:43:18Z'
title: LineWrite
---

## Description

Writes the contents of a string to a file starting at the specified
line. A newline character is appended to the end of the string you want
to write, so you do not need to include it.

## Technical Details {#technical_details}

### Arguments

det(6,"FILENAME","STRINGTOWRITE",start_at_this_line)

### Outputs

**(See Description)**
