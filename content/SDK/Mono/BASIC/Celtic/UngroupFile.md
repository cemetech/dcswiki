---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Fully ungroups files using\nan\
    \ experimental routine designed to overcome the OS version @ \\| \\>\n1.13 limit.\
    \ Buggy, but should still work for program-type-only group\n\u2026\u2019"
  timestamp: '2010-08-03T02:24:53Z'
title: UngroupFile
---

## Description

Fully ungroups files using an experimental routine designed to overcome
the OS version @ \| \> 1.13 limit. Buggy, but should still work for
program-type-only group files.

Default behavior is to skip any pre-existing files, but if \[option\] is
set to 1, the behavior changes to overwrite. Careful when you do this
because overwritten files are gone. Forever if in RAM and in archive if
a garbage-collect was done over after the file went missing.

## Technical Details {#technical_details}

### Arguments

det(10,"GROUPFILENAME",\[option\])

### Outputs

Ungroups the specified file, if it exists.
