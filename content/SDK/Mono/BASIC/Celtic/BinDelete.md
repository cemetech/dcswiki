---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Deletes a number of bytes\nfrom\
    \ a file starting at the specified location. The zeroth byte is\nthe start of\
    \ the file. ==Technical Details== ===Arguments===\ndet(16\u2026\u2019"
  timestamp: '2010-08-03T02:37:59Z'
title: BinDelete
---

## Description

Deletes a number of bytes from a file starting at the specified
location. The zeroth byte is the start of the file.

## Technical Details {#technical_details}

### Arguments

det(16,"FILENAME",start_here,delete_this_many_bytes)

### Outputs

Deletes bytes from a file at a location.
