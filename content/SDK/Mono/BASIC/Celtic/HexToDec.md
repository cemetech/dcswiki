---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Converts up to 4 hex\ndigits back\
    \ to decimal. Useful for the previous command. If you pass\na string longer than\
    \ 4 digits, only the first four are read.\n==Technica\u2026\u2019"
  timestamp: '2010-08-03T21:32:24Z'
title: HexToDec
---

## Description

Converts up to 4 hex digits back to decimal. Useful for the previous
command. If you pass a string longer than 4 digits, only the first four
are read.

## Technical Details {#technical_details}

### Arguments

det(28,"HEXSTRING")

### Outputs

Returns integer converted from hex string.
