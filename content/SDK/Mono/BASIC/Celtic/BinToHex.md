---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Just the opposite of the\nHEXTOBIN\
    \ command. This converts a binary string back to hexadecimal.\nUseful for making\
    \ sense of some ASM code/data files. ==Technical\nDe\u2026\u2019"
  timestamp: '2010-08-03T02:43:08Z'
title: BinToHex
---

## Description

Just the opposite of the HEXTOBIN command. This converts a binary string
back to hexadecimal. Useful for making sense of some ASM code/data
files.

## Technical Details {#technical_details}

### Arguments

det(18,"BINSTRING")

### Outputs

Converts a binary string into a string of hex characters.
