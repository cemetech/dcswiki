---
revisions:
- author: Sorunome
  timestamp: '2011-06-01T15:14:28Z'
title: LineReplace
---

## Description

Replaces the specified line at start with the contents of your string.
If you specified a continue, it would truncate that many more lines. For
example, if you wanted to replace 4 lines with just the word "BAR" from
the second line from the file "FOO":

`det(8,"FOO","BAR",2,3)`

## Technical Details {#technical_details}

### Arguments

det(8,"FN","STR",line#,\[#oflines\])

### Outputs

Replaces lines in specified file
