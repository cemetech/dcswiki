---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Converts a number between\n0 and\
    \ 65,536 to its hexadecimal equivalent. The number of\nhexadecimal output to the\
    \ string will have its leading zeroes\nstripped so inpu\u2026\u2019"
  timestamp: '2010-08-03T21:36:02Z'
title: DecToHex
---

## Description

Converts a number between 0 and 65,536 to its hexadecimal equivalent.
The number of hexadecimal output to the string will have its leading
zeroes stripped so inputting 15 will result in "F" and 16 will result in
"10". Set autooverride to 1 to output all leading zeroes, which may be
useful for routines that require four hex digits at all times but cannot
spend the memory/time whipping up a BASIC solution to fill the missing
zeroes.

## Technical Details {#technical_details}

### Arguments

det(29,SomeRealNumber,\[autooverride\])

### Outputs

Returns up to four digits of hexadecimal in a string.
