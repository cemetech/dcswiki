---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== ==Technical Details== This\nis\
    \ a multi-purpose command used to read some of thesystem\u2019s status.\nJust\
    \ provide a function, and an output will result. ===Arguments=\u2026\u2019"
  timestamp: '2010-07-30T20:38:42Z'
title: ChkStats
---

## Description

## Technical Details {#technical_details}

This is a multi-purpose command used to read some of thesystem's status.
Just provide a function, and an output will result.

### Arguments

det(4,function)

Possible functions:\
\* 0 = Outputs value of free RAM

-   1 = Outputs value of free ROM (Archive)
-   2 = Hex string output of first ten digits of calc serial (still not
    quite enough to identify each calc)
-   3 = Get calc's OS version (string, such as "1.13") (note that the
    period will be bugged. It still works)

### Outputs

A number or string.
