---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Note: A \u201Cword\u201D in this\n\
    sense is two bytes. Useful for editing a binary string which entries\nare all\
    \ two bytes in length, such as a special string tilemap.\nYou\u2019re \u2026\u2019"
  timestamp: '2010-08-03T21:38:21Z'
title: EditWord
---

## Description

Note: A "word" in this sense is two bytes. Useful for editing a binary
string which entries are all two bytes in length, such as a special
string tilemap. You're required, however, to specify offset in bytes.
Also know that all words are stored little-endian. That means that the
least significant byte is stored before the most significant byte is.

This command, otherwise, works just like EDIT1BYTE. Its documentation is
rewritten here for convenience. Replaces a word in some string variable,
Str1 to Str0, with a replacement value 0 thru 65535 starting at some
specified byte (start is at 0). The string supplied is edited directly
so there's no output, per se. See EDIT1BYTE for more details.

The replacement is written in little-endian form and if the number is
between 0 and 255, the second byte is written in as a zero.

## Technical Details {#technical_details}

### Arguments

det(30,Str?,starting_byte,replace_with_this_word)

### Outputs

String is modified. However, there is no explicit return value.
