---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Outputs the binary\nequivalent\
    \ of an input string. For example, if you did this:\ndet(17,\u201C41424344\u201D\
    ) The result would be: \u201CABCD\u201D, since each of those\nhex digits eq\u2026\
    \u2019"
  timestamp: '2010-08-03T02:41:38Z'
title: HexToBin
---

## Description

Outputs the binary equivalent of an input string. For example, if you
did this:

`det(17,"41424344")`

The result would be: "ABCD", since each of those hex digits equate to
the tokens 'A','B','C', and 'D' in that order. Very useful if you want
to provide input that would other wise be impossible from BASIC, such as
the start of an ION shell header for the FINDPROG command, or supplying
binary-only input for some of the few commands that require it.

This command replaces the TOKENCODE command, since using HEXTOBIN is
more convenient due to the fact that most token equates are in
hexadecimal, not decimal. You can also grab as many tokens as you want
through this.

For a listing of tokens, see the contents of the MOREINFO folder located
within the main folder of the Celtic III distribution.

## Technical Details {#technical_details}

### Arguments

det(17,"HEXSTRING")

### Outputs

Converts hex code to binary.
