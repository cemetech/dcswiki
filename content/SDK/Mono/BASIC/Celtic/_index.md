---
bookCollapseSection: true
title: Celtic III
weight: 12
---

Celtic III is a set of BASIC libraries originally written by
Iambian Zenith (Rodger Weisman) designed to provide programs
with editing capabilities.

**[ToggleSet]({{< ref "ToggleSet.md" >}})** - det(0,"FN",f#)
0=Arc;1=Lk;2=PA;3=Hid;4=Del;5=Crt;6=PS;7=Stat\
**[NumToString]({{< ref "NumToString.md" >}})** - det(1,var)
Out:type code\
**[GetListElem]({{< ref "GetListElem.md" >}})** -
det(2,"LN",elem) out:lNAME(elem) -\> Real/Cpx\
**[GetArgType]({{< ref "GetArgType.md" >}})** -
det(3,argofanytype) filetype code\
**[ChkStats]({{< ref "ChkStats.md" >}})** - det(4,f#) 0=RAM
1=ARC 2=ID 3=OSVER\
**[LineRead]({{< ref "LineRead.md" >}})** -
det(5,"FN",line#,\[#oflines\])\
**[LineWrite]({{< ref "LineWrite.md" >}})** -
det(6,"STR","FN",line#)\
**[LineErase]({{< ref "LineErase.md" >}})** -
det(7,"FN",line#,\[#oflines\])\
**[LineReplace]({{< ref "LineReplace.md" >}})** -
det(8,"FN","STR",line#,\[#oflines\])\
**[FindProg]({{< ref "FindProg.md" >}})** -
det(9,\["SEARCH"\])\
**[UngroupFile]({{< ref "UngroupFile.md" >}})** -
det(10,"GFN")\
**[GetGroup]({{< ref "GetGroup.md" >}})** - det(11,"GFN")\
**[ExtGroup]({{< ref "ExtGroup.md" >}})** -
det(12,"GFN",item#)\
**[GroupMem]({{< ref "GroupMem.md" >}})** -
det(13,"GFN",item#)\
**[BinRead]({{< ref "BinRead.md" >}})** -
det(14,"FN",bytestart,#ofbytes)\
**[BinWrite]({{< ref "BinWrite.md" >}})** -
det(15,"HEX","FN",bytestart)\
**[BinDelete]({{< ref "BinDelete.md" >}})** -
det(16,"FN",bytestart,del#ofbytes)\
**[HexToBin]({{< ref "HexToBin.md" >}})** - det(17,"484558")\
**[BinToHex]({{< ref "BinToHex.md" >}})** - det(18,"BIN")\
**[FastCopy]({{< ref "FastCopy.md" >}})** - det(19)\
**[ExecHex]({{< ref "ExecHex.md" >}})** - det(20,"C9")\
**[Edit1Byte]({{< ref "Edit1Byte.md" >}})** -
det(21,Str?,StartByte,ReplaceWithThisByte)\
**[ModeChange]({{< ref "ModeChange.md" >}})** - det(22...) Not
implemented\
**[IndexFile]({{< ref "IndexFile.md" >}})** -
det(23,"FILENAME","NEWINDEXNAME")\
**[LookupIndex]({{< ref "LookupIndex.md" >}})** -
det(24,"FILENAME","INDEXNAME",line#,\[#oflines\])\
**[ErrorHandle]({{< ref "ErrorHandle.md" >}})** -
det(25,function,string)\
**[MatToStr]({{< ref "MatToStr.md" >}})** - det(26,Matrix#)\
**[StringRead]({{< ref "StringRead.md" >}})** -
det(27,"binstring",start,readThisMany)\
**[HexToDec]({{< ref "HexToDec.md" >}})** -
det(28,"HEXSTRING")\
**[DecToHex]({{< ref "DecToHex.md" >}})** -
det(29,SomeRealNumber,\[autoOverride\])\
**[EditWord]({{< ref "EditWord.md" >}})** -
det(30,start_byte,replace_with_this_word)\
**[BitOperate]({{< ref "BitOperate.md" >}})** -
det(31,value1,value2,logic)\
**[GetProgList]({{< ref "GetProgList.md" >}})** -
det(32,"SEARCHSTRING",\[type\])\
