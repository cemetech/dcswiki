---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== The search phrase is NOT\npart\
    \ of the filename. It is, instead, the leading characters in the\nfile you want\
    \ to search for. The output will be a space-delimited\n(se\u2026\u2019"
  timestamp: '2010-08-03T02:22:27Z'
title: FindProg
---

## Description

The search phrase is NOT part of the filename. It is, instead, the
leading characters in the file you want to search for. The output will
be a space-delimited (separated by spaces) string containing the names
of the files that match the search phrase. For example, say you had a
few programs that start with ":CCN". You would get all files that would
match that if you did:

`det(9,":CCN")`

If you wanted to get a complete program/appvar file listing then you'd
just omit the search phrase. You will get some oddly-named program files
as the first of the results, since they are system-reserved program
files with illegal file names.

A neat little example with this command is locating all ION-type files.
For that, you'd use an additional command inline with this one.

`det(9,det(17,"BB6DC930"))`

That command uses the HEXTOBIN command to convert the hex to the binary
that ASM programs (w/ION Head) are stored in.

**Note:** This command is not to be confused with GETPROGLIST, which
outputs a string consisting of files whose NAMES start with the
specified characters.

## Technical Details {#technical_details}

### Arguments

det(9,\["SEARCHPHRASE"\])

### Outputs

Finds specified file(s). See description.
