---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Outputs a real \\#\ndepicting\
    \ the type of variable that was input. A table follows of\nthe possible non-erroneous\
    \ input that can be made. Useful for\ndetermining whet\u2026\u2019"
  timestamp: '2010-07-30T18:37:05Z'
title: GetArgType
---

## Description

Outputs a real \# depicting the type of variable that was input. A table
follows of the possible non-erroneous input that can be made. Useful for
determining whether or not an output to a Celtic III command was a
number or an error. To prevent this from altering the Ans variable, use
this command within if() statements and the like.

-   Real = 0
-   List = 1
-   Matrix = 2
-   Equation = 3
-   String = 4
-   Complex = 12
-   Cpx List = 13

## Technical Details {#technical_details}

### Arguments

det(3,argument_of_any_type)

### Outputs

Real number indicating type of argument (see description).
