---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Reads a file (BASIC\nprogram)\
    \ starting at the specified line and, if included, reads that\nmany more lines.\
    \ If a number of lines is included, it adds that many\nmore\u2026\u2019"
  timestamp: '2010-07-30T20:40:06Z'
title: LineRead
---

## Description

Reads a file (BASIC program) starting at the specified line and, if
included, reads that many more lines. If a number of lines is included,
it adds that many more to the output. For example, if you wanted to read
from the first line and grab one more line after that from the file
"FOO", you'd have the following:

`det(5,"FOO",1,1)`

If you just want to find how many lines there are in the file, just try
to read the 0th line. It'll output a real number containing the number
of lines in the file.

## Technical Details {#technical_details}

### Arguments

det(5,"FILENAME",read_this_line,\[by_this_many_more_lines\])

### Outputs

**(See Description)**
