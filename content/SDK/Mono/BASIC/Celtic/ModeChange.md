---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Allows one to read or\nchange\
    \ stuff in Celtic III\u2019s hidden appvar. Used for settings or\nreading status\
    \ of delayed functions. The offset starts at 0. If\nfunction= \u2026\u2019"
  timestamp: '2010-08-03T02:52:50Z'
title: ModeChange
---

## Description

Allows one to read or change stuff in Celtic III's hidden appvar. Used
for settings or reading status of delayed functions. The offset starts
at 0.

If function= ...

-   0: Reads data at offset.
-   1: Writes \[newvalue\] to the offset. Old value is read out

More information about which offset does what will be explained later.
Try not to pass bogus values into this command, as you CAN cause
improper application function if used incorrectly.

## Technical Details {#technical_details}

### Arguments

det(22,function,offset,\[newvalue\])

### Outputs

Change Celtic modes.
