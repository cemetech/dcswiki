---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Gets an element from a\nlist.\
    \ If you could use the standard TI-OS function on it, you\nshould, but this command\
    \ is very useful if the list is ARCHIVED.\nOtherwise, i\u2026\u2019"
  timestamp: '2010-07-30T18:31:00Z'
title: GetListElem
---

## Description

Gets an element from a list. If you could use the standard TI-OS
function on it, you should, but this command is very useful if the list
is ARCHIVED. Otherwise, it works just like your standard L1(n) command.
If you omit the list element, or set it to zero, this function will
return the dimensions of the list.

RULES:\
If the name is predefined (L1 thru L6), just put that in quotes. If it
is a user-defined list, you must prefix the name with that little "L".
This is done automatically if you paste the name of the list from the
list variable list.

## Technical Details {#technical_details}

### Arguments

det(2,"LISTNAME",\[get_this_element\])

### Outputs

Specified element of the specified list
