---
revisions:
- author: Techboy6601
  comment: Made arguments more clear
  timestamp: '2012-11-09T04:44:54Z'
title: ErrorHandle
---

## Description

Executes BASIC code with an error handler installed. That means if the
code you execute can do anything it wants including divide by zero. The
execution will end but an obvious system error will not trigger.
Instead, this function will return with a value that indicates the error
condition.

If function = 0, the indicated string contains the name of the program
to execute with these special parameters. If function = 1, the string
contains the program code itself. Just don't do anything complicated in
it. Do not recurse and certainly try not to call this command inside any
program or subroutines you've already called this command in. If you
attempt to do so, the original calling routine will be lost.

**NOTE:** This function is not fully implemented. Reliability is not
guaranteed.

## Technical Details {#technical_details}

### Arguments

det(25,function,Str)

### Outputs

Executes BASIC code safely.
