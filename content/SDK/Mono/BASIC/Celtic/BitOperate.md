---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Performs a bitwise\noperation\
    \ between value1 and value2 using a supplied logic value.\nLogic: NONE=0 AND=1\
    \ ; OR=2 ; XOR=3. These numbers are converted to a\n16-bit \u2026\u2019"
  timestamp: '2010-08-03T21:42:51Z'
title: BitOperate
---

## Description

Performs a bitwise operation between value1 and value2 using a supplied
logic value. Logic: NONE=0 AND=1 ; OR=2 ; XOR=3. These numbers are
converted to a 16-bit number and then the operation is carried out in
order to maintain simplicity.

If the numbers are out of bounds, then the function will exit out with
an error. If a negative number is introduced, it will be treated as
65536-abs(value). This command really helps mask out hex digits but if
you use strings to store those digits, you'll need to use the HEXTODEC
command for each value you need. This can easily be done inline, as the
example shows:

`det(31,det(28,"AAAA"),det(28,"5555"),2)`

That example should turn out to be 65535, or "FFFF" if you were to
extend the idea of functions:

`det(29,det(31,det(28,"AAAA"),det(28,"5555"),2))`

## Technical Details {#technical_details}

### Arguments

det(31,value1,value2,logic)

### Outputs

Returns a number based on bitwise math on the inputs.
