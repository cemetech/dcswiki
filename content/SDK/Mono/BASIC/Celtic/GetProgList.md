---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== This function will return\na space-delimited\
    \ string consisting of the names of programs/appvars\nthat match partial name\
    \ of the SEARCHSTRING. Which is to say:\ndet(\u2026\u2019"
  timestamp: '2010-08-03T21:47:30Z'
title: GetProgList
---

## Description

This function will return a space-delimited string consisting of the
names of programs/appvars that match partial name of the SEARCHSTRING.
Which is to say:

`det(32,"TEMP") `

would return all program names that start with the characters "TEMP",
which may be something like "TEMP001 " or "TEMP001 TEMP002 TEMP003 ",
etc. If SearchType = 1, appvars will be searched for; if SearchType = 2,
groups will be searched for. This function will not look for lists. This
is easy enough to do using other BASIC commands and Celtic III commands.

NOTE: This command is NOT to be confused with FINDPROG, which outputs a
string consisting of files whose CONTENTS starts with the specified
string. Also use the fact that the final name in the list is terminated
with a space to make extracting names from the list easier.

Unlike all the other commands, this one will not find hidden variables.
You'll have to manually use the starting character that the file was
hidden by. Celtic III gives you all the tools needed to do this but
you'll need to read the CIII tutorial to find out how to do this.

## Technical Details {#technical_details}

### Arguments

det(32,"SERACHSTRING",SearchType)

### Outputs

Get filtered list of programs on the calculator.
