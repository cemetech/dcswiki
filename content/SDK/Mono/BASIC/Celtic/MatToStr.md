---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Outputs a hex string based\non\
    \ the input matrix, for use with PicArc\u2019s STRINGTILE command. Any\nentry\
    \ in a matrix over 255 will be treated as matrix_element\nmodulo\u2026\u2019"
  timestamp: '2010-08-03T21:12:18Z'
title: MatToStr
---

## Description

Outputs a hex string based on the input matrix, for use with PicArc's
STRINGTILE command. Any entry in a matrix over 255 will be treated as
matrix_element modulo 256; for example, 511=1023=255.

## Technical Details {#technical_details}

### Arguments

det(26,Matrix#)

### Outputs

Hex string based on input matrix.
