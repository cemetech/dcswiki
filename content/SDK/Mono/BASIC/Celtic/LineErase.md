---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Deletes a line from a file\nstarting\
    \ at the specified line and if specified, deletes that many\nmore lines from the\
    \ file. This is NOT an undoable operation, so be\nc\u2026\u2019"
  timestamp: '2010-07-30T20:44:31Z'
title: LineErase
---

## Description

Deletes a line from a file starting at the specified line and if
specified, deletes that many more lines from the file. This is NOT an
undoable operation, so be careful.

## Technical Details {#technical_details}

### Arguments

det(7,"FILENAME",delete_this_line,\[and_these_many_more\])

### Outputs

Deletes lines from a specified file.
