---
revisions:
- author: KermMartian
  timestamp: '2010-08-04T04:24:22Z'
title: ExecHex
---

## Description

It converts up to 767 bytes into code (1534 hex characters) and executes
it from the savesscreen buffer, useful for implementing simple stuff
that was missed by a library as large as Celtic III/DCS7. Although it
automatically appends a C9 (RET) instruction at the end of the string,
you can still seriously screw up your calculator if used with erroneous
ASM code.

## Technical Details {#technical_details}

### Arguments

det(20,"HEXSTRING")

### Outputs

Executes a given string of hex as binary.
