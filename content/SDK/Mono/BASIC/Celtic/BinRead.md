---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Reads a file starting at a\nspecified\
    \ byte and continues reading for the specified number of\nbytes. The output will\
    \ be a hex string representing those bytes. The\n\u2026\u2019"
  timestamp: '2010-08-03T02:31:10Z'
title: BinRead
---

## Description

Reads a file starting at a specified byte and continues reading for the
specified number of bytes. The output will be a hex string representing
those bytes. The zeroth byte is the start of the file. Additional note:
The starting point will accept a negative number but it'll only work
correctly if the file is in RAM.

## Technical Details {#technical_details}

### Arguments

det(14,"FILENAME",start_at_this_byte,read_this_many_bytes)

### Outputs

Reads a given number of bytes from a specified program or file.
