---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== This generates a\nspace-delimited\
    \ string containing all the file names of programs and\nappvars that are found\
    \ in the group in the order that they are\nfound. The or\u2026\u2019"
  timestamp: '2010-08-03T02:27:05Z'
title: GetGroup
aliases:
- /BasicLibs/GetGroup/
---

## Description

This generates a space-delimited string containing all the file names of
programs and appvars that are found in the group in the order that they
are found. The order is important, as it lends itself well to the
[ExtGroup]({{< ref "ExtGroup.md" >}}) command.

## Technical Details {#technical_details}

### Arguments

det(11,"GROUPFILENAME")

### Outputs

String specifying the contents of the given group file.
