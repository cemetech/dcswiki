---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Replaces a byte in some\nstring\
    \ variable, Str1 to Str0, using a decimal number 0 thru 255 in\nreplace_with_this_byte\
    \ within that string at this specified offset\n(at\u2026\u2019"
  timestamp: '2010-08-03T02:50:05Z'
title: Edit1Byte
---

## Description

Replaces a byte in some string variable, Str1 to Str0, using a decimal
number 0 thru 255 in replace_with_this_byte within that string at this
specified offset (at_this_byte), all of which start at zero. You will
get an error if you try to overwrite something past the end of this
string.

Since the specified string is edited directly, there will be no output
and thus keep Ans intact, except if an error occurred. This means you
cannot put in an immediate string as nothing but a discarded temporary
variable will have been changed. Useful for quiety editing a string.

## Technical Details {#technical_details}

### Arguments

det(21,StringVar,at_this_byte,replace_with_this_byte)

### Outputs

Replace a byte in a string.
