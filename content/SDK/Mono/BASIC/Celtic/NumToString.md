---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Converts any variable of\nthe\
    \ real type to a string. This includes real vars, real lists, and\nmatrices. Useful\
    \ if you want to store variables into program files,\np\u2026\u2019"
  timestamp: '2010-07-30T18:29:16Z'
title: NumToString
---

## Description

Converts any variable of the real type to a string. This includes real
vars, real lists, and matrices. Useful if you want to store variables
into program files, perhaps as a high score or just data storage for
later use. Note that storing numbers this way takes up a whole lot less
space unless your numbers are sporting many decimals.

If your input is a list and it is in quotes, you'll also be able to read
that list even if it's from archive. For rules about inputting a list,
see the [GetListElem]({{< ref "GetListElem.md" >}}) command
(det(2)).

## Technical Details {#technical_details}

### Arguments

det(1,variable_of_any_real_type)

### Outputs

**(See Description)**
