---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Works almost identically\nto BASIC\u2019\
    s sub() command, except that the output will be in\nhexadecimal and two-byte tokens\
    \ will read as two instead of one\nbyte. It is p\u2026\u2019"
  timestamp: '2010-08-03T21:25:41Z'
title: StringRead
---

## Description

Works almost identically to BASIC's sub() command, except that the
output will be in hexadecimal and two-byte tokens will read as two
instead of one byte. It is particularly useful for extracting data from
a string that may contain nonsensical data that simply needs to be
manipulated. If you allow the start point to be zero, the size of the
string in bytes is taken. For data manipulation, you should use the
[Edit1Byte]({{< ref "Edit1Byte.md" >}}) command.

## Technical Details {#technical_details}

### Arguments

det(27,"binstring",start,readthismany)

### Outputs

Substring is returned.
