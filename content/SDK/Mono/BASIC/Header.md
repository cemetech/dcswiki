---
revisions:
- author: KermMartian
  timestamp: '2012-08-22T14:18:53Z'
title: Program Header
weight: 1
aliases:
- /SDK/BASIC_Header/
---

All Doors CS versions to date, including Doors CS 7, automatically
recognize all BASIC programs as BASIC programs. However, if you want
your programs to be a bit more spiffy, Doors CS offers the ability to
add icons and descriptions to your program, as well as automatically
associating subprograms with your main program so that they can be
automatically unarchived before running and re-archived afterwards by
Doors CS. Finally, you can specify that certain BASIC programs, like
support subprograms, be hidden from the Doors CS desktop. The header
formats below explain each of these features. Please note that you can
preface any header starting in ::D... with a description line
(::"DESCRIPTION...)

## Icons

You can get more info about how icons are made
[here]({{< ref "SDK/icons.md" >}}). If you want an automated program to
let you draw 8x8 or 16x16 icons that will give you the hex code, I
recommend [TIFreak's Icon
Generator](http://www.ticalc.org/archives/files/fileinfo/398/39839.html).
If you want a program that can also add a description, set up the rest
of the header, and create the program for you, try [Skylite Icon
Suite](http://www.cemetech.net/programs/index.php?mode=file&id=765)

### 8x8 Icon {#x8_icon}

    ::DCS
    :"16-char_hex_icon
    :Program code

### 8x8 Icon + Subprograms {#x8_icon_subprograms}

    ::DCS
    :"16-char_hex_icon
    ::A
    :ZSUB1
    :ZSUB2
    :SUBPRG5
    ::
    :Program code

### 16x16 Icon {#x16_icon}

    ::DCS6
    :"64-char_hex_icon
    :Program code

### 16x16 Icon + Subprograms {#x16_icon_subprograms}

    ::DCS6
    :"64-char_hex_icon
    ::A
    :ZSUB1
    :ZSUB2
    :SUBPRG5
    ::
    :Program code

## Other Functions {#other_functions}

### Ignore Program {#ignore_program}

    :rand
    :Program code

or

    :Ans
    :Program code

### "Hybrid" MOS-DCS Header {#hybrid_mos_dcs_header}

    ::"DESCRIPTION
    ::"64-CHAR 16x16 ICON

or

    ::
    ::"64-CHAR 16x16 ICON
