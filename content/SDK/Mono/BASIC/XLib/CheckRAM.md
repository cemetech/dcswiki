---
revisions:
- author: Merthsoft
  comment: "Created page with \u2018==Description== Returns the amount of\nfree RAM,\
    \ measured in bytes. ==Technical Details== ===Arguments===\nreal(14) ===Outputs===\
    \ See description. \\[\\[Category:Basic\nLibraries\\]\\] \u2026\u2019"
  timestamp: '2010-08-04T00:55:00Z'
title: CheckRAM
---

## Description

Returns the amount of free RAM, measured in bytes.

## Technical Details {#technical_details}

### Arguments

real(14)

### Outputs

See description.
