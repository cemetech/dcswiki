---
revisions:
- author: Graphmastur
  timestamp: '2010-08-04T02:16:32Z'
title: ClearScreen
---

## Description

Clears off the graph buffer.

## Technical Details {#technical_details}

### Arguments

real(0,update_lcd)

**Update_lcd:**

:   0: Do not update
:   1: Update the cleared screen

### Outputs

**(See Description)**
