---
revisions:
- author: Merthsoft
  comment: "Created page with \u2018==Description== Reads the keypad for any\nkeypress.\
    \ This function will not block, and it\u2019ll return zero if no\nkeys were pressed\
    \ at the time. This routine supports multiple arrow\nk\u2026\u2019"
  timestamp: '2010-08-04T00:33:52Z'
title: GetKey
---

## Description

Reads the keypad for any keypress. This function will not block, and
it'll return zero if no keys were pressed at the time. This routine
supports multiple arrow key presses.

The key codes are as follows as it appears on the keyboard:

    +-------------------------------------------------+
    |   F1        F2        F3        F4        F5    |
    | [ 53 ]----[ 52 ]----[ 51 ]----[ 50 ]----[ 49 ]  |
    |                               ______________    |
    |   2nd       MODE      DEL    / 7    Up    8 \   |
    | [ 54 ]----[ 55 ]----[ 56 ]  |     / 4  \     |  |
    |                             | Left 2 3 Right |  |
    |   Alpha     x,T,0,n   STAT  |     \ 1  /     |  |
    | [ 48 ]----[ 40 ]----[ 32 ]  |  5   Down   6  |  |
    |                              \______________/   |
    |   MATH      APPS      PRGM      VARS      CLEAR |
    | [ 47 ]----[ 39 ]----[ 31 ]----[ 23 ]----[ 15 ]  |
    |                                                 |
    |   X^-1      SIN       COS       TAN       ^     |
    | [ 46 ]----[ 38 ]----[ 30 ]----[ 22 ]----[ 14 ]  |
    |                                                 |
    |   X^2       ,         (         )         /     |
    | [ 45 ]----[ 37 ]----[ 29 ]----[ 21 ]----[ 13 ]  |
    |                                                 |
    |   LOG       7         8         9         *     |
    | [ 44 ]----[ 36 ]----[ 28 ]----[ 20 ]----[ 12 ]  |
    |                                                 |
    |   LN        4         5         6         -     |
    | [ 43 ]----[ 35 ]----[ 27 ]----[ 19 ]----[ 11 ]  |
    |                                                 |
    |   STO       1         2         3         +     |
    | [ 42 ]----[ 34 ]----[ 26 ]----[ 18 ]----[ 10 ]  |
    |                                                 |
    |   ON        0         .         (-)       ENTER |
    | [ N/A]----[ 33 ]----[ 25 ]----[ 17 ]----[ 9  ]  |
    +-------------------------------------------------+
    Other (unofficial) codes:
    57 = left/right
    58 = left/right/down
    59 = up/down
    60 = up/left/down
    61 = up/right/down
    62 = up/right/left
    63 = all arrow keys mashed

## Technical Details {#technical_details}

### Arguments

real(8)

### Outputs

See description.
