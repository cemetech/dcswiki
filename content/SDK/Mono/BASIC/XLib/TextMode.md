---
revisions:
- author: Merthsoft
  comment: "Created page with \u2018==Description== Performs a text-related\nfunction,\
    \ if\u2019\u2018\u2019function\u2019\u2019\u2019 is: \\* 0: Unsets the text inverse\n\
    function \\* 1: Sets text inverse \\* 2: Sets lowercase access. (press\nAlpha\
    \ twic\u2026\u2019"
  timestamp: '2010-08-04T00:52:24Z'
title: TextMode
---

## Description

Performs a text-related function, if **function** is:

-   0: Unsets the text inverse function
-   1: Sets text inverse
-   2: Sets lowercase access. (press Alpha twice)
-   3: Unsets lowercase support
-   4: See note below.

Some menus in the TI-OS will reset these flags.

Note: For command 4, you can output a character onto the screen given a
character ASCII code, some number between 0 and 255. As a hint, the
space is 32, the upper-case letter "A" is 65, and the checkerboard
cursor is 241.

## Technical Details {#technical_details}

### Arguments

real(13,function,\[character\])

### Outputs

See description.
