---
revisions:
- author: Graphmastur
  timestamp: '2010-08-04T03:06:10Z'
title: DrawShape
---

## Description

Draws a shape define by points (**x1**,**y1**) and (**x2**,**y2**)

If **update_LCD** is 0, the screen will not be updated, otherwise it
will.

## Technical Details {#technical_details}

### Arguments

real(12,shape_type,x1,y1,x2,y2,update_LCD)\
**shape_type:**

:   0 = Draw a single BLACK LINE.
:   1 = Draw a single WHITE LINE.
:   2 = Draw a single INVERTED LINE.
:   3 = Draw an EMPTY RECTANGLE with BLACK OUTLINE
:   4 = Draw an EMPTY RECTANGLE with WHITE OUTLINE
:   5 = Draw an EMPTY RECTANGLE with INVERTED OUTLINE
:   6 = Draw a RECTANGLE with a BLACK FILLING
:   7 = Draw a RECTANGLE with a WHITE FILLING
:   8 = Draw a RECTANGLE with an INVERTED FILLING
:   9 = Draw a WHITE RECTANGLE with a BLACK OUTLINE
:   10 = Draw a BLACK RECTANGLE with a WHITE OUTLINE

**x1:** top left x coordinate\
**y1:** top left y coordinate\
**x2:** bottom x coordinate\
**y2:** bottom y coordinate\
**Update_lcd:**

:   0: Do not update
:   1: Update the cleared screen

### Outputs

See description.
