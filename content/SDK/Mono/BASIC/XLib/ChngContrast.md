---
revisions:
- author: KermMartian
  comment: 'moved \[\[BasicLibs:ChngConstrast\]\] to

    \[\[BasicLibs:ChngContrast\]\]: Typo'
  timestamp: '2011-01-26T17:25:04Z'
title: ChngContrast
---

## Description

Sets or gets the current contrast

## Technical Details {#technical_details}

### Arguments

real(5, function, value)\
**function:**

:   0: Set contrast
:   1: Current contrast is returned by the function into Ans, (**value**
    argument is ignored for this).

**value:** A value 0-39, where 0 is the lightest, and 39 is the darkest.

### Outputs

Sets or gets the contrast, depending on **function**:
