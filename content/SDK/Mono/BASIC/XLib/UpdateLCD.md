---
revisions:
- author: Graphmastur
  timestamp: '2010-08-04T02:46:57Z'
title: UpdateLCD
---

## Description

Updates the screen with any changes made since the last update.

## Technical Details {#technical_details}

### Arguments

real(6)

### Outputs

See description.
