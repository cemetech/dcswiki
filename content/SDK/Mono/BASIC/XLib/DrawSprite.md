---
revisions:
- author: Sonlen
  comment: /\* Arguments \*/
  timestamp: '2011-01-21T20:15:27Z'
title: DrawSprite
---

## Description

Draws a sprite to the buffer at a specified location. The sprite is
taken from a Pic file. This will work even if the Pic file is archived.

## Technical Details {#technical_details}

### Arguments

real(1, Spr_X, Spr_Y, Spr_Width, Spr_Height, sPIC_Num,sPIC_X, sPIC_Y,
Spr_Method, Spr_Flip, Spr_UpdateLCD

**Spr_X:** Upper-left of sprite to be displayed, onscreen, between 0 and
95\
**Spr_Y:** Upper-left of sprite to be displayed, between 0 and 63\
**Spr_Width:** Sprite width in bytes. An 8-pixel-wide sprite is 1 bye
wide , 16 pixels wide is 2 bytes, and so on.\
**Spr_Height:** Sprite height in pixels\
**sPIC_Num:** The picture number that this sprite is stored in. 1-9 for
Pic1-Pic9, 0 for Pic10.\
**sPIC_X:** The offset for the stored sprite in the pic horizontally.
Must be a value from 0-11, and is aligned on bytes (with every 8
pixels).\
**sPIC_Y:** The offset for the sprite in the pic vertically. Does not
need to be aligned.\
**Spr_Method:**

:   0: overwrite
:   1: AND logic
:   2: OR logic
:   3: XOR logic.
:   4: Invert Sprite

**Spr_Flip:**

:   0: No flip
:   1: Horizontal Flip

**Spr_UpdateLCD:**

:   0: Do not update
:   1: Update the cleared screen

### Outputs

**(See Description)**
