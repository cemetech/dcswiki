---
revisions:
- author: KermMartian
  timestamp: '2010-10-10T23:36:58Z'
title: ExecArcPrgm
---

## Description

**Ans** contains the name of the program you want to use in a string,
then you use the **function**, then you run the generated temporary
program file according to **temp_prog_number**. But, you'll need to know
the function codes to further explain how this works:

-   0: Copy file to a file numbered by temp_prog_number
-   1: Delete a temporary file numbered by temp_prog_number
-   2: Delete all temporary files.

For example, say you wanted to copy an archived program "FOO" to
prgmXTEMP002, do the following:\
"FOO":real(10,0,2):prgmXTEMP002

If you wanted to do this to an ASM program "BAR" and have it copied to
the 12th temporary file, do the following:\
"BAR":real(10,0,12):Asm(prgmXTEMP012)

If you decided you are done with the copy of "FOO" from the first
example and you wanted to delete it, do this:\
real(10,1,2)

That will delete prgmXTEMP002 but will not touch the original file.

If you want to clean up (get rid of all temp files), you can do the
following:\
real(10,2

Files will not be overwritten if you attempt to copy to a preexisting
temp file.

## Technical Details {#technical_details}

### Arguments

"PRGMNAME\
real(10,function,temp_prog_number)

You can run the resultant temporary program via one of the following,
depending on format:\
prgmXTEMP0XX or :Asm(prgmXTEMP0XX)

Note that only prgmXTEMP000 to prgmXTEMP015 are valid; anything above
prgmXTEMP015 will return Undefined.

### Outputs

See description.
