---
revisions:
- author: Graphmastur
  timestamp: '2010-08-04T02:47:47Z'
title: RunIndicator
---

## Description

Turns on or off the run indicator.

## Technical Details {#technical_details}

### Arguments

real(7, value)\
**value**:

:   0: Off
:   1: On

### Outputs

See description.
