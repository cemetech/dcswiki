---
bookCollapseSection: true
title: XLib
weight: 11
---

[xLIB](https://www.ticalc.org/archives/files/fileinfo/359/35907.html)
is a set of BASIC libraries that adds assorted extra functionality
to the language, originally created by tr1p1ea (Patrick Prendergast).

**[ClearScreen]({{< ref "ClearScreen.md" >}})** -
real(0,ULCD)\
**[DrawSprite]({{< ref "DrawSprite.md" >}})** -
real(1,x,y,w,h,p#,pX,pY,logic,flp,ULCD)\
**[DrawTileMap]({{< ref "DrawTileMap.md" >}})** -
real(2,MTRX#,xO,yO,W,H,xS,xE,yS,yE,p#,LOGIC,tSiz,ULCD)\
**[RecallPic]({{< ref "RecallPic.md" >}})** -
real(3,p#,LOGIC,ULCD)\
**[ScrollScreen]({{< ref "ScrollScreen.md" >}})** -
real(4,scrDir,scrStep,ULCD)\
**[ChngContrast]({{< ref "ChngContrast.md" >}})** -
real(5,f#,val). 0=SetContrast;1=ReturnCurrentContrast\
**[UpdateLCD]({{< ref "UpdateLCD.md" >}})** - real(6)\
**[RunIndicator]({{< ref "RunIndicator.md" >}})** -
real(7,value) 0=off;1=on\
**[GetKey]({{< ref "GetKey.md" >}})** - real(8) Returns
keycode\
**[CreatePIC]({{< ref "CreatePIC.md" >}})** -
real(9,f#,p#,\[1=64 rows\]) 0=stoPic;1=DelPic\
**[ExecArcPrgm]({{< ref "ExecArcPrgm.md" >}})** -
real(10,f#,tPrg#) Pass Ans="FN"\
**[GetCalcVer]({{< ref "GetCalcVer.md" >}})** - real(11)
Returns calcver. 0=83+;1=SE;2=84+;3=SE\
**[DrawShape]({{< ref "DrawShape.md" >}})** -
real(12,shapeType,x1,y1,x2,y2,ULCD)\
**[TextMode]({{< ref "TextMode.md" >}})** -
real(13,f#,\[chr\]) 0=uninv;1=inv;2=lwrcse;3=un;4=outputchar\
**[CheckRAM]({{< ref "CheckRAM.md" >}})** - real(14) Returns
amount of free RAM.
