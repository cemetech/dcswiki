---
revisions:
- author: Sonlen
  comment: /\* Arguments \*/
  timestamp: '2010-11-07T03:25:34Z'
title: CreatePIC
---

## Description

Draws or deletes a pic

## Technical Details {#technical_details}

### Arguments

real(9, function, pic#\[, full-size\])\
**function:**

:   0: Update the screen from buffer and copy what's there
:   1: Delete Pic (not undoable. Exercise caution)
:   2: Copy image from buffer to pic without updating screen

**Pic#:** a number between 1 and 255, corresponding to the pic number.
pic0=10

**Full-Size:** If this is 1, then the created image is 64 rows high. If
this argument is 0 or omitted, then the created image is the
BASIC-standard 63 rows high.

### Outputs

See description.
