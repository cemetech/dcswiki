---
revisions:
- author: Sorunome
  comment: /\* Arguments \*/
  timestamp: '2011-06-16T06:44:47Z'
title: DrawTileMap
---

## Description

Draws a tile map to the screen based off of the given matrix and
picture. This routine supports up to 65535 tiles, each starting from the
Pic number you specified and following to each numerically adjacent Pic
\# for each overflow. Let's say your starting Pic# is 5 and you want to
access tile 4324. There are 96 tiles per pic on 8x8 mode, so tile 4324
will be on pic 50. Pic data will be read from archive if need be. It'll
be slower, though. An additional feature added into the sprite routine
is the ability to invert the sprite prior to displaying. To do this, add
4 to whatever you use for LOGIC. For example, to invert a sprite and use
XOR display logic, use 7 since 3 + 4 = 7.

## Technical Details {#technical_details}

### Arguments

real(2,Matrix_number,x_offset,y_offset,Width,Height,SStartX,SEndX,SStartY,SEndY,Pic#,Logic,TileSize,Update_LCD)

**Matrix_number:** Number of matrix with map data. 0=\[A\], 9=\[J\]\
**x_offset:** x-offset for the part of map you want to draw.\
**y_offset:** y-offset for the part of map you want to draw.\
**Width:** width of the tilemap\
**Height:** height of the tilemap\
**SStartX:** column to start drawing. 0-12 (8x8) or 0-6 (16x16)\
**SEndX:** column to end drawing. 0-12 (8x8) or 0-6 (16x16)\
**SStartY:** row to start drawing. 0-8 (8x8) or 0-4 (16x16)\
**SEndY:** row to end drawing. 0-8 (8x8) or 0-4 (16x16)\
**Pic#:** Pic file to start looking for tiles from.\
**Logic:**

:   0: Overwrite logic
:   1: And logic
:   2: Or logic
:   3: Xor logic

**tile_size:** set to 16 for 16x16 tiles. Otherwise, it's 8x8.\
**Update_LCD:** Does not update the screen if 0, otherwise it does.\
The SStartX, SEndX, SStartY, and SEndY allow you to draw a "windowed"
map so you can fit things on the side, like a HUD or something. Drawing
will also be accelerated for smaller maps. For example, if you wanted to
draw an 8x8 tilesized map with a 1 tile blank border on all sides:

**SStartX:** 1\
**SEndX:** 11\
**SStartY:** 1\
**SEndY:** 7

### Outputs

See description.
