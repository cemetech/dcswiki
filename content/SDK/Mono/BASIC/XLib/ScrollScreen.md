---
revisions:
- author: KermMartian
  timestamp: '2010-09-13T18:50:15Z'
title: ScrollScreen
---

## Description

Scrolls the screen in any of 8 directions

## Technical Details {#technical_details}

### Arguments

real(4, scroll_direction, scroll_step, update_LCD\
**scroll_direction:**

:   0: Up
:   1: Down
:   2: Left
:   3: Right
:   4: UpLeft
:   5: UpRight
:   6: DownLeft
:   7: DownRight

**scroll_step:** The number pixels to scroll\
**Update_lcd:**

:   0: Do not update
:   1: Update the scrolled screen

### Outputs

See description
