---
revisions:
- author: KermMartian
  comment: /\* Arguments \*/
  timestamp: '2010-11-07T03:23:37Z'
title: RecallPic
---

## Description

Recalls a Pic into the graph buffer. This will work even if the Pic is
archived

## Technical Details {#technical_details}

### Arguments

real(3, rPIC_Num, rPIC_Method, Recall_UpdateLCD\[, Full-Size\])

**rPIC_Num:** The pic to recall. Pics from 0-255 are valid, instead of
the usual Pic0-Pic9\
**rPIC_Method:**

:   0: Overwrite
:   1: AND logic
:   2: OR logic
:   3: XOR Logic

**Update_lcd:**

:   0: Do not update
:   1: Update the cleared screen

**Full-Size:** If this is 1, then the created image is 64 rows high. If
this argument is 0 or omitted, then the created image is the
BASIC-standard 63 rows high.

### Outputs

**(See Description)**
