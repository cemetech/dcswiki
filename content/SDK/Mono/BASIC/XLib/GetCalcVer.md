---
revisions:
- author: Merthsoft
  comment: "Created page with \u2018==Description== Outputs a number to Ans\nindicating\
    \ which type of calculator is being used: \\* 0: 83+ \\* 1:\n83+ SE \\* 2: 84+\
    \ \\* 3: 84+ SE ==Technical Details== ===Arguments===\nreal(\u2026\u2019"
  timestamp: '2010-08-04T00:45:37Z'
title: GetCalcVer
---

## Description

Outputs a number to Ans indicating which type of calculator is being
used:

-   0: 83+
-   1: 83+ SE
-   2: 84+
-   3: 84+ SE

## Technical Details {#technical_details}

### Arguments

real(11)

### Outputs

See description.
