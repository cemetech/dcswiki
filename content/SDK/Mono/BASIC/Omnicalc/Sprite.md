---
revisions:
- author: Sorunome
  comment: /\* Arguments \*/
  timestamp: '2011-06-16T07:43:05Z'
title: Sprite
---

## Description

This command is to Draw sprites in XOR or in OR-logic.

## Technical Details {#technical_details}

### Arguments

real(20,pic#,XPic,YPic,Width,Hight,XPos,YPos,ULCD)\
**pic#:** Number of the pic were the sprite is in.\
**XPix:** X-Position of the sprite located in the pic measured in
pixels. But when the Number can't be devided by 8 it will be the next at
the bottom. e.g. 8 would be the same as 15, but 16 is again the next
sprite.\
**YPic:** Y-Position in Pixels, same as XPic, so just numbers dividable
through 8 will work, otherwise it is the next one smaller.\
**Width:** Width in Pixels, same as XPic, so just numbers dividable
through 8 will work, otherwise it is the next one smaller.\
**Hight:** Hight in Pixels, this time the real amount counts.\
**XPos:** X-Position of the sprite on the screen, the real amount
counts.\
**YPos:** Y-Position of the sprite on the screen, the real amount
counts.\
**Function:** Valid inputs:

::\*0: XOR-logic, screen is updated

::\*1: XOR-logic, screen is not updated

::\*2: OR-logic, screen is updated

::\*3: OR-logic, screen is not updated

### Outputs

A sprite with the XOR/OR logic at Xpos, YPosa with the screen updated/
not updated.
