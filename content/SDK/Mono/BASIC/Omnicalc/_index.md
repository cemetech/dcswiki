---
bookCollapseSection: true
title: Omnicalc
weight: 10
---

Michael Vincent's [Omnicalc](https://www.ticalc.org/archives/files/fileinfo/226/22626.html)
provided some additional BASIC functions to users. Doors CS implements
some, but not all of, the functions that Omnicalc does.

**[sprite]({{< ref "Sprite.md" >}})** -
real(20,pic#,XPic,YPic,Width,Hight,XPos,YPos,Function)\
**ExecAsm** - real(33,"HEXSTRING")
