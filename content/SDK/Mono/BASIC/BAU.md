---
revisions:
- author: KermMartian
  timestamp: '2006-10-24T03:58:17Z'
title: BASIC AutoUpgrade
aliases:
- /SDK/BAU/
---

The BAU, or BASIC AutoUpgrade feature, is a segment of code that
attempts to upgrade programs written with Doors CS 4, 5, or other
earlier versions that follow the now-obsolete method of putting the icon
as 16 hex digits on a line by itself. The new format adds a single quote
at the beginning of the line to prevent the TI-OS from trying to
mathematically evaluate the file:

    ::DCS
    :"ICONICONICONICON
