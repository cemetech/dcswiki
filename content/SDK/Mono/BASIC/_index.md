---
bookCollapseSection: true
revisions:
- author: Sorunome
  comment: "Created page with \u2018In this Category are all the Basic lib\ncommands\
    \ that come with DCS.\u2019"
  timestamp: '2011-06-15T08:12:44Z'
title: BASIC
weight: 2
aliases:
- "/Basic_Libraries/"
- "/SDK/Third-Party_BASIC_Libraries/"
---

Doors CS offers compatibility for all the major BASIC libraries,
including [Celtic III]({{< ref "/SDK/Mono/BASIC/Celtic/" >}}),
[XLib]({{< ref "/SDK/Mono/BASIC/XLib/" >}}),
[PicArc]({{< ref "/SDK/Mono/BASIC/PicArc/" >}}), and (partially)
[Omnicalc]({{< ref "/SDK/Mono/BASIC/Omnicalc/" >}}). Doors CS
also contains its own set of powerful
[Doors CS BASIC (DCSB) libraries]({{< ref "/SDK/Mono/BASIC/DCS/" >}}).
If you need to detect whether
the Doors CS-provided Hybrid BASIC libraries are present (which you
should do in any program or game that uses them), the 
[DCSB library index]({{< ref "/SDK/Mono/BASIC/DCS/" >}}) illustrates
the necessary code.

Some functions are using a program name to edit a program. To use an
AppVar instead of a program as input, prepend the string with the
"rowSwap(" token.
