---
title: Doors CS 7 (TI-83+/TI-84+)
weight: 10
---

Doors CS 7 for the monochrome calculators offers services for programs written
in TI-BASIC and assembly. Features that are available to all of them include:

* The [GUI system]({{< ref "./Asm/DCS/GUI/" >}}) provides a framework
  within which to build user interfaces using the same idioms as Doors CS itself.
* [Calcnet<sup>2.2</sup>]({{< ref "./Asm/DCS/CN2/" >}}) implements a
  networking system allowing many calculators to communicate concurrently
  across large networks, including across the Internet when used with an
  appropriate bridge in [globalCALCnet](https://www.cemetech.net/downloads/files/551/).

## Assembly

Doors CS is backward-compatible with [Ion]({{< ref "./Asm/Ion/" >}}) and [MirageOS]({{< ref "./Asm/MOS/" >}}), with 
capability to run programs written for either of those shells as well as providing
[its own set of libraries]({{< ref "./Asm/DCS/" >}}) beyond what is offered by those earlier systems.

Assembly programs can also handle [Associated Programs]({{< ref "./Asm/DCS/APs/" >}})
which launch a handler program when they are opened from Doors CS, useful for
programs that edit data stored on the calculator such as [Document
DE](https://www.ticalc.org/archives/files/fileinfo/398/39851.html).

The data related to the APIs are all defined in the [`dcs7.inc`]({{< resource "dcs7.inc" >}})
include file, which is also packaged with the [SDK downloads]({{< ref "../#sdk-downloads" >}}).

## BASIC

BASIC programs may optionally include a [Doors CS header]({{< ref "./BASIC/Header.md" >}}),
and have access to a variety of [extension libraries]({{< ref "./BASIC/" >}}) beyond the normal capabilities
of TI-BASIC. 
