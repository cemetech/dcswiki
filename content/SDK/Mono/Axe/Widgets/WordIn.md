---
revisions:
- author: Matrefeytontias
  timestamp: '2012-09-11T17:30:27Z'
title: WordIn
---

## Description

Adds a widget that allows the user to choose a number no higher than 255
and no lesser than 0 to the GUIStack. It's exactly the same widget as
[ByteIn]({{< ref "ByteIn.md" >}}) but with two bytes values.

*Key : ZInterval \[stats\] \[←\]*

## Inputs

-   **X,Y** : the spinner's position, relative to the container.
-   **curValue**<sup>r</sup> : the current value of the spinner. **Note
    that this field will be changed by the user when choosing a number
    !**
-   **min**<sup>r</sup> : the minimum allowed value.
-   **max**<sup>r</sup> : the maximum allowed value.

## Format

`Data(X, Y, curValue`<sup>`r`</sup>`, min`<sup>`r`</sup>`, max`<sup>`r`</sup>

    )→XXX
    WordIn(XXX

Or :

`WordIn(Data(X, Y, curValue`<sup>`r`</sup>`, min`<sup>`r`</sup>`, max`<sup>`r`</sup>`))`

Always follow this scheme when setting parameters !
