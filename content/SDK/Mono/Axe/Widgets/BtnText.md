---
revisions:
- author: Matrefeytontias
  comment: "Created page with \u201CYou are here : \\[\\[Developers\u2019 SDK\\]\\\
    ]\n\\>\\> \\[\\[AxeDCS GUI Reference\\]\\] \\>\\> \\[\\[BtnText\\]\\] ==\nDescription\
    \ == Adds a clickable button with custom position and text\nto the GUIStack. Jumps\
    \ \u2026\u201D"
  timestamp: '2012-09-11T15:23:19Z'
title: BtnText
---

## Description

Adds a clickable button with custom position and text to the GUIStack.
Jumps to a specific address in case of clicking.

*Key : CubicReg \[stats\] \[→\]*

## Inputs

-   **X,Y** : the button's position, relative to the current container.
-   **onClickFunc**<sup>r</sup> : the address to jump to in case of
    clicking.
-   **Text** : zero-terminated string to be displayed on the button.

## Format

`Data(X, Y, onClickFunc`<sup>`r`</sup>

    )→XXX
    "Text"[00
    BtnText(XXX

Or :

`Data(X, Y, `<sub>`L`</sub>`labelOnClickFunc`<sup>`r`</sup>

    )→XXX
    "Text"[00
    BtnText(XXX

Always follow this scheme when setting parameters !
