---
revisions:
- author: Matrefeytontias
  comment: /\* Format \*/
  timestamp: '2012-09-11T15:36:45Z'
title: BtnImg
---

## Description

Adds a clickable button with custom position and image. The program will
jump to the given address in case of clicking.

*Key : QuartReg \[stats\] \[→\]*

## Inputs

-   **X,Y** : the button's position, relative to the container.
-   **onClickFunc**<sup>r</sup> : the address to jump to when clicked.
-   **spriteWidth** : the number of 8\*5 sprites you want to align
    horizontally.
-   **buttonWidth** : the width of the button's clickable area in pixels
    (starting at the first column of the sprite).
-   **sprite** : the sprite data, composed of spriteWidth\*5 bytes.

## Format

`Data(X,Y, onClickFunc`<sup>`r`</sup>

    , spriteWidth, buttonWidth)→XXX
    [...      ← sprite data
    BtnImg(XXX

Or :

`Data(X,Y, `<sub>`L`</sub>`labelOnClickFunc`<sup>`r`</sup>

    , spriteWidth, buttonWidth)→XXX
    [...      ← sprite data
    BtnImg(XXX

Always follow this scheme when setting parameters !
