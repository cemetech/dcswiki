---
revisions:
- author: Matrefeytontias
  comment: "Created page with \u201CYou are here : \\[\\[Developers\u2019 SDK\\]\\\
    ]\n\\>\\> \\[\\[AxeDCS GUI Reference\\]\\] \\>\\> \\[\\[Radio\\]\\] == Description\n\
    == Adds a radio button with custom position, group, default value\nand text to\
    \ the \u2026\u201D"
  timestamp: '2012-09-11T17:42:41Z'
title: Radio
---

## Description

Adds a radio button with custom position, group, default value and text
to the GUIStack. Only one radio button of each group can be selected
(handled automatically by [Mouse]({{< ref "Mouse.md" >}})).

*Key : TInterval \[stats\] \[←\]*

## Inputs

-   **X,Y** : the button's position, relative to the container.
-   **group** : the group the button belong to. Groups are arbitrary
    defined by the user with one byte numbers.
-   **curStatus** : the current status of the button. 0 is unchecked and
    1 is checked. **Note that this field will be modified during user's
    input !**
-   **Text** : the text to be displayed at the right of the button.

## Format

    Data(X, Y, group, curStatus)→XXX
    "Text"[00
    Radio(XXX

Always follow this scheme when setting parameters !
