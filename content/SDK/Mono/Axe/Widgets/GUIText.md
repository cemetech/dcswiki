---
revisions:
- author: Matrefeytontias
  comment: "Created page with \u201CYou are here : \\[\\[Developers\u2019 SDK\\]\\\
    ]\n\\>\\> \\[\\[AxeDCS GUI Reference\\]\\] \\>\\> \\[\\[GUIText\\]\\] ==\nDescription\
    \ == Adds a text zone with custom position and font (not\nyet activated by Doors\
    \ CS\u2026\u201D"
  timestamp: '2012-09-11T15:51:03Z'
title: GUIText
---

## Description

Adds a text zone with custom position and font (not yet activated by
Doors CS 7) to the GUIStack.

*Key : T-Test \[stats\] \[←\]*

## Inputs

-   **X,Y** : text zone's position, relative to the container.
-   **font** : **always leave it 0 !**
-   **Text** : zero-terminated string to be displayed at the given
    position.

## Format

    Data(X, Y, font = 0)→XXX
    "Text"[00
    GUIText(XXX

Always follow this scheme when setting parameters !
