---
revisions:
- author: Matrefeytontias
  comment: "Created page with \u201CYou are here : \\[\\[Developers\u2019 SDK\\]\\\
    ]\n\\>\\> \\[\\[AxeDCS GUI Reference\\]\\] \\>\\> \\[\\[TextMultiline\\]\\] ==\n\
    Description == Adds a multiline text prompter with custom position,\nsize and\
    \ offset to\u2026\u201D"
  timestamp: '2012-09-11T17:15:49Z'
title: TextMultiline
---

## Description

Adds a multiline text prompter with custom position, size and offset to
the GUIStack. It works the same way as
[TextLineIn]({{< ref "TextLineIn.md" >}}).

*Key : 1-PropZTest( \[stats\] \[←\]*

## Inputs

-   **X,Y** : the text input's position, relative to the container.
-   **rows** : the number of rows of the text input.
-   **W** : the width of the text input.
-   **offset**<sup>r</sup> : the position of the first character to
    display (starting from 0).
-   **Text** : the text displayed in the text input. **Note that this
    field will be modified by the user when inputting new text !**

## Format

`Data(X, Y, rows, W, offset`<sup>`r`</sup>

    )→XXX
    "Text"[00
    TextMultiline(XXX

Always follow this scheme when setting parameters !
