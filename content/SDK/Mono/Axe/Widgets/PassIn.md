---
revisions:
- author: Matrefeytontias
  comment: "Created page with \u201CYou are here : \\[\\[Developers\u2019 SDK\\]\\\
    ]\n\\>\\> \\[\\[AxeDCS GUI Reference\\]\\] \\>\\> \\[\\[PassIn\\]\\] == Description\n\
    == Adds a single-line password prompter with custom width, maximum\ncharacters\
    \ all\u2026\u201D"
  timestamp: '2012-09-11T16:08:50Z'
title: PassIn
---

## Description

Adds a single-line password prompter with custom width, maximum
characters allowed, offset and default text to the GUIStack. It's
exactly the same as [TextLineIn]({{< ref "TextLineIn.md" >}}) but with
the entered text replaced by 'x'.

*Key : 2-SampTTest \[stats\] \[←\]*

## Inputs

-   **X,Y** : the password input's position, relative to the container.
-   **W** : the password input's width in pixels.
-   **maxChar**<sup>r</sup> : the maximum number of characters allowed.
-   **offset**<sup>r</sup> : the position in the string of the first
    character to display (starting from 0).
-   **defaultText** : the text displayed in the password input (it'll be
    replaced by 'x' indeed). **Note that this field will be modified by
    the user when inputting new password !**

## Format

`Data(X, Y, W, maxChar`<sup>`r`</sup>`, offset`<sup>`r`</sup>

    )→XXX
    "Pass"[00
    PassIn(XXX

Always follow this scheme when setting parameters !
