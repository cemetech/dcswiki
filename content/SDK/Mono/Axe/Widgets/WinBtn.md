---
revisions:
- author: Matrefeytontias
  comment: "Created page with \u201CYou are here : \\[\\[Developers\u2019 SDK\\]\\\
    ]\n\\>\\> \\[\\[AxeDCS GUI Reference\\]\\] \\>\\> \\[\\[WinBtn\\]\\] == Description\n\
    == Adds window buttons to a container, allowing the handle of window\nfunctions\
    \ Cl\u2026\u201D"
  timestamp: '2012-09-11T14:56:18Z'
title: WinBtn
---

## Description

Adds window buttons to a container, allowing the handle of window
functions Close, Maximize and Minimize. It only works with
[SmallWin]({{< ref "SmallWin.md" >}}) and
[LargeWin]({{< ref "LargeWin.md" >}}), it'll crash if used with
[NullContain]({{< ref "NullContain.md" >}}).

*Key : QuadReg \[stats\] \[→\]*

## Inputs

-   **Buttons** : this number defines wihch buttons you're using : \[-\]
    \[O\] \[X\] 0 0 0 0 0 → activate bit to activate button.
-   **onclickFunc1**<sup>r</sup> : the address of the routine the
    program will jump to if the first button \[-\] is clicked.
-   **onClickFunc2**<sup>r</sup> : same for button 2 \[O\].
-   **onClickFunc3**<sup>r</sup> : same for button 3 \[X\].

## Format

`Data(buttons, onClickFunc1`<sup>`r`</sup>`, onClickFunc2`<sup>`r`</sup>`, onClickFunc3`<sup>`r`</sup>

    )→XXX
    WinBtn(XXX

Or :

`Data(buttons, `<sub>`L`</sub>`labelOnClickFunc1`<sup>`r`</sup>`, `<sub>`L`</sub>`labelOnClickFunc2`<sup>`r`</sup>`, `<sub>`L`</sub>`labelOnClickFunc3`<sup>`r`</sup>

    )→XXX
    WinBtn(XXX

Or :

`WinBtn(Data(...))`
