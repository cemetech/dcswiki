---
revisions:
- author: Matrefeytontias
  comment: "Created page with \u201CYou are here : \\[\\[Developers\u2019 SDK\\]\\\
    ]\n\\>\\> \\[\\[AxeDCS GUI Reference\\]\\] \\>\\> \\[\\[Chkbox\\]\\] == Description\n\
    == Adds a checkbox with custom position, group, default value and\ntext to the\
    \ GUI\u2026\u201D"
  timestamp: '2012-09-11T17:45:13Z'
title: Chkbox
---

## Description

Adds a checkbox with custom position, group, default value and text to
the GUIStack. It's exactly the same widget as
[Radio]({{< ref "Radio.md" >}}), but several boxes can be checked into
each group.

*Key : 2-SampZInt( \[stats\] \[←\]*

## Inputs

-   **X,Y** : the box's position, relative to the container.
-   **group** : the group the box belong to. Groups are arbitrary
    defined by the user with one byte numbers.
-   **curStatus** : the current status of the box. 0 is unchecked and 1
    is checked. **Note that this field will be modified during user's
    input !**
-   **Text** : the text to be displayed at the right of the box.

## Format

    Data(X, Y, group, curStatus)→XXX
    "Text"[00
    Chkbox(XXX

Always follow this scheme when setting parameters !
