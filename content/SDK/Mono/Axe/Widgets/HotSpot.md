---
revisions:
- author: Matrefeytontias
  comment: "Created page with \u201CYou are here : \\[\\[Developers\u2019 SDK\\]\\\
    ]\n\\>\\> \\[\\[AxeDCS GUI Reference\\]\\] \\>\\> \\[\\[HotSpot\\]\\] ==\nDescription\
    \ == Adds an invisible clickable area with custom position\nto the GUIStack. The\
    \ pr\u2026\u201D"
  timestamp: '2012-09-11T15:43:53Z'
title: HotSpot
---

## Description

Adds an invisible clickable area with custom position to the GUIStack.
The program will jump to the given address in case of clicking.

*Key : LinReg(a+bx) \[stats\] \[→\]*

## Inputs

-   **X,Y** : the hotspot's position, relative to the container.
-   **W,H** : the hotspot's width and height in pixels.
-   **onClickFunc**<sup>r</sup> : the address to jump to in case of
    clicking.

## Format

`Data(X, Y, W, H, onClickFunc`<sup>`r`</sup>

    )→XXX
    HotSpot(XXX

Or :

`Data(X, Y, W, H, `<sub>`L`</sub>`labelOnClickFunc`<sup>`r`</sup>

    )→XXX
    HotSpot(XXX

Or again :

`HotSpot(Data(...))`

Always follow this scheme when setting parameters !
