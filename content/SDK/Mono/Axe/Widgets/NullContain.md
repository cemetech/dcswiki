---
revisions:
- author: Matrefeytontias
  comment: /\* Description \*/
  timestamp: '2012-09-10T17:16:58Z'
title: NullContain
---

## Description

Adds an invisible container to the GUIStack, allowing the drawing of
widgets. The background can be transparent or opaque.

It's one of the three groupmasters, with
[SmallWin]({{< ref "SmallWin.md" >}}) and
[LargeWin]({{< ref "LargeWin.md" >}}).

*Key : 2-Var Stats \[stats\] \[→\]*

## Inputs

-   **Background** : 0 for transparent, 1 for opaque.

## Format

    Data(background)->XXX
    NullContain(XXX

Or :

`NullContain(Data(background))`
