---
revisions:
- author: Matrefeytontias
  timestamp: '2012-09-10T17:16:19Z'
title: Alert
---

## Description

This function **draws and renders** a modal message box with a custom
text positioned relatively to the container. Don't forget that it
doesn't add or remove anything from the GUIStack.

*Key : 1-Var Stats \[stats\] \[→\]*

## Inputs

-   **relativeX, relativeY** : the position of the text, relative to the
    message box (it's a [SmallWin]({{< ref "SmallWin.md" >}}) widget).
-   **font** : **always leave it 0**.
-   **Text** : the text to be displayed in the message box.

## Format

    Data(relativeX,relativeY,font)→XXX
    "Text"[00
    Alert(XXX

Always set the parameters by following this scheme !
