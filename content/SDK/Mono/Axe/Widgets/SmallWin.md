---
revisions:
- author: Matrefeytontias
  comment: /\* Description \*/
  timestamp: '2012-09-11T14:36:00Z'
title: SmallWin
---

## Description

Adds a small window container with custom position, icon and title to
the GUIStack. **It doesn't handle any window function !**

The window is 81\*49, and with
[NullContain]({{< ref "NullContain.md" >}}) and
[LargeWin]({{< ref "LargeWin.md" >}}), one of the three containers that
starts a GUI form.

*Key : Med-Med \[stats\] \[→\]*

## Inputs

-   **X, Y** : window position on the screen (0 - 15).
-   **5x5icon** : 5 bytes icon, byte-aligned.
-   **Title** : zero-terminated title string.

## Format

    Data(X,Y)→XXX
    [xxxxxxxxxx
    "Title"[00
    SmallWin(XXX

You can set the parameters in different ways, but you **must** put in a
static pointer the return of the **first** data command (*Data()* or
*\[HEX\]*, always put the text in the last position).
