---
revisions:
- author: Matrefeytontias
  comment: "Created page with \u201CYou are here : \\[\\[Developers\u2019 SDK\\]\\\
    ]\n\\>\\> \\[\\[AxeDCS GUI Reference\\]\\] \\>\\> \\[\\[TextLineIn\\]\\] ==\n\
    Description == Adds a single-line text prompter with custom width,\nmaximum characters\
    \ all\u2026\u201D"
  timestamp: '2012-09-11T16:01:55Z'
title: TextLineIn
---

## Description

Adds a single-line text prompter with custom width, maximum characters
allowed, offset and default text to the GUIStack.

*Key : 2-SampZTest( \[stats\] \[←\]*

## Inputs

-   **X,Y** : the text input's position, relative to the container.
-   **W** : the text input's width in pixels.
-   **maxChar**<sup>r</sup> : the maximum number of characters allowed.
-   **offset**<sup>r</sup> : the position in the string of the first
    character to display (starting from 0).
-   **defaultText** : the text displayed in the text input. **Note that
    this field will be modified by the user when inputting new text !**

## Format

`Data(X, Y, W, maxChar`<sup>`r`</sup>`, offset`<sup>`r`</sup>

    )→XXX
    "Text"[00
    TextLineIn(XXX

Always follow this scheme when setting parameters !
