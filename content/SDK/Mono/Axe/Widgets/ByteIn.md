---
revisions:
- author: Matrefeytontias
  comment: "Created page with \u201CYou are here : \\[\\[Developers\u2019 SDK\\]\\\
    ]\n\\>\\> \\[\\[AxeDCS GUI Reference\\]\\] \\>\\> \\[\\[ByteIn\\]\\] == Description\n\
    == Adds a widget that allows the user to choose a number no higher\nthan 255 and\
    \ n\u2026\u201D"
  timestamp: '2012-09-11T17:25:18Z'
title: ByteIn
---

## Description

Adds a widget that allows the user to choose a number no higher than 255
and no lesser than 0 to the GUIStack. It provides custom position,
bounds and default value.

*Key : 2-PropZTest( \[stats\] \[←\]*

## Inputs

-   **X,Y** : the spinner's position, relative to the container.
-   **curValue** : the current value of the spinner. **Note that this
    field will be changed by the user when choosing a number !**
-   **min** : the minimum allowed value.
-   **max** : the maximum allowed value.

## Format

    Data(X, Y, curValue, min, max)→XXX
    ByteIn(XXX

Or :

`ByteIn(Data(X, Y, curValue, min, max))`

Always follow this scheme when setting parameters !
