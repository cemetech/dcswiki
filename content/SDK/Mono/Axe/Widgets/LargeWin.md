---
revisions:
- author: Matrefeytontias
  comment: /\* Description \*/
  timestamp: '2012-09-11T14:34:35Z'
title: LargeWin
---

## Description

Adds a fullscreen window container with custom icon and title to the
GUIStack. **It doesn't handle any window function !**

With [NullContain]({{< ref "NullContain.md" >}}) and
[SmallWin]({{< ref "SmallWin.md" >}}), it's one of the three containers
that starts a GUI form.

*Key : LinReg(ax+b) \[stats\] \[→\]*

## Inputs

-   **5x5icon** : 5 bytes byte-aligned icon.
-   **Title** : zero-terminated title string.

## Format

    [XxXxXxXxXx→XXX
    "Title"[00
    LargeWin(XXX

**Always follow this scheme when setting parameters !**
