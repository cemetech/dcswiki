---
title: AxeDCS
weight: 10
bookCollapseSection: true
aliases:
- /AxeDCS_AP_system/
- /AxeDCS_GUI_Reference/
- /Setting_up_AxeDCS_in_an_AP-enabled_program/
---

[AxeDCS][axedcs] is an [Axe][axe] Axiom which provides bindings to the majority
of the Doors CS 7 GUI API. You can use it only with Doors CS 7 or higher; it
will throw an "Invalid token" error if you try to compile a program with it for
a shell other than Doors CS.

[axe]: https://www.ticalc.org/archives/files/fileinfo/456/45659.html
[axedcs]: https://www.ticalc.org/archives/files/fileinfo/450/45022.html

You use the GUI API with AxeDCS the same way you would [use it with ASM]({{< ref
"/SDK/Mono/Asm/DCS/GUI/" >}}): you push the widgets to the GUI stack, and then
call a render function or a mouse function.

## GUI Widget Tokens {#axedcs_gui_widget_tokens}

| AxeDCS Token                                    | Function                                                                                                                                                                  | ASM equivalent                                              |
|-------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------|
| [Alert]({{< ref "Widgets/Alert.md" >}})                 | Draws a modal message box with custom text.                                                                                                                               |                                                             |
| [NullContain]({{< ref "Widgets/NullContain.md" >}})     | Adds an invisible container with opaque or transparent background to the GUIStack. With LargeWin and SmallWin, it's one of the three widgets that must starts a GUI form. | [GUIRnull]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRnull.md" >}})                   |
| [LargeWin]({{< ref "Widgets/LargeWin.md" >}})           | Adds a fullscreen window to the GUIStack. With NullContain and SmallWin, it's one of the three groupmasters.                                                              | [GUIRLargeWin]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRLargeWin.md" >}})           |
| [SmallWin]({{< ref "Widgets/SmallWin.md" >}})           | Adds a 81\*49 window to the GUIStack. With NullContain and LargeWin, it's one of the three groupmasters.                                                                  | [GUIRSmallWin]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRSmallWin.md" >}})           |
| [WinBtn]({{< ref "Widgets/WinBtn.md" >}})               | Adds window buttons to the GUIStack. They'll be drawn on the current groupmaster (except [NullContain]({{< ref "NullContain.md" >}}); it'll surely crash).                | [GUIRWinButtons]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRWinButtons.md" >}})       |
| [BtnText]({{< ref "Widgets/BtnText.md" >}})             | Adds a clickable button with text to the GUIStack.                                                                                                                        | [GUIRButtonText]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRButtonText.md" >}})       |
| [BtnImg]({{< ref "Widgets/BtnImg.md" >}})               | Adds a clickable button with 5\*5 sprite to the GUIStack.                                                                                                                 | [GUIRButtonImg]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRButtonImg.md" >}})         |
| [HotSpot]({{< ref "Widgets/HotSpot.md" >}})             | Adds an invisible clickable area to the GUIStack.                                                                                                                         | [GUIRHotspot]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRHotspot.md" >}})             |
| [GUIText]({{< ref "Widgets/GUIText.md" >}})             | Adds text with position relative to the groupmaster to the GUIStack.                                                                                                      | [GUIRText]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRText.md" >}})                   |
| [TextLineIn]({{< ref "Widgets/TextLineIn.md" >}})       | Adds a single-line text prompter to the GUIStack.                                                                                                                         | [GUIRTextLineIn]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRTextLineIn.md" >}})       |
| [PassIn]({{< ref "Widgets/PassIn.md" >}})               | Adds a single-line password prompter to the GUIStack.                                                                                                                     | [GUIRPassIn]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRPassIn.md" >}})               |
| [TextMultiline]({{< ref "Widgets/TextMultiline.md" >}}) | Adds a multiline text prompter to the GUIStack.                                                                                                                           | [GUIRTextMultiline]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRTextMultiline.md" >}}) |
| [ByteIn]({{< ref "Widgets/ByteIn.md" >}})               | Adds an integer spinner in range \[0,255\] to the GUIStack.                                                                                                               | [GUIRByteInt]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRByteInt.md" >}})             |
| [WordIn]({{< ref "Widgets/WordIn.md" >}})               | Adds an integer spinner in range \[0,65535\] to the GUIStack.                                                                                                             | [GUIRWordInt]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRWordInt.md" >}})             |
| [Radio]({{< ref "Widgets/Radio.md" >}})                 | Adds a radio button to the GUIStack.                                                                                                                                      | [GUIRRadio]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRRadio.md" >}})                 |
| [Chkbox]({{< ref "Widgets/Chkbox.md" >}})               | Adds a checkbox to the GUIStack.                                                                                                                                          | [GUIRCheckbox]({{< ref "/SDK/Mono/Asm/DCS/GUI/Items/GUIRCheckbox.md" >}})           |

## GUI functions {#axedcs_gui_functions}

-   [GUIEvent]({{< ref "Functions/GUIEvent.md" >}}) - Specify that the following
    code is part of a routine called by Mouse. Always put it as first
    instruction in this case !
-   [Mouse]({{< ref "Functions/Mouse.md" >}}) - Renders the GUI and begin input
    functions.
-   [FindFirst]({{< ref "Functions/FindFirst.md" >}}) - Returns the starting of
    the **data** of the first non-groupmaster widget of the GUIStack.
-   [FindNext]({{< ref "Functions/FindNext.md" >}}) - Returns the starting of the
    **data** of the widget following the current one in the GUIStack.
    **Always call it directly after FindFirst or FindNext since it uses
    ASM registers !**
-   [ClearGUI]({{< ref "Functions/ClearGUI.md" >}}) - Removes all the widgets from
    the GUIStack.

## Associated Programs

Refer to the [Associated Programs]({{< ref "/SDK/Mono/Asm/DCS/APs/" >}}) page for
more info on the AP system.

| AxeDCS Token                                                 | Function                                                                           | ASM equivalent                                      |
|--------------------------------------------------------------|------------------------------------------------------------------------------------|-----------------------------------------------------|
| [FileOpen]({{< ref "Functions/FileOpen.md" >}})     | Starts a GUI input for the user to choose a file to open with the current program. | [FileOpen]({{< ref "/SDK/Mono/Asm/DCS/APs/FileOpen.md" >}})     |
| [FileSaveAs]({{< ref "Functions/FileSaveAs.md" >}}) | Starts a GUI input for the user to choose a name for a file to be saved.           | [FileSaveAs]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSaveAs.md" >}}) |

### Setting up AP-enabled programs

AxeDCS works a bit differently with AP-enabled program, a.k.a *viewers*.

Your AP-enabled programs will need a special header to work correctly ;
a header that Axe can't provide. So if you want to compile an AP-enabled
program :

-   copy the content of DCSAP.8xp at the really beginning of your file,
    before any code and right after the Axe source header

-   AP-enabled programs starts at a different address than the beginning
    of the code when it's opened through a file. Set the
    beginning of the code that open the current file with

    ```
    :Lbl APStart
    :Asm(DDE5E1)->*variable name here*
    ```

    and then the indicated variable will contain a pointer on the opened
    file (see below for more info).

-   compile it for Noshell ; that's very important. The program will
    still not run without DCS7.

Don't forget that these parts are only required if your program is
AP-enabled. If it's not, don't apply any of these latters and compile
your program for DoorsCS 7.

### Using files for AP-enabled programs {#using_files_for_ap_enabled_programs}

Files are special ASM programs that contains data to be interpreted by a
particular program called a viewer. If you program a viewer (or as I
said before, an AP-enabled program), you might want to make it able to
open and read files.

First, set the number of supported file types at the end of the header,
at the address 9DA5h (see the comments in the header or DCSAP.8xp), and
once you're done with it you can set the supported file types. A file
type is a set of 3 bytes which doesn't have any other sense than the one
you give to it.

But how to create recognizable files ? That's pretty easy. All files
have a common format that is to be respected :

    BB6D              <- if you compile your file for Noshell with Axe, it's automatically added
    C9                <- Return in Axe
    3180              <- required, indicates that it's an AP file
    000000            <- the 3-byte filetype
    ...datas here...

And that's all. Then, when the user clicks on a file that has one of the
type you set, DCS7 automatically opens the corresponding viewer, load
the file, and using the Asm() line I provided above, returns the first
byte of data of the file (that is, the byte after the 8-bytes header, so
you don't have to add any value to access the file's core).
