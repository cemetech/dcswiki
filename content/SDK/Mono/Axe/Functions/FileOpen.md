---
revisions:
- author: Matrefeytontias
  comment: "Created page with \u201CYou are here : \\[\\[Developers\u2019 SDK\\]\\\
    ]\n\\>\\> \\[\\[AxeDCS AP system\\]\\] \\>\\> FileOpen (AxeDCS) == Description\n\
    == This function starts a GUI instance for the user to choose a file\nwithin DC\u2026\
    \u201D"
  timestamp: '2013-01-13T17:42:27Z'
title: FileOpen
---

## Description

This function starts a GUI instance for the user to choose a file within
DCS7's file system.

*Key : 2-PropZInt \[stats\] \[←\]*

## Inputs

-   **openMode** : 0 if you want to display only the files with a
    supported filetype, non-zero else.

## Output

Outputs the first byte of the data of the opened file (skips the 8-bytes
header) if succeed, 0 else.

## Notes

This function will move all VAT pointers, so all the variables you
previously allocated using GetCalc are now lost, and you'll need to
GetCalc them again.

## Example

    :If FileOpen(0)→P
    :.Opening succeed
    :Else
    :.Opening failed
    :End
