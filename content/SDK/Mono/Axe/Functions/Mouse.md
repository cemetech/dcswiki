---
revisions:
- author: Matrefeytontias
  comment: /\* Description \*/
  timestamp: '2012-09-12T05:37:58Z'
title: Mouse
---

## Description

Renders the GUI on the screen and begin input functions. It handles all
inputs and text editing by itself. The input phase will last until you
click on an item that make the program jump to a subroutine.

**/!\\ Event if Mouse seems to `<u>`{=html}call`</u>`{=html} event
subroutines (like on a button click for example), it only
`<u>`{=html}jumps`</u>`{=html} to it ! So if you use onEvent subroutines
(the one which starts with [GUIEvent]({{< ref "GUIEvent.md" >}})), make
sure to replace the Return by a Goto. /!\\**

*Key : Logistic \[stats\] \[→\]*

## Inputs

-   All the widgets to draw must be pushed in the GUIStack using their
    widget commands.

## Format

    .Push widgets here
    Mouse
    .Goto here at the end of a subroutine starting with 

[`GUIEvent`]({{< ref "GUIEvent.md" >}})

    .
    Lbl Mouse
    .Code
