---
revisions:
- author: Matrefeytontias
  comment: /\* Inputs \*/
  timestamp: '2013-03-16T11:08:58Z'
title: FileSaveAs
---

## Description

This function starts a GUI instance for the user to input a name that
will be attributed to a new file containing the input datas.

## Inputs

-   **fileContent** : the raw content of the file you want to create.
    Don't input the header !
-   **rawSize** : the size of the **raw content** of the file. Again,
    the size doesn't include the 8-bytes header.
-   **typePointer** : pointer on the 3-bytes type of the new file.

## Output

Outputs the first byte of the raw data of the newly created file if
succeed, or 0 else.

## Notes

Same remarks as for [FileOpen]({{< ref "FileOpen.md" >}}).

## Example

    :"This is content"→Str1
    :Data(0,1,2)
    :FileSaveAs(Str1,15,Str1+16)
