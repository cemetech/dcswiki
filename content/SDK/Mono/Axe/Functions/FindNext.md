---
revisions:
- author: Matrefeytontias
  comment: "Created page with \u201CYou are here : \\[\\[Developers\u2019 SDK\\]\\\
    ]\n\\>\\> \\[\\[AxeDCS GUI Reference\\]\\] \\>\\> \\[\\[FindNext\\]\\] ==\nDescription\
    \ == Returns the address of the first data element of the\nnext widget in HL.\
    \ \u2019\u2019\u2019I\u2026\u201D"
  timestamp: '2012-09-11T16:52:34Z'
title: FindNext
---

## Description

Returns the address of the first data element of the next widget in HL.
**It must be called directly after FindFirst or FindNext since it uses
ASM registers inaccessible from Axe !**

*Key : LinRegTTest \[stats\] \[←\]*

## Inputs

-   It must be called directly after FindFirst or FindNext. Make sure
    that you didn't change any ASM register from when you called one of
    these funcs.

## Format

    .Push widgets here
    FindFirst
    FindNext
    .Code
