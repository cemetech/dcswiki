---
revisions:
- author: Matrefeytontias
  comment: /\* Description \*/
  timestamp: '2012-09-11T16:54:39Z'
title: FindFirst
---

## Description

Returns the address of the first data element of the first
non-groupmaster widget of the top group in the GUIStack in HL. For
example, the following code will put 7 in A :

    Data(0,0)→GDB1WIN
    [F8F8F8F8F8000000
    "Title"[00
    Data(0,7,

<sub>`L`</sub>`Click`<sup>`r`</sup>

    )→GDB1BTN
    "Click"[00
    SmallWin(GDB1WIN
    BtnText(GDB1BTN
    {FindFirst+1}→A

In other words, it goes to the top set of items in the GUIStack, if you
have multiple windows pushed, each containing several items. Within that
topmost group, it skips the groupmaster (the small window, large window,
or null window), and returns the address of the first byte of data in
the widget after the groupmaster.

''Key : 2-SampFTest \[stats\] \[←\]

''

## Inputs

-   Requires that there is at least one non-groupmaster widget in the
    GUIStack.

## Format

    .Push widgets here
    FindFirst
