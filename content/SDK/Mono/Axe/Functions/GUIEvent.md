---
revisions:
- author: Matrefeytontias
  comment: /\* Description \*/
  timestamp: '2012-09-11T16:55:16Z'
title: GUIEvent
---

## Description

This instruction must be called as the **first command of a subroutine
called by [Mouse]({{< ref "Mouse.md" >}})**. If you don't do it, your
program will crash !

*Key : PwrReg \[stats\] \[→\]*

## Inputs

None.

## Format

    Lbl onEventFunc
    GUIEvent
    .Your subroutine here
