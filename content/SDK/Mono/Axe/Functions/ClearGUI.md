---
revisions:
- author: Matrefeytontias
  comment: "Created page with \u201CYou are here : \\[\\[Developers\u2019 SDK\\]\\\
    ]\n\\>\\> \\[\\[AxeDCS GUI Reference\\]\\] \\>\\> \\[\\[ClearGUI\\] == Description\n\
    == Removes all the widgets from the GUIStack. \u2018\u2019Key : ExpReg\n\\[stats\\\
    ] \\[\u2192\\]\u2019\u2019 =\u2026\u201D"
  timestamp: '2012-09-11T16:57:17Z'
title: ClearGUI
---

## Description

Removes all the widgets from the GUIStack.

*Key : ExpReg \[stats\] \[→\]*

## Inputs

None.

## Format

`ClearGUI`
