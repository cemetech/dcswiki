---
revisions:
- author: KermMartian
  timestamp: '2011-03-05T15:58:02Z'
title: RunProg
---

## Description

Runs a program via Doors CS's RunProg subsystem. This call can handle
any program that Doors CS itself can handle, including AP programs and
AP files, nostub BASIC and Hybrid BASIC, Axe source code, nostub, DCS,
Ion, and MirageOS ASM, plus any other filetype that gets added to Doors
CS after this document was written. Be very careful to take into account
all the things that the running program could do to this program's
SafeRAM, modes, and variables (see Outputs below).

## Technical Details {#technical_details}

### Inputs

**hl =** Pointer to high (first) byte of VAT entry of given program or
file.\

### Outputs

Runs the program. Be aware this may trash a variety of variables and
settings, create new variables and settings, and switch many calculator
modes, depending on the type of program run. Also note that Hybrid BASIC
programs and an ASM program may completely destroy any SafeRAM in use,
so be sure to either back up necessary variables

### Destroyed

All
