---
revisions:
- author: KermMartian
  comment: /\* Outputs \*/
  timestamp: '2010-08-02T17:14:04Z'
title: Pause
---

## Description

Waits for a keypress

## Technical Details {#technical_details}

### Inputs

*None*

### Outputs

\_getcsc keycode returned in af

### Shortcut

Pause()

### Destroyed

af
