---
revisions:
- author: KermMartian
  timestamp: '2010-08-02T15:58:45Z'
title: LargeWindow
---

## Description

This routine previously drew a full-screen large window in Doors CS 5.0
and lower. It is **strongly** recommended that this routine not be used,
and instead the power of the GUI API be harnessed.
