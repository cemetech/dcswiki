---
revisions:
- author: KermMartian
  comment: /\* Technical Details \*/
  timestamp: '2007-03-17T13:11:41Z'
title: ResetAppPage
---

## Description

Resets Doors CS's active page to page 0.

## Technical Details {#technical_details}

This routine *must* be called as the first command of a segment of code
triggered by GUIMouse.

### Inputs

*none*

### Outputs

*none*

### Shortcut

ResetAppPage()\
MouseRecover() - Alias to ResetAppPage()

### Destroyed

hl, de, bc
