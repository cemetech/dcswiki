---
revisions:
- author: KermMartian
  timestamp: '2010-08-02T16:19:50Z'
title: VDispHL
---

## Description

Displays HL as an up to five-digit positive integer onscreen.

## Technical Details {#technical_details}

### Inputs

hl = value to display

### Outputs

Up to five digits of HL displayed onscreen starting at (pencol, penrow).

### Destroyed

af, hl, Op1, Op2
