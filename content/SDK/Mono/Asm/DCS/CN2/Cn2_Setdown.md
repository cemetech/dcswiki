---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Disable the\n\\[\\[Interfacing\
    \ CALCnet2\\|CALCnet2\\]\\] system, including shutting\ndown the interrupt and\
    \ freeing the memory areas used by CALCnet for\nthe interrupt and its\u2026\u2019"
  timestamp: '2010-11-12T05:14:57Z'
title: Cn2_Setdown
---

## Description

Disable the [CALCnet2]({{< ref "/SDK/Mono/Asm/DCS/CN2/" >}}) system,
including shutting down the interrupt and freeing the memory areas used
by CALCnet for the interrupt and its buffer.

## Technical Details {#technical_details}

### Inputs

*None*

### Outputs

The CALCnet<sup>2.2</sup> interrupt is disabled and its memory areas are
freed.

### Destroyed

All
