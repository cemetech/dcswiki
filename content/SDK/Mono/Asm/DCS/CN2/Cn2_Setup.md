---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Initialize the CALCnet\nsystem,\
    \ including starting the interrupt and setting up the memory\nareas and buffers\
    \ to be used by CALCnet. CALCnet is a\nlinking/networkin\u2026\u2019"
  timestamp: '2010-11-12T05:13:23Z'
title: Cn2_Setup
---

## Description

Initialize the CALCnet system, including starting the interrupt and
setting up the memory areas and buffers to be used by CALCnet. CALCnet
is a linking/networking library for linking between two and
16<sup>10</sup> calculators or other two-wire serial devices together in
a decentralized network.

## Technical Details {#technical_details}

### Inputs

*None*

### Outputs

The CALCnet<sup>2.2</sup> interrupt is installed. Be sure to carefully read the
core [CALCnet2]({{< ref "/SDK/Mono/Asm/DCS/CN2/" >}}) documentation or the
corresponding page in the SDK PDF to understand the memory areas that will be
used and the routines that must be used and avoided.

### Destroyed

All
