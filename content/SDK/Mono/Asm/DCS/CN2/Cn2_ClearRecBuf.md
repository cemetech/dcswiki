---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Clears the receive buffer,\na\
    \ total of 256+5+2=263 bytes. In practice, the receive buffer may be\nconceptually\
    \ cleared as far as the interrupt is concerned simply b\u2026\u2019"
  timestamp: '2010-11-12T05:17:33Z'
title: Cn2_ClearRecBuf
---

## Description

Clears the receive buffer, a total of 256+5+2=263 bytes. In practice,
the receive buffer may be conceptually cleared as far as the interrupt
is concerned simply by resetting the MSB of the size field in the
buffer, after which you may receive a new frame.

## Technical Details {#technical_details}

### Inputs

*None*

### Outputs

Receive buffer is cleared. See caveat in Description above.

### Destroyed

All
