---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Clears the send buffer, a\ntotal\
    \ of 256+5+2=263 bytes. In practice, the send buffer may be\nconceptually cleared\
    \ as far as the interrupt is concerned simply by\nrese\u2026\u2019"
  timestamp: '2010-11-12T05:18:10Z'
title: Cn2_ClearSendBuf
---

## Description

Clears the send buffer, a total of 256+5+2=263 bytes. In practice, the
send buffer may be conceptually cleared as far as the interrupt is
concerned simply by resetting the MSB of the size field in the buffer,
after which you may load in a new frame.

## Technical Details {#technical_details}

### Inputs

*None*

### Outputs

Send buffer is cleared. See caveat in Description above.

### Destroyed

All
