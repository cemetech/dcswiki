---
revisions:
- author: Geekboy1011
  comment: "Created page with \u2018==Description== Returns the GetCsc code\nfor the\
    \ key being held. This is de-bounced ==Technical Details==\n===Inputs===\u2019\
    \u2018None\u2019\u2019 ===Outputs=== The GetCsc Code in A\n===Destroyed\u2026\u2019"
  timestamp: '2012-03-07T22:11:10Z'
title: Cn2_GetK
---

## Description

Returns the GetCsc code for the key being held. This is de-bounced

## Technical Details {#technical_details}

### Inputs

*None*

### Outputs

The GetCsc Code in A

### Destroyed

A
