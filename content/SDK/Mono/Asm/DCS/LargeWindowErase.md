---
revisions:
- author: KermMartian
  timestamp: '2010-08-02T15:59:56Z'
title: LargeWindowErase
---

## Description

This routine previously erased a
[LargeWindow]({{< ref "LargeWindow.md" >}}), ie, cleared the screen. It
is very strongly recommended that the GUI API be used instead of this
and related functions.
