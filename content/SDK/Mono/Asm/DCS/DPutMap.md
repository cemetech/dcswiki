---
revisions:
- author: KermMartian
  timestamp: '2010-08-02T17:32:03Z'
title: DPutMap
---

## Description

Display a character using the Doors CS font-display routines, completely
separate from (and somewhat faster than) the TI-OS VPutMap routine. In
the future it will support multiple font sets, although currently only
fontset 0 (3x5 variable-width graphscreen font) is supported.

## Technical Details {#technical_details}

### Inputs

a = character to display\
e = fontset to use (only set 0 is currently valid)

### Outputs

Character displayed onscreen at (pencol, penrow) and pencol advanced by
the width of the character including its rightmost blank column.

### Destroyed

af, bc, de, hl
