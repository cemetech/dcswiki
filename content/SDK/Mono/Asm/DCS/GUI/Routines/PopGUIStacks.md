---
revisions:
- author: KermMartian
  comment: /\* Technical Details \*/
  timestamp: '2007-03-17T13:07:18Z'
title: PopGUIStacks
---

## Description

## Technical Details {#technical_details}

### Inputs

b = Number of items to pop from the stack

### Outputs

b = 0

### Shortcut

PopGUIStacks(\[b\]) - Pass an integer indicating how many to pop

### Destroyed

All except 'c'
