---
revisions:
- author: KermMartian
  comment: /\* Technical Details \*/
  timestamp: '2007-03-17T13:05:17Z'
title: RenderGUI
---

## Description

This routine renders the GUI to the graph buffer, then copies the graph
buffer to the screen. It renders every item on the stack, layering
multiple entries if necessary.

## Technical Details {#technical_details}

### Inputs

*None*

### Outputs

Draws the GUI to the LCD

### Shortcut

RenderGUI()\
GUIRender()

### Destroyed

All
