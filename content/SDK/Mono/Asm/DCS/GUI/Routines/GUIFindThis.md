---
revisions:
- author: KermMartian
  comment: "Created page with \u2018==Description== Finds\nthe\u2019\u2018a\u2019\u2018\
    th\u2019\u2018non-groupmaster\u2019\u2019 element in the \u2018top group\u2019\
    \ of the\n\\[\\[GUI Stack\\]\\]. In other words, it actually returns the second\
    \ or\nlater item in the group,\u2026\u2019"
  timestamp: '2010-06-18T06:13:42Z'
title: GUIFindThis
---

## Description

Finds the *a*th *non-groupmaster* element in the 'top group' of the
[GUI_Stack]({{< ref "../Memory_Areas.md" >}}). In other words, it actually
returns the second or later item in the group, after the
[GUIRSmallWin]({{< ref "../Items/GUIRSmallWin.md" >}}),
[GUIRLargeWin]({{< ref "../Items/GUIRLargeWin.md" >}}), or
[GUIRnull]({{< ref "../Items/GUIRnull.md" >}}) item. Unlike
[GUIFindFirst]({{< ref "GUIFindFirst.md" >}}) and
[GUIFindNext]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/GUIFindNext.md" >}}), **assumes there is an
open GUI stack**. This routine is useful for extracting information from
input forms after the user quits a form or other GUI group.

## Technical Details {#technical_details}

### Inputs

**a** = number of items to skip before returning an item

### Outputs

**hl** = the first byte of the size of the first element in the top
group\
**de** = the byte after the last byte of the top group.\
**bc** = size of the current element\
It is recommended that you push hl and de if you plan to use
[GUIFindNext]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/GUIFindNext.md" >}}); that routine requires
the data of these two outputs as inputs.

### Shortcut

GUIFindThis(\[a\])

### Destroyed

a, bc, de, hl, Op1
