---
revisions:
- author: KermMartian
  comment: /\* Technical Details \*/
  timestamp: '2010-06-19T02:53:01Z'
title: PushGUIStacks
---

## Description

Pushes a group of items onto the GUI stack.

## Technical Details {#technical_details}

The data chunk at hl consists of several entries to insert into the GUI
stack, each formatted as:

    .dw Size
    .db Type
    .db Data

Please note that the Size is is the size of the data plus the type byte
plus the size word, or *sizeof(data)+3*. After the last item, the
two-byte sequence \$FF,\$FF must be appended to signal the end of the
data section.

### Inputs

**hl =** Pointer to first byte of stack data.\

### Outputs

Adds the specified entries to the GUI stack. This routine does not
render the GUI, however.

### Shortcut

PushGUIStacks(\[hl\])

### Destroyed

All
