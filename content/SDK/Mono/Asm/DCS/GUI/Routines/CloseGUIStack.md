---
revisions:
- author: KermMartian
  comment: /\* Description \*/
  timestamp: '2010-05-30T05:06:18Z'
title: CloseGUIStack
---

## Description

Closes the Doors CS GUI system and removes the gui6 appvar from memory.
No ill effects can be accidentally incurred by calling this routine
multiple times, although the return value of the c register is used to
indicate whether the stack had already been closed. This routine is the
counterpart of the [OpenGUIStack]({{< ref "OpenGUIStack.md" >}})
function. **Warning:** this will delete any information currently in the
GUI stack. Copy any important data into files or saferam before closing
the stack.

## Technical Details {#technical_details}

### Inputs

*None*

### Outputs

**c** = 0 if the stack was open, now close; 1 if the stack was already
closed.\
**hl** = size of stack before close if c=0.\
**de** = where stack was if c=0.\
*(Previous two register values useful for ptr updating)*\

### Shortcut

CloseGUIStack()

### Destroyed

a, bc, de, hl, Op1
