---
revisions:
- author: KermMartian
  comment: /\* Outputs \*/
  timestamp: '2010-06-27T18:26:52Z'
title: PushGUIStack
---

## Description

Pushes an item onto the GUI stack. The first element on the stack must
be either GUIRNull, GUIRLargeWin, or GUIRSmallWin. Every time one of
those three items appears on the stack, it indicates the start of a new
entry. This function takes three arguments. Note that the
[OpenGUIStack]({{< ref "OpenGUIStack.md" >}}) routine should be
called before using this function. Also note that if there is
insufficient RAM to fit the GUI elements being pushed, undesirable
results may be obtained. Doors CS will check for and catch situations in
which there is less memory than would be required for a push operation,
but the program initiating the push(es) should be aware that it may lose
control in such circumstance. Avoid pushing excessive information onto
the GUI Stack without checking that there is sufficient memory available
to do so. Also be aware that a very small amount of data is added to the
GUI Stack within the GUIMouse routine to incorporate clickable hotspots.

## Technical Details {#technical_details}

### Inputs

**a =** Item type. Examples: GUIRSmallWin, GUIRRadio, GUIRPassIn\
**hl =** Pointer to stack entry data.\
**de =** Length of data in bytes.\

### Outputs

Adds the specified entry to the GUI stack. This routine does not render
the GUI, however.\
**hl** = first byte of stack entry (after 2 bytes of size and 1 byte of
type)

### Shortcut

PushGUIStack(\[a\],\[hl\],\[de\])

### Destroyed

All
