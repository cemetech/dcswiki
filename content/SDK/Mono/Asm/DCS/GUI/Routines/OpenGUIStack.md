---
revisions:
- author: KermMartian
  timestamp: '2010-06-19T04:25:32Z'
title: OpenGUIStack
---

## Description

Creates and initializes the Doors CS GUI system. Sets up all appvars and
variables necessary. Repeatedly calling this function will have no
negative affects, but the c register will be modified to indicate
whether the GUI stack had already been opened previously. The
counterpart to this function is the
[CloseGUIStack]({{< ref "CloseGUIStack.md" >}}) routine. Note
that the GUI uses several SafeRAM areas that are normally reserved for
ASM programs, so your code should be written accordingly.

## Technical Details {#technical_details}

### Inputs

**a** = \$c7 to use special VAT-swapping speed optimization. If not
\$c7, the optimization will be omitted. In general, it is safe to avoid
explicitly setting the accumulator. It is not recommended that programs
use the \$C7 mode, as it will offset any pointers into the VAT by 11
bytes.

### Outputs

**c** = 0 if the stack was closed, 1 if the stack was open.\
**hl** = first data byte of the stack appvar\
**de** = first byte of the VAT data\

### Shortcut

OpenGUIStack()

### Destroyed

a, bc, de, hl, Op1
