---
revisions:
- author: Geekboy1011
  comment: /\* Shortcut \*/
  timestamp: '2013-06-30T05:13:23Z'
title: GUIMouse
---

## Description

Renders the contents of the GUIStack and begins input functions.

## Technical Details {#technical_details}

Please note three issues when calling GUIMouse:\
\# RenderGUI is automatically called from GUIMouse. If you're going to
be using GUIMouse, don't bother with RenderGUI first.\
\# You need to **call** GUIMouse, but when control is returned to your
program, the stack will be in a condition as if you **jp**ed to
GUIMouse. Keep that in mind!!

1.  You **must** call ResetAppPage as the very first instruction
    anywhere in your program to which GUIMouse might jump.

### Inputs

**hl** = Location of hook to call once per cycle. You *must* set this to
\$0000 if not using one. Hook Note: the current mouse location as (x,y)
will be in (a,l) when the hook routine triggers. You can also get/set
the location by checking in MouseX and MouseY. To force the GUIMouse to
return control to your program from inside the hook, set the coordinates
to an object that would cause it to exit anyway, and add an extra pop to
the routine. if you have no suitable gui object you can use an offscreen
GUIRHotspot, and set the mouse location to that.\
if the hook begins with `.db $3f` it will disable the debouncing
routines, the click checking, and use a custom acceleration mode. This
option is useful if you need to be able to press keys while moving the
mouse.

### Outputs

May update data inside elements on the GUI Stack. Will end up jumping to
the location defined by hotspots, etc.

### Shortcut

GUIMouse(0) - Use this if there's no hook\
GUIMouse(\[hl\]) - Pass a label name of the hook

### Destroyed

All plus [GUI Memory Areas]({{< ref "/SDK/Mono/Asm/DCS/GUI/Memory_Areas.md" >}})
