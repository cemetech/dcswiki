---
title: Functions
bookCollapseSection: true
---

-   [OpenGUIStack]({{< ref "OpenGUIStack.md" >}}) - Opens the GUI
    stack for use with the other GUI functions.
-   [CloseGUIStack]({{< ref "CloseGUIStack.md" >}}) - Closes
    the GUI stack and frees memory used by the stack.
-   [PushGUIStack]({{< ref "PushGUIStack.md" >}}) - Pushes an item
    onto the GUI stack.
-   [PushGUIStacks]({{< ref "PushGUIStacks.md" >}}) - Saves program
    space via mass GUI stack pushes.
-   [PopGUIStack]({{< ref "PopGUIStack.md" >}}) - Removes an
    item from the GUI stack
-   [PopGUIStacks]({{< ref "PopGUIStacks.md" >}}) - Removes
    multiple items from the GUI stack
-   [GUIFindFirst]({{< ref "GUIFindFirst.md" >}}) - Returns
    pointers to the first item on the stack
-   [GUIFindNext]({{< ref "GUIFindNext.md" >}}) - Given the pointers
    to an item, finds the next item
-   [GUIFindThis]({{< ref "GUIFindThis.md" >}}) - Finds a
    specific item in the stack
-   [RenderGUI]({{< ref "RenderGUI.md" >}}) - Renders all the
    items on the GUI stack
-   [GUIMouse]({{< ref "GUIMouse.md" >}}) - Should be used
    with a call, behaves like a jp. Renders the GUI, then moves into the
    mouse subsystem.
