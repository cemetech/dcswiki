---
revisions:
- author: KermMartian
  timestamp: '2010-06-21T02:23:17Z'
title: GUIFindFirst
---

## Description

Finds the first *non-groupmaster* element in the 'top group' of the
[GUI Stack]({{< ref "/SDK/Mono/Asm/DCS/GUI/" >}}). In other words, it actually
returns the second item in the group, after the
[GUIRSmallWin]({{< ref "../Items/GUIRSmallWin.md" >}}),
[GUIRLargeWin]({{< ref "../Items/GUIRLargeWin.md" >}}), or
[GUIRnull]({{< ref "../Items/GUIRnull.md" >}}) item. Returns an error if
there is no open GUI stack. This routine is useful for extracting
information from input forms after the user quits a form or other GUI
group.

## Technical Details {#technical_details}

### Inputs

*None*

### Outputs

**c** = 0 if the stack was open and this routine suceeded; 1 if the
stack was closed, therefore no items to see.\
**hl** = the first byte of the size of the first element in the top
group\
**de** = the byte after the last byte of the top group.\
It is recommended that you push hl and de if you plan to use
[GUIFindNext]({{< ref "GUIFindNext.md" >}}); that routine requires
the data of these two outputs as inputs.

### Shortcut

GUIFindFirst()

### Destroyed

a, bc, de, hl, Op1
