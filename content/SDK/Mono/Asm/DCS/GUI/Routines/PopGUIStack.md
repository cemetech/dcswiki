---
revisions:
- author: KermMartian
  comment: /\* Technical Details \*/
  timestamp: '2007-03-17T13:06:32Z'
title: PopGUIStack
---

## Description

## Technical Details {#technical_details}

### Inputs

*(None)*

### Outputs

Removes the top item from the GUIStack\
**bc** = number of bytes removed. Can be used for ptr updates.\
**hl** = where it was removed from.\

### Shortcut

PopGUIStack()

### Destroyed

a, bc, de, hl, Op1
