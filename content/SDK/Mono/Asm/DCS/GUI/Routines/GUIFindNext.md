---
revisions:
- author: KermMartian
  comment: /\* Description \*/
  timestamp: '2010-05-30T05:31:08Z'
title: GUIFindNext
---

## Description

Finds the next element in the 'top group' of the
[GUI Stack]({{< ref "../Memory_Areas.md" >}}). It assumes you have already
called [GUIFindFirst]({{< ref "GUIFindFirst.md" >}}) or have
in some way verified that the stack is actually open (via
[OpenGUIStack]({{< ref "OpenGUIStack.md" >}})) and elements are on
it (via [PushGUIStack]({{< ref "PushGUIStack.md" >}})). This routine
is useful for extracting information from input forms after the user
quits a form or other GUI group.

## Technical Details {#technical_details}

### Inputs

**hl** = the first byte of the entry the program is currently at.\
**de** = the first byte after the last byte of the top group.\
You can use the **hl** and **de** values that were returned by
[GUIFindFirst]({{< ref "GUIFindFirst.md" >}}) or a previous
call of [GUIFindNext]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/GUIFindNext.md" >}})

### Outputs

**c** = 0 if there is a next item and de and hl are valid; 1 if we were
already at the end and hl / de are invalid.\
**hl** = the first byte of the size of the next element in the top
group\
**de** = the byte after the last byte of the top group.\
It is recommended that you push hl and de if you plan to use
[GUIFindNext]({{< ref "/SDK/Mono/Asm/DCS/GUI/Routines/GUIFindNext.md" >}}) again; that routine
requires the data of these two outputs as inputs.

### Shortcut

GUIFindNext(\[hl\],\[de\]) - To specify non-register values\
GUIFindNext() - To use already-set contents of hl and de

### Destroyed

a, bc, de, hl, Op1
