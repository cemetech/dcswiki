---
revisions:
- author: KermMartian
  comment: /\* Description \*/
  timestamp: '2007-02-16T16:28:56Z'
title: GUIRSprite
---

**Small (8 x n) Sprite**

## Description

This item renders an 8 pixel by n pixel sprite.\
**Type ID:** \$10

## Format

*Datalength: 4+ bytes*

    0: x-coordinate [1 byte]
    1: y-coordinate [1 byte]
    2: height in pixels [1 byte]
    3: sprite data [height bytes]
