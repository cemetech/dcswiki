---
revisions:
- author: KermMartian
  timestamp: '2006-11-02T13:46:41Z'
title: GUIRFullScreenImg
---

**Full-Screen Image Entry**

## Description

This item should normally be used with a GUIRNull item to provide a
custom background to a GUI or window. It is useful for showing non-GUI
content in the background of things like a small window.\
**Type ID:** 3

## Format

*Datalength: 768 bytes*

`0: (a 96x64 bit-encoded monochrome image) [768 bytes]`
