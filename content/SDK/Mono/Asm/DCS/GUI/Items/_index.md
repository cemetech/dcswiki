---
title: Stack Items
bookCollapseSection: true
---

-   [GUIRnull]({{< ref "GUIRnull.md" >}}) - Provides a non-rendered
    invisible GUI wrapper
-   [GUIRLargeWin]({{< ref "GUIRLargeWin.md" >}}) - Provides a
    fullscreen GUI wrapper window
-   [GUIRSmallWin]({{< ref "GUIRSmallWin.md" >}}) - provides a
    windowed GUI wrapper
-   [GUIRFullScreenImg]({{< ref "GUIRFullScreenImg.md" >}}) -
    Renders a 96x64 image to the screen
-   [GUIRText]({{< ref "GUIRText.md" >}}) - Renders a single line of
    text
-   [GUIRWinButtons]({{< ref "GUIRWinButtons.md" >}}) - Adds
    clickable Minimize/Maximize/Close buttons to a window
-   [GUIRWrappedText]({{< ref "GUIRWrappedText.md" >}}) - Renders a
    wrapped chunk of text
-   [GUIRButtonText]({{< ref "GUIRButtonText.md" >}}) - Renders and
    handles a clickable button with text
-   [GUIRButtonImg]({{< ref "GUIRButtonImg.md" >}}) - Renders and
    handles a clickable button with an icon
-   [GUIRTextLineIn]({{< ref "GUIRTextLineIn.md" >}}) - Renders and
    handles a plaintext single-line input element
-   [GUIRRadio]({{< ref "GUIRRadio.md" >}}) - Renders and handles a
    radio button member of a group
-   [GUIRCheckbox]({{< ref "GUIRCheckbox.md" >}}) - Renders and
    handles a checkbox
-   [GUIRByteInt]({{< ref "GUIRByteInt.md" >}}) - Renders and
    handles a 1-byte numerical input spinner
-   [GUIRWordInt]({{< ref "GUIRWordInt.md" >}}) - Renders and
    handles a 2-byte numerical input spinner
-   [GUIRHotspot]({{< ref "GUIRHotspot.md" >}}) - Creates an onClick
    area to an external routine
-   [GUIRTextMultiline]({{< ref "GUIRTextMultiline.md" >}}) -
    Renders and handles multiline plaintext input
-   [GUIRSprite]({{< ref "GUIRSprite.md" >}}) - Draws a small (8
    by n) sprite to the screen
-   [GUIRLargeSprite]({{< ref "GUIRLargeSprite.md" >}}) - Draws a
    large (8\*m by n) sprite to the screen
-   [GUIRPassIn]({{< ref "GUIRPassIn.md" >}}) - Renders and handles
    a password single-line input element
-   [GUIRScrollVert]({{< ref "GUIRScrollVert.md" >}}) - Renders and
    handles a vertical scrollbar
-   [GUIRScrollHoriz]({{< ref "GUIRScrollHoriz.md" >}}) - Renders
    and handles a horizontal scrollbar
-   [GUIRBorder]({{< ref "GUIRBorder.md" >}}) - Draws an unfilled
    rectangular border
-   [GUIRRect]({{< ref "GUIRRect.md" >}}) - Draws a filled
    rectangle
-   [GUIRCustom]({{< ref "GUIRCustom.md" >}}) - Calls out to a
    custom rendering routine
-   [GUIRMouseCursor]({{< ref "GUIRMouseCursor.md" >}}) - Allows the
    cursor to change within a specified rectangular area
