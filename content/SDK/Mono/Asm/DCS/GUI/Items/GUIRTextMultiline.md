---
revisions:
- author: KermMartian
  timestamp: '2007-02-16T16:25:43Z'
title: GUIRTextMultiline
---

**Multi-line Text Input Element**

## Description

This item creates a text input element allowing CRLFs and more organized
formatting (plus a higher character limit) than the Single-line input
element.\
**Type ID:** \$0F

## Format

*Datalength: 7+ bytes*

    0: x-coordinate [1 byte]
    1: y-coordinate [1 byte]
    2: number of rows [1 byte]
    3: width in pixels [1 byte]
    4: offset to start of onscreen text from start of string [2 bytes]
    6: Zero-terminated string data [1+ bytes]
