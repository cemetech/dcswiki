---
revisions:
- author: KermMartian
  comment: /\* Description \*/
  timestamp: '2006-11-02T13:43:09Z'
title: GUIRnull
---

**Null Window Entry**

## Description

This item is one of the three possible items that start a GUI stack
entry: GUIR Null, GUIR Small Window, and GUIR Large Window. This item
only acts as a placeholder and container for the data within it; it does
not render anything to the screen.\
**Type ID:** 0

## Format

*Datalength: 1 byte*

`0: $FF (used by the rendering routines) [1 byte]`
