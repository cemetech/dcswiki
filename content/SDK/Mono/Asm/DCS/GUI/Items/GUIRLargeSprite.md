---
revisions:
- author: KermMartian
  timestamp: '2007-02-16T16:28:42Z'
title: GUIRLargeSprite
---

**Large (8\*m x n) Sprite**

## Description

This item renders an 8\*m pixel by n pixel sprite.\
**Type ID:** \$11

## Format

*Datalength: 5+ bytes*

    0: x-coordinate [1 byte]
    1: y-coordinate [1 byte]
    2: width in bytes [1 byte]
    3: height in pixels [1 byte]
    4: sprite data [height*width bytes]
