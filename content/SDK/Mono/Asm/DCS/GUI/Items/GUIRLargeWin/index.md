---
revisions:
- author: KermMartian
  timestamp: '2010-06-24T23:55:52Z'
title: GUIRLargeWin
---

**Large Window Item**

## Description

This item is one of the three possible items that start a GUI stack
entry: GUIR Null, GUIR Small Window, and GUIR Large Window. This item
acts as a container for the data within it; it also renders a large
window onto the screen.\
**Type ID:** 1

![]({{< resource "largewin_dimensions.png" >}})

## Format

*Datalength: 7+ byte*

    0: 5x5 icon [5 bytes]
    5: zero-terminated title string [- bytes]
