---
revisions:
- author: KermMartian
  comment: /\* Format \*/
  timestamp: '2006-11-02T13:59:28Z'
title: GUIRText
---

**Static Non-Wrapping Text Entry**

## Description

This item places static text in the GUI using the custom Doors CS text
routines. It supports multiple fonts, but this feature is not yet
activated; only the default font, fontset '0', is available.\
**Type ID:** 4

## Format

*Datalength: 5+ byte*

    0: x-coord of leftmost text column [1 byte]
    1: y-coord of row above text [1 byte]
    2: font (always 0 for DCS6) [1 byte]
    3: zero-terminated text string [two or more bytes]
