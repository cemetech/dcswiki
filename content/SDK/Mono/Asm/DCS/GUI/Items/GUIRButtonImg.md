---
revisions:
- author: KermMartian
  timestamp: '2007-02-16T16:12:46Z'
title: GUIRButtonImg
---

**Clickable Image Button**

## Description

This item places an image button on the screen that jumps to a specified
location in memory when it is clicked.\
**Type ID:** 8

## Format

*Datalength: 11+ bytes*

    0: x-coordinate [1 byte]
    1: y-coordinate [1 byte]
    2: Onclick pointer [2 bytes]
    4: Image width in bytes [1 byte]
    5: Button width in pixels [1 byte] (account for buttons that are not x8 wide)
    6: Image data [image width in bytes times 5]
