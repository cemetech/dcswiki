---
revisions:
- author: KermMartian
  timestamp: '2010-07-27T05:17:14Z'
title: GUIRWrappedText
---

**Static Wrapping Text Entry**

## Description

This item places text on the screen; it will wrap at a \$D6 or if the
width exceeds (width). Does not check for overflowing the bottom of the
window. As of Doors CS 6.9 beta, you can add the two-byte sequence
\$D6,\$01 in the middle of a string as a line break.\
**Type ID:** 6

## Format

*Datalength: 6+ bytes*

    0: x-coordinate [1 byte]
    1: y-coordinate [1 byte]
    2: width (pixels) [1 byte]
    3: font (always 0 in this version) [1 byte]
    4: zero-terminated text string [2+ bytes]
