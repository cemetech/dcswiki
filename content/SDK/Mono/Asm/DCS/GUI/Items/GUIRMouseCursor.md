---
revisions:
- author: Eeems
  comment: fixed first two arguements, it works out to y,x not x,y
  timestamp: '2010-07-27T18:35:55Z'
title: GUIRMouseCursor
---

**User-Defined Cursor Shuffler**

## Description

This item calls a custom rendering routine in the render loop.\
**Type ID:** \$18

## Format

*Datalength: 20 bytes*

    0: word-packed top-left y,x of affected area [2 bytes]
    2: word-packed bottom-right y,x of affected area [2 bytes]
    4: 8-byte cursor mask [8 bytes]
    12: 8-byte cursor sprite [8 bytes]
