---
revisions:
- author: KermMartian
  timestamp: '2012-10-10T19:48:03Z'
title: GUIRRect
---

**Filled Rectangle**

## Description

This item renders a filled rectangle onscreen.\
Note on color values:

-   \$FF = white
-   \$FE = black
-   \$FD = invert (xor)
-   \$AA = checkered

**Type ID:** \$16

## Format

*Datalength: 5 bytes*

    0: x-coordinate [1 byte]
    1: y-coordinate [1 byte]
    2: width in pixels [1 byte]
    3: height in pixels [1 byte]
    4: fill color (0x00 or 0xFF - intermediate values created dashed lines) [1 byte]
