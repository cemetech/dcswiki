---
revisions:
- author: KermMartian
  timestamp: '2007-02-16T16:40:17Z'
title: GUIRCustom
---

**Custom Rendering Routine**

## Description

This item calls a custom rendering routine in the render loop.\
**Type ID:** \$17

## Format

*Datalength: 2 bytes*

`0: pointer to render routines [2 bytes]`
