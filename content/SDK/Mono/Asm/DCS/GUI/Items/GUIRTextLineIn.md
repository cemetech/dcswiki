---
revisions:
- author: ACagliano
  comment: /\* Format \*/
  timestamp: '2011-11-10T17:09:33Z'
title: GUIRTextLineIn
---

**Single-Line Plaintext Input**

## Description

This item places a single-line text input item onscreen. In the GUIMouse
section, Doors CS handles all text-editing functionality.\
**Type ID:** 9

## Format

*Datalength: 7+ bytes*

    0: x-coordinate [1 byte]
    1: y-coordinate [1 byte]
    2: width in pixels [1 byte]
    3: Maximum characters allows [2 bytes]
    5: Current offset from start of data string [2 bytes]
    7: Zero-terminated string [1+ bytes] (appears in the box)
