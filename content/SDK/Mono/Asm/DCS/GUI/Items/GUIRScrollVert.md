---
revisions:
- author: KermMartian
  timestamp: '2007-02-16T16:34:29Z'
title: GUIRScrollVert
---

**Vertical Scrollbar**

## Description

This item places a clickable vertical scrollbar on the screen and
handles all movement clicking with associated handlers.\
**Note for onClick:** (bc) and (bc+1) are the little-endian current
values of the scrollbar position for OnScrollUp and OnScrollDown.

**Type ID:** \$13

## Format

*Datalength: 16 bytes*

    0: x-coordinate [1 byte]
    1: y-coordinate [1 byte]
    2: height in pixels [1 byte]
    3: ID number [1 byte]
    4: Increment/decrement per click [2 bytes]
    6: Minimum value on scrollbar [2 bytes]
    8: Maximum value on scrollbar [2 bytes]
    10: Current value on scrollbar [2 bytes]
    12: Onclick pointer for upward scrolling [2 bytes]
    14: Onclick pointer for downward scrolling [2 bytes]
