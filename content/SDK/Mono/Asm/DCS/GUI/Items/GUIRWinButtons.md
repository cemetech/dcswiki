---
revisions:
- author: Techboy6601
  comment: Fixed minor grammar mistake
  timestamp: '2012-11-03T02:01:59Z'
title: GUIRWinButtons
---

**Clickable Window Titlebar Button Entry**

## Description

This item places one or more of the possible window titlebar buttons,
close, maximize, and/or minimize, in the upper-right of the current GUI.
It should only be used with
[GUIRSmallWin]({{< ref "GUIRSmallWin.md" >}}) or
[GUIRLargeWin]({{< ref "GUIRLargeWin.md" >}}) items; use with
[GUIRnull]({{< ref "GUIRnull.md" >}}) will probably cause a crash.\
**Type ID:** 5

## Format

*Datalength: 7 bytes*

    0: button flags: %[-],[o],[x],00000.  Any bit set to 1 indicates that button should be shown. [1 byte]
    1: Onclick pointer for [-] (or 0 if not present) [2 bytes]
    3: Onclick pointer for [o] (or 0 if not present) [2 bytes]
    5: Onclick pointer for [x] (or 0 if not present) [2 bytes]
