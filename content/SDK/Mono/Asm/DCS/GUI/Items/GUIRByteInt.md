---
revisions:
- author: KermMartian
  timestamp: '2007-02-16T16:20:54Z'
title: GUIRByteInt
---

**Byte Input Spinner**

## Description

This item allows the user to choose a numerical value no smaller than 0
nor larger than 255 within predefined limits.\
**Type ID:** \$0C

## Format

*Datalength: 5 bytes*

    0: x-coordinate [1 byte]
    1: y-coordinate [1 byte]
    2: initial value [1 byte]
    3: Minimum allowed value [1 byte]
    4: Maximum allowed value [1 byte]
