---
revisions:
- author: KermMartian
  timestamp: '2007-02-16T16:23:15Z'
title: GUIRHotspot
---

**Clickable Hotspot**

## Description

This item creates an invisible, clickable hotspot for the mouse
routine.\
**Type ID:** \$0E

## Format

*Datalength: 6 bytes*

    0: x-coordinate [1 byte]
    1: y-coordinate [1 byte]
    2: width in pixels [1 byte]
    3: height in pixels [1 byte]
    4: Onclick pointer [2 bytes]
