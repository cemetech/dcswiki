---
revisions:
- author: KermMartian
  timestamp: '2006-11-02T14:04:07Z'
title: GUIRButtonText
---

**Clickable Text Button**

## Description

This item places a text button on the screen that jumps to a specified
location in memory when it is clicked.\
**Type ID:** 7

## Format

*Datalength: 6+ bytes*

    0: x-coordinate [1 byte]
    1: y-coordinate [1 byte]
    2: Onclick pointer [2 bytes]
    4: zero-terminated text string [2+ bytes]
