---
revisions:
- author: KermMartian
  timestamp: '2007-02-16T16:21:37Z'
title: GUIRWordInt
---

**Word Input Spinner**

## Description

This item allows the user to choose a numerical value no smaller than 0
nor larger than 65535 within predefined limits.\
**Type ID:** \$0D

## Format

*Datalength: 8 bytes*

    0: x-coordinate [1 byte]
    1: y-coordinate [1 byte]
    2: initial value [2 bytes]
    3: Minimum allowed value [2 bytes]
    4: Maximum allowed value [2 bytes]
