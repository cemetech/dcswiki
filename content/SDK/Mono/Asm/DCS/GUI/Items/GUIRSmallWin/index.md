---
revisions:
- author: KermMartian
  timestamp: '2010-06-24T22:59:31Z'
title: GUIRSmallWin
---

**Small Window Item**

## Description

This item is one of the three possible items that start a GUI stack
entry: GUIR Null, GUIR Small Window, and GUIR Large Window. This item
acts as a container for the data within it; it also renders a small
window onto the screen, preserving any items behind it.\
**Type ID:** 2

![]({{< resource "smallwin_dimensions.png" >}})

## Format

*Datalength: 9+ bytes*

    0: x coordinate (0-15) [1 byte]
    1: y coordinate (0-15) [1 byte]
    2: 5x5 icon [5 bytes]
    7: zero-terminated title string [- bytes]

## Notes

As of Doors CS 6.6 beta and above, pressing the \[Clear\] key on the
calculator's keypad is now a shortcut to click the \[X\] at the
top-right of a GUIRSmallWin, if such a button exists. If you wish to
override this behavior, you may create a MouseHook (see
[GUIMouse]({{< ref "../../Routines/GUIMouse.md" >}})) to catch and handle the
\[Clear\] key.
