---
revisions:
- author: KermMartian
  timestamp: '2007-02-16T16:38:40Z'
title: GUIRBorder
---

**Rectangular Border**

## Description

This item renders an unfilled rectangular border onscreen.\
**Type ID:** \$15

## Format

*Datalength: 5 bytes*

    0: x-coordinate [1 byte]
    1: y-coordinate [1 byte]
    2: width in pixels [1 byte]
    3: width in pixels [1 byte]
    4: color (0x00 or 0xFF - intermediate values created dashed lines) [1 byte]
