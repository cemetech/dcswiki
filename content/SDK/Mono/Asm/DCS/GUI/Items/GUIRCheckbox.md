---
revisions:
- author: KermMartian
  timestamp: '2007-02-16T16:19:15Z'
title: GUIRCheckbox
---

**Ungrouped Checkbox**

## Description

This item renders a checkbox to the screen in a specified group and
handles all clicking necessary.\
**Type ID:** \$0B

## Format

*Datalength: 7+ bytes*

    0: x-coordinate [1 byte]
    1: y-coordinate [1 byte]
    2: group [1 byte] (enumeration is arbitrarily defined by the programmer)
    3: Current state [1 byte] (1 = selected)
    4: Zero-terminated display string [1+ bytes]
