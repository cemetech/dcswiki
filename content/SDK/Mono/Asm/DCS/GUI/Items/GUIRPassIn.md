---
revisions:
- author: KermMartian
  timestamp: '2007-02-16T16:30:15Z'
title: GUIRPassIn
---

**Single-Line Password Input**

## Description

This item places a single-line password input item onscreen. In the
GUIMouse section, Doors CS handles all text-editing functionality.
Identical to Single-Line Text Input except for obscured text rendering.\
**Type ID:** \$12

## Format

*Datalength: 7+ bytes*

    0: x-coordinate [1 byte]
    1: y-coordinate [1 byte]
    2: width in pixels [1 byte]
    3: Maximum characters allows [2 bytes]
    5: Current offset from start of data string [2 bytes]
    6: Zero-terminated string [1+ bytes]
