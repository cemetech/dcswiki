---
revisions:
- author: KermMartian
  timestamp: '2010-07-28T05:36:25Z'
title: Memory Areas
weight: 1
aliases:
- /SDK/GUI_Memory_Areas/
- /GUI_Stack/
---

Due to the complexity of the GUI system, several pieces of RAM may be
overwritten while it is running. They are:

-   73 bytes starting at AppBackupScreen (used for GUI rendering and
    mouse) (for future compatibility, it is recommended to leave the
    first 129 bytes of AppBackupScreen free, if possible)
-   4 bytes starting at iMathPtrs (used only in the GUI mouse)
-   4 bytes starting at iMathPtrs+5 (used only in
    [FileOpen]({{< ref "/SDK/Mono/Asm/DCS/APs/FileOpen.md" >}}) and
    [FileSaveAs]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSaveAs.md" >}}))
-   RelocatablePtr and RelocatablePtr2 (used only in
    [FileOpen]({{< ref "/SDK/Mono/Asm/DCS/APs/FileOpen.md" >}}),
    [FileSave]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSave.md" >}}), and
    [FileSaveAs]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSaveAs.md" >}}))

In addition, you should be aware that Doors CS will change the size and
contents of the 'gui6'/'gui7' appvar during GUI execution. It will also
change the contents, but *not* size of the DCS6/DCS7 appvar during GUI
execution. It is not recommended that you use undeclared areas of the
free user ram for storage while the GUI stack is open, as data
corruption may occur. Declare any RAM you need as programs or appvars.
Also remember to update pointers if your data file is in higher RAM than
the gui6/gui7 appvar. In general, if you use the [Associated
Program]({{< ref "/SDK/Mono/Asm/DCS/APs/" >}}) (AP) system, Doors CS
will handle keeping gui7 in higher memory than any AP file that is
opened. Another important note is that the three GUI text input types,
[GUIRTextLineIn]({{< ref "Items/GUIRTextLineIn.md" >}}),
[GUIRPassIn]({{< ref "Items/GUIRPassIn.md" >}}), and
[GUIRTextMultiline]({{< ref "Items/GUIRTextMultiline.md" >}}) all use an
edit buffer, which temporarily consumes all of free RAM (but only when
the user is actually typing in that text box). Therefore, be sure not to
store any data in free RAM above in-RAM programs, which is already a bad
idea to begin with.
