---
revisions:
- author: KermMartian
  timestamp: '2011-07-23T10:23:16Z'
title: DGetCharWidthLocFromA
---

## Description

Get the page and address for a character bitmap from the DCS font
tables, given a character code in A. The width is also returned.

## Technical Details {#technical_details}

### Inputs

a=char

### Outputs

a=width\
c=raw first byte\
d=page of entry\
hl-\>first byte of entry (c=(hl))

### Destroyed

af, bc, de, hl
