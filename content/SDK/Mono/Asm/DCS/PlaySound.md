---
revisions:
- author: KermMartian
  timestamp: '2010-08-02T16:18:42Z'
title: PlaySound
---

## Description

Plays a monophonic note on both channels of the serial I/O port, which
can be heard with either headphones or an AM radio tuned to static.

## Technical Details {#technical_details}

### Inputs

hl = frequency of note (see notes.inc in the Doors CS 7.0 SDK)\
de = duration of note (see notes.inc in the Doors CS 7.0 SDK)

### Outputs

The specified note is played. Note that this routine is blocking: ie, it
will not return to the caller program until the end of the note.

### Destroyed

All
