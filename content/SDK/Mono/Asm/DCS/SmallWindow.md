---
revisions:
- author: KermMartian
  timestamp: '2010-08-02T15:54:39Z'
title: SmallWindow
---

## Description

This routine previously drew a centered small window on the screen in
Doors CS 5.0 and lower. It is **strongly** recommended that this routine
not be used, and instead the power of the GUI API be harnessed.
