---
revisions:
- author: KermMartian
  timestamp: '2010-08-02T15:56:02Z'
title: SmallWindowErase
---

## Description

This routine previously erased a
[SmallWindow]({{< ref "SmallWindow.md" >}})-sized area centered on the
LCD. It is very strongly recommended that it not be used, and that the
GUI API be used instead.
