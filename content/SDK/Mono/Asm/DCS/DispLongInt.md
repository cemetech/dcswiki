---
revisions:
- author: KermMartian
  comment: "Created page with \u2018== Description == Displays an integer\nbetween\
    \ 0 and 16,777,215 (inclusive) onscreen in small (graphscreen)\nfont. == Technical\
    \ Details == === Inputs === hl = pointer to the MSB\no\u2026\u2019"
  timestamp: '2010-08-02T17:22:16Z'
title: DispLongInt
---

## Description

Displays an integer between 0 and 16,777,215 (inclusive) onscreen in
small (graphscreen) font.

## Technical Details {#technical_details}

### Inputs

hl = pointer to the MSB of a three-byte big-endian integer pencol/penrow
= location to display number

### Outputs

Up to 8 digits displayed onscreen

### Destroyed

af, bc, de, hl, Op1
