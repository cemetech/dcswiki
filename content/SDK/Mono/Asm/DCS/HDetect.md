---
revisions:
- author: KermMartian
  timestamp: '2010-08-02T17:16:27Z'
title: HDetect
---

## Description

This routine checks if a coordinate pair is within a rectangular area,
the top-left and bottom-right of which are defined by two other
coordinate pairs. It can be used for collision detection or mouse click
evaluation, among other things.

## Technical Details {#technical_details}

### Inputs

hl = Location to check if within; (h,l)\
bc = Upper left corner of area (b,c)\
de = Lower right corner of area (d,e)

### Outputs

Zero flag is set if hl is inside area, reset otherwise.

### Destroyed

af, bc, de, hl
