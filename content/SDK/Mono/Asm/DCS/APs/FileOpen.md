---
revisions:
- author: KermMartian
  comment: /\* Inputs \*/
  timestamp: '2010-07-25T13:30:42Z'
title: FileOpen
---

## Description

Opens a GUI to open files. Fully supports the folder system etc.

## Technical Details {#technical_details}

As of Doors CS 6.4 beta, it uses the new
[RunProg_Chaining]({{< ref "RunProg_Chaining.md" >}}) system to operate.
It puts away any open program before opening a new program, so be aware
that pointers in memory might have moved around. It is strongly
recommended to re-chkfindsym any RAM (and ROM) variables after calling
this function. Note that this function will also move the gui7 AppVar
back above any newly-opened files. Doors CS will handle closing any open
AP files when your program quits. Note that unlike Doors CS 6.4 beta and
lower, Doors CS 6.5 and higher may return pointers to an AP file even if
the FileOpen was canceled, ie, the pointers to an already-open file.
Nevertheless, it is mandatory that any time FileOpen is called, any open
AP file(s) is/are ready to be written back and closed.

### Inputs

**a** = zero to only show files of the type(s) defined in this AP's
header, or nonzero to show all oncalc files

### Outputs

**hl** = 0 if no file was returned, or the first byte of data if a file
was returned\
**de** = pointer to the size word of the data for updating. **Please
note:** the size at the size word is *not* the same as the size word in
bc. bc is 8 bytes smaller than (de) due to the AP header
(\$BB,\$6D,\$C9,\$31,\$80,type,type,type)\
**bc** = size of the pure data section of the file.\
If necessary, you should also check hl-3 through hl-1 (3 bytes) to see
if it's a valid type for this program.

### Destroyed

a, bc, de, hl, Op1, iMathPtrs,
[GUI_Memory_Areas]({{< ref "/SDK/Mono/Asm/DCS/GUI/Memory_Areas.md" >}})
