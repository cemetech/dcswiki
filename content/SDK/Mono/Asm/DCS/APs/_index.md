---
title: Associated Programs
bookCollapseSection: true
weight: 1
aliases:
- /AP/Associated_Programs/
- /AP/Registered_Filetypes/
---

The Associated Program (AP) system allows Doors CS to recognize
specially-formatted programs as data files with an associated viewer
program. When the user runs the data files, Doors CS recognizes them as
AP files and instead launches the viewer program, giving it pointers to
the data file as necessary. It also handles unarchiving the file before
running the viewer and rearchives the files after the viewer closes.

## File format

The header format for an associated file contains eight bytes:

       .org datastart
       .db $BB,$6D     ;required
       .db $C9         ;don't let the TI-OS run this
       .db $31,$80     ;this is an AP file
       .db 0,1,4       ;3-byte type code

    Start:

### Registered filetypes

The following is a list of the registered 3-byte filetypes to be used in
programs using the AP system. If you make your own filetype, please add it to
the list.

**Important note:** this list is to prevent collisions. It is not embedded in
Doors CS anywhere. It is up to each programmer to ensure that they are not
stealing someone else's filetype.

-   **0, 1, 0** = Strategic Conquest game save
-   **0, 1, 1** = Plaintext
-   **0, 1, 2** = Document DE 6 text
-   **0, 1, 3** = iPaint image
-   **0, 1, 4** = mobileTunes3 Song
-   **0, 1, 5** = Civilization Simulator II game save
-   **0, 1, 6** = Acelgoyobis DE Table
-   **0, 1, 7** = ProgPad file
-   **0, 1, 8** = AxeAid Project
-   **0, 1, 9** = Athena Installer metadata file
-   **0, 1, 10** = Tanner's Platformer Levels
-   **0, 1, 11** = CFD 4 Channel Music Project file
-   **0, 2, 4** = floppyTunes3 Song\
-   **1, 1, 0** = CalcRAR file\
-   **2, 0, 2** = Blast AntiVirus Definitions Updater
-   **85, 62, 0** = WiredWorks 3D model

## Program header

The header for an associated program that is a viewer for the files
above:

    ; Program Name: 
    ; Author: 
    ; Version: 
    ; Date: 
    ; Written for Doors CS 7.0 and higher (http://dcs.cemetech.net)
    .nolist
    #include "dcs7.inc"
    .list
       .org progstart
       .db $BB,$6D
    Init:
       xor d
       ret
       jr Start
       
       .dw $0000
       .db $07,$00
       .dw Icon
       .dw $0000
       .dw Apstart         ;the routine to open files DCS will start you here instead of $9D95 if AP file pending
       .db $31,$7F         ;argh, this be an APMain
       .db $02             ;number of accepted filetypes
    MyTypes:
       .db 0,1,4           ;whatever this is
       .db 0,1,2           ;DDE6 text
    START:                             ;main routines
       [...]
    APStart:

When a user simply runs the specified program from Doors CS, it will
start at \$9D95, jump to Start, and begin executing there, as usual. If
a user clicks a file meant to be opened with this program, Doors CS will
take care of copying the file to RAM if necessary, setting up the viewer
program, and then executing it. However, execution will start at APStart
instead of at \$9D95. When the program begins executing at APStart, the
ix register will contain a pointer to the first data byte of the AP
file, ie, the first byte after the 8-byte header. The header will be at
ix-8 through ix-1, and the size of the file, including the 8-byte
header, will be at ix-10 (LSB) and ix-9 (MSB). The size of the data
section of the AP file is of course the file size minus 8.

Within your program, you can modify the AP file as necessary, including
inserting memory and deleting memory (be sure to update the size word
accordingly!). Make sure you leave the header intact, and only insert or delete
memory at ix or higher. When your program quits, Doors CS will writeback the
file to the archive if necessary, or delete the temporary RAM copy if it is
unchanged. If you call the [FileOpen]({{< ref "FileOpen.md" >}}), [FileSave]({{<
ref "FileSave.md" >}}), or [FileSaveAs]({{< ref "FileSaveAs.md" >}}) within your
program, any open AP files will be closed, including removing the RAM copy and
writing it back, **even if the user chooses not to open a new file**.

## Routines

-   **[FileOpen]({{< ref "FileOpen.md" >}})** - GUI-based
    routine to open a file.
-   **[FileSave]({{< ref "FileSave.md" >}})** - GUIless
    routine to save a file.
-   **[FileSaveAs]({{< ref "FileSaveAs.md" >}})** - GUI-based
    routine to save a file.
