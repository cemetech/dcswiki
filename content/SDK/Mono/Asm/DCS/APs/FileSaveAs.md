---
revisions:
- author: KermMartian
  comment: /\* Inputs \*/
  timestamp: '2010-07-25T13:30:31Z'
title: FileSaveAs
---

## Description

Create a new AP file with a given type, size, and data. Operates with a
GUI, used to ask the user for a name for the file, and optionally a
folder in which to store the new file.

## Technical Details {#technical_details}

As of Doors CS 6.4 beta, it uses the new
[RunProg_Chaining]({{< ref "RunProg_Chaining.md" >}}) system to operate.
It puts away any open program before creating the new program, so be
aware that pointers in memory might have moved around. It is strongly
recommended to re-chkfindsym any RAM (and ROM) variables after calling
this function. Note that this function will also move the gui7 AppVar
back above any newly-created file. Doors CS will handle closing any open
AP files when your program quits.

### Inputs

**a** = zero to only show files of the type(s) defined in this AP's
header, or nonzero to show all oncalc files **hl** = pointer to data to
put in new file\
**bc** = size of pure data section (the program created will be larger
than this because of the AP header)\
**de** = pointer to 3-byte type sequence\
*Note that unlike [FileSave]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSave.md" >}}), Op1
is not used as an input*

### Outputs

**zero flag** = Set if successful. If not zero, then the function
FAILED, and the existing open file might have been closed! Be aware of
this.\
**hl** = 0 if no file was returned, or the first byte of data if a file
was returned\
**de** = pointer to the size word of the data for updating. **Please
note:** the size at the size word is *not* the same as the size word in
bc. bc is 8 bytes smaller than (de) due to the AP header
(\$BB,\$6D,\$C9,\$31,\$80,type,type,type)\
**bc** = size of the pure data section of the file.

### Destroyed

a, bc, de, hl, Op1, iMathPtrs,
[GUI_Memory_Areas]({{< ref "/SDK/Mono/Asm/DCS/GUI/Memory_Areas.md" >}})
