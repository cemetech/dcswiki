---
revisions:
- author: KermMartian
  timestamp: '2010-05-19T03:25:20Z'
title: FileSave
---

## Description

Create a new AP file with a given name, type, size, and data. Operates
with no GUI.

## Technical Details {#technical_details}

As of Doors CS 6.4 beta, it uses the new
[RunProg_Chaining]({{< ref "RunProg_Chaining.md" >}}) system to operate.
It puts away any open program before creating the new program, so be
aware that pointers in memory might have moved around. It is strongly
recommended to re-chkfindsym any RAM (and ROM) variables after calling
this function. Note that this function will also move the gui7 AppVar
back above any newly-created file. Doors CS will handle closing any open
AP files when your program quits.

### Inputs

**hl** = pointer to data to put in new file\
**bc** = size of pure data section (the program created will be larger
than this because of the AP header)\
**de** = pointer to 3-byte type sequence\
**op1** contains name, eg. .db 06,"NAME",0\
*None*

### Outputs

**zero flag** = Set if successful. If not zero, then the function
FAILED, and the existing open file might have been closed! Be aware of
this.\
**hl** = 0 if no file was returned, or the first byte of data if a file
was returned\
**de** = pointer to the size word of the data for updating. **Please
note:** the size at the size word is *not* the same as the size word in
bc. bc is 8 bytes smaller than (de) due to the AP header
(\$BB,\$6D,\$C9,\$31,\$80,type,type,type)\
**bc** = size of the pure data section of the file.

### Destroyed

a, bc, de, hl, Op1, iMathPtrs,
[GUI_Memory_Areas]({{< ref "/SDK/Mono/Asm/DCS/GUI/Memory_Areas.md" >}})
