---
revisions:
- author: KermMartian
  comment: "Created page with \u2018== Description == This internal Doors CS\nroutine\
    \ is used to move the \u201Cgui7\u201D AppVar to the highest spot in\nuser memory\
    \ (directly below the free RAM chunk) so that the size of\nthe \u2026\u2019"
  timestamp: '2010-08-02T17:36:44Z'
title: APGui_gui7ToTop
aliases:
- /Libraries/AP_GUIAVToTop/
- /Libraries/APGui_gui7ToTop/
---

## Description

This internal Doors CS routine is used to move the "gui7" AppVar to the
highest spot in user memory (directly below the free RAM chunk) so that
the size of the appvar can change without needing to move any data above
it. Although this is primarily used by Doors CS itself, it should be
called if you manually create any files from within your program. If you
use [FileOpen]({{< ref "/SDK/Mono/Asm/DCS/APs/FileOpen.md" >}}),
[FileSave]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSave.md" >}}), or
[FileSaveAs]({{< ref "/SDK/Mono/Asm/DCS/APs/FileSaveAs.md" >}}), you need not call
this routine, as Doors CS will handle it automatically.

## Technical Details {#technical_details}

### Inputs

None

### Outputs

gui7 moved above any newly-created programs or files

### Destroyed

All
