---
revisions:
- author: KermMartian
  comment: /\* Destroyed \*/
  timestamp: '2012-09-02T02:48:29Z'
title: iRandom
---

## Description

Returns a random number within set bounds, such that the returned value
a is, in relation to the input upper bound b: 0\<=a\<b

## Technical Details {#technical_details}

### Inputs

**b** = One larger than the highest random number this routine may
return

### Outputs

**a** = A random number between 0 and b-1, inclusive.\
**b** = 0

### Destroyed

af and bc only
