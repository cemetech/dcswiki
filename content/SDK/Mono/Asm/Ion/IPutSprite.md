---
revisions:
- author: KermMartian
  timestamp: '2010-06-21T02:39:48Z'
title: iPutSprite
---

## Description

XORs an 8-pixel-wide sprite to the graph buffer, aligned or unaligned.
Does not clip. The sprite can be any height 1-64 pixels.

## Technical Details {#technical_details}

### Inputs

**a** = X-coordinate of upper-left of sprite\
**b** = Sprite height, in pixels\
**l** = Y-coordinate of upper-left of sprite\
**ix** = Pointer to first byte of sprite

### Outputs

*Sprite XORed to graph buffer*

### Destroyed

All
