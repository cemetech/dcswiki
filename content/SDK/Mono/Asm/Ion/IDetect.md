---
revisions:
- author: JosJuice
  comment: Typo fix
  timestamp: '2014-06-09T20:47:44Z'
title: iDetect
---

## Description

Finds a given file in the VAT, if it exists, searching based on the
contents of the file. This routine is designed to be reentrant, in the
sense that you can call it multiple times with inputs based on its
outputs. For example, you can call it the first time with hl=(progptr),
and then subsequent times with hl = the de output of the previous run.
It can also properly deal with archived programs. This is particularly
useful for finding game levels that always have a specific header (or
even DCS APs!).

## Technical Details {#technical_details}

### Inputs

**hl** = Pointer to where to begin in the VAT, for example (progptr)\
**ix** = Detection string, zero-terminated

### Outputs

**de** = Address of place stopped in the VAT + 1\
**hl** = Pointer to first byte of program data\
**z flag** = set if no program was found (hl and de are invalid), and
reset if a program was found

### Destroyed

af, bc, de, hl
