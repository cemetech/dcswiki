---
revisions:
- author: KermMartian
  timestamp: '2010-06-21T02:39:06Z'
title: iLargeSprite
---

## Description

Draws an aligned or unaligned sprite to the graph buffer with a width
that is a multiple of 8 pixels, and any height. Does not perform
clipping.

## Technical Details {#technical_details}

### Inputs

**a** = X-coordinate of top-left of sprite\
**b** = Sprite height\
**c** = Sprite width (in bytes, so divide by 8)\
**l** = Y-coordinate of top-left of sprite\
**ix** = Pointer to first byte of sprite

### Outputs

Draws the sprite to the graph buffer.

### Destroyed

All
