---
revisions:
- author: KermMartian
  timestamp: '2010-06-21T02:26:17Z'
title: iFastCopy
---

## Description

Copies the graph buffer to the LCD, very fast. Properly handles the
emulator in the Nspire as well as the delays necessary for the TI
Presenter device.

## Technical Details {#technical_details}

This routine temporarily disables interrupts, but is careful to
re-enable them when it completes.

### Inputs

None

### Outputs

Graph buffer copied to LCD.

### Destroyed

All
