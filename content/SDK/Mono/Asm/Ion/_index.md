---
title: Ion
bookCollapseSection: true
---

Joe Wingbermuehle's
[Ion](https://www.ticalc.org/archives/files/fileinfo/130/13058.html) was the
first major shell for the TI-83+. Its library functions now represent something
of a lowest common denominator for support provided by shells. These functions
are available to programs using the identifying program headers for Ion,
MirageOS and Doors CS.

-   **[iVersion]({{< ref "iVersion.md" >}})** - Return the
    version of Ion emulated by these routines (1.6)
-   **[iRandom]({{< ref "iRandom.md" >}})** - Return a random
    integer within given bounds.
-   **[iPutSprite]({{< ref "iPutSprite.md" >}})** - Render a
    small (8-pixel-wide) sprite into the graph buffer.
-   **[iLargeSprite]({{< ref "iLargeSprite.md" >}})** - Render
    a large sprite into the graph buffer.
-   **[iFastCopy]({{< ref "iFastCopy.md" >}})** - Quickly copy
    the graph buffer to the LCD. Nspire- and TI Presenter-compatible.
-   **[iDetect]({{< ref "iDetect.md" >}})** - Similar to
    ChkFindSym.
-   **[iDecompress]({{< ref "iDecompress.md" >}})** -
    Decompress bit-packed data.
-   **[iGetPixel]({{< ref "iGetPixel.md" >}})** - Get graph
    buffer offset and mask for a given pixel.
