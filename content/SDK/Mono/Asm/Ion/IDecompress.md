---
revisions:
- author: KermMartian
  comment: /\* Outputs \*/
  timestamp: '2010-06-21T02:37:04Z'
title: iDecompress
---

## Description

Decompresses bitpacked data.

## Technical Details {#technical_details}

### Inputs

**b** = length of compressed data.\
**c** = compression factor, either 1, 3, or 15.\
**hl** = Pointer to first byte of compressed data.\
**de** = Pointer to first byte of RAM to store decompressed data.

### Outputs

Data decompressed from hl to de.\
**hl** = Pointer to next byte of compressed data.

### Destroyed

af, bc, de, hl
