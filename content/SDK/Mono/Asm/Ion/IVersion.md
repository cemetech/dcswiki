---
revisions:
- author: KermMartian
  timestamp: '2010-06-21T02:41:37Z'
title: iVersion
---

## Description

Returns the version of the Ion libraries supported by Doors CS.

## Technical Details {#technical_details}

Doors CS emulates the libraries of Ion v1.6, containing 8 library
routines.

### Inputs

**None**

### Outputs

**a** = Ion compatibility number (0)\
**d** = Library compatibility number (0)\
**e** = Number of supported routines (8)\
**h** = Major Ion version emulated (1)\
**l** = Minor Ion version emulated (6)

### Destroyed

a, de, hl
