---
revisions:
- author: KermMartian
  timestamp: '2010-06-21T02:45:31Z'
title: iGetPixel
---

## Description

Gets the offset of a specified pixel into the graph buffer, as well as a
bitmask isolating the selected pixel in its group of eight.

## Technical Details {#technical_details}

### Inputs

**a** = x-coordinate of pixel\
**e** = y-coordinate of pixel

### Outputs

**a** = pixel mask for specified pixel\
**hl** = address of 8-pixel group containing this pixel in the graph
buffer

### Destroyed

**All**
