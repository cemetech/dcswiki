---
revisions:
- author: KermMartian
  timestamp: '2013-02-22T18:22:40Z'
title: Program Header
weight: 10
aliases:
- /ASM_Header/
---

## Normal Header {#normal_header}

This header is used for normal Doors CS programs, allowing the program to
provide a description and icon that is displayed in the shell.

    ; Program Name: 
    ; Author: 
    ; Version: 
    ; Date: 
    ; Written for Doors CS 7.0 and higher (http://dcs.cemetech.net)
    .nolist
    #include "ti83plus.inc"
    #include "dcs7.inc"
    .list
       .org progstart
       .db $BB,$6D
    Init:
       xor d
       ret
       jr Start
       
       .dw Description         ;or .dw $0000 if you don't have a description
       .db $07,$00         ;always this string
       .dw Icon            ;or .dw $0000 if you don't have an icon
       .dw ALE             ;usually .dw $0000 if you don't have or know what an ALE is
    Start:                             ;main routines
       ;etc etc etc
       ;ret
    Description:
       .db "Description",0 ;can be omitted if .dw Description is .dw 0000 above
    Icon:              ;a 16x16 icon (can be omitted if .dw Icon is .dw 0000 above)
       .db %11111111,%11111000
       .db %10000000,%00001100
       .db %10111100,%00001010
       .db %10010010,%00001111
       .db %10010010,%01010001
       .db %10010010,%10101001
       .db %10111100,%01010101
       .db %10000000,%00000001
       .db %10101010,%10101001
       .db %10010101,%01010101
       .db %10101010,%10101001
       .db %10000000,%00000001
       .db %10010101,%01010101
       .db %10101010,%10101001
       .db %10000000,%00000001
       .db %11111111,%11111111
    ALE:                   ;must be omitted if .dw ALE is .dw 0000 above
       .db "ZALE",0,0,0,0 ;always eight bytes, use
       .db "ZLALE",0,0,0 ;zeros for extra bytes
       .db $FF ;put after last ALE

## AP-Enabled program

These headers are used with the [Associated Program subsystem]({{< ref "./DCS/APs/" >}}).

### Main Program {#ap-main}

This header is used for a program that processes APs and identifies the program
types that it can operate on as well as providing a separate entrypoint used when
opening an associated program from the shell.

    ; Program Name: 
    ; Author: 
    ; Version: 
    ; Date: 
    ; Written for Doors CS 7.0 and higher (http://dcs.cemetech.net)
    .nolist
    #include "ti83plus.inc"
    #include "dcs7.inc"
    .list
       .org progstart
       .db $BB,$6D
    Init:
       xor d
       ret
       jr Start
       
       .dw Description
       .db $07,$00
       .dw Icon
       .dw $0000
       .dw Apstart         ;the routine to open files.  DCS will start you here instead of at $9D95 if an AP file is pending
       .db $31,$7F         ;argh, this be an APMain
       .db $02             ;number of accepted filetypes
    MyType1:
       .db 0,1,1           ;plaintext
    MyType2:
       .db 0,1,2           ;DDE6 text
    START:                             ;main routines
    Description:
       .db "Description",0 ;can be omitted if .dw Description is .dw 0000 above
    Icon:              ;a 16x16 icon (can be omitted if .dw Icon is .dw 0000 above)
       .db %11111111,%11111000
       .db %10000000,%00001100
       .db %10111100,%00001010
       .db %10010010,%00001111
       .db %10010010,%01010001
       .db %10010010,%10101001
       .db %10111100,%01010101
       .db %10000000,%00000001
       .db %10101010,%10101001
       .db %10010101,%01010101
       .db %10101010,%10101001
       .db %10000000,%00000001
       .db %10010101,%01010101
       .db %10101010,%10101001
       .db %10000000,%00000001
       .db %11111111,%11111111
    IconAP:                ;a 16x16 icon
       .db $3F,$F0,$20,$18
       .db $E0,$14,$A0,$1E
       .db $A0,$02,$AF,$C2
       .db $A6,$62,$A6,$32
       .db $A6,$32,$A6,$32
       .db $A6,$62,$AF,$C2
       .db $A0,$02,$BF,$FE
       .db $80,$08,$FF,$F8

### Files

This header identifies programs that can be opened as associated programs by
another program that uses the AP [main program header](#ap-main).

    ; Header for data files meant to be opened with the corresponding viewer
       .db $BB,$6D              ; required
       ret                      ; don't let the TI-OS run it
       .db $31,$80              ; required, identify the program as a data
    type :
       .db 0,0,0                ; the file type here
       
       ...                      ; datas here

## Shell Expansion (SE)

This header is used only for [shell expansions]({{< ref "Shell_Expansions.md" >}}),
which are loaded into the shell instead of running like normal programs.

    .nolist                ;defines, includes, and equates
    #include "ti83plus.inc"
    #include "dcs7.inc"        ;token interpretation
    SEOffset   equ $86ec
    .list
       .org 0
       .db $BB,$6D     ;ASM program header
       .db $AB,$C9     ;ignore program; ret
    SEStart:
       .db $31,$87,$50     ;SE for DCS 5 and higher
       .dw SEBoot-SEStart  ;or .dw 0 if there is no boot section
       .dw SEMouse-SEStart ;or .dw 0 if there is no mouse section
       .dw SEUnload-SEStart    ;or .dw 0 if there is no unload section
    SEBoot:            ;if there is a boot section...
       ;...code...
       ret
    SEMouse:           ;if there is a mouse section...
       ;...code...
       ld d,0
       ret
    SEUnload:          ;if there is an unload section...
       ;...code...
       ret

## "Ignore" Header {#ignore_header}

If you want Doors CS to ignore a particular ASM program and not list it,
then all you need to do is make xor e the first instruction in the
program:

    ; Program Name: 
    ; Author: 
    ; Version: 
    ; Date: 
    ; Written for Doors CS 7.0 and higher (http://dcs.cemetech.net)
    .nolist
    #include "ti83plus.inc"
    #include "dcs7.inc"
    .list
       .org progstart
       .db $BB,$6D
    Init:
       xor e
