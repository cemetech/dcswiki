---
revisions:
- author: KermMartian
  timestamp: '2010-07-01T05:36:54Z'
title: Shell Expansions
weight: 20
aliases: /Shell_Expansions/
---

## What?

Shell Expansions, or SEs, are modules for Doors CS. They are similar to
the concept of Ion Modules, except that they are far more versatile.
Shell Expansions can have one or more of three sections: Boot, Mouse,
and Unload. A shell expansion with a Boot section is called while Doors
CS is booting, before it counts programs and displays the desktop.
Possible uses: load interrupts, set up hardware, sort programs, etc etc.
Mouse sections are the most versatile: not only does Doors CS call them
continuously while the mouse is active, they can actually interject
actions into Doors CS itself. They can force mouse movement, clicking,
and coordinate jumping via well-placed memory editing (not necessarily
advocated). Finally, unload sections are, obviously, called while Doors
CS is quitting, after it has closed down the GUI system but before it
reverts to the TI-OS. Since a program can contain one or more of these
sections, it can be anywhere from 768 byte to 2,304 bytes; each section
can only be up to 768 bytes, since it must fit in SavesScreen.

## Why?

As a programmer, why would you want to write your program as an SE
rather than a full program? Well, not only would you have the
programming challenge of fitting your entire program into 768, 1,536, or
2,304 bytes, you would have the advantage of interacting directly with
the Doors CS kernel. Startup programs could directly set flags and
options within Doors CS, or through some fancy programming, even prevent
it from booting entirely (password protection, anyone?). Because Doors
CS is now an app, SEs have access to each and every routine Doors CS has
to offer to ASM programs, although it is not recommended that they mess
with the GUI system, in case Doors CS itself is taking advantage of
undocumented GUI API features. The mouse SEs are the crowning jewel of
the SE system, for thety allow the programmer full control over cursor
movement, cursor clicking, and even the direct cursor coordinates. Such
power gives SEs the opportunity to completely change the way the
interface works for the user. A low-end example could be remapping the
keys on the keyboard, while a higher-end one might be a PS/2 or USB
hardware driver.

## Technical Details: How? {#technical_details_how}

### New mem pointer {#new_mem_pointer}

Each SE is allocated two bytes of RAM for mode storage. IT can be found
at (ix) and (ix+1) when the SE is loaded.

### dModes

These are the values that are returned from the Mouse section of any SE,
and represent an action that the SE would like Doors CS to perform.

-   0 = nothing\
-   1 = leftclick\
-   2 = rightclick\
-   3 = up\
-   4 = down\
-   5 = left\
-   6 = down\
-   7-102 = set X-coord to d-7\
-   103-166 = set Y-coord to d-103\

### Notes

In some betas between Doors CS 6.2 and Doors CS 6.7, notably 6.3, 6.4,
6.5, and 6.6, the dModes listed above were nonfunctional.

### Header

    .nolist                ;defines, includes, and equates
    #include "dcs6.inc"        ;token interpretation
    SEOffset   equ $86ec
    .list
       .org 0
       .db $BB,$6D     ;ASM program header
       .db $AB,$C9     ;ignore program; ret
    SEStart:
       .db $31,$87,$50     ;SE for DCS 5 and higher
       .dw SEBoot-SEStart  ;or .dw 0 if there is no boot section in this SE
       .dw SEMouse-SEStart ;or .dw 0 if there is no mouse section in this SE
       .dw SEUnload-SEStart    ;or .dw 0 if there is no unload section in this SE
    SEBoot:            ;if there is a boot section...
       ;...code...
       ret
    SEMouse:           ;if there is a mouse section...
       ;...code...
       ld d,0
       ret
    SEUnload:          ;if there is an unload section...
       ;...code...
       ret
