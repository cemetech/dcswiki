---
revisions:
- author: KermMartian
  timestamp: '2010-06-21T02:46:25Z'
title: Directin
---

## Description

Scans a given key group and returns a keycode. Returns with the Z flag
set if no key was pressed.

## Technical Details {#technical_details}

(None)

### Inputs

**a** = Key group to scan. Standard direct-input codes.

### Outputs

**a** = Key mask returned. As with direct input, \$ff means no key
pressed.\
**z flag** = Set if no key in specified group was pressed. Reset if at
least one key in group was pressed.

### Destroyed

a, b
