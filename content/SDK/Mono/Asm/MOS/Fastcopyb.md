---
revisions:
- author: The Tari
  timestamp: '2006-09-21T21:00:44Z'
title: Fastcopyb
---

## Description

Copies 768 bytes at (HL) to LCD.

## Technical Details {#technical_details}

### Inputs

**HL** = address of 768 byte bitmap image

### Outputs

**None**

### Destroys

*Unknown, please add*
