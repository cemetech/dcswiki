---
revisions:
- author: KermMartian
  comment: /\* Destroys \*/
  timestamp: '2010-06-20T23:52:50Z'
title: Sendbytetios
---

## Description

Sends a byte through the link port (TI-OS protocol)

## Technical Details {#technical_details}

### Inputs

**A** = byte to send

### Outputs

**Byte sent**

### Destroys

**All**
