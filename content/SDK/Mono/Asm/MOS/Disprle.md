---
revisions:
- author: KermMartian
  timestamp: '2014-07-31T01:41:15Z'
title: Disprle
---

disprle - Decompresses of rle compressed data into 768 bytes

                   - Input  hl = locataion of compressed data
                   -        de = location to store decompressed data

The compressed data has the following format:

-   "Literal" mode is assumed until otherwise specified. The routine
    copies bytes out one-by-one
-   If a \$91 is encountered, the calculator displays an RLE-encoded
    block. These three-byte blocks are formatted as follows:

```{=html}
<!-- -->
```
    Offset 0: $91
    Offset 1: Byte to repeat
    Offset 2: Number of repetitions (1-256)

-   After a three-byte RLE section, the decompressor returns to
    "literal" mode until the next \$91.
-   To display a literal \$91, use the three-byte sequence \$91,\$91,1
