---
revisions:
- author: The Tari
  timestamp: '2006-09-21T20:58:36Z'
title: Delayb
---

## Description

Performs a halt loop.

## Technical Details {#technical_details}

### Inputs

**B** = number of times to halt

### Outputs

**None**

### Destroys

**B**
