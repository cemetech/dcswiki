---
revisions:
- author: The Tari
  timestamp: '2006-11-13T01:52:47Z'
title: Putsprite8
---

## Description

Runs [iPutSprite]({{< ref "/SDK/Mono/Asm/Ion/iPutSprite.md" >}}), but sets
height to 8.

## Technical Details {#technical_details}

### Inputs

See [iPutSprite]({{< ref "/SDK/Mono/Asm/Ion/iPutSprite.md" >}})

### Outputs

See [iPutSprite]({{< ref "/SDK/Mono/Asm/Ion/iPutSprite.md" >}})

### Destroyed

See [iPutSprite]({{< ref "/SDK/Mono/Asm/Ion/iPutSprite.md" >}})
