---
revisions:
- author: The Tari
  timestamp: '2006-09-21T21:04:49Z'
title: Quittoshell
---

## Description

Quits to shell, stack level does not matter.

## Technical Details {#technical_details}

### Inputs

**None**

### Outputs

**None**

### Destroys

**Unimportant**

### Comments

Control is returned to the shell, program is terminated.
