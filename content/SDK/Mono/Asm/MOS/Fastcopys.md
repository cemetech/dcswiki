---
revisions:
- author: The Tari
  timestamp: '2006-09-21T20:57:34Z'
title: Fastcopys
---

## Description

Copies plotSScreen to LCD (fast).

## Technical Details {#technical_details}

### Inputs

**None**

### Outputs

**None**

### Destroys

**None**
