---
aliases:
- Filledrectange
revisions:
- author: KermMartian
  comment: 'KermMartian moved page \[\[Filledrectange\]\] to

    \[\[Filledrectangle\]\]'
  timestamp: '2014-07-31T00:49:10Z'
title: Filledrectangle
---

-   Draws a filled rectangle the graph buffer (fast!)
-   a = fill type: 0 = white, 1 = black, 2 = invert
-   Draws from top-left at (h,l) to bottom-right at (d,e)
