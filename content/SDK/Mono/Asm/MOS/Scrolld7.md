---
revisions:
- author: The Tari
  timestamp: '2006-11-13T01:56:54Z'
title: Scrolld7
---

## Description

Scrolls the graph buffer down 7 pixels.

## Technical Details {#technical_details}

### Inputs

None (?)

### Outputs

Graph buffer scrolled

### Destroyed

Unknown, please add
