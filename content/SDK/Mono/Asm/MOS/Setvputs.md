---
revisions:
- author: KermMartian
  timestamp: '2010-06-21T02:50:12Z'
title: Setvputs
---

## Description

Sets the coordinates of the top-left of a line of text to display, then
displays it.

## Technical Details {#technical_details}

This routine just stores de to (pencol), then calls vputs with hl.

### Inputs

**d** = row (will be stored in penrow)\
**e** = column (will be stored in pencol)\
**hl** = pointer to first character of zero-terminated string.

### Outputs

*(none)*

### Destroyed

*(all)*
