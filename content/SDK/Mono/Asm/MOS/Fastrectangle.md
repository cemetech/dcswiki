---
revisions:
- author: KermMartian
  timestamp: '2014-07-31T00:46:50Z'
title: Fastrectangle
---

-   Draws a rectangle the graph buffer (fast!)
-   Same line types as [Fastline]({{< ref "Fastline.md" >}}) in a
-   Draws from top-left at (h,l) to bottom-right at (d,e)
