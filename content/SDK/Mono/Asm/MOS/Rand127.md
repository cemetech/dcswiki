---
revisions:
- author: Geekboy1011
  timestamp: '2011-05-15T21:41:11Z'
title: Rand127
---

rand127

`          - Returns a number between 0 and 127 in register a`
