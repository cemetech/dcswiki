---
revisions:
- author: KermMartian
  timestamp: '2010-06-21T02:50:36Z'
title: Setpixel
---

## Description

Turns the specified pixel dark in the graph buffer, preserving all other
pixels in the graph as-is.

## Technical Details {#technical_details}

This routine uses the same inputs as
[iGetPixel]({{< ref "/SDK/Mono/Asm/Ion/iGetPixel.md" >}}), and preserves hl.

### Inputs

**a** = x-coordinate of pixel to set\
**e** = y-coordinate of pixel to set

### Outputs

*(Graph buffer modified)*

### Destroyed

All except hl
