---
revisions:
- author: KermMartian
  timestamp: '2010-06-02T01:52:22Z'
title: Fastline
---

    fastline   - draws a line to the graph buffer (fast!)
           - Input  (h,l) = (x1,y1)
           -        (d,e) = (x2,y2)
           -        a = line style
           -            0 = white
           -            1 = black
           -            2 = xor
           -            3 = pattern (one byte pattern stored in (mlinebitmap))
           - Output line draw to graphbuffer
