---
title: MirageOS
bookCollapseSection: true
---

Doors CS implements most of the library functions provided by
[MirageOS](http://www.detachedsolutions.com/mirageos/), though not all of them
are documented directly here. For those that do not have a documentation page
here, refer to the
[documentation provided with MirageOS]({{< resource "mirgen&libs.txt" >}}).
These functions are available to programs that use the MirageOS or Doors CS
program headers.

-   **[directin]({{< ref "directin.md" >}})** - scans one
    group of the keypad for pressed keys via direct input.
-   **[sendbytetios]({{< ref "sendbytetios.md" >}})** - sends
    one byte via TI-OS linking routines.
-   **[getbytetios]({{< ref "getbytetios.md" >}})** - receives
    one byte via TI-OS linking routines.
-   **[version]({{< ref "version.md" >}})** - retrieves
    MirageOS library compatibility information.
-   **[setvputs]({{< ref "setvputs.md" >}})** - sets (penrow)
    and (pencol) and displays a string in variable-width graphscreen
    font.
-   **[setpixel]({{< ref "setpixel.md" >}})** - sets a
    specific pixel in the graph buffer to dark.
-   **[fastcopys]({{< ref "fastcopys.md" >}})** - copies the
    graph buffer to the LCD, preserving all registers.
-   **[multhe](multhe)** - multiplies h\*e.
-   **[multhl](multhl)** - multiplies h\*l.
-   **[quittoshell]({{< ref "quittoshell.md" >}})** - quits
    directly to Doors CS, regardless of stack level.
-   **[fastline]({{< ref "fastline.md" >}})** - draws a line to the
    graph buffer, fast.
-   **[pixelonhl](pixelonhl)** - similar to
    [setpixel]({{< ref "setpixel.md" >}})
-   **[pixeloff](pixeloff)** - turns a specified pixel to white.
-   **[pixelxor](pixelxor)** - xor's a specified pixel.
-   **[pixeltest](pixeltest)** - returns the state of a specified
    pixel.
-   **[pixeloffhl](pixeloffhl)** - similar to [pixeloff](pixeloff).
-   **[pixelxorhl](pixelxorhl)** - similar to [pixelxor](pixelxor).
-   **[pixeltesthl](pixeltesthl)** - similar to [pixeltest](pixeltest).
-   **[fastlineb](fastlineb)** - draws a black line to the graph
    buffer.
-   **[fastlinew](fastlinew)** - draws a white line to the graph
    buffer.
-   **[fastlinex](fastlinex)** - XORs a line onto the graph buffer.
-   **[pointonc](pointonc)** - like [pixelon](pixelon), but with
    clipping.
-   **[pointoffc](pointoffc)** - like [pixeloff](pixeloff), but with
    clipping.
-   **[pointxorc](pointxorc)** - like [pixelxor](pixelxor), but with
    clipping.
-   **[centertext]({{< ref "centertext.md" >}})** - draws a
    centered string to the graph buffer in variable-width font.
-   **[cphlbc](cphlbc)** - compares hl and bc.
-   **[putsprite8]({{< ref "putsprite8.md" >}})** - like
    [iPutSprite]({{< ref "iPutSprite.md" >}}) width height=8.
-   **[fastcopyb]({{< ref "fastcopyb.md" >}})** - similar to
    [iFastCopy]({{< ref "iFastCopy.md" >}}), but using hl
    instead of gbuf as the memory location.
-   **[vputsc](vputsc)** - like vputs, but copying to both the LCD and
    the graph buffer.
-   **[scrolld7]({{< ref "scrolld7.md" >}})** - scroll the
    graph buffer by 7 lines.
-   **[vnewline](vnewline)** - like CRLF, calling
    [scrolld7]({{< ref "scrolld7.md" >}}) if necessary.
-   **[rand127]({{< ref "rand127.md" >}})** - return a random number
    between 0 and 127, inclusive.
-   **[disprle]({{< ref "disprle.md" >}})** - decompress RLE-compressed
    image.
-   **[cphlde](cphlde)** - compare hl and de.
-   **[multdehl](multdehl)** - multiply hl and de.
-   **[fastlined](fastlined)** - draws a fast line with a dotted line
    style.
-   **[disprlel]({{< ref "disprlel.md" >}})** - similar to
    [disprle]({{< ref "disprle.md" >}}), but with a specified size.
-   **[getnext](getnext)** - steps forward (high mem to low mem) through
    the VAT, one program at a time.
-   **[getprev](getprev)** - steps backwards (low mem to high mem)
    through the VAT, one program at a time.
-   **[compstrs]({{< ref "compstrs.md" >}})** - compare two
    zero-terminated strings.
-   **[nextstr](nextstr)** - increment through a zero-terminated string
    to the following string.
-   **[fastrectangle]({{< ref "fastrectangle.md" >}})** - draws an
    unfilled rectangle to the graph buffer.
-   **[gettext](gettext)** - like vputs, but gets input.
-   **[gettextv](gettextv)** - same as [gettext](gettext) with
    additional options.
-   **[disp3spaces](disp3spaces)** - displays three spaces in small
    (graphscreen) font.
-   **[vputa](vputa)** - displays the value of the accumulator in base
    10.
-   **[compstrsn](compstrsn)** - compares two strings for a specified
    number of characters.
-   **[largespritehl]({{< ref "largespritehl.md" >}})** -
    similar to [iLargeSprite]({{< ref "iLargeSprite.md" >}})
-   **[setupint](setupint)** - sets up a simplified MirageOS interrupt
    tasker.
-   **getchecksum** - (not included in Doors CS).
-   **freearc** - gets amount of free archive (not included in Doors
    CS).
-   **swapram** - swaps two areas of memory via the stack (not included
    in Doors CS).
-   **vatswap** - swaps two VAT entries. Removed from Doors CS for space
    reasons.
-   **sendprog** - attempt to transfer a program using TI-OS linking
    routines (not included in Doors CS).
-   **arcprog** - archived program (not included in Doors CS).
-   **renameprog** - rename program (not included in Doors CS).
-   **delprog** - delete a specified program. (not included in Doors
    CS)
-   **[filledrectangle]({{< ref "filledrectangle.md" >}})** - draws a
    filled rectangle.
