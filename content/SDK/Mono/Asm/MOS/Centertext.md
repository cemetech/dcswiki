---
revisions:
- author: The Tari
  timestamp: '2006-09-21T21:01:50Z'
title: Centertext
---

## Description

Displays centered text (small font).

## Technical Details {#technical_details}

### Inputs

**HL** = string to display

### Outputs

**None**

### Destroys

*Unknown, please add*
