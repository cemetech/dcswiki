---
revisions:
- author: KermMartian
  timestamp: '2010-06-20T23:53:56Z'
title: Getbytetios
---

## Description

Gets a byte through the link port (TI-OS protocol)

## Technical Details {#technical_details}

### Inputs

**None**

### Outputs

**A** = value received

### Destroys

**All**
