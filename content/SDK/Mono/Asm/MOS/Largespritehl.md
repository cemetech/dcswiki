---
revisions:
- author: KermMartian
  timestamp: '2011-07-20T19:46:11Z'
title: Largespritehl
---

*Same as [iLargeSprite]({{< ref "/SDK/Mono/Asm/Ion/iLargeSprite.md" >}}) Except
H = Xpos*

## Description

Draws an aligned or unaligned sprite to the graph buffer with a width
that is a multiple of 8 pixels, and any height. Does not perform
clipping.

## Technical Details {#technical_details}

### Inputs

**b** = Sprite height\
**c** = Sprite width (in bytes, so divide by 8)\
**h** = X-coordinate of top-left of sprite\
**l** = Y-coordinate of top-left of sprite\
**ix** = Pointer to first byte of sprite

### Outputs

Draws the sprite to the graph buffer.

### Destroyed

All
