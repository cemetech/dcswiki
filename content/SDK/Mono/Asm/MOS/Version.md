---
revisions:
- author: KermMartian
  timestamp: '2010-06-21T02:49:43Z'
title: Version
---

## Description

Returns the MirageOS version supported by the Doors CS libraries
(v1.1.2)

### Inputs

*(None)*

### Outputs

**a** = library compatbility version (2)\
**h** = major version (1)\
**l** = minor version (1)

### Destroyed

a, hl
