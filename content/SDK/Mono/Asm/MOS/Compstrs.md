---
revisions:
- author: Geekboy1011
  comment: "Created page with \u201C==Description== Compares strings\n==Technical\
    \ Details== (None) ===Inputs=== \u2019\u2018\u2019HL\u2019\u2019\u2019 = First\n\
    String`<br>`{=html} \u2019\u2018\u2019DE\u2019\u2019\u2019 = Second String ===Outputs===\
    \ \u2019\u2018\u2019Z\u2019\u2019\u2019\nSet = Strings are the\u2026\u201D"
  timestamp: '2013-09-19T01:05:49Z'
title: Compstrs
---

## Description

Compares strings

## Technical Details {#technical_details}

(None)

### Inputs

**HL** = First String\
**DE** = Second String

### Outputs

**Z** Set = Strings are the same\
**HL** = Byte after String 1s null terminators

### Destroyed

**Unknown**
